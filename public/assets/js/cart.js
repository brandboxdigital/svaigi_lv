$(document).ready(function () {
    bindRemove();
    bindUpdate();
    forms();
});

function showCartTotals(response) {
    $('.minicart-totals').text(response.contents.cartTotals.toPay + " €");
    $('.cart').addClass('has-items');
}

function bindRemove() {
    $('.discard').unbind().click(function (e) {
        e.preventDefault();
        $.post($(this).attr('href'), '', function (response) {
            if (response.status === true) {
                redrawCart(response);
                showCartTotals(response);
                bindRemove();
                bindUpdate();
                forms();
            }
        });
    });
}


function bindUpdate() {
    $('select[name=amount]').unbind().change(function (e) {
        el = $(this);
        $('.sv-cart > .list').addClass('loading-items');
        e.preventDefault();
        $.post($(this).data('href'), $(this).closest('form').serialize(), function (response) {
            redrawCart(response);
            showCartTotals(response);
            bindRemove();
            bindUpdate();
            forms();
            $('.sv-cart > .list').removeClass('loading-items');
            if (!response.status) {
                alert(response.message);
            }
        });
    });

    $('select[name=variation_id]').unbind().change(function (e) {
        el = $(this);
        $('.sv-cart > .list').addClass('loading-items');
        e.preventDefault();

        $.post($(this).data('href'), $(this).closest('form').serialize(), function (response) {
            redrawCart(response);
            showCartTotals(response);
            bindRemove();
            bindUpdate();
            forms();
            $('.sv-cart > .list').removeClass('loading-items');
            if (!response.status) {
                alert(response.message);
            }
        });
    });

    $('.setDelivery').unbind().click(function (e) {
        $('.sv-cart-tabs').addClass('loading-items');

        el = jQuery(this);
        $.get($(this).data('run'), function (response) {
            $('.sv-cart-tabs').removeClass('loading-items');

            // console.log('setDelivery::response', response, Vue);
            if (window.app) {
                window.app.updateDelivery(response.delivery);
            }

            if (response.status) {
                jQuery('.sv-cart-tabs .tab').removeClass('active');
                el.addClass('active');
                redrawCart(response);

                showCartTotals(response);
                bindRemove();
                bindUpdate();
                forms();
                return;
            }
            alert(response.message);
        });
        e.preventDefault();
        return false;
    })

    $('#discountCodeForm').unbind().submit(function (e) {
        $('.sv-cart > .list').addClass('loading-items');
        $.post($(this).attr('action'), $(this).serialize(), function (response) {
            redrawCart(response);
            showCartTotals(response);
            bindRemove();
            bindUpdate();
            forms();
            $('.sv-cart > .list').removeClass('loading-items');
            if (!response.status) {
                alert(response.message);
            }
        }).fail(function(response) {
            alert(response.responseJSON.errors.code[0])
            $('.sv-cart > .list').removeClass('loading-items');
            $('#discountCodeForm')[0].reset();
        });
        e.preventDefault();
        return false;
    });

    $('#removeDiscountCode').unbind().click(function (e) {
        $('.sv-cart>.sticky .totals').addClass('loading-items');
        $.get($(this).attr('href'),'', function (response) {
            redrawCart(response);
            showCartTotals(response);
            bindRemove();
            bindUpdate();
            forms();
            $('.sv-cart>.sticky .totals').removeClass('loading-items');
            if (!response.status) {
                alert(response.message);
            }
        }).fail(function(response) {
            alert(response.responseJSON.errors.code[0])
            $('.sv-cart>.sticky .totals').removeClass('loading-items');
            $('#discountCodeForm')[0].reset();
        });
        e.preventDefault();
        return false;
    })
}

function redrawCart(response) {
    list = $('.sv-cart>.list');
    list.children(":not(.header, .coupon, .clear, .sv-blank-spacer)").remove();
    $(response.contents.items).insertAfter('.header');
    totals = $('.sv-cart>.sidebar--sticky .totals');
    cartTotals = response.contents.cartTotals;
    for (x in cartTotals) {

        cartVal = cartTotals[x];

        if (x === 'discount_description'){
            console.log('discount-description');
            totals.find('.discount-description').html(cartTotals['discount_description']).show();
        }

        if (x === 'discount'){
            cartVal = '-&nbsp;' + cartTotals[x];
        }

        if (x === 'delivery') {
            if (isNaN(cartVal)) {
                cartVal = cartVal;
            } else {
                cartVal = parseFloat(cartVal) + parseFloat(cartTotals.delivery_vat);
                cartVal = cartVal.toFixed(2);
            }
        }

		if (x === 'delivery_vat') {
            continue;
		}

        if (isNaN(cartVal)) {
            totals.find("." + x).html(cartVal).parent().show();
        } else {
            totals.find("." + x).html(cartVal + '&nbsp;€').parent().show();
        }

        totals.find('.dcode').html(response.contents.code);

        $('input.enderDiscountCode').val(response.contents.code);
        if (x === 'discount' && !response.contents.code) {
            totals.find("." + x).html(cartTotals[x] + '&nbsp;€').parent().hide();
            totals.find('.discount-description').html('').hide();
        }
    }
    $('.selectpicker').selectric();
    jQuery('#spinner, .spinner').spinner({
        min: 1,
        max: 5,
        step: 1,
        numberFormat: "n"
    });
}


function showCartTotals(response) {
    $('.minicart-totals').text(response.contents.cartTotals.toPay + " €");
    $('.cart').addClass('has-items');
}
