<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'facebook' => [
        // 'client_id' => '1194186254659374',
        // 'client_secret' => '2a16a29044182e39189aeb2abaf9dc65',

        'client_id' => '1471865966581567',
        'client_secret' => '943bbe827ab323708a6dfd1eaa9021e2',

        // 'client_id' => '2388091094616530',
        // 'client_secret' => '26ec13240997ca94a86340ef1921803f',

        'client_id' => '1471865966581567',
        'client_secret' => '943bbe827ab323708a6dfd1eaa9021e2',
        // 'redirect' => env('APP_URL')."/".language()."/fb/callback",
        'redirect' => env('APP_URL')."/fb/callback",
    ],
    'mandrill'=>[
        'secret'=> env('MANDRILL_SECRET'),
    ]
    // 'facebook' => [
    //     'client_id' => '2196794257278276',
    //     'client_secret' => 'cc5753e7ada25a1a7e334f3b8094b089',
    //     //'redirect' => env('APP_URL')."/fb/callback",
    //     'redirect' => 'https://visimplex.com/fb/callback',
    // ],
];
