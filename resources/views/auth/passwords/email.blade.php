@extends('layouts.app')

@section('content')

<div class="sv-text-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="sv-page-title">
                    <h1>{{ __('translations.ressetPassword') }}</h1>
                </div>

                <div class="sv-blank-spacer small"></div>

                <div class="">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="input-wrapper req">
                            <label>{{ __('translations.profileFormEmail') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            {{-- @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif --}}
                        </div>

                        <div class="section form-comments">
                            <div class="sv-blank-spacer small"></div>
                            <div class="btn-row btn-row--center">
                                <button type="submit" class="btn btn-green">
                                    {{ __('translations.SendPasswordResetLink') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
