<div class="sv-newsletter-sidebar">
    <h3>Jaunumi e-pastā</h3>
    <p id="message-block">
        Piesakies jaunumu saņemšanai un uzzini svaigākās ziņas par zemnieku produktiem un aktuālajiem piedāvājumiem.
    </p>
    <form id="subscribe-form" method="post" action="/subscribe">
        <input name="email" type="email" placeholder="Tava e-pasta adrese" />
        <input type="submit" value="Pieteikties" class="sv-btn" />
    </form>
</div>