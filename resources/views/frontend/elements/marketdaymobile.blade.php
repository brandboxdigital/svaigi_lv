@php
/** @var \App\Http\Controllers\CacheController $cache **/
@endphp

<div class="d-flex ml-4 mt-5 mob-market-day-container">
    <div class="days">
        @foreach($cache->getClosestMarketDayList() as $availableTo => $marketDay)
        <a href="javascript:void(0)" class="sv-day {{ $cache->isSelectedMarketDay($marketDay)?"is-active":"d-none" }}">
            <div>
                <div class="nr">
                    <span>{{ $marketDay->date->format('d') }}</span>
                    <span>{{ ucfirst(__('translations.'.$marketDay->date->format('F'))) }}</span>
                </div>
                <div class="name">
                    {{ $marketDay->name }}
                </div>
            </div>
        </a>
        @endforeach
    </div>
    <div class="text-block">
        <div class="text-block-content">
            <div class="text-block-content-title">Tev redzami produkti no šīs Tirgus dienas.</div>
            <button class="open-mob-drop btn btn-green">Mainīt Tirgus dienu</button>
        </div>
    </div>
</div>

<div class="mob-marketday-overlay"></div>

<div class="mob-market-day-dropdown">
    <div class="content">
        <div class="calendar">
            <div class="container">
                <div class="row">
                    <div class="days">
                        @foreach($cache->getClosestMarketDayList() as $availableTo => $marketDay)
                        <a href="{{ r('setMarketDay', [$availableTo]) }}" class="sv-day {{ $cache->isSelectedMarketDay($marketDay)?"is-active":"" }}">
                            <div>
                                <div class="nr">
                                    <span>{{ $marketDay->date->format('d') }}</span>
                                    <span>{{ ucfirst(__('translations.'.$marketDay->date->format('F'))) }}</span>
                                </div>
                                <div class="name">
                                    {{ $marketDay->name }}
                                </div>
                            </div>
                            <span class="button">{!! _t('translations.chooseMarketDay') !!}</span>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
