<div class="sv-user-data-forms">
    <form method="post" action="{{$action}}" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="section">
            <div class="input-wrapper req">
                <label class="form-label" for="supplier_image">Saimniecības profila attēls</label>

                <div class="help--ratio-1x1">
                    <div class="inner help--image cover">
                        <img src="{{ $supplier->getImageByKey('supplier_image') }}" class="image " style="width:100%;height:100%">
                    </div>
                </div>

                <p>Augšuplielādet citu attēlu</p>
                <input type="file" class="form-control" id="supplier_image" name="supplier_image[]" />
            </div>

            <div class="input-wrapper req">
                <label>Saimniecības epasts</label>
                <input type="text" name="email" id="emailaddress" value="{{ old('email')??$supplier->email??"" }}"/>
            </div>

            <div class="form-toogle-fields">
                <div class="input-wrapper req">
                    <label>Saimniecības juridiskais nosaukums</label>
                    <input type="text" name="meta[jur_name]" value="{{ old('meta[jur_name]')??$supplier->getMeta('jur_name')??"" }}"/>
                </div>

                <div class="sv-line-spacer"></div>
                <div class="sv-blank-spacer small"></div>

                <div class="input-wrapper">
                    <label>Saimniecības Atrašanās vietas nosaukums</label>
                    <input type="text" name="meta[location]" value="{{ old('meta[location]')??$supplier->getMeta('location')??"" }}"/>
                </div>

                <div class="input-wrapper">
                    <label>Saimniecības Atrašanās vietas koordinātes</label>
                    <input type="text" name="coords" value="{{ old('coords')??$supplier->coords??"" }}"/>
                </div>

			</div>

			<div class="sv-blank-spacer small"></div>
            <div class="btn-row btn-row--fill">
                <button class="btn btn-green">{!! $buttonText??"Uz Priekšu" !!}</button>
            </div>
		</div>
    </form>
</div>
