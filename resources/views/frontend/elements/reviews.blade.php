@php
    use App\Plugins\Products\Model\ProductRating;

    $rews = collect();
    $colors = collect([
        (object)["light" => '#E6EAF8', "dark" => "#7775CE" ],
        (object)["light" => '#D5F4DA', "dark" => "#45C270" ],
        (object)["light" => '#FEE2E2', "dark" => "#EA6A89" ],
        (object)["light" => '#F3E9DE', "dark" => "#E68054" ],
        (object)["light" => '#F8E6F7', "dark" => "#E776CE" ],
        (object)["light" => '#F8F4CF', "dark" => "#E8A420" ],
    ]);

    if ($type == 'suppliers') {
        $rews = ProductRating::approvedSupplierReviews()->get()->map(function($item) {
            return $item->getSupplierReviewObject();
        });
    } else if ($type == 'products') {
        $rews = ProductRating::approvedProductReviews()->get()->map(function($item) {
            return $item->getProductReviewObject();
        });
    }

@endphp

@if (count($rews) > 0 && $hasReviews??false)

<div class="reviews">
    <div class="row">
        <div class="col-md-10 col-md-push-1">
            <h2 class="reviews-title">Pircēju atsauksmes</h2>

            <div class="reviews-items">
                @foreach ($rews as $review)
                    @php
                        $color = $colors->random();
                    @endphp
                    <div class="reviews-item" >
                        <div class="reviews-profilepicture"
                             style="background-color: {{$color->light}}; color: {{$color->dark}}; background-image: url({{null}})"
                        >
                            {{$review->short}}
                        </div>

                        <div class="reviews-profiletexts">
                            <div class="name">{{$review->name}} {{$review->surname}}</div>
                            @if ($review->title)
                                <h5>{{$review->title}}</h5>
                            @endif
                            @if ($review->body)
                                <p>{{$review->body}}</p>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endif

@if ($hasSubmit??false)
    <div class="sv-blank-spacer small"></div>

    <div class="reviews">
        <div class="row">
            <div class="col-md-6 col-md-push-1">
                <h2 class="reviews-title">Pievieno atsauksmi</h2>
                <div class="reviews-form">
                    <review-submit> </review-submit>
                </div>
            </div>
        </div>
    </div>
@endif
