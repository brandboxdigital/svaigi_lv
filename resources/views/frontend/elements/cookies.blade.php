<div class="sv-cookies hidden">
    <div>
        {!! __("translations.cookieAgreement") !!} <a target="_blank" href="https://svaigi.lv/privatuma-politika">{!! __("translations.cookieAgreementRulesText") !!}</a>
    </div>
    <div>
        <a data-consent="yes" href="#">{!! _t('translations.acceptCookies') !!}</a>
        <a data-consent="no" href="#">{!! _t('translations.denyCookies') !!}</a>
    </div>
</div>
