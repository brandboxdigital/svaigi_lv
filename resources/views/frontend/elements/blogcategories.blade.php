    <ul class="menu">
        @foreach($categories as $category)
        	@if($category->id == $currentCategoryId)
        	<li class="active">
        	@else
            <li>
            @endif
                <a href="{{ r('blog', [__('postcategory.slug.'.$category->id)]) }}">{{ __('postcategory.name.'.$category->id) }}</a>
            </li>
        @endforeach
    </ul>
