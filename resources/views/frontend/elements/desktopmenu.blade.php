<div class="row">
    {{-- <span class="sv-menu-mobile-close"></span> --}}

    <div class="menu-row-item has-hamburger has-padding">
        <button class="hamburger hamburger--slider sv-hamburger toggle-sv-menu-mobile" type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>
    </div>

    <div class="menu-row-item has-logo has-padding">
        <div class="logo">
            <a href="{{ r('page') }}"><img src="{{ asset("assets/img/logo-svaigilv-1.svg") }}" /></a>
        </div>
    </div>

    {{-- <a href="javascript:void(0)" class="sv-btn-menu toggle-sv-menu-mobile">
        <span>
            <s></s>
            <s></s>
            <s></s>
        </span>
    </a> --}}

    <div class="menu-row-item has-menu max">
        <div class="menu">
            <ul>
                @include("frontend.partials.menu.main", ['slug' => 'main', 'topMenu' => true])

                <li><a href="https://svaigi.lv/berni-saimnieko"><span>Bērni Saimnieko</span></a> <div class="main-menu--dropdown sv-dropdown sv-default-dropdown"><div class="inner-wrap container"><div class="left"><ul></ul></div> <div class="right"><div class="bg-static"><div class="image" style="background-image: url(&quot;https://svaigi.lv/assets/img/tmp/photo-15.jpg&quot;);"></div></div></div></div></div></li>
            </ul>
        </div>
    </div>

    <div class="menu-row-item has-profile">
        <div class="right">
            <div class="user">
                <a  {{(Auth::user() && Auth::user()->registered)?"":"class=toggle-sv-signin"}}>
                    <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="21" viewBox="0 0 24 21">
                            <path d="M21.162,21A10,10,0,0,0,2.838,21H0.7A11.984,11.984,0,0,1,23.3,21h-2.14ZM12,10a5,5,0,1,1,5-5A5,5,0,0,1,12,10Zm0-8a3,3,0,1,0,3,3A3,3,0,0,0,12,2Z" />
                        </svg>
                    </span>
                </a>

                @if(Auth::user() && (Auth::user()->registered || Auth::user()->isSocialAuthed()))
                <div class="sv-dropdown sv-user-dropdown">
                    <ul>
                        @php
                            $user = auth::user()
                        @endphp
                        @if($user->isAdmin())
                            <li class="admin">
                                <a href="{{ route('dashboard') }}"><span>Admin</span></a>
                            </li>
                        @endif
                        <li class="favourites">
                            {{-- <a href="{{ r('favouriteProucts') }}"><span>{!! _t('translations.favouriteProducts') !!}</span></a> --}}
                            <a href="{{ r('favouriteProucts') }}"><span>Iepirkumu saraksts</span></a>
                        </li>
                        <li class="history">
                            <a href="{{ r('orderhistory') }}"><span>{!! _t('translations.orderHistory') !!}</span></a>
                        </li>
                        <li class="docs">
                            <a href="{{ r('profile') }}"><span>{!! _t('translations.profile') !!}</span></a>
                        </li>
                        @if ($user->supplier)
                            <li class="supplier-profile">
                                <a href="{{ r('supplierProfile') }}"><span>Saimniecības info</span></a>
                            </li>
                        @endif
                        <li>
                            <a href="{{ r('frontlogout') }}"><span>{!! _t('translations.logout') !!}</span></a>
                        </li>
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="menu-row-item has-cart">
        <div class="right">
            <div class="cart {{ $frontController->cartHasItems() }}">
                <div class="alert" id="alert-affix">+1</div>
                <a href="{{ r('cart') }}">
                    <span class="icon">
                        <s></s>
                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="14" viewBox="0 0 26 14">
                            <path d="M25.91,2l0.081,0.046L21,13.538V14H6l-0.806-.014L0.009,2.046,0.09,2H0V0H26V2H25.91ZM2.234,2L6.577,12H19.423L23.766,2H2.234Z" />
                        </svg>
                    </span>
                    <span class="minicart-totals">{{ $frontController->getCartTotals()->toPay }}&nbsp;€</span>
                </a>
            </div>
        </div>
    </div>
</div>
