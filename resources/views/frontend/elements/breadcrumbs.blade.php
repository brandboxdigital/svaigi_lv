@if (isset($breadcrumbs))
    <div class="row">
        <div class="col-md-12">
            <div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        @foreach($breadcrumbs??[] as $crumb)
                            @if(is_null($crumb))
                                @continue
                            @endif
                                <li class="breadcrumb-item"><a href="{{ URL::to($crumb['unique_name']) }}"
                                    class="breadcrumb-link">{{ $crumb['text'] }}</a></li>
                        @endforeach
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endif
