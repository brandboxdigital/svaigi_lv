<div class="sv-footer sticky-stopper">
    <div class="container">
        <div class="row">
            <ul class="menu">
                @include("frontend.partials.menu.main", ['menuSlug' => 'footer'])
            </ul>
        </div>
    </div>
</div>
