<!--<div class="sv-footer">
	<div class="container">
		<div class="row">
			<ul class="menu">
				<li>
					<a href="#">Sadarbībai</a>
				</li>
				<li>
					<a href="#">Abonements piegādēm</a>
				</li>
				<li>
					<a href="#">Lietošanas noteikumi</a>
				</li>
				<li>
					<a href="#">Kontakti</a>
				</li>
			</ul>
			<div class="social">
				<a href="#" class="insta"><i class="fa fa-instagram"></i></a>
				<a href="#" class="fb"><i class="fa fa-facebook"></i></a>
			</div>
		</div>
	</div>
</div>-->

<div class="sv-copyright">
	<div class="container">
		<div class="row">
			{{-- <a href="#" class="logo">© SVAIGI.LV. Web dizains un izstrāde: <img src="{{ url('img/logo-born-1.svg') }}" /></a> --}}
			{{-- <a href="#" class="logo">© SVAIGI.LV. Web dizains un izstrāde: brandbox.digital --}}
            <div class="contacts">
                    <div class="newsletter-wrapper">
                        <div id="copyright-newsletter-subscriber">
                            <form id="footer-newsletter-form" action="javascript:void(0);">
                                <label for="copy-right-newsletter">Piesakies Svaigi.lv jaunumiem </label>
                                <div  class="d-flex">
                                    <input id="copy-right-newsletter" type="text" placeholder="Epasts" name="email">
                                    <button type="submit">Pieteikties</button>
                                </div>
                            </form>
                            <span id="error" class="hide"></span>
                            <span id="success" class="hide">Paldies, pieprasījums nosūtīts!</span>
                        </div>
                    </div>
				<div>
					<span>T:</span><a href="tel:+371 24 335 225">+371 24 335 225</a>
				</div>
				<div>
					<span>E:</span><a href="mailto:svaigi@svaigi.lv">svaigi@svaigi.lv</a>
				</div>
				<div>
					<img src="{{ url('img/icon-visa.png') }}" class="visa">
				</div>
			</div>
		</div>
        <div class="row"><a href="/" class="logo logo-copy-right">©SVAIGI.LV</a></div>
	</div>
</div>

{{-- @push('head') --}}
    {{-- <script src="{{ asset('js/custom.js')}}"></script> --}}
{{-- @endpush --}}
