

<div class="sv-menu-mobile">
    <div class="logo">
        <a href="{{ r('page') }}"><img src="{{ asset('assets/img/logo-svaigilv-3.svg') }}"></a>
    </div>

    <a href="javascript:void(0)" class="sv-btn-menu toggle-sv-menu-mobile">
        <span>
            <s></s>
            <s></s>
            <s></s>
        </span>
    </a>

    <div class="content-scroller">
        {{-- @include("frontend.elements.marketdaymobile") --}}
        {{-- <div class="content visible-xs" style="justify-content: center;">
            @include("frontend.partials.menu.mobile", ['menuSlug' => 'shop'])
        </div> --}}

        <div class="content" style="justify-content: center;">
            <div id="sv-product-side-menu" class="slinky-menu slinky-theme-default "
                style="transition-duration: 300ms;">
                <ul style="transition-duration: 300ms;">
                    @include("frontend.partials.menu.desktopsidemenu", ['menuSlug' => 'desktopsidemenu'])
                </ul>
            </div>
        </div>

        {{-- <div class="sidemenu-spacer"></div> --}}

        <div class="sidemenu-footer">
            <div>
                <span>T: </span><a class="reverse" href="tel:+371 24 335 225">+371 24 335 225</a>
            </div>
            <div>
                <span>E: </span><a class="reverse" href="mailto:svaigi@svaigi.lv">svaigi@svaigi.lv</a>
            </div>
            <div>
                <img class="sidemenu-visa-img" src="{{ url('img/icon-visa.png') }}" class="visa">
            </div>
        </div>
    </div>

</div>
<div class="sv-mobile-menu-close-bg toggle-sv-menu-mobile visuallyhidden hidden"></div>
