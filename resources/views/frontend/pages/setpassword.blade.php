@extends('layouts.app')

@section('content')
    @include("Orders::frontend.partials.step")

    @include("frontend.elements.profileForm", [
        'action' => r("profile.savePassword"),
        'checkboxText' => _t('translations.iAmLegalPerson'),
        'showOnlyPassword' => true,
        'buttonText' => _t('translations.profileSaveButton')
    ])

@endsection
