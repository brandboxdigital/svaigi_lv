@extends('layouts.app')

{{-- @section('content')
    <div class="sv-page-title">
        <h2>
            <a href="{{ r('supplierOpen', [__("pages.slug.{$page->id}"), __("supplier.slug.{$supplier->id}")]) }}">{{ __("supplier.name.{$supplier->id}") }}</a>
        </h2>
        <h3>
            <a href="{{ r('page') }}">{!! _t('translations.startpage') !!}</a>
            <a href="{{ r('page', [__("pages.slug.{$page->id}")]) }}">{!! _t('translations.suppliers') !!}</a>
        </h3>
    </div>

    <div class="sv-blank-spacer medium"></div>

    <div class="vc_row wpb_row vc_row-fluid" style="padding: 0 25%;">
        <div class="wpb_column vc_column_container">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">

                    <div class="sv-text-block">
                        <img src="{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}" />
                        <br /><br />
                        <p style="font-size: 18px; line-height: 30px;">
                            {{ __("supplier.description.{$supplier->id}") }}
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="sv-blank-spacer medium"></div>

    <div class="sv-title">
        <h3>{!! __("translations.products") !!}</h3>
    </div>

    <div class="sv-blank-spacer medium"></div>

    <div class="sv-product-card-slider">
        <div class="owl-carousel">
            @foreach($supplier->products as $product)
                @php
                    $item = $cache->getProduct($product->id);
                @endphp
                @include("Products::frontend.listitem")
            @endforeach
        </div>
    </div>

    <div class="sv-blank-spacer medium"></div>

    <div id="farmersmap-canvas"></div>
@endsection --}}

{{-- @push('preScript')
    <script>
        var farmers = [
            ["{{ __("supplier.name.{$supplier->id}") }}", {{ $supplierCache->getCoords()[0] }}, {{ $supplierCache->getCoords()[1] }}, 1, "{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}"],
        ];
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi1BNqkST_SmY7gsbRxuKZziOyF4ceJB4"></script>
    <script src="{{ asset('assets/js/richmarker-compiled.js') }}">
    </script>
@endpush --}}

@section('content')

    <div class="sv-blank-spacer small"></div>

    <div class="sv-blog-list page-list">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="title-image help--image cover">
                        {{-- <img src="{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}" alt="{{ __("supplier.description.{$supplier->id}") }}" class="image"> --}}
                        <img loading="lazy" src="{{ $supplier->getImageByKey('supplier_image') }}" alt="{{ __("supplier.description.{$supplier->id}") }}" class="image">
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="sv-page-title">
                        <h2>
                            {{-- <a href="{{ r('supplierOpen', [__("pages.slug.{$page->id}"), __("supplier.slug.{$supplier->id}")]) }}">{{ __("supplier.name.{$supplier->id}") }}</a> --}}
                            {{ __("supplier.name.{$supplier->id}") }}
                        </h2>
                        <h3>
                            <a href="{{ r('page') }}">{!! _t('translations.startpage') !!}</a>
                            <a href="{{ r('page', [__("pages.slug.{$page->id}")]) }}">{!! _t('translations.suppliers') !!}</a>
                        </h3>
                    </div>


                    <div class="post">
                        {!! $supplier->meta['description'][language()] ?? '' !!}
                        {{-- {!! __("supplier.description.{$supplier->id}") !!} --}}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="sv-blog-sidebar">

                        {{-- @include('frontend.elements.blogcategories') --}}

                        {{-- @include('frontend.elements.subscribe') --}}

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="sv-blank-spacer medium"></div>
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <div class="sv-product-card-slider">
                        <div class="owl-carousel">
                            @php
                                $products = $supplier
                                    ->products()
                                    ->isPublished()
                                    ->get();
                            @endphp

                            @foreach ($products->take(4) as $product)
                                @include('Products::frontend.listitem', [
                                    'item' => $cache->getProduct($product->id)
                                ])
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="sv-blank-spacer small"></div>

                <div class="col-xs-12">
                    <div class="btn-row btn-row--center btn-row--high-z">
                        <a href="{{ r('supplier-products', ['supplier' => $supplier->slug]) }}" class="btn btn-green">Visi produkti</a>
                    </div>
                </div>


            </div>

            @if ( $user = Auth::user() )

                <div class="row">
                    <div class="sv-blank-spacer medium"></div>
                </div>

                <div class="row">
                    @include('frontend.elements.reviews', [
                        'type' => null,
                        'hasSubmit' => true,
                    ])
                </div>

            @endif
        </div>
    </div>
@endsection
