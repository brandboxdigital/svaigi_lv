@extends('layouts.app')

@php

@endphp

@section('content')

    @if ($item->youtube_id ?? null)
        @include('frontend.pages.blog_item_templates.video')
    @else
        @include('frontend.pages.blog_item_templates.default')
    @endif

@endsection
