@extends('layouts.app')

@php
    $templateView = null;
    // dd($item->linked_products);
    try {
        $templateView = 'frontend.pages.blog_templates.' . $currentCategory->view_template ?? 'default';
    } catch (\Throwable $th) {
        //throw $th;
    }
@endphp

@section('content')

    @if($templateView && View::exists($templateView))
        @include($templateView);
    @else
        @include('frontend.pages.blog_templates.default')
    @endif

@endsection
