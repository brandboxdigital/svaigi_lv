@extends('layouts.app')

@section('content')
    <div class="sv-blank-spacer small"></div>

    <div class="sv-linked-products-slider container">
        <div class="row" style="width:100%">
            @foreach($favouriteProducts as $otherProduct)
                @php
                    $item = $cache->getProduct($otherProduct->id);
                @endphp
                <div class="col-sm-6 col-md-3">
                    @include("Products::frontend.listitem")
                </div>
            @endforeach
        </div>
    </div>

    {{-- @include("Orders::frontend.partials.step") --}}
    {{-- @include("frontend.elements.profileForm", ['action' => r("profile.save"), 'checkboxText' => _t('translations.iAmLegalPerson'), 'showPassword' => ($user && !$user->isSocialAuthed()), 'buttonText' => _t('translations.profileSaveButton')]) --}}
@endsection
