@extends('layouts.app')

@section('content')
    <div class="sv-order-history">
        <div class="list-open">
            <div class="item current">
                <div class="container">
                    <div class="row">
                        <div class="file">
                            <div>
                                @php
                                    $date = (new \Carbon\Carbon($order->market_day_date))->addDays($order->delivery ? $order->delivery->deliveryTime : 0);
                                    $md = \App\Plugins\MarketDays\Model\MarketDay::withTrashed()->where('marketDaysSlug', strtolower($date->format('l')))->first();
                                    $mdName = $md->marketDay[language()];
                                @endphp

                                {{ $mdName }}, {{ $date->format('j') }}. {{ __('translations.'.$date->format('F')) }}
                            </div>
                                <div>
                                    @if ($order->delivery)
                                        {!! __("deliveries.description.{$order->delivery->id}", ['freeAbove' => number_format($order->delivery->freeAbove,2), 'dayName' => substr($mdName,0, -1)]) !!}

                                        @if ($order->delivery->delivery_provider_class)
                                            <br> {{ $order->delivery->getMethodDataFromOrder($order) }}
                                        @endif
                                    @else
                                        <br>
                                    @endif
                                </div>
                        </div>
                        <div class="download">
                            <a href="{{ route('getInvoice', [$order->invoice]) }}">{!! _t('translations.downloadInvoice') !!}
                                <span class="icon"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item header">
                <div class="container">
                    <div class="row">
                        <div class="product">
                            {!! _t('translations.cartProducts') !!}
                        </div>
                        <div class="price-single">
                            {!! _t('translations.cartPrice') !!}
                        </div>
                        <div class="quantity">
                            {!! _t('translations.cartQuantity') !!}
                        </div>
                        <div class="price-total">
                            {!! _t('translations.cartSum') !!}
                        </div>
                    </div>
                </div>
            </div>
            @foreach($items as $item)
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="product">
                                <span class="name">{{ $item->product_name }}</span>
                                <span class="farmer">{{ $item->supplier_name }}</span>
                            </div>
                            <div class="price-single">
                                <span>{{ $displayValues[$item->id]["price"] }}&nbsp;€ / {{ $item->display_name }}</span>
                                @if ($item->deposit_amount)
                                    <small class="price-depostit">+&nbsp;depozīts {{ number_format($item->deposit_amount, 2, '.', '') }}&nbsp;€</small>
                                    @endif
                                </div>
                            <div class="quantity">
                                <span>{{ $item->amount }}</span>
                            </div>
                            <div class="price-total">
                                <span>{{ number_format($displayValues[$item->id]["sum"],2, ".","") }}&nbsp;€</span>
                                <small class="price-depostit">+&nbsp;depozīts {{ number_format($displayValues[$item->id]["depositSum"],2, ".","") }}&nbsp;€</small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="item totals">
                <div class="container">
                    <div class="row">
                        <div class="product">
                            <a href="{{ r('recreateCart', [$order->id]) }}" class="repeat"><span class="icon"></span>Atkārtot
                                pirkumu</a>
                        </div>
                        <div class="data-wrapper">
                            @foreach($displayValues["vatTotals"] as $vat_id => $vatTotals)
                                    <div class="data-row">
                                        <div class="type">
                                            Kopā bez PVN ({{ $vatTotals['amount'] }}%):
                                        </div>
                                        <div class="price">
                                            {{number_format($vatTotals['summWoVat'],2, ".","") }}&nbsp;€
                                        </div>
                                    </div>
                                    <div class="data-row">
                                        <div class="type">
                                            PVN ({{ $vatTotals['amount'] }}%):
                                        </div>
                                        <div class="price">
                                            {{ number_format($vatTotals['summVat'],2, ".","") }}&nbsp;€
                                        </div>
                                    </div>
                            @endforeach

                            @if ($totalPrices->depositSum > 0)
                            <div class="data-row">
                                <div class="type">
                                    {!! _t('translations.cartDeposit') !!}
                                </div>
                                <div class="price">
                                    {{ number_format($totalPrices->depositSum,2) }}&nbsp;€
                                </div>
                            </div>
                            @endif

                            <div class="data-row">
                                <div class="type">
                                    {!! _t('translations.cartDelivery') !!}
                                </div>
                                <div class="price">
                                    {{ number_format($totalPrices->delivery,2, ".","") }}&nbsp;€
                                </div>
                            </div>

                            <div class="data-row">
                                <div class="type">
                                    Piegades VAT ({{$totalPrices->delievery_vat_amount}}%)
                                </div>
                                <div class="price">
                                    {{ number_format($totalPrices->delivery_vat,2, ".","") }}&nbsp;€
                                </div>
                            </div>
                            @if($displayValues["discountPriceNotCoupon"] > 0)
                                <div class="data-row">
                                    <div class="type">
                                        Promotions
                                    </div>
                                    <div class="price">
                                        - {{ number_format($displayValues["discountPriceNotCoupon"],2, ".","") }}&nbsp;€
                                    </div>
                                </div>
                            @endif
                            @if($order->discount_target??false)
                                <div class="data-row">
                                    <div class="type">
                                        {!! _t('translations.cartDiscount') !!} (<span
                                                style="text-transform: uppercase; font-weight:bold; ">{{$order->discount_code}}</span>)
                                    </div>
                                    <div class="price">
                                        -{{ $totalPrices->discount }}&nbsp;€
                                    </div>
                                </div>
                            @endif
                            <div class="data-row">
                                <div class="type">
                                    {!! _t('translations.cartToPay') !!}
                                </div>
                                <div class="price">
                                    {{ $totalPrices->toPay }}&nbsp;€
                                </div>
                            </div>
                            @if($order->paidAmountText > 0)
                                <div class="data-row">
                                    <div class="type">
                                        {!! _t('translations.total_paid') !!}
                                    </div>
                                    <div class="price">
                                        {{ number_format($order->paid,2, ".","") }}&nbsp;€
                                    </div>
                                </div>
                                <div class="data-row">
                                    <div class="type">
                                        {!! _t('translations.to_pay_or_return') !!}
                                    </div>
                                    <div class="price">
                                        {{ number_format($totalPrices->toPay - $order->paid,2) }}&nbsp;€
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
