
<div class="sv-blank-spacer medium"></div>

<div class="sv-blog-list page-list">
    <div class="container">
        <div class="row help--flex-wrap hidden">
            <div class="list">
                @foreach($items as $item)
                    <div class="item">
                        <a href="{{ r('blog', [__('postcategory.slug.'.$item->main_category), __('posts.slug.'.$item->id)]) }}"
                            class="link"></a>
                        <div class="title">
                            <h2>{{ __('posts.name.'.$item->id) }}</h2>
                        </div>
                        <div class="image" style="background-image: url({{ $item->getImageByKey('blog_picture') }});"></div>
                        <div class="sizing"></div>
                    </div>
                @endforeach
            </div>
            <div class="sv-blog-sidebar">

                {{-- @include('frontend.elements.blogcategories') --}}

                {{-- @include('frontend.elements.subscribe') --}}

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="centered-button btn-row btn-row--left">
                    <a class="btn btn-purple" href="/berni-saimnieko">
                        <svg width="15" height="14" viewBox="0 0 15 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.0864258 13.5L12.6572 26.071L14.0712 24.657L2.91416 13.5L14.0712 2.34296L12.6572 0.928955L0.0864258 13.5Z" fill="white"/>
                        </svg>
                        Atpakaļ
                    </a>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="sv-blog-list">
                    <div class="row help--flex-wrap">
                        @foreach($items as $item)
                            <div class="col-sm-3">
                                <div class="item">
                                    <a href="{{ r('blog', [__('postcategory.slug.'.$item->main_category), __('posts.slug.'.$item->id)]) }}"
                                        class="link"
                                    >
                                        <div class="image help--overimage">
                                            <img loading="lazy" src="{{ $item->getImageByKey('blog_picture') }}" alt="{{ __('posts.name.'.$item->id) }}" class="image">

                                            <div class="overimage">
                                                <p>Lasīt vairāk</p>
                                            </div>
                                        </div>

                                        <div class="title">
                                            <h4>{{ ___('posts.name.'.$item->id) }}</h4>
                                            <p>{{ ___('postcategory.name.'.$item->main_category) }}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>


                    @if ($pagination && $pagination->hasPages())

                    <div class="sv-pagination hidden-xs">
                        {{$pagination->onEachSide(5)->render() }}
                    </div>
                    <div class="sv-pagination visible-xs">
                        {{$pagination->onEachSide(1)->render() }}
                    </div>

                    @endif
                </div>
            </div>
        </div>

    </div>
</div>
