@php
$cat = request()->route('blogCategory');

$supplier = $frontController->getFeaturedSupplier();
@endphp


<div class="sv-blank-spacer small"></div>

<div class="sv-blog-list sv-blog-video-template page-list {{ $isBerniSaimniekoCat ? 'berni-saimnieko-blog--video' : '' }}">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                @if ($isBerniSaimniekoCat)
                    <div class="centered-button btn-row btn-row--left">
                        <a class="btn btn-purple" href="/berni-saimnieko">
                            <svg width="15" height="14" viewBox="0 0 15 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0864258 13.5L12.6572 26.071L14.0712 24.657L2.91416 13.5L14.0712 2.34296L12.6572 0.928955L0.0864258 13.5Z" fill="white"/>
                            </svg>
                            Atpakaļ
                        </a>
                    </div>
                @endif
            </div>
            <div class="col-md-6">
                @include('frontend.partials.blogTitle')

                <div class="body-text">
                    {!! $item->getMeta('excerpt') !!}

                    {{-- <div class="post">
                        {!! \App\Plugins\Blog\Model\Blog::findAndReplaceBodyBlocks(  ) !!}
                    </div> --}}
                </div>
            </div>

            <div class="col-md-12 title-video-wrap">
                <div class="title-video">
                    {!! $item->youtube_embed_html !!}
                    {{-- <img loading="lazy" src="{{ $item->getImageByKey('blog_picture') }}" alt="{{ __('posts.name.'.$item->id) }}" class="image"> --}}
                </div>
            </div>

            <div class="col-md-12">
                    {{-- @include('frontend.elements.blogcategories') --}}

                    {{-- @include('frontend.elements.subscribe') --}}

                    <div class="post">
                        {!! \App\Plugins\Blog\Model\Blog::findAndReplaceBodyBlocks( __('posts.content.'.$item->id) ) !!}
                    </div>
            </div>
        </div>

        <div class="row">
            <div class="sv-blank-spacer medium"></div>
        </div>

        @if (count($item->linked_products ?? 0) > 0)

            <div class="row">
                <div class="col-xs-12">
                    <div class="sv-product-card-slider">
                        <div class="owl-carousel">
                            @foreach ($item->linked_products as $product)
                                @include('Products::frontend.listitem', [
                                    'item' => $cache->getProduct($product->id)
                                ])
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if (!$isBerniSaimniekoCat)
        <div class="row">
            <div class="sv-blank-spacer medium"></div>
        </div>
        @endif

        {{-- <div class="row">
            @include('frontend.elements.reviews', [
                'hasSubmit' => true,
            ])
        </div> --}}
    </div>

    @if ($isBerniSaimniekoCat)
        @include('Blog::form', ['cat' => $cat])

        <div class="sv-blank-spacer medium--minus"></div>
    @endif
</div>
