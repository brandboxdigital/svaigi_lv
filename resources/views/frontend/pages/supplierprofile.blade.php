@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @include("frontend.elements.supplierProfileForm", [
                    'action' => r("supplierProfile.save"),
                    'buttonText' => _t('translations.profileSaveButton')
                ])

                <div class="row">
                    <div class="sv-blank-spacer medium"></div>
                </div>

                <div class="row">
                    @include('frontend.elements.reviews', [
                        'type' => 'suppliers',
                        'hasReviews' => true,
                        'hasSubmit' => false,
                    ])
                </div>
            </div>
        </div>
    </div>
    {{-- @include("Orders::frontend.partials.step") --}}

@endsection
