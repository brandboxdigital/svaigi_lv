@php
    use App\Plugins\Categories\Model\Category;
    use \App\Cache\MenuCache;

    $parentCategory = $menuItem->id ?? 501;
    $categories = Category::with('descendants')->defaultOrder()->descendantsOf($parentCategory);
    $tree = $categories->toTree();
    // dd($categories->first()->descendants);
@endphp

@if (count($tree) > 0)
    <div class="main-menu--dropdown {{ $subMenuClass ?? 'sv-dropdown sv-default-dropdown' }}">
        <div class="inner-wrap container">
            @foreach ( $tree as $cat)
                <div class="main-cat">
                    <a href="{{$cat->getUrl()}}"><span>{{  $cat->name }}</span></a>

                    @if ($cat->children && $cat->full_product_count > 0)
                        <div class="sv-subcategory-wraper">

                            @foreach ($cat->children as $subCat)
                                <a href="{{$subCat->getUrl()}}" class="sub-cat"><span>{{  $subCat->name }}</span></a>
                            @endforeach

                            {{-- @if(
                                isset($topMenu)
                                || ($currentMenuId??false) == $menuItem
                                || (request()->route('slug3') && in_array($currentMenuId, $menu->getItems($menuItem))) // only if child = current
                            )
                                @include('frontend.partials.menu._submenu', ['menuItem' => $menuItem, 'rightColumn' => true])
                            @endif --}}

                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
@endif
