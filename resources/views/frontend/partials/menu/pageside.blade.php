@php
    $hasChilderen = count($children) > 0;
    $hasParent    = $slug_category->parent != null;
    // dd($slug_category);
    // dd($slug_category->parent, $hasParent, !$hasParent);
    // dd($category->parent->children);

    try {
        $showTopCat = $slug_category->id == $category->id;
    } catch (\Throwable $th) {}


@endphp

<ul class="list-menu">
    @if (!$hasParent)
        <li class="list-menu-item">
            <div class="list-menu-link is-active">
                {{ ___('category.name.'.$slug_category->id) }}
                <div class="" style="height:40px"> </div>
            </div>
        </li>
    @else
        <li class="list-menu-item">
            <div class="list-menu-link is-active">
                {{ ___('category.name.'.$category->id) }}
                <div class="" style="height:40px"> </div>
            </div>
        </li>
    @endif

{{-- If has children then render childern --}}
@foreach ($children as $cat)
    @php
        $isActive = $category->id == $cat->id;
        $isActive = false;
        $hasProducts = $cat->full_product_count > 0;
    @endphp

    @if ($hasProducts)
        <li class="list-menu-item">
            <a href="{{ $cat->getUrl() }}" class="list-menu-link {{ ($isActive)?'is-active':'' }}">
                {{ ___('category.name.'.$cat->id) }}
                <div class="arrow arrow-next is-small"></div>
            </a>
        </li>
    @endif
@endforeach
</ul>
