{{-- @todo Somethings is not working right.. Should rework this to proper code --}}

@php
    /** @var \App\Cache\MenuCache $menu */
    // $menu = $cache->getMenuCache($menuSlug??"main");
    $categories = $cache->getCategoryCache();

    $atLeastOneMenu = false;
@endphp

@foreach($menu->getItems(($menuId??"first")) as $menuItem)
        @if (
            isset($subMenuClass)
            && request()->route('slug2')
            && !in_array($currentMenuId, $menu->getItems($menuItem))
            && ($currentMenuId??false) != $menuItem
        )
            @continue
        @endif

        @php
            $atLeastOneMenu = true;
        @endphp

        @push('menuitems')
            <li class="{{ ($currentMenuId??false)==$menuItem?"active":"" }}{{$menu->hasItems($menuItem)?" sv-default-dropdown-toggle ":""}}">
                {{-- <a href="{{$menu->getUrl($menuItem)}}"><span>{{  $menu->getName($menuItem) }}</span></a> --}}

                <div class="main-menu--dropdown {{ $subMenuClass ?? 'sv-dropdown sv-default-dropdown' }}">
                    <div class="inner-wrap">

                        @if ($menu->getType($menuItem) == 'category')
                            @include('frontend.partials.menu.categories', ['menuItem' => $menu->getItem($menuItem) ])

                        @elseif(
                            isset($topMenu)
                            || ($currentMenuId??false) == $menuItem
                            || (request()->route('slug3') && in_array($currentMenuId, $menu->getItems($menuItem))) // only if child = current
                        )
                            @include('frontend.partials.menu._submenu', ['menuItem' => $menuItem, 'rightColumn' => true])
                        @endif
                    </div>
                </div>
            </li>
        @endpush
@endforeach

@if ($atLeastOneMenu)

    <span data-toggle="collapse" data-target="#filter-category" class="title collapsed">Apakškategorijas</span>
    <div id="filter-category" class="collapse">
        <div class="content">
            @stack('menuitems')
        </div>
    </div>

@endif
