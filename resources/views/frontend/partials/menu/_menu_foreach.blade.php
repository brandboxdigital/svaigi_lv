@foreach($menu->getItems($menuItem) as $subItemID)
	<li class="{{ ($currentMenuId??false)==$subItemID?"active":"" }}">
		<a href="{{$menu->getUrl($subItemID)}}"><span>{{  $menu->getName($subItemID) }}</span></a>
		{{--@include('frontend.partials.menu._submenu', ['menuItem' => $subItemID, 'rightColumn' => false])--}}
	</li>
@endforeach