@php
    $deepnes = isset($deepnes)
        ? $deepnes+1
        : 4;
    $active = $active??false
@endphp

@if($menu->hasChildren($menuData) && $deepnes < 4)
    {{-- {{$menuData}} --}}
    <ul class="{{ $active ? 'active current-active-category' : '' }}">
        @foreach($menu->getItems($menuData) as $subItem)
            <li>
                {{-- <a {{ $menu->hasChildren($subItem)?"href=# class=next":"href=".$menu->getUrl($subItem) }}> --}}

                <a class="slinky-clickable" href="{{ $menu->getUrl($subItem) }}" >
                    {{ $menu->getName($subItem) }}
                </a>
                @include('frontend.partials.menu.items.mobile', ['menuData' => $subItem, 'deepnes' => $deepnes, 'active' => $menu->getUrl($subItem) == Request::url()])
            </li>
        @endforeach
    </ul>
@endif
