@php
    /** @var \App\Cache\MenuCache $menu */
    $menu = $cache->getMenuCache($menuSlug??"main");
    $categories = $cache->getCategoryCache();
    //$slug = request()->route('slug3') ? 'slug2' : 'slug1'; // Иной вариант меню
@endphp

{{-- @include('frontend.partials.menu.categories') --}}

@foreach($menu->getItems(($menuId??"first")) as $menuItem) {{-- /, $slug/ в getItems --}}
        @if (
            isset($subMenuClass)
            && request()->route('slug2')
            && !in_array($currentMenuId, $menu->getItems($menuItem))
            && ($currentMenuId??false) != $menuItem
        )
            @continue
        @endif
        <li class="{{ ($currentMenuId??false)==$menuItem?"active":"" }}{{$menu->hasItems($menuItem)?" sv-default-dropdown-toggle ":""}}">
            <a href="{{$menu->getUrl($menuItem)}}"><span>{{  $menu->getName($menuItem) }}</span></a>


            @if ($menu->getType($menuItem) == 'category')
                @include('frontend.partials.menu.categories', ['menuItem' => $menu->getItem($menuItem) ])

            @elseif(
                isset($topMenu)
                || ($currentMenuId??false) == $menuItem
                || (request()->route('slug3') && in_array($currentMenuId, $menu->getItems($menuItem))) // only if child = current
            )
                <div class="main-menu--dropdown {{ $subMenuClass ?? 'sv-dropdown sv-default-dropdown' }}">
                    <div class="inner-wrap container">
                        @include('frontend.partials.menu._submenu', ['menuItem' => $menuItem, 'rightColumn' => true])
                    </div>
                </div>
            @endif
        </li>
@endforeach
