@if($menuImage = $categories->dropDownMenuImage('1473x247', $menu->getMenuOwnerId($menuItem)))
	<div class="{{$rightColumn ? 'left' : 'deep-inside'}}">
		<ul>
			@include('frontend.partials.menu._menu_foreach')
		</ul>
	</div>
	@if ($rightColumn)
		<div class="right">
			<div class="bg-static">
				<div class="image"
					 style="background-image: url({{ asset($menuImage) }});"></div>
			</div>
		</div>
	@endif
@else
	<ul>
		@include('frontend.partials.menu._menu_foreach')
	</ul>
@endif