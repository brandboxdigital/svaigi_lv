@php
    /** @var \App\Cache\MenuCache $menu */
    $menu = $cache->getMenuCache($menuSlug??"main");
    $content = $menu->getItemsTreeJson();
@endphp

<mobile-side-menu :items="{{ $content }}"></mobile-side-menu>
