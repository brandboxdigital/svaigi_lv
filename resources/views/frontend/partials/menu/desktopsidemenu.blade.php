@php
    /** @var \App\Cache\MenuCache $menu */
    $menu = $cache->getMenuCache($menuSlug??"main");

    $deepnes = 0;
@endphp

<a href="/partika">Pārtika</a>

@if (isset($firstAsHeader) && $firstAsHeader)
    @foreach($menu->getItems(($menuId??"first")) as $menuItem)
    <h4 class="sv-mobile-menu-title">
        <a href="{{ $menu->getUrl($menuItem) }}">{{ $menu->getName($menuItem) }}</a>
    </h4>
    <div class="sv-product-mobile-menu slinky-menu slinky-theme-default visible-xs" style="transition-duration: 300ms;">
        {{-- <ul style="transition-duration: 300ms;"> --}}
            @include('frontend.partials.menu.items.mobile', ['menuData' => $menuItem, 'deepnes' => $deepnes])
        {{-- </ul> --}}
    </div>
    @endforeach
@else
    @foreach($menu->getItems(($menuId??"first")) as $menuItem)
        <li>
            <a {{ $menu->hasChildren($menuItem)?"href=# class=next":"href=".$menu->getUrl($menuItem) }}>{{ $menu->getName($menuItem) }}</a>
            @include('frontend.partials.menu.items.mobile', ['menuData' => $menuItem, 'deepnes' => $deepnes])
        </li>
    @endforeach
@endif
