<div class="sv-page-title">
    <h2>
        <a href="{{ r('blog') }}">{{ __('translations.blog') }}</a>

        @if($currentCategoryId)
            <a href="{{ r('blog', [__('postcategory.slug.'.$currentCategoryId)]) }}">{{__('postcategory.name.'.$currentCategoryId)}}</a>
        @endif
    </h2>
</div>
