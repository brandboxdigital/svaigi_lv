@php
    $title            = $component->getData('title');
    $link            = $component->getData('link');
    $is_btn_wide = $component->getData('is_btn_wide');
    $only_desktop = $component->getData('only_desktop');
@endphp

<div class="centered-button btn-row btn-row--center {{ $only_desktop ? 'only-desktop' : ''}}">
    <a class="btn btn-purple {{ $is_btn_wide ? 'btn-wide' : '' }}" href="{{ $link }}">{{ $title }}</a>
</div>