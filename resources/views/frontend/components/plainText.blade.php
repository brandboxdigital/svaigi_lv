@php
    $cssClass = $component->getData('isWide')
        ? 'col-xs-12'
        : 'col-md-10 col-md-push-1';
@endphp
<div class="sv-text-page">
    <div class="container">
        <div class="row">
            {{-- <div class="{{$cssClass}}"> --}}
            <div>
                {!! $component->getData('content') !!}
            </div>
            {{-- {!! __("pages.plainTextContent.{$page->id}") !!} --}}
        </div>
    </div>
</div>
