@php
    // $list = \App\Plugins\Suppliers\Model\Supplier::orderBy('custom_id')->whereHas('products')->get();
    use App\Http\Controllers\CacheController;

    $list = \App\Plugins\Suppliers\Model\Supplier::orderBy('custom_id')
        ->whereHas('products', function($query) {
            $query
                ->isPublished()
                ->whereHas('market_days', function ($q) {
                    $md = (new CacheController)->getSelectedMarketDay();
                    $q->where('market_day_id', $md->id);
                });
        })
        ->get();
@endphp

@foreach($list->pluck('id') as $supplier)
    @php
        $supplierCache = $cache->getSupplier($supplier);
        // dd($supplierCache->isFarmer, $supplierCache->isCraftsman);
    @endphp

    @if($supplierCache->isFarmer)
        @push('farmers')
            <a href="{{ r('supplierOpen', [__("pages.slug.{$page->id}"), __("supplier.slug.$supplier")]) }}" class="item">
                <img loading="lazy" src="{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}">
                <h3>{{ __("supplier.name.$supplier") }}</h3>
                <h4>{{ __("supplier.location.$supplier") }}</h4>
            </a>
        @endpush
        @if($supplierCache->hasCoords())
            @push('farmer-coords')
                ["{{ __("supplier.name.$supplier") }}", {{ $supplierCache->getCoords()[0] }}, {{ $supplierCache->getCoords()[1] }}, {{ $loop->iteration }}, "{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}"],
            @endpush
        @endif
    @endif

    @if($supplierCache->isCraftsman)
        @push('craftsman')
            <a href="{{ r('supplierOpen', [__("pages.slug.{$page->id}"), __("supplier.slug.$supplier")]) }}" class="item">
                <img loading="lazy" src="{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}">
                <h3>{{ __("supplier.name.$supplier") }}</h3>
                <h4>{{ __("supplier.location.$supplier") }}</h4>
            </a>
        @endpush
        @if($supplierCache->hasCoords())
            @push('craftsman-coords')
                ["{{ __("supplier.name.$supplier") }}", {{ $supplierCache->getCoords()[0] }}, {{ $supplierCache->getCoords()[1] }}, {{ $loop->iteration }}, "{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}"],
            @endpush
        @endif
    @endif

    {{-- Dont know what are isFarmer and isCraftsman --}}
    @if($supplierCache->hasCoords())
        @push('supplier-coords')
            ["{{ __("supplier.name.$supplier") }}", {{ $supplierCache->getCoords()[0] }}, {{ $supplierCache->getCoords()[1] }}, {{ $loop->iteration }}, "{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}"],
        @endpush
    @endif

@endforeach

@push('preScript')
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.Default.css' rel='stylesheet' />
    <link href='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.css' rel='stylesheet' />
    <link rel="stylesheet" href="//unpkg.com/leaflet-gesture-handling/dist/leaflet-gesture-handling.min.css" type="text/css">

    <script src='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js'></script>
    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>
    <script src="//unpkg.com/leaflet-gesture-handling"></script>

    <script>
        var farmers = [
            @stack('farmer-coords')
        ];

        var masters = [
            @stack('craftsman-coords')
        ];

        var all_suppliers = [
            @stack('supplier-coords')
        ];
    </script>

    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi1BNqkST_SmY7gsbRxuKZziOyF4ceJB4"></script> --}}
    {{-- <script src="{{ asset('assets/js/richmarker-compiled.js') }}">
    </script> --}}
@endpush

{{--<div class="sv-tabs">
    <div class="container">
        <div class="row">
            <ul class="">
                <li class="active">
                    <a href="#farmers" class="tab" id="reloadFarmersMarkers">{!! _t('translations.suppliers') !!}</a>
                </li>
                <li>
                    <a href="#masters" class="tab" id="reloadMastersMarkers">{!! _t('translations.craftsmen') !!}</a>
                </li>
            </ul>
        </div>
    </div>
</div>--}}

{{-- <div id="farmersmap-canvas"></div>s --}}

<div class="container">
    <div class="row">

        <div class="col-xs-12 text-center mt-3 mb-3">
            <h3>Mūsu saimniecības</h3>
        </div>

        <div class="col-xs-12">

            <div class="row help--flex-wrap">
                @foreach($list as $supplier)
                    @php
                        $supplierCache = $cache->getSupplier($supplier->id);
                    @endphp

                    <div class="col col-md-4 mb-3 supplier-card">
                        <div class="card">
                            <div class="card-image-ratio" />
                                <img loading="lazy" loading="lazy" src="{{ $supplier->getImageByKey('supplier_image', 'main') }}" />
                            </div>
                            {{-- <img loading="lazy" class="card-image" src="{{ $supplierCache->image(config('app.imageSize.supplier_image.main')) }}" /> --}}

                            <div class="card-content">
                                <p class="card-label"> {{ ___("supplier.location.{$supplier->supplier_id}") }} </p>
                                <h3 class="card-title"> {{ __("supplier.name.$supplier->id") }} </h3>
                                {{-- {{ __("supplier.location.$supplier") }} --}}
                                <div class="card-paragraph">
                                    {!! $supplier->getMeta('excerpt') !!}
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="btn-row btn-row--fill">
                                    @php
                                        $attrArr = [getSupplierSlugs(language()), __('supplier.slug.' . $supplier->id)];
                                    @endphp
                                    <a href="{{ r('supplier-products', ['supplier' => __('supplier.slug.' . $supplier->id)]) }}" class="btn btn-green"> Visi Produkti </a>
                                    <a href="{{ r('supplierOpen', $attrArr) }}" class="btn btn-gray">{{__('translations.readMore')}}</a>
                                </div>
                            </div>

                        </div>
                    </div>

                @endforeach
            </div>

        </div>

    </div>
</div>

{{--<div class="sv-farmers-wrapper">
    <div class="sv-tab-content">
        <div class="tab-pane fade in active" id="farmers">
            <div class="container">
                <div class="row">

                    <div class="sv-farmers-list">
                        @stack('farmers')
                    </div>

                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="masters">
            <div class="container">
                <div class="row">

                    <div class="sv-farmers-list">
                        @stack('craftsman')
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>--}}
