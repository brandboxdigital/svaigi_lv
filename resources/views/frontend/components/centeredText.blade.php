@php
    $hasBerniSaiimniekoStyles            = $component->getData('hasBerniSaimniekoStyles');
    $isTextAlignCenter            = $component->getData('isTextAlignCenter');
@endphp
<div class="{{ $hasBerniSaiimniekoStyles ? 'berni-saimnieko-centered-text' : '' }} vc_row wpb_row vc_row-fluid" style="padding: 0 20%;">
    <div class="wpb_column vc_column_container">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">

                <div class="sv-text-block {{ $isTextAlignCenter ? 'text-center' : '' }}">
                    <p>
                        {!! $component->getData('centeredText') !!}
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>