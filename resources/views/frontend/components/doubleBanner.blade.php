<div class="sv-blank-spacer small"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="sv-image-banner">
                <div class="item">
                    <a href="{{ $component->getData('leftBannerUrl', 'meh') }}" class="link"></a>
                    <div class="title">
                        <h3>
                            @if ($component->getData('leftBannerIcon', null))
                                <span class="glyphicon glyphicon-{{ $component->getData('leftBannerIcon') }}"
                                    aria-hidden="true"></span>
                            @endif
                            {{ $component->getData('leftBannerTitle') }}
                        </h3>
                    </div>
                    <div class="subtitle">
                        <p>{{ $component->getData('leftBannerSubtitle') }}</p>
                    </div>
                    <div class="image"
                        style="background-image: url({{ $component->getComponentImage(false, 'original', 'pageimage1') }});">
                    </div>
                    <div class="sizing"></div>
                </div>
                <div class="item">
                    <a href="{{ $component->getData('rightBannerUrl') }}" class="link"></a>
                    <div class="title">
                        <h3>
                            @if ($component->getData('rightBannerIcon', null))
                                <span class="glyphicon glyphicon-{{ $component->getData('rightBannerIcon') }}"
                                    aria-hidden="true"></span>
                            @endif
                            {{ $component->getData('rightBannerTitle') }}
                        </h3>
                    </div>
                    <div class="subtitle">
                        <p>{{ $component->getData('rightBannerSubtitle') }}</p>
                    </div>
                    <div class="image"
                        style="background-image: url({{ $component->getComponentImage(false, 'original', 'pageimage2') }});">
                    </div>
                    <div class="sizing"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sv-blank-spacer small"></div>
