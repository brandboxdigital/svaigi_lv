@php
    $metaContent = $component->getMetaAttribute();
    $imageFileIds = isset($metaContent['data']['lv']['image']) ? $metaContent['data']['lv']['image'] : [];
    $imgSourceArr = [];
    foreach ($imageFileIds as $id) {
        $img = \App\Plugins\Admin\Model\File::find($id);
        $path = $metaContent['data']['lv']['select'];

        if ($img) {
            $imgSourceArr[] = '/' . implode('/', [config("app.uploadFile.{$img->owner}"), $path ?: current(config("app.imageSize.{$img->owner}")), $img->filePath]);
        }
    }
@endphp

@if (count($imgSourceArr))
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="image-retrieve-container">
                    @foreach ($imgSourceArr AS $src)
                        <img class="image-retrieve" class="rere" src="{{$src}}">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif


