@php
    $item = null;

    if ($component->getData('showProduct') == 'on') {
        $product = \App\Plugins\Products\Model\Product::where(['is_highlighted' => 1])->first();

        if ($product) {
            $item = $cache->getProduct($product->id);
        }
    }

@endphp

<div class="sv-blank-spacer small"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="sv-page-title-banner">
                <div class="row">
                    <div class="col col-md-8 col-md-offset-2 flex">
                        <div class="contain">
                            <div class="title">
                                <h3>{{ $component->getData('bannerHeader') }}</h3>

                                @if ($item)
                                <div class="product-item visible-xs">
                                    @include('Products::frontend.listitem', [
                                        'item' => $item
                                    ])
                                </div>
                                @endif


                                <h5>{{ $component->getData('bannerText') }}</h5>

                                <div class="spacer"></div>

                                @if ($component->getData('buttonUrl'))
                                    <a
                                        href="{{$component->getData('buttonUrl')}}"
                                        class="btn btn-light btn-transparent"
                                    >
                                        {{$component->getData('buttonTitle')}}
                                    </a>
                                @endif
                            </div>

                            @if ($item)
                            <div class="product-item hidden-xs">
                                @include('Products::frontend.listitem', [
                                    'item' => $item
                                ])
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="bg bg-parallax">
                    <div class="image" style="background-image: url({{ $component->getComponentImage('last') }});"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sv-blank-spacer small"></div>
