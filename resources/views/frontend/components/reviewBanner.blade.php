@php
    // $content = collect($component->getData('content'))
    //     ->map(function($item) {
    //         $body = '';

    //         foreach (explode("\n", $item['body']) as $p) {
    //             $body .= "<p>$p</p>";
    //         }

    //         $item['body'] = $body;

    //         return $item;
    //     })
    //     ->toJson();

    $faker = Faker\Factory::create();

    $items = [
        (object)[ 'title' => $faker->paragraph, 'author' => $faker->name, 'url' => "https://google.lv" ],
        (object)[ 'title' => $faker->paragraph, 'author' => $faker->name, 'url' => "https://google.lv" ],
        (object)[ 'title' => $faker->paragraph, 'author' => $faker->name, 'url' => "https://google.lv" ],
        (object)[ 'title' => $faker->paragraph, 'author' => $faker->name, 'url' => "https://google.lv" ],
        (object)[ 'title' => $faker->paragraph, 'author' => $faker->name, 'url' => "https://google.lv" ],
        (object)[ 'title' => $faker->paragraph, 'author' => $faker->name, 'url' => "https://google.lv" ],
        (object)[ 'title' => $faker->paragraph, 'author' => $faker->name, 'url' => "https://google.lv" ],
    ]
@endphp

<div class="sv-review-banner owl-carousel">
    @foreach ($items as $item)
        <div class="review-banenr-item " style="background-image: url(https://images.unsplash.com/photo-1632871975364-e6d471ece9e5)">
            <div class="container">
                <div class="content">
                    <div class="texts">
                        <img loading="lazy" src="https://avatars.dicebear.com/api/big-smile/{{$faker->name}}.svg" alt="{{$item->title}}">
                        <h2>{{$item->title}}</h2>
                        <p>{{$item->author}}</p>
                    </div>

                    <div class="btn-row btn-row--center">
                        <div class="btn btn-green">{{__('translations.readMore')}}</div>
                    </div>
                </div>
            </div>

            <div class="bg help--dark-bg help--blur-bg"></div>
        </div>
    @endforeach
</div>
