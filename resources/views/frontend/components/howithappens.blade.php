@php
    $items = [];

    foreach ([
        1,2,3
    ] as $index) {
        $items[] = [
            'image_src'   => $component->getComponentImage(false, 'original', 'pageimage'.$index),
            'title'       => $component->getData('howstep' . $index . 'title'),
            'description' => $component->getData('howstep' . $index . 'description'),
        ];
    }

@endphp


<div class="sv-blank-spacer small"></div>

<div class="container sv-steps">
    <div class="row">
        <div class="col-xs-12">
            <div class="sv-title">
                <h3>{{ $component->getData('howheader') }}</h3>
            </div>
        </div>
    </div>
</div>

<div class="sv-blank-spacer small"></div>

<div class="container sv-blog-list how-it-happens">
    <div class="row">
        @foreach ($items as $item)
            <div class="col-lg-4">
                <div class="item has-loaded">
                    <div class="wrapper">
                        <div class="image help--overimage">
                            <img loading="lazy"
                                src="{{ $item['image_src'] }}"
                                alt="Kā garšo Latvija? Intervija žurnālam OK!" class="image "
                            >

                            <p>
                                {{ $item['title'] }}
                            </p>

                            <div class="overimage">
                                {!! $item['description'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@php
    $text = $component->getData('howdescription', null);
@endphp

@if ($text)
    <div class="container sv-steps">
        <div class="row">
            <div class="col-xs-12 mt-3">
                <div class="how-it-happens-description-text">
                    {!! $text !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="container sv-steps">
    <div class="row">
        <div class="col-xs-12 mt-3">
            <div class="btn-row btn-row--center">
                <a href="/ka-tas-notiek" class="btn btn-green">{{ __('translations.readMore') }}</a>
            </div>
        </div>
    </div>
</div>


<div class="sv-blank-spacer small"></div>
