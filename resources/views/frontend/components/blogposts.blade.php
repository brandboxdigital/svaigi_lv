@php
    $extention       = $component->getExtention();

    $title           = $component->getData('title', 'Svaigi.lv padomi ērtākai ikdienai!');
    $isTitleDisabled = $component->getData('is_title_disabled') == 'on';

    $items           = $extention->getPosts();

    $singleFullpage  = $component->getData('is_single_fullpage' , null) == 'on';

    $hasBerniSaimniekoStyles  = $component->getData('has_berni_saimnieko_styles' , null) == 'on';
    $is_slider = $component->getData('is_slider' , null) == 'on';
    $hideMobile = $component->getData('hide_mobile' , null) == 'on';
@endphp

<div class="{{ $hideMobile ? 'hide-mobile' : '' }}">
    {{-- <div class="sv-blank-spacer {{ $hasBerniSaimniekoStyles ? 'hide-mobile ten-pixels' : 'small' }}"></div> --}}

    @if (!$isTitleDisabled)
        <div class="sv-title">
            <h3> {{ $title }} </h3>
        </div>
        <div class="sv-blank-spacer small"></div>
    @endif

    <div class="container {{ $hasBerniSaimniekoStyles && $singleFullpage ? 'berni-saimnieko-single-full-post' : '' }} {{ $hasBerniSaimniekoStyles && !$singleFullpage ? 'berni-saimnieko-posts' : '' }}">
        <div class="row">
            <div class="col-xs-12">
                <div class="sv-blog-list {{ $is_slider ? 'sv-blog-posts-slider' : '' }}">
                    <div class="{{ $is_slider ? 'owl-carousel' : 'row' }}">
                        @foreach($items as $item)
                            <div class="{{ ($singleFullpage) ? 'col-sm-12' : ($is_slider ? '' : 'col-sm-4') }}">
                                <div class="item">

                                    <a href="{{ r('blog', [__('postcategory.slug.'.$item->main_category), __('posts.slug.'.$item->id)]) }}"
                                        class="link"
                                    >
                                        @if ($singleFullpage && $item->youtube_id)
                                            <div class="title-video-wrap">
                                                <div class="title-video">
                                                    {!! $item->youtube_embed_html !!}
                                                </div>
                                            </div>
                                        @else
                                            <div class="image help--overimage">
                                                <img
                                                    loading="lazy"
                                                    src="{{ $item->getImageByKey('blog_picture') }}"
                                                    alt="{{ __('posts.name.'.$item->id) }}"
                                                    class="image {{($singleFullpage) ? 'help--is-fullwidth-image' : ''}}"
                                                >

                                                <div class="overimage">
                                                    <p>Lasīt vairāk</p>
                                                </div>
                                            </div>
                                        @endif

                                        @if ($singleFullpage)
                                            <div class="title-and-button">
                                                <div class="title">
                                                    <h4>{{ ___('posts.name.'.$item->id) }}</h4>
                                                    <p>{{ ___('postcategory.name.'.$item->main_category) }}</p>
                                                </div>
                                                <div class="btn-wrap">
                                                    <a href="{{ r('blog', [__('postcategory.slug.'.$item->main_category), __('posts.slug.'.$item->id)]) }}" class="btn btn-purple is-purple-light btn-text-is-green">Lasīt vairāk</a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="title">
                                                <h4>{{ ___('posts.name.'.$item->id) }}</h4>
                                                <p>{{ ___('postcategory.name.'.$item->main_category) }}</p>
                                            </div>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($hasBerniSaimniekoStyles && !$singleFullpage)
    <div class="sv-blank-spacer medium"></div>
    @else
    <div class="sv-blank-spacer small"></div>
    @endif

</div>
