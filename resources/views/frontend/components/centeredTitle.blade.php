<div class="sv-title {{ $component->getData('is_berni_saimnieko') ? 'berni-saimnieko-title' : '' }} {{ $component->getData('mobile_only') ? 'hide-desktop' : '' }}">
    <h3>{{ $component->getData('title') }}</h3>
</div>