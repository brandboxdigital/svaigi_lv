@php
    $title = $component->getData('title');
    $content = $component->getData('content');
    $btn_text = $component->getData('link_text');
    $btn_link = $component->getData('link');
    $image = $component->getComponentImage(false, 'original', 'pageimage');
@endphp

<div class="sv-head-image">
    <div class="background-image" style="background-image: url({{ $image }})"></div>
    <div class="background-image-overlay" ></div>

    <div class="container">
        <div class="row">
            <div class="col col-xs-12 col-md-6">
                <h1 class="giga">{{$title}}</h1>
            </div>
            <div class="col col-xs-12 col-md-2"></div>
            <div class="col col-xs-12 col-md-4">
                <div class="content">
                    <div class="body">
                        {!! $content !!}
                    </div>

                    <div class="btn-row ">
                        {{-- <a href="{{$btn_link}}" class="btn btn-white-link-arrow"> {{ $btn_text }}</a> --}}
                        <a href="{{$btn_link}}" class="btn btn-green"> {{ $btn_text }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
