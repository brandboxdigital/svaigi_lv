@php
    $content = collect($component->getData('content'))
        ->map(function($item) {
            $body = '';

            foreach (explode("\n", $item['body']) as $p) {
                $body .= "<p>$p</p>";
            }

            $item['body'] = $body;

            return $item;
        })
        ->toJson();
@endphp

<div class="container sv-text-page">
    <div class="row">
        <div class="col-md-6">
            <accordion :items="{{ $content }}" open-first></accordion>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-5">
            <div class="help--image">
                <img loading="lazy" src="{{$component->getComponentImage(false, 'original', 'pageimage')}}" alt="{{ $component->getData('title') }}">
            </div>
        </div>
    </div>
</div>
