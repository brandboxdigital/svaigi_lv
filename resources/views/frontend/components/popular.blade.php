@php
    $items = \App\Plugins\Products\Model\Product::where(['is_highlighted' => 1])
        ->isPublished()
        ->pluck('storage_amount', 'id')
        ->toArray();
@endphp

<div class="sv-blank-spacer small"></div>

<div class="sv-title">
    <h3>{{ $component->getData('title') }}</h3>
</div>

<div class="sv-blank-spacer small"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="sv-product-card-slider">
                <div class="owl-carousel">
                    @foreach ($items as $itemId => $itemAmount)
                        @include('Products::frontend.listitem', [
                            'item' => $cache->getProduct($itemId)
                        ])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sv-blank-spacer small"></div>
