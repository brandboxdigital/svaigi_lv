@php
    $slides           = [];
    $isBerniSaimnieko = $component->getData('is_berni_saimnieko');

    foreach ([1,2,3] as $key) {
        if ($component->getData('is_enabled' . $key) == 'on') {
            $banner = [
                'label' => $component->getData('label' . $key),
                'title' => $component->getData('title' . $key),
                'content' => $component->getData('content' . $key),
                'link' => $component->getData('link' . $key),
                'image' => $component->getComponentImage(false, 'original', 'pageimage' . $key),
            ];

            $slides[] = (object) $banner;
        }
    }

    // dd($slides);
@endphp


<div class="sv-blank-spacer small {{ $isBerniSaimnieko ? 'berni-saimnieko-hide' : '' }}"></div>

<div class="container {{ $isBerniSaimnieko ? 'berni-saimnieko-carousel' : '' }}">
    <div class="row">
        <div class="col-xs-12">
            <div class="sv-carousel-banner">
                <div class="owl-carousel">
                @foreach ($slides as $slide)
                    <div class="sv-carousel-banner--item">

                        <div class="sv-carousel-banner--left">
                            <div class="sv-carousel-banner--image">
                                <img loading="lazy" src="{{ $slide->image }}" alt="{{$slide->title}}">
                            </div>
                        </div>

                        <div class="sv-carousel-banner--right">
                        <div class="sv-carousel-banner--content">
                            <div class="sv-carousel-banner--label">
                                <span>{{$slide->label}}</span>
                            </div>
                            <div class="sv-carousel-banner--spacer"></div>
                            <div class="sv-carousel-banner--title">
                                <h2> {{$slide->title}} </h2>
                                <div> {!! $slide->content !!} </div>
                            </div>

                            <div class="btn-row btn-row--right">
                                <a href="{{$slide->link}}" class="btn btn-green">{{__('translations.readMore')}}</a>
                                {{-- @if ($isBerniSaimnieko)
                                <a href="{{$slide->link}}" class="btn btn-desktop">Lasīt Vairāk</a>
                                <a href="{{$slide->link}}" class="btn btn-mobile">Visi jaunumi</a>
                                @else
                                <a href="{{$slide->link}}" class="btn btn-green">Pasūtīt</a>
                                @endif --}}
                            </div>
                        </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sv-blank-spacer small"></div>
