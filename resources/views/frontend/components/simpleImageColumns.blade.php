@php
    $extention       = $component->getExtention();
    // $title           = $component->getData('title', 'Svaigi.lv padomi ērtākai ikdienai!');
    // $isTitleDisabled = $component->getData('is_title_disabled') == 'on';
    // $isBerniSaimniekoPage = request()->is(['*/berni-saimnieko', 'berni-saimnieko']);
    $path = request()->path();
    $images = [];
    if ($path == 'berni-saimnieko') {
        $images = [
            0 => '../images/darza.png',
            1 => '../images/virtuve.png',
            2 => '../images/majas.png'

        ];
        $images = collect($images);
    }


@endphp

<div class="sv-blank-spacer between-small-and-very-small hide-mobile"></div>

<simple-image-columns :columns="{{$extention->getColumns()}}" :images="{{ $images }}"></simple-image-columns>
