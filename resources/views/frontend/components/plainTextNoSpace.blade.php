@php
    $cssClass = $component->getData('isWide')
        ? 'col-xs-12'
        : 'col-md-10 col-md-push-1';
@endphp
<div class="sv-text-page sv-text-page-no-spacer">
    <div class="container">
        <div class="row">
            <div class="{{$cssClass}}">
                {!! $component->getData('content') !!}
            </div>
            {{-- {!! __("pages.plainTextContent.{$page->id}") !!} --}}
        </div>
    </div>
</div>
