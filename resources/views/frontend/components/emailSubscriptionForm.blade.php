@php
    $title             = $component->getData('title');
    $text              = $component->getData('text');
    $email             = 'E-pasta adrese';
    $nameAndSurname    = 'Vārds un uzvārds';
    $confirmationText  = $component->getData('confirmation_text');
    $btnText           = $component->getData('btn_text');

    $extention         = $component->getExtention();

    $extention->countPageView();
@endphp

<div class="sv-form" style="background-image: url('../images/rediss.png');">
    <div class="container">
        <div class="row">
            <div class="sv-form__text col-xs-12 col-md-6">
                <div class="sv-form__text__inner">
                    <h3 class="sv-form__text__title">{{ $title }}</h3>
                    <p class="sv-form__text__text">{{ $text }}</p>
                </div>
            </div>

            <form class="sv-form__form col-xs-12 col-md-6" method="POST" action="{{ route('api.subscribe-component-post') }}">
                @csrf
                <div class="sv-form__form__inner">
                    <div class="form-group">
                        <label for="email">{{ $email }}</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="">
                    </div>
                    <!-- Second Input -->
                    <div class="form-group">
                        <label for="name">{{ $nameAndSurname }}</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="">
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="agree" id="agreeCheck">
                        <label class="form-check-label" for="agreeCheck">{{ $confirmationText }}</label>
                    </div>
                    <!-- Button -->
                    <button type="submit" class="btn btn-purple">{{ $btnText }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
