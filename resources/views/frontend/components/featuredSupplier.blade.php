@if ($supplier = $frontController->getFeaturedSupplier())
    <div class="sv-blank-spacer small"></div>

    <div class="container sv-farmer-products">
        <div class="row">
            <div class="col col-md-1"> <div class="help--spacer"></div> </div>
            <div class="col col-md-4">
                <div class="card">
                    {{-- <img class="card-image" src="{{ $supplier->supplier->getImageByKey('supplier_image', 'main') }}" /> --}}
                    {{-- <img class="card-image" src="{{ $supplier->getImageByKey('featured_image') }}" /> --}}

                    <div class="card-image-ratio" />
                        <img loading="lazy" src="{{ $supplier->getImageByKey('featured_image') }}" alt="{{ __("supplier.name.{$supplier->supplier_id}") }}">
                    </div>

                    <div class="card-content">
                        <p class="card-label"> {{ ___("supplier.location.{$supplier->supplier_id}") }} </p>
                        <h3 class="card-title"> {{ __("supplier.name.{$supplier->supplier_id}") }} </h3>
                        <div class="card-paragraph">
                            {!! $supplier->meta['description'][language()] ?? '' !!}
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="btn-row btn-row--fill">
                            @php
                                $attrArr = [getSupplierSlugs(language()), __('supplier.slug.' . $supplier->supplier_id)];
                            @endphp
                            <a href="{{ r('supplier-products', ['supplier' => __('supplier.slug.' . $supplier->supplier_id)]) }}" class="btn btn-green"> Visi Produkti </a>
                            <a href="{{ r('supplierOpen', $attrArr) }}" class="btn btn-gray">{{__('translations.readMore')}}</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col col-md-6">
                <div class="products">
                    @foreach ($supplier->supplier->products()->take(4)->pluck('storage_amount', 'id')->toArray() as $itemId => $itemAmount)
                        @php
                            $item = $cache->getProduct($itemId);
                        @endphp
                        <div class="product-item">
                            @include('Products::frontend.listitem')
                        </div>
                    @endforeach
                </div>
            </div>


            {{-- <div class="col col-md-1"> <div class="help--spacer"></div> </div> --}}
        </div>


        {{-- <div class="sv-farmer-products d-none">
            <div class="sv-blank-spacer big"></div>
            <div class="row">
                <div class="intro">

                    <a href="{{ r('supplierOpen', [getSupplierSlugs(language()), __('supplier.slug.' . $supplier->supplier_id)]) }}"
                        class="farmer">
                        <img src="{{ $supplier->supplier->getImageByKey('supplier_image', 'main') }}" />
                        <h3>{{ __("supplier.name.{$supplier->supplier_id}") }}</h3>
                        <h4>{{ __("supplier.location.{$supplier->supplier_id}") }}</h4>
                    </a>

                    <h3><a
                            href="{{ r('supplierOpen', [getSupplierSlugs(language()), __('supplier.slug.' . $supplier->supplier_id)]) }}">{{ $supplier->meta['title'][language()] ?? '' }}</a>
                    </h3>
                    <p>
                        {!! $supplier->meta['description'][language()] ?? '' !!}
                    </p>
                </div>

                <div class="products">

                    <div class="sv-product-card-slider">
                        <div class="owl-carousel">
                            @foreach ($supplier->supplier->products()->pluck('storage_amount', 'id')->toArray() as $itemId => $itemAmount)
                                @php
                                    $item = $cache->getProduct($itemId);
                                @endphp
                                @include('Products::frontend.listitem')
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
            <div class="sv-blank-spacer medium"></div>
        </div>
        <div class="bg-static d-none">
            <div class="image"
                style="background-image: url({{ $supplier->getImageByKey('featured_image') }});"></div>
        </div> --}}
    </div>

    <div class="sv-blank-spacer small"></div>
@endif
