<!DOCTYPE html>

<html class="sv-lightbox-open_" lang="{{ $pageLanguage }}">

<head>
    <title>{{isset($meta['title']) ? 'Svaigi.lv | ' . $meta['title'] : 'Svaigi.lv'}}</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="google_description" content="{{isset($meta['description']) ?  $meta['description']['meta_value']: ''}}">
    <meta name="description" content="{{isset($meta['description']) ?  $meta['description']['meta_value']: ''}}">
    <meta name="google_keywords" content="{{isset($meta['keywords']) ? $meta['keywords']['meta_value'] : ''}}">
    <meta name="facebook-domain-verification" content="qays7lbtacexzaf928ry7qu10i0xc7">

    <meta name="verify-paysera" content="ab6f81bdd50b7d173b62492dffc33e86">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link rel="shortcut icon" href="{{ asset("assets/img/favicon.ico") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/jquery-ui.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/owl.carousel.min.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/font-awesome.min.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/animate.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/slinky.min.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/main.css") }}?ver={{ filemtime(public_path("assets/css/main.css")) }}">
    <link rel="stylesheet" href="{{ asset("assets/css/custom.css") }}?ver={{ filemtime(public_path("assets/css/custom.css")) }}">
    <link rel="stylesheet" href="{{ asset('assets/css/noty.css') }}">

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    @stack('css')

    @if (!config('app.debug', true))

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WXFW2HX');</script>
        <!-- End Google Tag Manager -->

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '436134863558527');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" src="https://www.facebook.com/tr?id=436134863558527&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel Code -->
        <!-- MailerLite Universal -->
        <script>
        (function(w,d,e,u,f,l,n){w[f]=w[f]||function(){(w[f].q=w[f].q||[])
        .push(arguments);},l=d.createElement(e),l.async=1,l.src=u,
        n=d.getElementsByTagName(e)[0],n.parentNode.insertBefore(l,n);})
        (window,document,'script','https://assets.mailerlite.com/js/universal.js','ml');
        ml('account', '297037');
        </script>
        <!-- End MailerLite Universal -->
    @endif
</head>

@php
    $isThankyouPage = request()->is(['*/thankyou', 'thankyou']);
    $isLogged = Auth::user() && Auth::user()->hasPassword() ? true : false;
    $isBerniSaimniekoPage = request()->is(['*/berni-saimnieko', 'berni-saimnieko']);
    $isBerniSaimniekoBlog = request()->route('blogCategory') === 'berni-saimnieko';
    $isMeistarklaseCat = request()->route('blogCategory') === 'meistarklase';
@endphp


<body class="
    sv-lightbox-open_
    {{!$isThankyouPage ? 'sv-has-marketday-dropdown' : ''}}
    {{ $isBerniSaimniekoPage ? 'page-berni-saimnieko' : '' }}
    {{ Auth::user() && Auth::user()->hasPassword()?" is-auth":"" }}
    "
>
    @if (!config('app.debug', true))
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WXFW2HX"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    @endif

    <div id="sv-app">

        @include("frontend.elements.cookies")
        @include("frontend.elements.banner")
        @include("frontend.elements.loginpopup")
        @include("frontend.elements.mobilemenu")

        {{-- @if (!$isThankyouPage)
            @include("frontend.elements.marketday")
        @endif --}}

        <div class="sv-dock" data-spy="affix" data-offset-top="90">
            <div class="container">
                @include("frontend.elements.desktopmenu")
            </div>
        </div>

        <div class="sv-dock--helper"></div>
        {{-- @include("frontend.elements.categorymenu") --}}

        @if (!$isBerniSaimniekoPage)
            @include("frontend.partials.pageTitle")
        @endif

        @include("frontend.partials.errors")
        @yield('content')
        @if (!$isBerniSaimniekoPage && !$isBerniSaimniekoBlog && !$isMeistarklaseCat)
            <div class="sv-blank-spacer medium"></div>
        @endif
        @include("frontend.elements.footer")

        @include("frontend.elements.copyright")
        {{-- @if (!$isLogged)
            <newsletter-subscriber-modal id="nl-modal"></newsletter-subscriber-modal>
        @endif --}}

    </div>



@stack('preScript')

<script>
    window.token = "{{ csrf_token() }}";
</script>

<script src="{{ asset("assets/js/jquery-3.1.1.min.js") }}"></script>
<script src="{{ asset("assets/js/bootstrap.min.js") }}"></script>
<script src="{{ asset("assets/js/jquery-ui.min.js") }}"></script>
<script src="{{ asset("assets/js/jquery.ui.touch-punch.min.js") }}"></script>
<script src="{{ asset("assets/js/owl.carousel.min.js") }}"></script>
<script src="{{ asset("assets/js/owl.animate.js") }}"></script>
<script src="{{ asset("assets/js/jquery.selectric.js") }}"></script>
<script src="{{ asset("assets/js/jquery.fileupload.js") }}?ver={{ filemtime(public_path("assets/js/jquery.fileupload.js")) }}"></script>
<script src="{{ asset("assets/js/imagesloaded.pkgd.min.js") }}"></script>
<script src="{{ asset("assets/js/slinky.min.js") }}"></script>
<script src="{{ asset("assets/js/custom.js") }}?ver={{ filemtime(public_path("assets/js/custom.js")) }}"></script>
<script src="{{ asset("assets/js/ajaxSetup.js") }}?ver={{ filemtime(public_path("assets/js/ajaxSetup.js")) }}"></script>
<script src="{{ asset("js/email-subscribe.js") }}"></script>
<script src="{{ asset("assets/js/noty.js") }}"></script>
<script src="{{ asset("assets/js/front.js") }}?ver={{ filemtime(public_path("assets/js/front.js")) }}"></script>
<script src="{{ asset('js/vue.js') }}?ver={{ filemtime(public_path("js/vue.js")) }}"></script>

@if(Auth::user() && Auth::user()->isAdmin)
    <script src="{{ asset('js/trans.js') }}?ver={{ filemtime(public_path("js/trans.js")) }}"></script>
@endif
@if(session()->has('openLogin'))
{{-- <script>
    $(document).ready(function() {
        if(!$('body').hasClass('sv-lightbox-open')) {
            $('.user > .toggle-sv-signin').click();
        }
    })
</script> --}}
@endif
@if(session('message'))
    @php
        $message = session('message');
    @endphp
    <script>
        jQuery(document).ready(function () {
            svaigi.showMessage("{{$message['msg']}}", '{{ ($message['isError']??false)?'error':'success' }}',500000);
        });
    </script>
@endif

@if(session('swal_message'))
    @php
        $message = session('swal_message');
    @endphp
    <script>
        jQuery(document).ready(function () {
            new swal({
                icon:  "{{ ($message['isError']??false)?'error':'success' }}",
                title: "{{$message['msg'] ?? 'Paldies'}}",
                text:  "{{$message['body'] ?? ''}}",
                confirmButtonText: "Aizvērt",
            });
        });
    </script>
@endif
@stack('scripts')
<!-- Start of Zendesk Widget script -->
{{-- <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=53101536-9f36-431f-9b0d-b8d58a577fa2"> </script> --}}
<!-- End of Zendesk Widget script -->
</body>

</html>
