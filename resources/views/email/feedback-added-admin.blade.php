@component("mail::message")

Ir pievienota jauna atsauksme:

<b>Norādītais epasts</b>: {{$feedback->email ?? 'nav'}} <br>
<b>Norādītais telefons</b>: {{$feedback->phone ?? 'nav'}} <br>
<b>Atsauksme:</b>
<p style="margin: 0px 0px 40px 40px; padding: 0px">
    {{$feedback->body}}
</p>

<br><br>

<a class="button button-green" href="{{ route('feedbacks.view', $feedback->id) }}">Atvērt</a>

@endcomponent
