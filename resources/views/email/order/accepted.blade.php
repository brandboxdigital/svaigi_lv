@component("mail::message")
Sveiki, {{$user->name}}!

{{-- Tavs pasūtījums <a href="{{ route('getInvoice', [$order->invoice]) }}">{{$order->order_number_string}}</a> virtuālajā tirgū ir apstradāts. --}}
Tavs pasūtījums ir apstiprināts un būs gatavs piegādei vai saņemšanai uz vietas.
Ielūkojies savā pasūtījuma <a href="{{ route('getInvoice', [$order->invoice]) }}">{{$order->order_number_string}}</a> rēķinā, varbūt nedaudz mainījies sveramo produktu svars, varbūt kāds no produktiem nav ticis piegādāts, un tāpēc tas no Tava pasūtījuma ir izņemts. Ja pasūtījumā kādas izmaiņas ir notikušas, tad būs mainījusies arī pasūtījuma kopsumma.

{{-- Reķinu tu vari aplūkot šeit:
@component('mail::button', [ 'url' => route('getInvoice', [$order->invoice]) ])
    Tavs Rēķins
@endcomponent --}}
{{-- Pirkuma kopsumma: {{ $totals->toPay }} EUR<br /><br /> --}}

Ja nepieciešams piemaksāt, tad to tu vari izdarīt ar pārskaitījumu uz kontu LV96HABA0551040281516 vai skaidrā naudā pasūtījuma saņemšanas brīdī.
Pārmaksas gadījumā naudu uz tavu kontu atgirezīsim 7 darba dienu laikā.

----

Informāciju par pirkumu skaties savā Svaigi.lv kontā <a href="{{ r('orderhistory') }}">Pirkumu vēsture</a>. Ja Tev vēl nav sava konta, tad <a href="{{ r('register') }}">reģistrē to</a> ar šo e-pasta adresi un seko līdzi pasūtījumu vēsturei.
@endcomponent
