{{-- Pirkuma kopsumma: {{ $totals->toPay }} EUR<br /><br /> --}}

@component("mail::message")
Sveiki, {{$user->name}}!


Tavs pasūtījums <a href="{{ route('getInvoice', [$order->invoice]) }}">{{$order->order_number_string}}</a> virtuālajā tirgū ir svaigi veikts, to nodosim saimniecībām produktu sagatavošanai: vākšanai, lasīšanai, griešanai, šmorēšanai un piegādei.

Izvēlētajā Tirgus dienā sagatavosim pasūtījumu, lai Tu saņemtu savu pasūtījumu ar piegādi uz mājām vai uz vietas pie mums. Apmaksu veic līdz pasūtījuma saņemšanas brīdim ar pārskaitījumu uz kontu LV96HABA0551040281516 vai skaidrā naudā pasūtījuma saņemšanas brīdī.

Vairāk informāciju par sava pasūtījuma saņemšanu un pasūtījuma apmaksu vari atrast mājas lapas sadaļā <a href="{{ r('url', ['ka-tas-notiek']) }}">Kā tas notiek?</a>.

Informāciju par pirkumu skaties savā Svaigi.lv kontā <a href="{{ r('orderhistory') }}">Pirkumu vēsture</a>. Ja Tev vēl nav sava konta, tad <a href="{{ r('register') }}">reģistrē to</a> ar šo e-pasta adresi un seko līdzi pasūtījumu vēsturei.`
@endcomponent
