@component("mail::message")
Sveiki, {{$user->name}}!

Mēs priecājamies Tevi redzēt Svaigi.lv - virtuālajā tirgū! Turpmāk katru nedēļu piedāvāsim iegādāties svaigus produktus no vietējiem saimniekiem un ražotājiem.

Mēs īpaši meklējam saimniecības un veicam produktu atlasi, lai tos piedāvātu mūsu pircējiem ikdienas maltītēm un svētku reizēm.

Apsolām personīgu produktu izvērtēšanu, godprātīgu darbu, nododot mūsu pašu laukos un pļavās audzēto, ievākto un atrasto tieši Tavās rokās. Lai gardi pirkumi!
@endcomponent
