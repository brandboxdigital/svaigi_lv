@component("mail::message")
Sveiciens!

Lūdzu apstipriniet epasta maiņu sekojot šim linkam: <a href="{{ r('verifyChangedEmail', ['emailchange', $verify->verifyString]) }}">Apstiprināt E-pasta adresi</a>

Ja Tev rodas jautājumi par pieeju savam Svaigi.lv kontam, droši dod mums ziņu!
@endcomponent
