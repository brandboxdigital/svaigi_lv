@php
    $routeName = $routeName??null;
@endphp

@if($routeName)
    <a href="{{ route($currentRoute.'.'.$routeName, array_merge(request()->route()->parameters??[], [$listItem->id])) }}"
        class="btn btn-xs btn-warning"><i class="fas fa-caret-square-right"></i></a>
@endif
