@extends('layouts.admin')

@php
// $orderable = Route::has($currentRoute . '.sort') ?: false;
// dd($currentRoute, Route::has($currentRoute . '.edit'));
@endphp


@section('content')

    @if ($filters ?? false)

        {{-- @include('admin.partials.filters') --}}

        {{-- @push('css')
            <link rel="stylesheet" href="{{ asset('css/daterangepicker.min.css') }}">
        @endpush --}}

        {{-- @push('scripts')
            <script src="{{ asset('js/moment.js') }}"></script>
            <script src="{{ asset('js/jquery.daterangepicker.min.js') }}"></script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('.daterangepicker').dateRangePicker({
                        startOfWeek: 'monday',
                        separator: ' ~ ',
                        format: 'DD.MM.YYYY HH:mm',
                        autoClose: false,
                        time: {
                            enabled: true
                        }
                    })
                })

            </script>
        @endpush --}}
    @endif

    <div class="row tableContent">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">{{ $header ?? '' }}</div>
                        <div class="col-md-6 text-right">
                            @if (Route::has($currentRoute . '.add'))
                                <a href="{{ route($currentRoute . '.add', request()->route()->parameters) }}"
                                    class="btn btn-primary btn-xs"><i class="fas fa-plus"></i></a>
                            @endif


                            @if ($massActions ?? false)
                                <div class="btn-group massActionGroup">
                                    @foreach ($massActions as $action)
                                        <a href="{{ $action->url }}" data-params="getCheckedRows"
                                            class="btn btn-xs massAction {{ $action->class ?? '' }} isAjax post"
                                            data-callback="{{ $action->callback ?? '' }}"
                                            title="{{ $action->label ?? '' }}">{{-- <i class="{{ $action->icon }}"></i> --}}{{ $action->label }}</a>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table table-hover">
                        <table class="table">
                            <tr>
                                <td style="width:20px;">
                                </td>
                                @foreach ($tableHeaders as $headerItem)
                                    @if ($headerItem['label'] == 'checkall')
                                        <th class="checkall">
                                            Select:<br />
                                            <a href="javascript:void(0)">All</a>
                                        </th>
                                    @else
                                        <th  style="vertical-align:top;">{{ $headerItem['label'] }}</th>
                                    @endif
                                @endforeach
                            </tr>
                        </table>

                        <tbody data-sortable>
                            @foreach ($list as $listItem)
                                @include('admin.elements.treeTableItem', ['listItem' => $listItem])
                            @endforeach
                        </tbody>

                        {{-- @if ($list instanceof Illuminate\Pagination\LengthAwarePaginator)
                            <tfoot>
                                <tr>
                                    <td colspan="{{ count($tableHeaders) }}">{{ $list->render() }}</td>
                                </tr>
                            </tfoot>
                        @endif --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    @foreach ($js ?? [] as $script)
        <script src="{{ asset($script) }}"></script>
    @endforeach
    <script>
        $('.modal').on('shown.bs.modal', function (e) {
            $('[data-toggle="tooltip"]').tooltip();
        })
    </script>
@endpush
@push('css')
    @foreach ($css ?? [] as $style)
        <link type="text/css" href="{{ asset($style) }}" />
    @endforeach
@endpush

@push('scripts')
        <script src="{{ asset('js/Sortable.min.js') }}"></script>
        {{-- <script>
            jQuery(document).ready(function() {
                new Sortable($('[data-sortable]')[0], {
                    handle: "td:first-child",
                    ghostClass: 'blue-background-class',
                    onEnd: function(evt) {
                        let sequence = [];
                        rows = $('[data-sortable]').find('tr').each(function(index, el, list) {
                            sequence.push($(el).data('id'));
                        });
                        console.log(sequence);
                        $.post("{{ route("$currentRoute.sort", request()->route()->parameters) }}",
                            "sequence=" + sequence.join(","),
                            function(response) {
                                if (response.status) {
                                    for (x in response.sequence) {
                                        $('[data-id=' + x + '] .seqTarget').text(response.sequence[
                                            x]);
                                    }
                                }
                            });
                    }

                });
            });

        </script> --}}
@endpush
