@php
    $hasSoftDeletes = method_exists($listItem, 'trashed');
    $collWidth = (100 / count($tableHeaders)) . "%";
@endphp

<table width="100%"><tr><td width="100%" style="padding: 0px">

<table width="100%">
<tr class="{{ ($hasSoftDeletes && $listItem->trashed()) || ($listItem->show_as_inactive ?? false) ?? false ? 'text-muted' : 'text-dark' }} {{ $listItem->rowClass ?? false }}"
    data-id="{{ $listItem->id ?? '' }}">
    @if (true)
        <td width="20px" style="border-left: 2px solid #e6e6f2;">
            {{-- <i class="fas fa-arrows-alt handle" style="cursor:grab;"></i> --}}
        </td>
    @endif

    @foreach ($tableHeaders as $headerItem)

        @if (($headerItem->field ?? ($headerItem['field'] ?? '')) == 'buttons')
            <td width="{{$collWidth}}" style="width:200px;" class="text-right {{ $headerItem['class'] ?? false }}">
                <div class="btn-group">
                    @foreach ($headerItem['buttons'] as $button)
                        @includeIf('admin.buttons.'.$button)
                    @endforeach
                </div>
            </td>
        @elseif(($headerItem->field??$headerItem['type']??"")=='yesno')
            <td width="{{$collWidth}}" {{ $headerItem['class'] ?? false ? 'class=' . $headerItem['class'] : '' }}>
                @if ($listItem->{$headerItem['field']})
                    <i class="fas fa-check-circle" style="color:green;"></i>
                @else
                    <i class="fas fa-window-close" style="color:darkred;"></i>
                @endif
            </td>
        @elseif(($headerItem->field??$headerItem['type']??"")=='tooltip')
            <td width="{{$collWidth}}"
                class="{{ $headerItem['class'] ?? false ? $headerItem['class'] : '' }} {{ $headerItem['classbyfield'] ?? false ? $listItem->{$headerItem['classbyfield']} ?? ($listItem[$headerItem['classbyfield']] ?? '') : '' }}">
                @php
                    $text = $listItem->{$headerItem['field']} ?? ($listItem[$headerItem['field']] ?? '');
                    $noHtml = strip_tags($text);
                @endphp

                @if (strlen($noHtml) > 40)
                    <div data-toggle="tooltip" data-placement="top" data-html="true" title="{!! $text !!}">
                        {!! substr($noHtml, 0, 40) !!}... </div>
                @else
                    {!! $text !!}
                @endif
            </td>
        @elseif($headerItem['translate']??false)
            @if ($headerItem['translate'] == 'array')
                <td width="{{$collWidth}}" {{ $headerItem['class'] ?? false ? 'class=' . $headerItem['class'] : '' }}>
                    {{ $listItem->{$headerItem['field']}[language()] ?? ($listItem[[$headerItem]['field']][language()] ?? '') }}
                </td>
            @else
                @if (!($listItem->{$headerItem['key']} ?? ($listItem[$headerItem['key']] ?? '')))
                    <td width="{{$collWidth}}" {{ $headerItem['class'] ?? false ? 'class=' . $headerItem['class'] : '' }}>
                    </td>
                @else
                    <td width="{{$collWidth}}" {{ $headerItem['class'] ?? false ? 'class=' . $headerItem['class'] : '' }}>
                        {{ __(implode('.', [$headerItem['translate'], $listItem->{$headerItem['key']} ?? ($listItem[$headerItem['key']] ?? '')])) }}
                    </td>
                @endif
            @endif
        @elseif($headerItem['fn']??false)
            <td {{ $headerItem['class'] ?? false ? 'class=' . $headerItem['class'] : '' }}>
                {!! $headerItem['fn']($listItem->{$headerItem['field']} ?? ($listItem[$headerItem['field']] ?? null), $headerItem['results']) ?? '' !!}</td>
        @else
            <td
                class="{{ $headerItem['class'] ?? false ? $headerItem['class'] : '' }} {{ $headerItem['classbyfield'] ?? false ? $listItem->{$headerItem['classbyfield']} ?? ($listItem[$headerItem['classbyfield']] ?? '') : '' }}">
                {!! $listItem->{$headerItem['field']} ?? ($listItem[$headerItem['field']] ?? '') !!}</td>
        @endif

    @endforeach
</tr>
@if ($listItem->children->count() > 0)
    <tr class="children">
        <td style="padding: 0px 0 50px 20px; margin: 0 0 30px; border-top: none; border-left: 2px solid #e6e6f2;" colspan="{{count($tableHeaders) + 1}}">
            @foreach ($listItem->children as $item)
            @include('admin.elements.treeTableItem', ['listItem' => $item])
            @endforeach
        </td>
    </tr>
@endif
</table>

<tr></td></table>
