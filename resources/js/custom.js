$(document).ready(function () {
    // Show Legal Forms
    $('#is_legal').change(function (e) {
        if ($(this).is(":checked")) {
            $('#legalform').removeClass('hidden');
            return true;
        }
        $('#legalform').addClass('hidden');
    });

    jQuery(document).keyup(function (e) {
        if (e.keyCode === 27 && jQuery('body').hasClass('sv-lightbox-open')) {
            jQuery('div.sv-lightbox-open .sv-close').click();
        }
    });

    jQuery('.reportClose').click(function (e) {
        e.preventDefault();
        console.log($(this).data('url'));
        jQuery.post($(this).data('url'));
        let el = $(this).parents('.sv-message');
        if (el) {
            el.hide('blind', {}, 500, function () {
                el.remove();
            });
        }
        return false;
    })

    addToCart();

    $('.sv-close:not(.toggle-sv-signin)').click(function (e) {
        e.preventDefault();
        $(this).parents('.sv-message').remove();
        return false;
    });

    $('#footer-newsletter-form').on("submit",async function(e){
        e.preventDefault();
        const data = {email: $('#copy-right-newsletter').val(), personasDatuApstrade: true};
        const response = await fetch('/api/subscribe-newsletter', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },
            body: JSON.stringify(data),
        })
        .then(response => response.json())
        .then(data => {
            if (data.errors) {
                $('#error').removeClass("hide");
                $('#success').addClass("hide");
                $('#error').text(data.errors.email[0]);
            } else {
                $('#error').addClass("hide");
                $('#copy-right-newsletter').val("");
                $('#success').removeClass("hide");
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    })
});


function addToCart() {
    console.log('Called::addToCart');

    $(document).on('submit', '.sv-product-card form, .sv-product-title form[data-cart-form]', function (e) {
        e.preventDefault();
        var serialized = $(this).serialize()
        var data = $(this).serializeArray().reduce(function(all, item) {
            all[item.name] = item.value;
            return all;
        },{});

        $.post($(this).attr('action'), $(this).serialize(), function (response) {
            if (response.status) {
                // Swal.fire({
                //     text: "Pievienots Grozam",
                //     icon: 'success',
                // });
                console.log(response);
                showCartAlert(data);
                showCartTotals(response);
                return false;
            }
            // alert(response.message);
            return false;
        });
        return false;
    });

    $('form[data-fav-form]').unbind().submit(function (e) {
        e.preventDefault();

        var form       = $(this);
        var action     = $(this).attr('action');
        var serialized = $(this).serialize();

        $.post(action, serialized, function (response) {
            if (response.status) {
                var reviewModel = response.data;
                var conf = {
                    html: response.message,
                    icon: 'success',
                    confirmButtonText: 'Labi',
                }

                Swal.fire(conf);

                if (response.newButtonText) {
                    form.find('button.btn.btn-outlined').first().text(response.newButtonText);
                }

                return false;
            }
            // alert(response.message);
            return false;
        });
        return false;
    });
    $('form[data-rate-form]').unbind().submit(function (e) {
        e.preventDefault();
        var action        = $(this).attr('action');
        var serialized = $(this).serialize();
        var data = $(this).serializeArray().reduce(function(all, item) {
            all[item.name] = item.value;
            return all;
        },{});

        $.post(action, serialized, function (response) {
            if (response.status) {
                var reviewModel = response.data;
                var conf = {
                    html: response.message,
                    icon: 'success',
                    confirmButtonText: 'Labi',
                }

                if (response.collectReview) {
                    conf['input'] = 'text';
                    conf['showCancelButton'] = true;
                    conf['showLoaderOnConfirm'] = true;
                    conf['cancelButtonText'] = 'Aizvērt';

                    conf['preConfirm'] = function(input) {
                        var formData = new FormData();
                        formData.append('id', reviewModel.id);
                        formData.append('review', input);
                        formData.append('_token', data._token);

                        return fetch(action,{
                            method: 'POST', // or 'PUT'
                            body: formData,
                        })
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)
                                }
                                return response;
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                    `Request failed: ${error}`
                                )
                            })
                    };
                }

                Swal.fire(conf);
                return false;
            }
            // alert(response.message);
            return false;
        });
        return false;
    });
}

function showCartTotals(response) {
    $('.minicart-totals').text(response.contents.cartTotals.toPay + " €");
    $('.cart').addClass('has-items');
}
