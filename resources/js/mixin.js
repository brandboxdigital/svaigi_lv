export default {
    methods: {
        async ajaxPost(url, payload) {
            return await fetch(url, {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            })
            .then((response) => response.json())
            .then((data) => data);
        },
        async ajaxGet(url) {
            return await fetch(url)
                .then((response) => response.json())
                .then((data) => data);
        }
    }
}
