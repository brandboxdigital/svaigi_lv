import VueTreeNavigation from 'vue-tree-navigation';
import VueCookie from 'vue-cookie';
import VModal from 'vue-js-modal';
import Vuex from 'vuex';
import storeData from "./store/index";
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import VueRouter from 'vue-router';

window.Vue = require('vue');


Vue.use(VueTreeNavigation);
Vue.use(VModal);
Vue.use(VueCookie);
Vue.use(Vuex);
Vue.use(Loading);
Vue.use(VueRouter);

Vue.component('review-banner',                require('./components/ReviewBanner.vue').default);
Vue.component('review-submit',                require('./components/ReviewSubmit.vue').default);
Vue.component('simple-image-columns',         require('./components/SimpleImageColumns.vue').default);
Vue.component('accordion',                    require('./components/Accordion.vue').default);


Vue.component('mobile-side-menu',             require('./components/MobileSideMenu.vue').default);
Vue.component('newsletter-subscriber-modal',  require('./components/NewsletterSubscriberModal.vue').default);
Vue.component('products-side-panel',          require('./components/ProductsSidePanel.vue').default);

Vue.component('category-view',                require('./components/CategoryView.vue').default);
Vue.component('products-data',                require('./components/ProductsData.vue').default);

Vue.component('delivery-providers',           require('./components/DeliveryProviders.vue').default);

//Delivery provider modules
Vue.component('izipizi-courier',              require('./components/DeliveryProviderList/IzipiziCourier/js/izipizi-courier.vue').default);
Vue.component('omniva-parcel',                require('./components/DeliveryProviderList/OmnivaParcel/js/omniva-parcel.vue').default);


const store = new Vuex.Store(storeData);
const router = new VueRouter({
    mode: 'history',
});

window.app = new Vue({
    el: '#sv-app',
    data: {
        // showNewsletterModal: false
    },
    methods: {
        updateDelivery(delivery) {
            if (delivery) {
                this.$root.$emit('update-delivery', delivery)
            } else {}
        }
    },
    store,
    router
});
