import BootstrapVue from 'bootstrap-vue';

window.Vue = require('vue');

Vue.use(BootstrapVue);

window.app = new Vue({
    el: '#sv-admin-app',
});
