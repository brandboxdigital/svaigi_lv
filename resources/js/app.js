window.$ = window.jQuery = require('./jquery.min');

require('./jquery.slimscroll');

// select2 = require('./select2.min');

require('./jquery.inputmask.bundle');
require('./jquery.repeater.min.js');

/**
 * npm packages
 */
require('infinite-scroll');


Noty = require('./noty');
svaigi = require('./svaigi');

require('./main');


/**
 * Import Modules
 *
 * Code parts used by page divided by usage
 */
require('./modules/infinite-scroll.js');
