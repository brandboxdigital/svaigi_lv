$('#subscribe-form').submit(function(e) {
    var $form = $(this);
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        dataType: "json"
    }).done(function(data) {
        $("#message-block").removeClass("error");
        $("#message-block").text(data.message);
    }).fail(function(data) {
        $("#message-block").addClass("error");
        $("#message-block").text(data.responseJSON.message);
    });

    e.preventDefault(); 
});