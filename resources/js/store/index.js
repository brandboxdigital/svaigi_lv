export default {
    state: {
        marketDays: [],
        mainNavigation: [],
        products: [],
        paginator: [],
        selectedMarketDay: '',
        breadcrumbs: [],
        category: [],
        slugCategory: [],
        landingCategory: null,
        landingCategoryBreadcrumbs: null,
        landingCategoryProducts: null,
        suppliers: [],
        pagination: [],
        suppliersPagination: [],
        currentRootCategory: null
    },
    mutations: {
        setCurrentRootCategory(state, data) {
            state.currentRootCategory = data;
        },
        setMarketDays(state, marketDays, selectedMarketDay)  {
            state.marketDays = marketDays;
            state.selectedMarketDay = selectedMarketDay;
        },
        setMainNavigation(state, menu) {
            state.mainNavigation = menu;
        },
        setProducts: (state, products) => {
            state.products = products;
        },
        setProductsSearch: (state, products) => {
            state.landingCategoryProducts = null;
            state.products = products;
        },
        setCategory: (state, category) => {
            state.category = category;
        },
        setSuppliers: (state, suppliers) => {
            state.suppliers = suppliers;
        },
        setSlugCategory: (state, slugCategory) => {
            state.slugCategory = slugCategory;
        },
        setLandingCategory: (state, landingCategory) => {
            state.landingCategory = landingCategory;
        },
        setLandingCategoryProducts: (state, landingCategoryProducts) => {
            state.landingCategoryProducts = landingCategoryProducts ? landingCategoryProducts.products : null;
        },
        setBreadcrumbs: (state, breadcrumbs) => {
            state.breadcrumbs = breadcrumbs;
        },
        setLandingCategoryBreadcrumbs: (state, landingCategoryBreadcrumbs) => {
            state.landingCategoryBreadcrumbs = landingCategoryBreadcrumbs;
        },
        setPagination: (state, pagination) => {
            state.pagination = pagination;
        },
        setSuppliersPagination: (state, pagination) => {
            state.suppliersPagination = pagination;
        },
    },
    actions: {
        async setCurrentRootCategory(context, slug) {
            const response = await fetch(`/api/get-root-category/${slug}`)
            .then(response => response.json())
            .then(data => data);
            context.commit('setCurrentRootCategory', response.rootCategory);
        },
        async setCategoryViewData(context) {
            const response = await fetch('/api/show-category-data')
            .then(response => response.json())
            .then(data => data);
            if(response) {
                context.commit('setMarketDays', response.marketDays, response.selectedMarketDay);
                context.commit('setMainNavigation', response.menu);
            }
        },
        async updatePagination(context, url) {
            const response = await fetch(url)
            .then(response => response.json())
            .then(data => data);

            if(response) {
                const productsPagination = response.productsAndPagination.pagination;
                const landingProductsPagination = response.landingCategoryProducts.pagination;
                context.commit('setPagination', productsPagination !== null ? productsPagination : landingProductsPagination);
            }

            return response;
        },
        async updateSuppliersPagination(context, url) {
            const response = await fetch(url)
            .then(response => response.json())
            .then(data => data);

            if(response) {
                context.commit('setSuppliersPagination', response);
            }

            return response;
        },
        async getProductsData(context, data) {
            const slug = data.slug;
            let query = '';
            if (Object.keys(data.query).length > 0) {
                query += '?';
                for (const [key, value] of Object.entries(data.query)) {
                    query += key;
                    query += '=';
                    query += value;
                    query += '&';
                }
            }

            const response = await fetch('/api/get-products/' + slug + query)
            .then(response => response.json())
            .then(data => data);

            if(response) {
                const productsPagination = response.productsAndPagination.pagination;
                const landingProductsPagination = response.landingCategoryProducts.pagination;

                context.commit('setPagination', productsPagination !== null ? productsPagination : landingProductsPagination);
                context.commit('setProducts', response.productsAndPagination.products);
                context.commit('setBreadcrumbs', response.breadcrumbs);
                context.commit('setCategory', response.categoryArray);
                context.commit('setSlugCategory', response.slugCategoryArray);
                context.commit('setLandingCategory', response.landingCategory);
                context.commit('setLandingCategoryProducts', response.landingCategoryProducts);
                context.commit('setLandingCategoryBreadcrumbs', response.landingCategoryBreadcrumbs);
            }

            return response;
        },
        async getSuppliers(context, data) {
            let query = '';
            if (Object.keys(data.query).length > 0) {
                query += '?';
                for (const [key, value] of Object.entries(data.query)) {
                    query += key;
                    query += '=';
                    query += value;
                    query += '&';
                }
            }
            const response = await fetch(`/api/get-suppliers/${data.slug}` + query)
            .then(response => response.json())
            .then(data => data);

            if(response) {
                context.commit('setSuppliers', response);
                context.commit('setSuppliersPagination', response);
            }

            return response;
        },
        setSearchProductsData(context, data) {
            context.commit('setProductsSearch', data.products);
        }
    }
}
