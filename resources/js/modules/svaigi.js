var winScrollTop = 0;


jQuery(window).scroll(function () {

    winScrollTop = jQuery(this).scrollTop();

    offsetYGlobal = window.pageYOffset;

    parallax();

});

jQuery(window).resize(function () {

    dropdowns();
    mobileslidernav();
    mobilemenuscroll();

});


jQuery(document).ready(function () {

    itemonscreen();
    dropdowns();
    parallax();
    carousels();
    forms();
    animatedisplaynone();
    marketdaydropdown();
    lightbox();
    mobilemenuscroll();

    if (jQuery('#farmersmap-canvas').length > 0) {
        farmersmap();
    }
    else if (jQuery('#contactsmap-canvas').length > 0) {
        contactsmap();
    }

    // ==============================================================
    // stars
    // ==============================================================
    if ($('[data-rate-form]').length) {
        console.log('aa');
        $('[data-rate-form] input[type=radio]').on('change', function (event) {
            // console.log($(this).val());
            // console.log($(this), $(this).parents('form').first());
            $(this).parents('form').first().submit();
        })
    }

    $(document).on('click', '[data-scroll-top]', function() {
        $("html, body").stop().animate({scrollTop:0}, 500, 'swing');
    })
});


jQuery(window).on('load', function () {

    if (jQuery(window).width() > 998) {

        // var sidebarheight = jQuery('.sv-cart .sidebar').height();
        //
        // jQuery('.sv-cart .list').css('min-height', sidebarheight);

        var $sticky = jQuery('.sticky');
        var $stickyrStopper = jQuery('.sticky-stopper');

        if (!!$sticky.offset()) {

            var windowTop = jQuery(window).scrollTop();

            var generalSidebarHeight = $sticky.height();
            var stickyTop = $sticky.offset().top;
            var stickOffset = 80;
            var stickyStopperPosition = $stickyrStopper.offset().top;
            var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset - stickOffset;
            var diff = stickyStopperPosition - stopPoint;

            var diff2 = stickyStopperPosition - stickyTop - stickOffset;

            console.log(diff);

            if (diff2 < generalSidebarHeight) {
                $sticky.css({ position: 'relative', top: 'initial', right: '0', width: 'calc(33.32% - 30px)' });
            }

            jQuery(window).scroll(function () {

                var windowTop = jQuery(window).scrollTop();

                if (stopPoint < windowTop) {
                    $sticky.css({ position: 'absolute', bottom: 0, top: 'auto' });
                }
                else if (stickyTop < windowTop + stickOffset) {
                    $sticky.css({ position: 'fixed', top: stickOffset });
                }
                else {
                    $sticky.css({ position: 'absolute', bottom: 'auto', top: 'initial' });
                }

                if (diff2 < generalSidebarHeight) {
                    $sticky.css({ position: 'relative', bottom: 'auto', top: 'initial' });
                }

            });

        }

    }

});


window.itemonscreen = function() {

    winScrollTop = jQuery(this).scrollTop();

    jQuery.fn.isOnScreen = function () {

        var win = jQuery(window);
        var viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft()
        };

        viewport.bottom = viewport.top + win.height();
        var bounds = this.offset();
        bounds.bottom = bounds.top + this.outerHeight();
        return (!(viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    };

}


window.dropdowns = function() {

    if ($(window).width() > 992) {

        jQuery('.sv-dock .cart, .sv-products-menu .cart').on('mouseenter', function () {
            clearTimeout(window.timeout);
            jQuery(this).find('.sv-dropdown').addClass('is-dom');
            jQuery(this).addClass('is-active');
            var drop = jQuery(this).find('.sv-dropdown');
            setTimeout(function () {
                drop.addClass('is-visible');
            }, 10);
        });

        jQuery('.sv-dock .cart, .sv-products-menu .cart').on('mouseleave', function () {
            jQuery(this).find('.sv-dropdown').removeClass('is-visible');
            var drop = jQuery(this).find('.sv-dropdown');
            jQuery(this).removeClass('is-active');
            window.timeout = setTimeout(function () {
                drop.removeClass('is-dom');
            }, 350);
        });

        jQuery('.sv-dock .user').on('mouseenter', function () {
            clearTimeout(window.timeout);
            jQuery(this).find('.sv-dropdown').addClass('is-dom');
            jQuery(this).addClass('is-active');
            var drop = jQuery(this).find('.sv-dropdown');
            setTimeout(function () {
                drop.addClass('is-visible');
            }, 10);
        });

        jQuery('.sv-dock .user').on('mouseleave', function () {
            jQuery(this).find('.sv-dropdown').removeClass('is-visible');
            var drop = jQuery(this).find('.sv-dropdown');
            jQuery(this).removeClass('is-active');
            window.timeout = setTimeout(function () {
                drop.removeClass('is-dom');
            }, 350);
        });

        // delayed hover

        var timer;
        jQuery('.sv-products-menu .sv-default-dropdown-toggle, .sv-dock .sv-default-dropdown-toggle').mouseenter(function () {
            // console.log('Wat!!');
            var element = jQuery(this);

            timer = setTimeout(function () {

                clearTimeout(window.timeout);
                element.find('.sv-dropdown').removeClass('is-dom').addClass('is-dom');
                element.addClass('is-active');
                var drop = element.find('.sv-dropdown');
                setTimeout(function () {
                    drop.addClass('is-visible');
                }, 10);

                // console.log('DELAYED HOVER');

            }, 200)

        }).mouseleave(function () {
            // console.log('Out!!');
            clearTimeout(timer);

            jQuery(this).find('.sv-dropdown').removeClass('is-visible');
            var drop = jQuery(this).find('.sv-dropdown');
            jQuery(this).removeClass('is-active');
            window.timeout = setTimeout(function () {
                drop.removeClass('is-dom');
            }, 350);

        });

        // jQuery('.sv-products-menu .sv-default-dropdown-toggle').on('mouseenter', function() {
        //   clearTimeout(window.timeout);
        //   jQuery(this).find('.sv-dropdown').removeClass('is-dom').addClass('is-dom');
        //   jQuery(this).addClass('is-active');
        //   var drop = jQuery(this).find('.sv-dropdown');
        //   setTimeout(function() {
        //     drop.addClass('is-visible');
        //   }, 10);
        // });
        //
        // jQuery('.sv-products-menu .sv-default-dropdown-toggle').on('mouseleave', function() {
        //   jQuery(this).find('.sv-dropdown').removeClass('is-visible');
        //   var drop = jQuery(this).find('.sv-dropdown');
        //   jQuery(this).removeClass('is-active');
        //   window.timeout = setTimeout(function() {
        //     drop.removeClass('is-dom');
        //   }, 350);
        // });

    }

    var productsdropdown = 280;
    var windowwidth = jQuery(window).width();
    var productsmenu = jQuery('.sv-products-menu .sv-default-dropdown-toggle');

    if (productsmenu.length > 1) {
        var productsmenuleft = productsmenu.offset().left;

        if (windowwidth - productsmenuleft - productsdropdown < 0) {
            jQuery('.sv-products-menu .sv-dropdown').css({
                "left": "auto",
                "right": "0"
            })
        }
    }


}


window.parallax = function() {

    var scrolled = jQuery(window).scrollTop();

    jQuery('.bg-parallax').each(function () {
        // if (jQuery(this).isOnScreen()) {
        //     var firstTop = jQuery(this).offset().top;
        //     var block1 = jQuery(this).find(".image");
        //     var block2 = jQuery(this).find("video");
        //     var block3 = jQuery(this).find(".slow");
        //     var moveTop = (firstTop - winScrollTop) * 0.3 //speed;
        //     var moveTop2 = (firstTop - winScrollTop) * 0.6 //speed;
        //     var moveTop3 = (firstTop - winScrollTop - 200) * 0.1 //speed;
        //     block1.css("transform", "translateY(" + -moveTop + "px)");
        //     block2.css("transform", "translate(" + -50 + "%, " + -moveTop2 + "px)");
        //     block3.css("transform", "translateY(" + -moveTop3 + "px)");
        // }
    });

}


window.carousels = function() {

    var owl = jQuery('.sv-product-card-slider .owl-carousel');
    owl.on('initialized.owl.carousel', function (e) {
        mobileslidernav();
    }).owlCarousel({
        autoWidth: false,
        loop: true,
        nav: true,
        rewind: false,
        autoplay: false,
        dots: false,
        mouseDrag: false,
        smartSpeed: 250,
        margin: 10,
        callbacks: true,
        items: 4,
        navText: [
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M16.929,25.657l1.414,1.414,0.157-.157L29.657,38.071l1.414-1.414L19.914,25.5,31.071,14.343l-1.414-1.414L18.5,24.086l-0.157-.157-1.414,1.414L17.086,25.5Z' /></svg>",
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M33.071,25.657l-1.414,1.414L31.5,26.914,20.343,38.071l-1.414-1.414L30.086,25.5,18.929,14.343l1.414-1.414L31.5,24.086l0.157-.157,1.414,1.414-0.157.157Z' /></svg>"
        ],
        responsive: {
            0: {
                items: 1,
                slideBy: 1,
                margin: 15
            },
            750: {
                items: 3,
                slideBy: 3
            },
            1016: {
                items: 4,
                slideBy: 4
            },
            1246: {
                items: 4,
                slideBy: 5
            }
        }
    });

    var owl = jQuery('.sv-linked-products-slider .owl-carousel');
    owl.on('initialized.owl.carousel', function (e) {
        mobileslidernav();
    }).owlCarousel({
        autoWidth: false,
        loop: true,
        nav: true,
        rewind: false,
        autoplay: false,
        dots: false,
        mouseDrag: false,
        smartSpeed: 250,
        margin: 30,
        callbacks: true,
        navText: [
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M16.929,25.657l1.414,1.414,0.157-.157L29.657,38.071l1.414-1.414L19.914,25.5,31.071,14.343l-1.414-1.414L18.5,24.086l-0.157-.157-1.414,1.414L17.086,25.5Z' /></svg>",
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M33.071,25.657l-1.414,1.414L31.5,26.914,20.343,38.071l-1.414-1.414L30.086,25.5,18.929,14.343l1.414-1.414L31.5,24.086l0.157-.157,1.414,1.414-0.157.157Z' /></svg>"
        ],
        responsive: {
            0: {
                items: 1,
                slideBy: 2,
                margin: 15
            },
            750: {
                items: 3,
                slideBy: 1
            },
            1016: {
                items: 4,
                slideBy: 1
            },
            1246: {
                items: 4,
                slideBy: 1
            },
            1470: {
                items: 4,
                slideBy: 1
            }
        }
    });

    var owl = jQuery('.sv-blog-list-slider .owl-carousel');
    owl.on('initialized.owl.carousel', function (e) {
        mobileslidernav();
    }).owlCarousel({
        autoWidth: false,
        loop: true,
        nav: true,
        rewind: false,
        autoplay: false,
        dots: false,
        mouseDrag: false,
        smartSpeed: 250,
        margin: 30,
        callbacks: true,
        navText: [
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M16.929,25.657l1.414,1.414,0.157-.157L29.657,38.071l1.414-1.414L19.914,25.5,31.071,14.343l-1.414-1.414L18.5,24.086l-0.157-.157-1.414,1.414L17.086,25.5Z' /></svg>",
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M33.071,25.657l-1.414,1.414L31.5,26.914,20.343,38.071l-1.414-1.414L30.086,25.5,18.929,14.343l1.414-1.414L31.5,24.086l0.157-.157,1.414,1.414-0.157.157Z' /></svg>"
        ],
        responsive: {
            0: {
                items: 1,
                slideBy: 1,
            },
            750: {
                items: 2,
                slideBy: 2
            },
            1016: {
                items: 3,
                slideBy: 3
            },
            1246: {
                items: 3,
                slideBy: 3
            }
        }
    });

    var owl = jQuery('.sv-review-banner.owl-carousel');
    owl.on('initialized.owl.carousel', function (e) {
        // mobileslidernav();
    }).owlCarousel({
        autoWidth: false,
        loop: true,
        nav: true,
        rewind: false,
        autoplay: true,
        dots: true,
        mouseDrag: true,
        smartSpeed: 250,
        margin: 0,
        callbacks: true,
        items: 1,
        slideBy: 1,
        navText: [
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M16.929,25.657l1.414,1.414,0.157-.157L29.657,38.071l1.414-1.414L19.914,25.5,31.071,14.343l-1.414-1.414L18.5,24.086l-0.157-.157-1.414,1.414L17.086,25.5Z' /></svg>",
            "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M33.071,25.657l-1.414,1.414L31.5,26.914,20.343,38.071l-1.414-1.414L30.086,25.5,18.929,14.343l1.414-1.414L31.5,24.086l0.157-.157,1.414,1.414-0.157.157Z' /></svg>"
        ],

    });

    var owl = jQuery('.sv-carousel-banner .owl-carousel');
    owl.on('initialized.owl.carousel', function (e) {
        // mobileslidernav();
    }).owlCarousel({
        autoWidth: false,
        loop: true,
        nav: true,
        rewind: false,
        autoplay: true,
        dots: true,
        mouseDrag: true,
        smartSpeed: 250,
        margin: 0,
        callbacks: true,
        items: 1,
        slideBy: 1,
        animateOut: 'fadeOut',
        autoHeight:true,
        // animateIn: 'fadeIn',
        navText: [
            '<svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="46" height="46" rx="2" fill="#F1F3F4"/><path d="M21.6667 27.6666L17 23M17 23L21.6667 18.3333M17 23H29" stroke="#09090F" stroke-width="2"/></svg>',
            '<svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="46" height="46" rx="2" fill="#F1F3F4"/><path d="M24.3333 18.3333L29 23M29 23L24.3333 27.6666M29 23H17" stroke="#09090F" stroke-width="2"/></svg>',
        ],

    });

    var owl = jQuery('.blog-gallery-fullpage-slider .owl-carousel');
    owl.on('initialized.owl.carousel', function (e) {
        // mobileslidernav();
    }).owlCarousel({
        autoWidth: false,
        loop: true,
        nav: true,
        rewind: false,
        autoplay: true,
        dots: false,
        mouseDrag: true,
        smartSpeed: 250,
        margin: 0,
        callbacks: true,
        items: 1,
        slideBy: 1,
        // animateOut: 'fadeOut',
        autoHeight:true,
        // animateIn: 'fadeIn',
        navText: ["<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M16.929,25.657l1.414,1.414,0.157-.157L29.657,38.071l1.414-1.414L19.914,25.5,31.071,14.343l-1.414-1.414L18.5,24.086l-0.157-.157-1.414,1.414L17.086,25.5Z' /></svg>", "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M33.071,25.657l-1.414,1.414L31.5,26.914,20.343,38.071l-1.414-1.414L30.086,25.5,18.929,14.343l1.414-1.414L31.5,24.086l0.157-.157,1.414,1.414-0.157.157Z' /></svg>"],
    });

    if ($(window).width() > 992) {
        var owl = jQuery('.sv-blog-posts-slider .owl-carousel');
        owl.on('initialized.owl.carousel', function (e) {
            // mobileslidernav();
        }).owlCarousel({
            autoWidth: false,
            loop: true,
            nav: true,
            rewind: false,
            autoplay: false,
            dots: false,
            mouseDrag: false,
            smartSpeed: 250,
            margin: 15,
            callbacks: true,
            items: 3,
            navText: ["<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M16.929,25.657l1.414,1.414,0.157-.157L29.657,38.071l1.414-1.414L19.914,25.5,31.071,14.343l-1.414-1.414L18.5,24.086l-0.157-.157-1.414,1.414L17.086,25.5Z' /></svg>", "<svg xmlns='http://www.w3.org/2000/svg' width='50' height='50' viewBox='0 0 50 50'><path class='bg' d='M-0.000,0.000 L50.000,0.000 L50.000,50.000 L-0.000,50.000 L-0.000,0.000 Z' /><path class='arrow' d='M33.071,25.657l-1.414,1.414L31.5,26.914,20.343,38.071l-1.414-1.414L30.086,25.5,18.929,14.343l1.414-1.414L31.5,24.086l0.157-.157,1.414,1.414-0.157.157Z' /></svg>"],
            responsive: {
            0: {
                items: 1,
                slideBy: 1,
                margin: 15
            },
            750: {
                items: 3,
                slideBy: 1,
                margin: 30
            },
            1016: {
                items: 3,
                slideBy: 1,
                margin: 30
            },
            1246: {
                items: 3,
                slideBy: 1,
                margin: 30
            }
            }
        });
    }

    
}


window.forms = function() {

    _initSelectics();

    jQuery('select').on('selectric-change', function (event, element, selectric) {
        jQuery(element).val(jQuery(element).selectric().val());
    });

    jQuery(':checkbox').on('click', function () {
        jQuery(this).parent().toggleClass('checked');
    });

    jQuery(function () {
        jQuery('.add').on('click', function () {
            var $qty = jQuery(this).closest('.quantity').find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
            }
        });
        jQuery('.minus').on('click', function () {
            var $qty = jQuery(this).closest('.quantity').find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
            }
        });
    });

    jQuery('.sv-cart-tabs .tab').on('click', function () {
        jQuery('.sv-cart-tabs .tab').removeClass('active');
        jQuery(this).addClass('active');
    });

    jQuery('#spinner, .spinner').spinner({
        min: 1,
        max: 5,
        step: 1,
        numberFormat: "n",
    });

    if (jQuery('.spinner').val() < 2) {
        jQuery('.ui-spinner-down').css('background', '#e1e1e1');
    }

    jQuery('.ui-spinner-button').click(function () {
        if (jQuery('.spinner').val() < 2) {
            jQuery('.ui-spinner-down').css('background', '#e1e1e1');
        }
        else {
            jQuery('.ui-spinner-down').css('background', '#1f9363');
        }
        if (jQuery('.spinner').val() > 4) {
            jQuery('.ui-spinner-up').css('background', '#e1e1e1');
        }
        else {
            jQuery('.ui-spinner-up').css('background', '#1f9363');
        }
    });

    jQuery('.coupon .enter').click(function () {
        jQuery('.coupon .nr').focus();
        if (jQuery('.coupon .nr').is(":focus")) {
            jQuery('.coupon').addClass('has-focus');
        }
        // setTimeout(function () {
        //   jQuery('.coupon .nr').css('width', '100%');
        // }, 350);
    });

    jQuery('.coupon .nr').blur(function () {
        jQuery('.coupon').removeClass('has-focus');
        // jQuery('.coupon .nr').css('width', 'auto');
    });

}

window._initSelectics = function () {
    jQuery('select').each(function(i, select) {
        const $select = jQuery(select);

        if ($select.data('selectric')) {
            //already exist and dont reinit!
        } else if (select.id !== 'sort-select') {
            $select.selectric({
                maxHeight: 300,
                disableOnMobile: false,
                nativeOnMobile: false,
                optionsItemBuilder: function (itemData, element, index) {
                    var appendHtml = $(itemData.element[0]).data('appendhtml') ? $(itemData.element[0]).data('appendhtml') : '';

                    if ($(itemData.element[0]).data('wrap')) {
                        return "<h3>" + ($(itemData.element[0]).data('origprice') ? "<s>" + $(itemData.element[0]).data('origprice') + "</s>" + itemData.text : itemData.text) + "</h3>" + appendHtml;
                    }
                    return ($(itemData.element[0]).data('origprice') ? "<s>" + $(itemData.element[0]).data('origprice') + "</s>" + itemData.text : itemData.text) + appendHtml;
                },
                labelBuilder: function (itemData) {
                    var appendHtml = $(itemData.element[0]).data('appendhtml') ? $(itemData.element[0]).data('appendhtml') : '';

                    if ($(itemData.element[0]).data('wrap')) {
                        return "<h3>" + ($(itemData.element[0]).data('origprice') ? "<s>" + $(itemData.element[0]).data('origprice') + "</s>" + itemData.text : itemData.text) + "</h3>" + appendHtml;
                    }
                    return ($(itemData.element[0]).data('origprice') ? "<s>" + $(itemData.element[0]).data('origprice') + "</s>" + itemData.text : itemData.text) + appendHtml;
                }
            })
        }
    })

}


window.animatedisplaynone = function() {

    var bgmenuclose = jQuery('.sv-mobile-menu-close-bg');
    var body = jQuery('body');
    var html = jQuery('html');

    jQuery('.toggle-sv-menu-mobile, .sv-mobile-menu-close-bg').on('click', function () {

        jQuery('.sv-hamburger').toggleClass('is-active');

        window.offsetYClose = window.offsetYGlobal;

        var offsetY = window.pageYOffset;

        jQuery('body').css('top', -offsetY);

        body.toggleClass('sv-menu-mobile-open');
        html.toggleClass('sv-menu-mobile-open');

        if (bgmenuclose.hasClass('hidden')) {
            bgmenuclose.removeClass('hidden');
            if (jQuery(window).width() < 767) {
                setTimeout(function () {
                    bgmenuclose.removeClass('visuallyhidden');
                }, 0);
            }
            else {
                setTimeout(function () {
                    bgmenuclose.removeClass('visuallyhidden');
                }, 100);
            }
        }

        else {
            bgmenuclose.addClass('visuallyhidden');
            setTimeout(function () {
                bgmenuclose.addClass('hidden');
            }, 300);
        }

    });

    jQuery('.sv-menu-mobile-close').on('click', function () {

        jQuery('body').css('top', 'auto');

        jQuery('.sv-hamburger').removeClass('is-active');
        jQuery('body, html').toggleClass('sv-menu-mobile-open');

        jQuery(window).scrollTop(window.offsetYClose);

        if (bgmenuclose.hasClass('hidden')) {
            bgmenuclose.removeClass('hidden');
            if (jQuery(window).width() < 767) {
                setTimeout(function () {
                    bgmenuclose.removeClass('visuallyhidden');
                }, 0);
            }
            else {
                setTimeout(function () {
                    bgmenuclose.removeClass('visuallyhidden');
                }, 100);
            }
        }

        else {
            bgmenuclose.addClass('visuallyhidden');
            setTimeout(function () {
                bgmenuclose.addClass('hidden');
            }, 300);
        }

    });

}

window.mobileslidernav = function() {
    if (jQuery(window).width() < 767) {
        var imageheight = jQuery('.sv-product-card-slider .sv-product-card .image').height();
        jQuery('.sv-product-card-slider .owl-nav').css('top', imageheight / 2 - 25);
    }
}

window.marketdaydropdown = function() {




    jQuery('.sv-marketday-dropdown .title').on('click', function () {
        jQuery('.sv-marketday-dropdown').toggleClass('is-open');

        var activeDay = jQuery('.sv-day.is-active');
        if (activeDay.length > 0) {
            setTimeout(function() {
                try {
                    activeDay[0].scrollIntoView({behavior: "smooth",inline: 'start'})
                } catch (error) {}
            }, 200);
        }
    });

    jQuery('.sv-marketday-dropdown .button').hover(
        function () {
            jQuery(this).parent().addClass('has-hover');
        },
        function () {
            jQuery(this).parent().removeClass('has-hover');
        }
    );

    jQuery('.mob-market-day-container .open-mob-drop').on('click', function () {
        jQuery('.mob-market-day-dropdown').toggleClass('mob-market-day-dropdown-roll');
        jQuery('.mob-marketday-overlay').toggleClass('mob-marketday-overlay-visible');

    });



}

jQuery('html').on('click', '.toggle-sv-signin', function () {
    console.log('click');
})


window.lightbox = function () {
    // console.log('lightbox?');

    var signin = jQuery('.sv-lightbox.sv-signin');
    var newsletter = jQuery('.sv-lightbox.sv-newsletter');
    var body = jQuery('body');
    var html = jQuery('html');

    jQuery(document).on('click', '.toggle-sv-signin', function () {

        console.log('lightbox? click');

        signin.toggleClass('sv-lightbox-open');
        html.toggleClass('sv-lightbox-open');
        body.toggleClass('sv-lightbox-open');

        if (signin.hasClass('hidden')) {

            signin.removeClass('hidden');

            setTimeout(function () {
                signin.removeClass('visuallyhidden');
            }, 50);

        } else {

            signin.addClass('visuallyhidden');

            setTimeout(function () {
                signin.addClass('hidden');
            }, 650);

        }

    });

    jQuery('.toggle-sv-newsletter').on('click', function () {

        newsletter.toggleClass('sv-lightbox-open');
        html.toggleClass('sv-lightbox-open');
        body.toggleClass('sv-lightbox-open');

        if (newsletter.hasClass('hidden')) {

            newsletter.removeClass('hidden');

            setTimeout(function () {
                newsletter.removeClass('visuallyhidden');
            }, 50);

        } else {

            newsletter.addClass('visuallyhidden');

            setTimeout(function () {
                newsletter.addClass('hidden');
            }, 650);

        }

    });

    jQuery('.toggle-lightbox-mobile-close').click(function () {
        jQuery('body').css('top', 'auto');
        jQuery(window).scrollTop(window.offsetYClose);
    });

    jQuery('.sv-newsletter .toggle-success').click(function () {
        jQuery(this).parents('.title').addClass('success');
        jQuery(this).parents('.title .data').animate({ height: '0px' }, 350);
    });

}


window.farmersmap = function() {

    var map;
    var markers = []; // Create a marker array to hold your markers
    var markersGroup = new L.MarkerClusterGroup();

    L.mapbox.accessToken = 'pk.eyJ1IjoiYWR4bHYiLCJhIjoiY2t1MmgxNGxqMmhtMzJ1cDhhNHpjc2t1OCJ9.u5cQRenrjc6E9c5m3PMU4Q';
    // L.mapbox.accessToken = 'pk.eyJ1IjoiYWR4bHYiLCJhIjoiYk5abi1vayJ9.PoajAncLFRbY0dLsYSQOHg';

    var myIcon = L.icon({
        iconUrl: '/assets/img/pin.png',
        iconRetinaUrl: '/assets/img/pin@2x.png',
        iconSize: [36,36],
        iconAnchor: [18,18],
        // popupAnchor: [-3, -76],
        // shadowUrl: 'my-icon-shadow.png',
        // shadowRetinaUrl: 'my-icon-shadow@2x.png',
        // shadowSize: [68, 95],
        // shadowAnchor: [22, 94]
    });

    function setMarkers(locations) {

        for (var i = 0; i < locations.length; i++) {
            var farmer = locations[i];

            // var myLatLng = new google.maps.LatLng(farmer[1][0], farmer[1][1]);
            // var marker = new RichMarker({
            //     position: myLatLng,
            //     map: map,
            //     animation: google.maps.Animation.DROP,
            //     title: farmer[0],
            //     zIndex: farmer[2],
            //     content: "<img title='" + farmer[0] + "' src='" + farmer[3] + "' class='googleMapsMarker'/>",
            //     flat: true
            // });

            console.log("FF",farmer);

            var myLatLng = new L.LatLng(farmer[1], farmer[2]);
            var marker = L.marker(myLatLng, {
                // icon: L.mapbox.marker.icon({'marker-symbol': 'post', 'marker-color': '0044FF'}),
                icon: myIcon,
                title: farmer[0],
            });

            // Push marker to markers array
            markers.push(marker);

            markersGroup.addLayer(marker);
        }

    }

    function reloadFarmersMarkers() {

        // Loop through markers and set map to null for each
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        // Reset the markers array
        markers = [];

        // Call set markers to re-add markers
        setMarkers(farmers);

    }

    function reloadMastersMarkers() {

        // Loop through markers and set map to null for each
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        // Reset the markers array
        markers = [];

        // Call set markers to re-add markers
        setMarkers(masters);

    }

    function initialize() {

        var center = { lat: 56.9428967, lng: 24.2437765 };

        if (jQuery(window).width() < 767) {
            var mapOptions = {
                zoom: 6,
                center: center,
                disableDefaultUI: true,
                scrollwheel: false,
                streetViewControl: true,
                zoomControl: true,
            }
        }
        else {
            var mapOptions = {
                zoom: 8,
                center: center,
                disableDefaultUI: true,
                scrollwheel: false,
                streetViewControl: true,
                zoomControl: true,
            }
        }

        // map = new google.maps.Map(document.getElementById('farmersmap-canvas'), mapOptions);

        setMarkers(all_suppliers);

        var mapboxTiles = L.mapbox.styleLayer('mapbox://styles/adxlv/cku2h76de3qno17p4o9new25q', {
            maxZoom: 9,
            minZoom: 8,
            // attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            attribution: '',
        });

        map = L.map('farmersmap-canvas', {
            center: center,
            zoom: 8,
            gestureHandling: true,
            // gestureHandlingOptions: {
            //     text: {
            //         touch: "Hey bro, use two fingers to move the map",
            //         scroll: "Hey bro, use ctrl + scroll to zoom the map",
            //         scrollMac: "Hey bro, use \u2318 + scroll to zoom the map"
            //     }
            // }
        });

        map.addLayer(mapboxTiles)
        map.addLayer(markersGroup);

        // Bind event listener on button to reload markers
        // document.getElementById('reloadFarmersMarkers').addEventListener('click', reloadFarmersMarkers);
        // document.getElementById('reloadMastersMarkers').addEventListener('click', reloadMastersMarkers);

    }

    initialize();

}


window.contactsmap = function () {

    var map;
    var markers = []; // Create a marker array to hold your markers
    var home = [
        ['Svaigi HQ', 56.9400374, 24.0568313, 1]
    ];

    function setMarkers(locations) {

        var markerIcon = {
            url: 'img/icon-svaigi-1.svg',
            scale: 1,
            width: 45,
            height: 45,
            // anchor: new google.maps.Point(12, 24),
            optimized: false,
            zIndex: 99999999
        };

        for (var i = 0; i < locations.length; i++) {
            var shop = locations[i];
            var myLatLng = new google.maps.LatLng(shop[1], shop[2]);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                animation: google.maps.Animation.DROP,
                title: shop[0],
                zIndex: shop[3],
                icon: markerIcon
            });

            // Push marker to markers array
            markers.push(marker);
        }

    }

    function initialize() {

        var center = { lat: 56.9400374, lng: 24.0568313 };

        if (jQuery(window).width() < 767) {
            var mapOptions = {
                zoom: 17,
                center: center,
                disableDefaultUI: true,
                scrollwheel: false,
                streetViewControl: true,
                zoomControl: true,
            }
        }
        else {
            var mapOptions = {
                zoom: 17,
                center: center,
                disableDefaultUI: true,
                scrollwheel: false,
                streetViewControl: true,
                zoomControl: true,
            }
        }

        map = new google.maps.Map(document.getElementById('contactsmap-canvas'), mapOptions);

        setMarkers(home);

    }

    initialize();

}


jQuery('.sv-tabs .tab').click(function (e) {
    e.preventDefault()
    jQuery(this).tab('show')
})


jQuery('.sv-category-title-banner .image').imagesLoaded({ background: true }, function () {
    jQuery('.sv-category-title-banner .bg-parallax').addClass('has-loaded');
});

jQuery('.sv-page-title-banner .image').imagesLoaded({ background: true }, function () {
    jQuery('.sv-page-title-banner .bg-parallax').addClass('has-loaded');
});

jQuery('.sv-image-banner .image').imagesLoaded({ background: true }, function () {
    jQuery('.sv-image-banner .item').addClass('has-loaded');
});

jQuery('.content-sv-newsletter .image').imagesLoaded({ background: true }, function () {
    jQuery('.content-sv-newsletter form').addClass('has-loaded');
});

jQuery('.sv-steps .image').imagesLoaded({ background: true }, function () {
    jQuery('.sv-steps .bg-parallax').addClass('has-loaded');
});

jQuery('.sv-blog-list .image').imagesLoaded({ background: true }, function () {
    jQuery('.sv-blog-list .item').addClass('has-loaded');
});

jQuery('.sv-blog-list-slider .image').imagesLoaded({ background: true }, function () {
    jQuery('.sv-blog-list-slider .item').addClass('has-loaded');
});


// ==============================================================
// Menu
// ==============================================================
jQuery(document).on('click', '.hamburger', function (event) {
    // jQuery('.hamburger').toggleClass('is-active');
});


window.mobilemenuscroll = function () {

    var container = jQuery('.sv-menu-mobile .content');
    var content = jQuery('.sv-menu-mobile .content').height();
    var total_height = 0;
    jQuery('.sv-menu-mobile .content ul').each(function () {
        total_height += jQuery(this).outerHeight(true);
    });

    if (total_height > content) {
        jQuery(container).css('justify-content', 'flex-start');
    }

    else {
        jQuery(container).css('justify-content', 'center');
    }

}

jQuery('.sv-product-mobile-menu').each(function(index, el) {

    /**
     * Override arrow and title clicks
     */
    jQuery('a', el).on('click', function (e) {
        var isSpanClicked = jQuery(e.target).is('span');
        console.log('.sv-product-mobile-menu :: click', isSpanClicked);
        if (isSpanClicked) {
            $(this).removeClass('next');
        }
    })

    jQuery(el).slinky({

        // auto resize height
        resize: true,

        // animation speed
        speed: 300,

        // theme CSS
        theme: 'slinky-theme-default',

        // shows title of sub menu
        title: true

    });

    // let $active = jQuery(el).find('.is-current').first();
    // if ($active.length > 0) {
    //     slinky.jump($active.parent('ul'), false)
    // }

})

jQuery('#sv-product-side-menu').slinky({

    // auto resize height
    resize: false,

    // animation speed
    speed: 300,

    // theme CSS
    theme: 'slinky-theme-default',

    // shows title of sub menu
    title: true

});

window.showCartAlert = function(data) {

    "use strict";

    var amount = 1;

    try {
        amount = (data && data['amount'])
            ? data['amount']
            : 1;
    } catch (error) {}

    // retrieve the elements
    var elements = [
        document.getElementById("alert"),
        document.getElementById("alert-affix")
    ].forEach(element => {
        try {
            element.innerHTML = "+" + amount;

            // -> removing the class
            element.classList.remove("is-visible-once");

            // -> triggering reflow /* The actual magic */
            // without this it wouldn't work. Try uncommenting the line and the transition won't be retriggered.
            // This was, from the original tutorial, will no work in strict mode. Thanks Felis Phasma! The next uncommented line is the fix.
            // element.offsetWidth = element.offsetWidth;
            void element.offsetWidth;

            // -> and re-adding the class
            element.classList.add("is-visible-once");
        } catch (error) {}
    });

}

svaigi = {
    showMessage: function (message, messageType, messageTimeout, messageLayout) {
        if (!message) {
            return;
        }
        console.log('aaaa');
        if (typeof messageTimeout === "undefined") {
            messageTimeout = 3000;
        }
        if (typeof messageType === "undefined") {
            messageType = "info";
        }
        if (typeof messageLayout === "undefined") {
            messageLayout = "bottomRight";
        }
        err = new Noty({
            theme: "bootstrap-v4",
            type: messageType,
            layout: messageLayout,
            text: message,
            timeout: messageTimeout
        }).show();
    }
};

window.readCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

window.eraseCookie = function(name) {
    createCookie(name, "", -1);
}

window.createCookie = function(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

jQuery(document).ready(function() {
    if ( readCookie('sv_cookie_hide') === null && readCookie('_consent_given') === null ) {
        // populate popup dialog
        jQuery('.sv-cookies').removeClass('hidden');
        jQuery('.sv-cookies [data-consent]').on('click', function(e) {

            e.preventDefault();

            createCookie('sv_cookie_hide', '1', 1);

            if( jQuery(this).data('consent') === 'yes' ) {
                createCookie('_consent_given', 'allow', 7);
            }

            jQuery('.sv-cookies').addClass('hidden');
            jQuery('body').removeClass('sv-has-marketday-dropdown');
        });
    } else {
        jQuery('body').removeClass('sv-has-marketday-dropdown');
    }
});
