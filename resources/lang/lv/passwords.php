<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parolei jābūt vismaz 6 simbolu garai un jāsakrīt ar paroles apstiprinājuma lauku',
    // 'reset' => 'Your password has been reset!',
    // 'sent' => 'We have e-mailed your password reset link!',
    // 'token' => 'This password reset token is invalid.',
    // 'user' => "We can't find a user with that e-mail address.",
    'reset' => __("translations.resetPassword_Success_PasswordHasBeenReset"),
    'sent' => __("translations.resetPassword_Success_SentEmail"),
    'token' => __("translations.resetPassword_Error_wrongToken"),
    'user' => __("translations.resetPassword_Error_noUser"),

];
