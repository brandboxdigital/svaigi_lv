<?php

return [
    'alreadySubscribed' => 'Email already subscribed',
    'subscribed' => 'Subscribed',
    'somethingHappend' => 'Something error happend. Try again later'
];