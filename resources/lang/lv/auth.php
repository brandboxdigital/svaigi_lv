<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Nepareizs e-pasts vai parole!',
    'throttle' => 'Pārāk daudz nesekmīgu mēģinājumu ielogoties. Lūdzu mēģiniet vēlreiz pēc :seconds sekundēm.',

];
