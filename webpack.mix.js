const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')

    .js('resources/js/vue.js', 'public/js')
    .js('resources/js/admin-vue.js', 'public/js')
    .js('resources/js/front.js', 'public/assets/js')
    .js('resources/js/custom.js', 'public/assets/js')

    .js('resources/js/admin.js', 'public/assets/js')

    .options({
        processCssUrls: mix.inProduction()
    })

    .sass('resources/sass/main.scss', 'public/assets/css/main.css', {
        sassOptions: {
            quietDeps: true,
        },
    })

    .sass('resources/sass/tinymce.scss', 'public/assets/css/tinymce.css', {
        sassOptions: {
            quietDeps: true,
        },
    })

    .styles([
        'resources/sass/inputmask.css',
        'resources/sass/select2.css',
        'resources/sass/tempusdominus-bootstrap-4.css',
        'resources/sass/jquery-ui.css',
        'resources/sass/themes/bootstrap-v4.css',
    ], 'public/css/app.css')
    .styles(['resources/sass/custom.css',],'public/css/custom.css')
    .styles(['resources/sass/style.css'], 'public/css/style.css')
    .styles(['resources/sass/bootstrap.min.css'], 'public/css/bootstrap.min.css')

    .copy('resources/js/jquery-ui.js', 'public/js/')
    .copy('resources/js/email-subscribe.js', 'public/js/')
    .copy('node_modules/nestedSortable/jquery.mjs.nestedSortable.js', 'public/js')

    .combine([
        'node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget',
        // 'node_modules/blueimp-file-upload/js/jquery.fileupload-ui.js',
        'node_modules/blueimp-file-upload/js/jquery.iframe-transport.js',
        'node_modules/blueimp-file-upload/js/jquery.fileupload.js',
    ], 'public/assets/js/jquery.fileupload.js');
