<?php

use Illuminate\Database\Seeder;

class OrderCoupon extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mainCat = \App\Model\Admin\Menu::where(
            [
                'slug'      => 'orders',
                'routeName' => 'orders',
            ],
        )->first();

        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => 'setcoupon/{id}',
                'routeName' => "orders.setcoupon",
            ],
            [
                'icon'        => 'fas fa-',
                'displayName' => 'Set Coupon',
                'action'      => '\App\Plugins\Orders\OrderController@setCoupon',
                'inMenu'      => '0',
                'sequence'    => 0,
                'parent_id'   => $mainCat->id,
                'method'      => 'POST',
            ]);
    }
}
