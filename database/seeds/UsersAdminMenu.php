<?php

use Illuminate\Database\Seeder;

class UsersAdminMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = \App\Model\Admin\Menu::where('routeName','config')->first();

        $lastSeq = \App\Model\Admin\Menu::where('parent_id', $menu->id)->orderBy('sequence', 'DESC')->first()->sequence ?? 0;
        $lastSeq++;

        $main = 'users';

        $vacations = \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => $main,
                'routeName' => $main,
            ],
            [
                'icon'        => 'fas fa-weight',
                'displayName' => 'Users',
                'action'      => '\App\Plugins\Users\UsersController@index',
                'inMenu'      => '1',
                'sequence'    => $lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "add",
                'routeName' => $main.".add",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'Add',
                'action'      => '\App\Plugins\Users\UsersController@add',
                'inMenu'      => '0',
                'sequence'    => ++$lastSeq,
                'parent_id'   => $vacations->id,
                'method'      => 'GET',
            ]);
        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "edit/{id}",
                'routeName' => $main.".edit",
            ],
            [
                'icon'        => 'fas fa-edit',
                'displayName' => 'Edit',
                'action'      => '\App\Plugins\Users\UsersController@edit',
                'inMenu'      => '0',
                'sequence'    => ++$lastSeq,
                'parent_id'   => $vacations->id,
                'method'      => 'GET',
            ]);
        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "destroy/{id}",
                'routeName' => $main.".destroy",
            ],
            [
                'icon'        => 'far fa-window-close',
                'displayName' => 'Delete',
                'action'      => '\App\Plugins\Users\UsersController@delete',
                'inMenu'      => '0',
                'sequence'    => ++$lastSeq,
                'parent_id'   => $vacations->id,
                'method'      => 'POST',
            ]);


        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "store/{id?}",
                'routeName' => $main.".store",
            ],
            [
                'icon'        => 'far fa-window-close',
                'displayName' => 'Save',
                'action'      => '\App\Plugins\Users\UsersController@store',
                'inMenu'      => '0',
                'sequence'    => ++$lastSeq,
                'parent_id'   => $vacations->id,
                'method'      => 'POST',
            ]);
    }
}
