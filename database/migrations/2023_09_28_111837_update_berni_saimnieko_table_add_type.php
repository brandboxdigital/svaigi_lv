<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBerniSaimniekoTableAddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::table('berni_saimnieko_analytics', function (Blueprint $table) {
            $table->string('type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->tryToDrop('berni_saimnieko_analytics', 'type');
    }

    private function tryToDrop($tableName, $colName)
    {
        try {
            Schema::table($tableName, function (Blueprint $table) use ($colName) {
                $table->dropColumn($colName);
            });
        } catch (\Throwable $th) {
            // dump($th->getMessage());
        }
    }
}
