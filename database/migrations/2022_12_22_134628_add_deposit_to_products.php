<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Plugins\Translations\Model\Translation;
use App\Plugins\Translations\TranslationController;

class AddDepositToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::table('order_lines', function (Blueprint $table) {
            $table->float('deposit_amount')->default(0);
        });

        // Schema::table('products', function (Blueprint $table) {
        //     $table->boolean("has_deposit")->nullable();
        //     $table->float('deposit_amount')->nullable();
        // });

        Schema::table('product_variations', function (Blueprint $table) {
            $table->boolean("has_deposit")->nullable();
            $table->float('deposit_amount')->default(0);
        });

        $this->addTranslationString('cartDeposit', 'Depozīts');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->tryToDrop('order_lines', 'deposit_amount');

        $this->tryToDrop('products', 'has_deposit');
        $this->tryToDrop('products', 'deposit_amount');

        $this->tryToDrop('product_variations', 'has_deposit');
        $this->tryToDrop('product_variations', 'deposit_amount');

    }

    private function tryToDrop($tableName, $colName)
    {
        try {
            Schema::table($tableName, function (Blueprint $table) use ($colName) {
                $table->dropColumn($colName);
            });
        } catch (\Throwable $th) {
            // dump($th->getMessage());
        }
    }

    private function addTranslationString($key, $translationLvString = '')
    {
        $translationsController = new TranslationController;
        $translationsController->collect();

        $translationModel = Translation::where('key', $key)->first();

        if ($translationModel) {
            $fakeRequest = $this->fakeRequest([
                'translation' => [
                    'lv' => $translationLvString
                ]
            ]);

            $translationsController->store($fakeRequest, $translationModel->id);
        }

    }

    protected function fakeRequest(array $data = [], $method = 'POST')
    {
        $request = app('request');

        $request->setMethod($method);

        $request->request->add($data);


        return $request;
        // $request->replace(['foo' => 'bar']);
    }
}
