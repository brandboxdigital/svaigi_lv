<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogInfoToPayseraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paysera', function (Blueprint $table) {
            $table->text('response_data')->nullable();
            $table->text('request_data')->nullable();
            $table->text('log')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paysera', function (Blueprint $table) {
            $table->dropColumn('response_data');
            $table->dropColumn('request_data');
            $table->dropColumn('log');
        });
    }
}
