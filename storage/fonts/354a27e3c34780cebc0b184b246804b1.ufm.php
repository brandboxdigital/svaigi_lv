<?php return array (
  'codeToName' => 
  array (
    13 => 'CR',
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    177 => 'plusminus',
    178 => 'twosuperior',
    179 => 'threesuperior',
    180 => 'acute',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    185 => 'onesuperior',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    256 => 'Amacron',
    257 => 'amacron',
    258 => 'Abreve',
    259 => 'abreve',
    260 => 'Aogonek',
    261 => 'aogonek',
    262 => 'Cacute',
    263 => 'cacute',
    264 => 'Ccircumflex',
    265 => 'ccircumflex',
    266 => 'Cdotaccent',
    267 => 'cdotaccent',
    268 => 'Ccaron',
    269 => 'ccaron',
    270 => 'Dcaron',
    271 => 'dcaron',
    272 => 'Dcroat',
    273 => 'dmacron',
    274 => 'Emacron',
    275 => 'emacron',
    278 => 'Edotaccent',
    279 => 'edotaccent',
    280 => 'Eogonek',
    281 => 'eogonek',
    282 => 'Ecaron',
    283 => 'ecaron',
    284 => 'Gcircumflex',
    285 => 'gcircumflex',
    286 => 'Gbreve',
    287 => 'gbreve',
    288 => 'Gdotaccent',
    289 => 'gdotaccent',
    290 => 'Gcommaaccent',
    291 => 'gcommaaccent',
    292 => 'Hcircumflex',
    293 => 'hcircumflex',
    294 => 'Hbar',
    295 => 'hbar',
    298 => 'Imacron',
    299 => 'imacron',
    302 => 'Iogonek',
    303 => 'iogonek',
    304 => 'Idot',
    305 => 'dotlessi',
    308 => 'Jcircumflex',
    309 => 'jcircumflex',
    310 => 'Kcommaaccent',
    311 => 'kcommaaccent',
    313 => 'Lacute',
    314 => 'lacute',
    315 => 'Lcommaaccent',
    316 => 'lcommaaccent',
    317 => 'Lcaron',
    318 => 'lcaron',
    321 => 'Lslash',
    322 => 'lslash',
    323 => 'Nacute',
    324 => 'nacute',
    325 => 'Ncommaaccent',
    326 => 'ncommaaccent',
    327 => 'Ncaron',
    328 => 'ncaron',
    332 => 'Omacron',
    333 => 'omacron',
    336 => 'Ohungarumlaut',
    337 => 'ohungarumlaut',
    338 => 'OE',
    339 => 'oe',
    340 => 'Racute',
    341 => 'racute',
    342 => 'Rcommaaccent',
    343 => 'rcommaaccent',
    344 => 'Rcaron',
    345 => 'rcaron',
    346 => 'Sacute',
    347 => 'sacute',
    348 => 'Scircumflex',
    349 => 'scircumflex',
    350 => 'Scedilla',
    351 => 'scedilla',
    352 => 'Scaron',
    353 => 'scaron',
    354 => 'Tcommaaccent',
    355 => 'tcommaaccent',
    356 => 'Tcaron',
    357 => 'tcaron',
    362 => 'Umacron',
    363 => 'umacron',
    364 => 'Ubreve',
    365 => 'ubreve',
    366 => 'Uring',
    367 => 'uring',
    368 => 'Uhungarumlaut',
    369 => 'uhungarumlaut',
    370 => 'Uogonek',
    371 => 'uogonek',
    376 => 'Ydieresis',
    377 => 'Zacute',
    378 => 'zacute',
    379 => 'Zdotaccent',
    380 => 'zdotaccent',
    381 => 'Zcaron',
    382 => 'zcaron',
    402 => 'florin',
    536 => 'Scommaaccent',
    537 => 'scommaaccent',
    567 => 'dotlessj',
    710 => 'circumflex',
    711 => 'caron',
    728 => 'breve',
    729 => 'dotaccent',
    730 => 'ring',
    731 => 'ogonek',
    732 => 'tilde',
    733 => 'hungarumlaut',
    900 => 'tonos',
    901 => 'dieresistonos',
    902 => 'Alphatonos',
    903 => 'anoteleia',
    904 => 'Epsilontonos',
    905 => 'Etatonos',
    906 => 'Iotatonos',
    908 => 'Omicrontonos',
    910 => 'Upsilontonos',
    911 => 'Omegatonos',
    912 => 'iotadieresistonos',
    913 => 'Alpha',
    914 => 'Beta',
    915 => 'Gamma',
    916 => 'increment',
    917 => 'Epsilon',
    918 => 'Zeta',
    919 => 'Eta',
    920 => 'Theta',
    921 => 'Iota',
    922 => 'Kappa',
    923 => 'Lambda',
    924 => 'Mu',
    925 => 'Nu',
    926 => 'Xi',
    927 => 'Omicron',
    928 => 'Pi',
    929 => 'Rho',
    931 => 'Sigma',
    932 => 'Tau',
    933 => 'Upsilon',
    934 => 'Phi',
    935 => 'Chi',
    936 => 'Psi',
    937 => 'Omega',
    938 => 'Iotadieresis',
    939 => 'Upsilondieresis',
    940 => 'alphatonos',
    941 => 'epsilontonos',
    942 => 'etatonos',
    943 => 'iotatonos',
    944 => 'upsilondieresistonos',
    945 => 'alpha',
    946 => 'beta',
    947 => 'gamma',
    948 => 'delta',
    949 => 'epsilon',
    950 => 'zeta',
    951 => 'eta',
    952 => 'theta',
    953 => 'iota',
    954 => 'kappa',
    955 => 'lambda',
    956 => 'mu',
    957 => 'nu',
    958 => 'xi',
    959 => 'omicron',
    960 => 'pi',
    961 => 'rho',
    962 => 'sigma1',
    963 => 'sigma',
    964 => 'tau',
    965 => 'upsilon',
    966 => 'phi',
    967 => 'chi',
    968 => 'psi',
    969 => 'omega',
    970 => 'iotadieresis',
    971 => 'upsilondieresis',
    972 => 'omicrontonos',
    973 => 'upsilontonos',
    974 => 'omegatonos',
    976 => 'beta1',
    1025 => 'afii10023',
    1026 => 'afii10051',
    1027 => 'afii10052',
    1028 => 'afii10053',
    1029 => 'afii10054',
    1030 => 'afii10055',
    1031 => 'afii10056',
    1032 => 'afii10057',
    1033 => 'afii10058',
    1034 => 'afii10059',
    1035 => 'afii10060',
    1036 => 'afii10061',
    1038 => 'afii10062',
    1039 => 'afii10145',
    1040 => 'afii10017',
    1041 => 'afii10018',
    1042 => 'afii10019',
    1043 => 'afii10020',
    1044 => 'afii10021',
    1045 => 'afii10022',
    1046 => 'afii10024',
    1047 => 'afii10025',
    1048 => 'afii10026',
    1049 => 'afii10027',
    1050 => 'afii10028',
    1051 => 'afii10029',
    1052 => 'afii10030',
    1053 => 'afii10031',
    1054 => 'afii10032',
    1055 => 'afii10033',
    1056 => 'afii10034',
    1057 => 'afii10035',
    1058 => 'afii10036',
    1059 => 'afii10037',
    1060 => 'afii10038',
    1061 => 'afii10039',
    1062 => 'afii10040',
    1063 => 'afii10041',
    1064 => 'afii10042',
    1065 => 'afii10043',
    1066 => 'afii10044',
    1067 => 'afii10045',
    1068 => 'afii10046',
    1069 => 'afii10047',
    1070 => 'afii10048',
    1071 => 'afii10049',
    1072 => 'afii10065',
    1073 => 'afii10066',
    1074 => 'afii10067',
    1075 => 'afii10068',
    1076 => 'afii10069',
    1077 => 'afii10070',
    1078 => 'afii10072',
    1079 => 'afii10073',
    1080 => 'afii10074',
    1081 => 'afii10075',
    1082 => 'afii10076',
    1083 => 'afii10077',
    1084 => 'afii10078',
    1085 => 'afii10079',
    1086 => 'afii10080',
    1087 => 'afii10081',
    1088 => 'afii10082',
    1089 => 'afii10083',
    1090 => 'afii10084',
    1091 => 'afii10085',
    1092 => 'afii10086',
    1093 => 'afii10087',
    1094 => 'afii10088',
    1095 => 'afii10089',
    1096 => 'afii10090',
    1097 => 'afii10091',
    1098 => 'afii10092',
    1099 => 'afii10093',
    1100 => 'afii10094',
    1101 => 'afii10095',
    1102 => 'afii10096',
    1103 => 'afii10097',
    1105 => 'afii10071',
    1106 => 'afii10099',
    1107 => 'afii10100',
    1108 => 'afii10101',
    1109 => 'afii10102',
    1110 => 'afii10103',
    1111 => 'afii10104',
    1112 => 'afii10105',
    1113 => 'afii10106',
    1114 => 'afii10107',
    1115 => 'afii10108',
    1116 => 'afii10109',
    1118 => 'afii10110',
    1119 => 'afii10193',
    1168 => 'afii10050',
    1169 => 'afii10098',
    8211 => 'endash',
    8212 => 'emdash',
    8213 => 'afii00208',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8224 => 'dagger',
    8225 => 'daggerdbl',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8240 => 'perthousand',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8364 => 'Euro',
    8470 => 'afii61352',
    8482 => 'trademark',
    8592 => 'arrowleft',
    8593 => 'arrowup',
    8594 => 'arrowright',
    8595 => 'arrowdown',
    8706 => 'partialdiff',
    8719 => 'product',
    8721 => 'summation',
    8722 => 'minus',
    8730 => 'radical',
    8734 => 'infinity',
    8743 => 'logicaland',
    8747 => 'integral',
    8776 => 'approxequal',
    8800 => 'notequal',
    8804 => 'lessequal',
    8805 => 'greaterequal',
    8901 => 'dotmath',
    9674 => 'lozenge',
    64257 => 'fi',
    64258 => 'fl',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'DINPro-Regular',
  'FullName' => 'DINPro-Regular',
  'Version' => 'Version 7.460;PS 7.046;hotconv 1.0.38',
  'PostScriptName' => 'DINPro-Regular',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '51',
  'UnderlinePosition' => '-210',
  'FontHeightOffset' => '10',
  'Ascender' => '1041',
  'Descender' => '-237',
  'FontBBox' => 
  array (
    0 => '-176',
    1 => '-229',
    2 => '1115',
    3 => '992',
  ),
  'StartCharMetrics' => '752',
  'C' => 
  array (
    0 => 0.0,
    13 => 249.0,
    32 => 249.0,
    33 => 316.0,
    34 => 423.0,
    35 => 653.0,
    36 => 596.0,
    37 => 833.0,
    38 => 739.0,
    39 => 255.0,
    40 => 298.0,
    41 => 298.0,
    42 => 474.0,
    43 => 522.0,
    44 => 264.0,
    45 => 426.0,
    46 => 270.0,
    47 => 355.0,
    48 => 522.0,
    49 => 522.0,
    50 => 522.0,
    51 => 522.0,
    52 => 522.0,
    53 => 522.0,
    54 => 522.0,
    55 => 522.0,
    56 => 522.0,
    57 => 522.0,
    58 => 297.0,
    59 => 297.0,
    60 => 522.0,
    61 => 522.0,
    62 => 522.0,
    63 => 497.0,
    64 => 713.0,
    65 => 611.0,
    66 => 668.0,
    67 => 637.0,
    68 => 673.0,
    69 => 600.0,
    70 => 580.0,
    71 => 650.0,
    72 => 696.0,
    73 => 282.0,
    74 => 501.0,
    75 => 650.0,
    76 => 573.0,
    77 => 816.0,
    78 => 732.0,
    79 => 650.0,
    80 => 630.0,
    81 => 649.0,
    82 => 656.0,
    83 => 588.0,
    84 => 548.0,
    85 => 683.0,
    86 => 546.0,
    87 => 854.0,
    88 => 551.0,
    89 => 514.0,
    90 => 556.0,
    91 => 313.0,
    92 => 355.0,
    93 => 313.0,
    94 => 537.0,
    95 => 537.0,
    96 => 500.0,
    97 => 526.0,
    98 => 545.0,
    99 => 490.0,
    100 => 545.0,
    101 => 530.0,
    102 => 314.0,
    103 => 543.0,
    104 => 562.0,
    105 => 253.0,
    106 => 253.0,
    107 => 536.0,
    108 => 297.0,
    109 => 879.0,
    110 => 562.0,
    111 => 530.0,
    112 => 545.0,
    113 => 545.0,
    114 => 421.0,
    115 => 496.0,
    116 => 332.0,
    117 => 562.0,
    118 => 447.0,
    119 => 724.0,
    120 => 478.0,
    121 => 447.0,
    122 => 459.0,
    123 => 354.0,
    124 => 326.0,
    125 => 354.0,
    126 => 539.0,
    160 => 249.0,
    161 => 316.0,
    162 => 498.0,
    163 => 548.0,
    164 => 660.0,
    165 => 514.0,
    166 => 335.0,
    167 => 557.0,
    168 => 500.0,
    169 => 881.0,
    170 => 449.0,
    171 => 554.0,
    172 => 522.0,
    173 => 426.0,
    174 => 881.0,
    175 => 500.0,
    176 => 463.0,
    177 => 522.0,
    178 => 342.0,
    179 => 356.0,
    180 => 500.0,
    181 => 561.0,
    182 => 603.0,
    183 => 270.0,
    184 => 500.0,
    185 => 261.0,
    186 => 455.0,
    187 => 554.0,
    188 => 777.0,
    189 => 790.0,
    190 => 822.0,
    191 => 497.0,
    192 => 611.0,
    193 => 611.0,
    194 => 611.0,
    195 => 611.0,
    196 => 611.0,
    197 => 611.0,
    198 => 922.0,
    199 => 637.0,
    200 => 600.0,
    201 => 600.0,
    202 => 600.0,
    203 => 600.0,
    204 => 282.0,
    205 => 282.0,
    206 => 282.0,
    207 => 282.0,
    208 => 687.0,
    209 => 732.0,
    210 => 650.0,
    211 => 650.0,
    212 => 650.0,
    213 => 650.0,
    214 => 650.0,
    215 => 522.0,
    216 => 652.0,
    217 => 683.0,
    218 => 683.0,
    219 => 683.0,
    220 => 683.0,
    221 => 514.0,
    222 => 642.0,
    223 => 556.0,
    224 => 526.0,
    225 => 526.0,
    226 => 526.0,
    227 => 526.0,
    228 => 526.0,
    229 => 526.0,
    230 => 835.0,
    231 => 490.0,
    232 => 530.0,
    233 => 530.0,
    234 => 530.0,
    235 => 530.0,
    236 => 253.0,
    237 => 253.0,
    238 => 253.0,
    239 => 253.0,
    240 => 530.0,
    241 => 562.0,
    242 => 530.0,
    243 => 530.0,
    244 => 530.0,
    245 => 530.0,
    246 => 530.0,
    247 => 522.0,
    248 => 530.0,
    249 => 562.0,
    250 => 562.0,
    251 => 562.0,
    252 => 562.0,
    253 => 445.0,
    254 => 546.0,
    255 => 445.0,
    256 => 611.0,
    257 => 526.0,
    258 => 611.0,
    259 => 526.0,
    260 => 611.0,
    261 => 526.0,
    262 => 637.0,
    263 => 490.0,
    264 => 637.0,
    265 => 490.0,
    266 => 637.0,
    267 => 490.0,
    268 => 637.0,
    269 => 490.0,
    270 => 673.0,
    271 => 545.0,
    272 => 687.0,
    273 => 545.0,
    274 => 600.0,
    275 => 530.0,
    278 => 600.0,
    279 => 530.0,
    280 => 600.0,
    281 => 530.0,
    282 => 600.0,
    283 => 530.0,
    284 => 650.0,
    285 => 543.0,
    286 => 650.0,
    287 => 543.0,
    288 => 650.0,
    289 => 543.0,
    290 => 650.0,
    291 => 543.0,
    292 => 696.0,
    293 => 562.0,
    294 => 718.0,
    295 => 562.0,
    298 => 282.0,
    299 => 253.0,
    302 => 282.0,
    303 => 253.0,
    304 => 282.0,
    305 => 253.0,
    308 => 501.0,
    309 => 253.0,
    310 => 650.0,
    311 => 536.0,
    313 => 573.0,
    314 => 297.0,
    315 => 573.0,
    316 => 297.0,
    317 => 573.0,
    318 => 297.0,
    321 => 582.0,
    322 => 315.0,
    323 => 732.0,
    324 => 562.0,
    325 => 732.0,
    326 => 562.0,
    327 => 732.0,
    328 => 562.0,
    332 => 650.0,
    333 => 530.0,
    336 => 650.0,
    337 => 530.0,
    338 => 989.0,
    339 => 861.0,
    340 => 656.0,
    341 => 421.0,
    342 => 656.0,
    343 => 421.0,
    344 => 656.0,
    345 => 421.0,
    346 => 588.0,
    347 => 496.0,
    348 => 588.0,
    349 => 496.0,
    350 => 588.0,
    351 => 496.0,
    352 => 588.0,
    353 => 496.0,
    354 => 548.0,
    355 => 332.0,
    356 => 548.0,
    357 => 332.0,
    362 => 683.0,
    363 => 562.0,
    364 => 683.0,
    365 => 562.0,
    366 => 683.0,
    367 => 562.0,
    368 => 683.0,
    369 => 562.0,
    370 => 683.0,
    371 => 562.0,
    376 => 514.0,
    377 => 556.0,
    378 => 459.0,
    379 => 556.0,
    380 => 459.0,
    381 => 556.0,
    382 => 459.0,
    402 => 403.0,
    536 => 588.0,
    537 => 496.0,
    538 => 548.0,
    539 => 332.0,
    567 => 253.0,
    710 => 500.0,
    711 => 500.0,
    728 => 500.0,
    729 => 500.0,
    730 => 500.0,
    731 => 500.0,
    732 => 530.0,
    733 => 500.0,
    900 => 255.0,
    901 => 488.0,
    902 => 611.0,
    903 => 270.0,
    904 => 600.0,
    905 => 696.0,
    906 => 282.0,
    908 => 650.0,
    910 => 514.0,
    911 => 650.0,
    912 => 297.0,
    913 => 611.0,
    914 => 668.0,
    915 => 549.0,
    916 => 603.0,
    917 => 600.0,
    918 => 556.0,
    919 => 696.0,
    920 => 650.0,
    921 => 282.0,
    922 => 650.0,
    923 => 598.0,
    924 => 816.0,
    925 => 732.0,
    926 => 573.0,
    927 => 650.0,
    928 => 681.0,
    929 => 630.0,
    931 => 557.0,
    932 => 548.0,
    933 => 514.0,
    934 => 732.0,
    935 => 551.0,
    936 => 675.0,
    937 => 650.0,
    938 => 282.0,
    939 => 514.0,
    940 => 545.0,
    941 => 493.0,
    942 => 562.0,
    943 => 297.0,
    944 => 521.0,
    945 => 545.0,
    946 => 553.0,
    947 => 447.0,
    948 => 531.0,
    949 => 493.0,
    950 => 470.0,
    951 => 562.0,
    952 => 552.0,
    953 => 297.0,
    954 => 536.0,
    955 => 479.0,
    956 => 561.0,
    957 => 447.0,
    958 => 492.0,
    959 => 530.0,
    960 => 591.0,
    961 => 517.0,
    962 => 490.0,
    963 => 546.0,
    964 => 378.0,
    965 => 521.0,
    966 => 659.0,
    967 => 478.0,
    968 => 641.0,
    969 => 723.0,
    970 => 297.0,
    971 => 521.0,
    972 => 530.0,
    973 => 521.0,
    974 => 723.0,
    976 => 553.0,
    1025 => 600.0,
    1026 => 744.0,
    1027 => 569.0,
    1028 => 637.0,
    1029 => 588.0,
    1030 => 282.0,
    1031 => 282.0,
    1032 => 501.0,
    1033 => 1025.0,
    1034 => 1043.0,
    1035 => 744.0,
    1036 => 610.0,
    1038 => 546.0,
    1039 => 696.0,
    1040 => 611.0,
    1041 => 656.0,
    1042 => 668.0,
    1043 => 569.0,
    1044 => 694.0,
    1045 => 600.0,
    1046 => 900.0,
    1047 => 616.0,
    1048 => 716.0,
    1049 => 716.0,
    1050 => 610.0,
    1051 => 677.0,
    1052 => 816.0,
    1053 => 696.0,
    1054 => 650.0,
    1055 => 696.0,
    1056 => 630.0,
    1057 => 637.0,
    1058 => 548.0,
    1059 => 546.0,
    1060 => 779.0,
    1061 => 551.0,
    1062 => 718.0,
    1063 => 645.0,
    1064 => 997.0,
    1065 => 1019.0,
    1066 => 727.0,
    1067 => 864.0,
    1068 => 630.0,
    1069 => 637.0,
    1070 => 903.0,
    1071 => 655.0,
    1072 => 526.0,
    1073 => 533.0,
    1074 => 539.0,
    1075 => 417.0,
    1076 => 556.0,
    1077 => 530.0,
    1078 => 715.0,
    1079 => 482.0,
    1080 => 577.0,
    1081 => 577.0,
    1082 => 498.0,
    1083 => 546.0,
    1084 => 656.0,
    1085 => 562.0,
    1086 => 530.0,
    1087 => 562.0,
    1088 => 545.0,
    1089 => 490.0,
    1090 => 423.0,
    1091 => 447.0,
    1092 => 690.0,
    1093 => 478.0,
    1094 => 579.0,
    1095 => 523.0,
    1096 => 801.0,
    1097 => 816.0,
    1098 => 563.0,
    1099 => 718.0,
    1100 => 506.0,
    1101 => 490.0,
    1102 => 728.0,
    1103 => 527.0,
    1105 => 530.0,
    1106 => 562.0,
    1107 => 417.0,
    1108 => 490.0,
    1109 => 496.0,
    1110 => 253.0,
    1111 => 253.0,
    1112 => 253.0,
    1113 => 801.0,
    1114 => 816.0,
    1115 => 562.0,
    1116 => 498.0,
    1118 => 447.0,
    1119 => 563.0,
    1168 => 569.0,
    1169 => 417.0,
    8211 => 522.0,
    8212 => 914.0,
    8213 => 914.0,
    8216 => 263.0,
    8217 => 263.0,
    8218 => 263.0,
    8220 => 434.0,
    8221 => 434.0,
    8222 => 434.0,
    8224 => 605.0,
    8225 => 605.0,
    8226 => 532.0,
    8230 => 757.0,
    8240 => 1179.0,
    8249 => 328.0,
    8250 => 328.0,
    8260 => 185.0,
    8364 => 678.0,
    8470 => 947.0,
    8482 => 839.0,
    8486 => 650.0,
    8592 => 1027.0,
    8593 => 1027.0,
    8594 => 1027.0,
    8595 => 1027.0,
    8598 => 1027.0,
    8599 => 1027.0,
    8600 => 1027.0,
    8601 => 1027.0,
    8706 => 544.0,
    8710 => 603.0,
    8719 => 696.0,
    8721 => 551.0,
    8722 => 522.0,
    8729 => 270.0,
    8730 => 562.0,
    8734 => 808.0,
    8740 => 270.0,
    8743 => 270.0,
    8747 => 372.0,
    8776 => 539.0,
    8800 => 537.0,
    8804 => 522.0,
    8805 => 522.0,
    8901 => 270.0,
    9674 => 649.0,
    64257 => 562.0,
    64258 => 602.0,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt3HW0VFUYxuH3HekSRRoEVFI6BUEaBEkJlZJuMQhJKSkRRAmVkpRGQaUturtVQJFUQukSZ826unQhxvVyL0t/z1pnvn32/s6cd885f4+sm7njpiu/FU3RFUMxFUuxFUdxFU/xlUB3KqHu0t1KpHuUWEmUVMmUXCmUUqmUWvcqjdIqne7T/XpA6ZVBGZVJmZVFDyqrsim7ciincim38iiv8im/CughFVQhPazCKqJHVFTFVFwlVFKlVFplVFaPqpzK6zFVUEVVUmVVUVU9rmqqrhqqqSf0pJ5SLdVWHdVVPT2t+mqghn9rh3+ukRqriZqqmZqrhVqqlVrrGbXRs3pOz+sFtVU7tVcHvaiO6qTO6qKu6qaX1F091FO91Fsvq4/6qp/6a4Be0UC9qkEarNc0RK/rDQ3VMA3XCL2pt/S2RmqURmuMxuodjdN4TdBETdJkvaspmqppmq4ZmqlZmq339L7maK4+0If6SPM0Xwu0UIu0WEv0sT7Rp/pMn2uplmm5VmilVmm11mit1mm9NmijNmmztmirtmm7dmhnaK+7tFt7tFdf6Et9pX3arwP6Wt/ooL7VIR3WER0N9R3T8VD9Tt/rhE6Gxqd0Wj/ox+DojM7qnM7rgi6GVi7psq7oqq7pJ13/5XW0HQjVOxzN0R3DMR3LsR3HcR3P8Z3Adzqh7/LdTuR7nNhJnNTJfnkkTu4UTulUTu17ncZpnc73/bp2vx9wemdwRmdy5r/3iJ3lX7wf/zF+0FmdzdnDeXWOiE3z3+KczvWXPbmdx3mdz/ldIDIy/V/4IRd0IT/swi7iR4LnRYNHMRd3CZd0KZd2GZf1oy7n8n7MFVzRlVzZVVzVj7tasLO6a7imn/CTfsq1XNt1XNf1/LTru4EbupEbu4mbupmbu4VbupVb+xm38bN+zs/7Bbd1O7d3B7/oju7kzu7iru7ml9w9+M09bvnOe7qXe/tl93Ff93N/D/ArHuhXgyuDPNiveYhf9xse6mEe7hF+02/5bY/0KI/2GI/1Ox7n8Z7giZ7kyX7XUzzV0zzdMzzTszzb7/l9z/Fcf+AP/ZHneb4XeKEXebGX+GN/4k/9mT/3Ui/zcq/wSq/yaq/xWq/zem/wxmCKTd7sLd7qbd7uHd7pXd7tPd4bXPnCX97yX+cr77vV9wAAAAAAAAAAAAAAAAAAAAAAAAAAIHy83wf8dWj0jQ/622A95MM+EqxHfczHw7q++4Mrv79h5oRP/kHfqYhNHNV8OqoTRDz/cJP5H8PqmcjLEll81ud83hdC44u+5Mu+EtWZIpqvhtVrN6z8FDyuB343F3AgbCLwm38ICUQLfUYPO4sR3iyBmP+wP1ZYjR2IE9573h4CcaM6AQAgagXiRXUCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABud4H4gQRRnQEAAACR5WdqLwCV',
  '_version_' => 6,
);