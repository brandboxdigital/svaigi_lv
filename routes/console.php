<?php

use App\Plugins\Categories\Model\Category;
use App\Plugins\Orders\Model\OrderHeader;
use Illuminate\Foundation\Inspiring;
use App\Plugins\Orders\Model\OrderLines;
use App\Plugins\Orders\Model\OriginalOrder;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('bboxdigi:fullclear', function() {

    exec('rm -f ' . storage_path('logs/*.log'));
    exec('rm -f ' . base_path('*.log'));

    $this->comment('Logs have been cleared!');

    \Artisan::call('cache:clear');
    $this->comment('Cache have been cleared!');

    \Artisan::call('optimize:clear');
    $this->comment('Optimize have been cleared!');

    \Artisan::call('debugbar:clear');
    $this->comment('Debugbar have been cleared!');

})->describe('Clear all temp files');

Artisan::command('bboxdigi:fix', function() {
    \App\Plugins\Suppliers\Model\SupplierMeta::where('meta_name', 'excerpt')
        ->get()
        ->each(function($item) {
            $orig = $item->meta_value;
            $item->meta_value = trim(strip_tags($orig));

            dump($orig . ' | '. $item->meta_value);
            $item->save();
        });

})->describe('Used for multiple purposes');

Artisan::command('bboxdigi:onettime-order-reverse-state', function() {
    $ids = [1825971,1825956,1825949,1825942,1824375,1825914,1825661,1825108,1824961,1824991,1800161,1824895,1824862,1824880,1776089,1824726,1824671,1824609,1824620,1388449,1824541,1775412,1824240,1824291,1824057,1824038,1824069,1823739,1436280,1823568,1588178,1770085,1823473,1823274,1823309,1823310,1680961,1808851,1823099,1821984,1819936,1819781,1819135,1815456,1811969,1810949,1806024,1802514,1799808];
    foreach ($ids as $id) {
        $order = OrderHeader::find($id);
        $order->state = 'ordered';
        $order->save();
    }


})->describe('One time Megija messed up :)');


Artisan::command('bboxdigi:restore-order-lines {id?}', function() {
    $id = $this->argument('id');

    if (!$id) {
        $id = $this->ask('Oreder Header ID');
    }

    $original_order = OriginalOrder::find($id);

    if ($original_order == null) {
        $this->error('Cant find OriginalOrder with id '. $id);
        return;
    }

    foreach ($original_order->items as $key => $item) {
        dump($item);
        if ( $this->confirm('Import?') ) {
            unset($item['id']);
            unset($item["order_number_string"]);
            unset($item['order']);

            try {
                $ol = new OrderLines;

                $ol->fill($item);
                $ol->order_header_id = $item["order_header_id"];
                $ol->created_at = $item["created_at"];
                $ol->updated_at = $item["updated_at"];

                $ol->save();
            } catch (\Throwable $th) {
                dump($th->getMessage());
            }

            //  'id', // добавил, странно что его не было
            // 'supplier_id',
            // 'supplier_name',
            // 'product_id',
            // 'product_name',
            // 'vat_id',
            // 'vat_amount',
            // 'vat',
            // 'price',
            // 'display_name',
            // 'variation_size',
            // 'discount',
            // 'amount',
            // 'variation_id',
            // 'real_amount',
            // 'total_amount',
            // 'amount_unit',
            // 'price_raw',
            // 'vat_raw',
            // 'cost',
            // 'markup',
            // 'markup_amount',
            // 'discount_name'
        }
    }

})->describe('Restore Original order lines');


Artisan::command('bboxdigi:rebuild-categories', function() {
    $data = [];
    $root = Category::where('parent_id', null)->get();

    foreach ($root as $category) {
        $item = rebuildCategoriesRecursive($category);

        $data[] = $item;
    }

    dd(Category::rebuildTree($data));

})->describe('Rebuild the category tree, so that categories descendants() work correcly');

Artisan::command('bboxdigi:cat-move', function() {
    $gala   = Category::where('unique_name', 'gala-zivis')->first();
    $akcija = Category::where('unique_name', 'akcijas')->first();

    if ($akcija && $gala) {
        $akcija->beforeNode($gala)->save();
        Category::fixTree();
    }

})->describe('Moving categories in tree');


function rebuildCategoriesRecursive($parent)
{
    $item = ['id' => $parent->id];

    if (count($parent->children) > 0) {
        $item['children'] = [];

        foreach ($parent->children as $key => $category) {
            $item['children'][] = rebuildCategoriesRecursive($category);
        }
    }

    return $item;
}
