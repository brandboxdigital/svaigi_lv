<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/subscribe-newsletter', 'NewsletterApiController@subscribe');
Route::get('show-category-data', 'FrontApiController@index');
Route::get('get-products/{slug}', 'FrontApiController@showCategory');
Route::get('get-suppliers/{slug?}', 'FrontApiController@loadSuppliers');
Route::get('get-root-category/{slug?}', 'FrontApiController@getRootCategory');

Route::post('/subscribe-newsletter-comp', 'NewsletterApiController@subscribeBySubscriptionComponent')->name('api.subscribe-component-post');
Route::post('/subscribe-newsletter-blog', 'NewsletterApiController@subscribeByBlogSubscriptionComponent')->name('api.subscribe-blog-post');
