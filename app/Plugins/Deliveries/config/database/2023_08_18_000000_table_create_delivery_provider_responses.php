<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCreateDeliveryProviderResponses extends Migration
{
    public function up()
    {
        $this->down();

        Schema::create('delivery_provider_responses', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();

            $table->integer('delivery_method_id')->unsigned();
            $table->integer('order_id')->unsigned();

            $table->string('provider');
            $table->string('type');

            $table->longtext('data')->nullable();

            $table->timestamps();
        });

        Schema::table('order_headers', function (Blueprint $table) {
            $table->string('delivery_label')->nullable();
        });

        $this->seed();
    }

    public function down()
    {
        $this->tryToDrop('order_headers', 'delivery_label');

        Schema::dropIfExists('delivery_provider_responses');

        $routeName = 'orders.createAndDownloadDeliveryLabel';

        \App\Model\Admin\Menu::where('routeName', $routeName)
            ->get()
            ->each(function($menu) {
                try {
                    $menu->delete();
                } catch (\Throwable $th) {}
            });
    }

    public function seed()
    {
        $lastMenu = \App\Model\Admin\Menu::where('routeName', 'like', '%orders%')->orderBy('sequence', 'DESC')->first();
        $lastSeq = $lastMenu->sequence ?? 0;
        $lastSeq++;

        $menu = \App\Model\Admin\Menu::updateOrCreate(
            [
                'routeName' => 'orders.createAndDownloadDeliveryLabel',
                'slug'      => 'createAndDownloadDeliveryLabel/{order_id}',
            ],
            [
                'icon'        => 'fas fa-chart-line',
                'displayName' => 'Download Delivery Label',
                'action'      => '\App\Plugins\Orders\OrderController@createAndDownloadDeliveryLabel',
                'inMenu'      => '0',
                'sequence'    => $lastSeq,
                'parent_id'   => 73,
                'method'      => 'GET',
            ]);
    }

    public function tryToDrop($tableName, $col)
    {
        try {
            Schema::table($tableName, function($table) use ($col)
            {
                $table->dropColumn($col);
            });
        } catch (\Throwable $th) {}
    }
}
