<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeliveryProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('delivery_provider_settings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('provider');
            $table->string('key');
            $table->text('value')->nullable();

            $table->timestamps();

            $table->unique(['provider', 'key'], 'deliveryprovider_key');
        });

        Schema::table('deliveries', function (Blueprint $table) {
            $table->string('delivery_provider')->nullable();
        });

        DB::statement("ALTER TABLE deliveries MODIFY type ENUM('local', 'delivery', 'has_provider')");

        Schema::table('order_headers', function (Blueprint $table) {
            $table->text('delivery_provider_data_for_order')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_provider_settings');

        $this->tryToDropColumn('deliveries',   'delivery_provider');
        $this->tryToDropColumn('order_headers', 'delivery_provider_data_for_order');
    }


    private function tryToDropColumn($tableName, $columnName)
    {
        try {
            Schema::table($tableName, function (Blueprint $table) use ($columnName) {
                $table->dropColumn($columnName);
            });
        } catch (\Throwable $th) {}
    }
}
