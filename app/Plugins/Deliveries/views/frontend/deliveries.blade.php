@php
    $md = session()->get('marketDay');
    /** @var \App\Plugins\Deliveries\Model\Delivery $deliveries */
    $deliveries = $md->deliveries()->get();
@endphp

@foreach($deliveries as $delivery)
    @php
        /** @var Carbon\Carbon $dt */
        $dt = $md->date->copy();
        $modifiedMD = $dt->addDays($delivery->deliveryTime??0);
        $mdDate = $modifiedMD->format('j');
        // $month   = __("translations.".$modifiedMD->format('F'));
        $month   = $modifiedMD->format('m');
        $dayName = __("translations.".$modifiedMD->format('l'));

        $shortMessage = $delivery->getMeta('shortMessage') ?? null;

        // dd($md->availableTo);
    @endphp
    <div class="tab setDelivery{{ $delivery->id==$cart->delivery_id?" active":"" }}" data-run="{{ r('setDelivery', [$delivery->id]) }}">
        <div class="icon"></div>
        <h3>
            {{-- {!! _t('translations.marketDayDeliveryText', ["dayname" => $dayName, 'date' => $mdDate, 'month' => $month]) !!} --}}
            {!! _t('translations.marketDayDeliveryText', ["dayname" => $dayName, 'date' => $modifiedMD->format('d.m'), 'month' => '']) !!}
            {{-- {{$dayName}} {{$month}} {{$mdDate}} --}}
        </h3>
        <div class="text">
            @if ($shortMessage )
                <h3>
                    {!! $delivery->getMeta('shortMessage') !!}
                </h3>
            @endif
            <p>
                {!! __("deliveries.description.{$delivery->id}", ['freeAbove' => number_format($delivery->freeAbove,2), 'dayName' => substr($dayName,0, -1)]) !!}
            </p>
            <p>
                <a target="_blank" href="{{ $delivery->addressUrl }}">{{ __("deliveries.address.{$delivery->id}") }}</a>
            </p>
        </div>

        <div class="price">
            @if ($delivery->hasDynamicPrice())
                <small>
                    Pēc adreses norādes
                </small>
            @else
                {{ number_format($delivery->price * (1 + ($delivery->vat->amount / 100)), 2, '.', '')}}&nbsp;€
            @endif
        </div>
    </div>
@endforeach
