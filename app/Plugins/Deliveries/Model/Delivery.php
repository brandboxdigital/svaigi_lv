<?php

namespace App\Plugins\Deliveries\Model;

use App\BaseModel;
use App\Plugins\Vat\Model\Vat;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Builder;
use App\Plugins\MarketDays\Model\MarketDay;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Plugins\Orders\Functions\CartFunctions;
use App\Plugins\Deliveries\Classes\HasDeliveryProviders;

class Delivery extends BaseModel
{
    use SoftDeletes;
    use HasDeliveryProviders;

    use CartFunctions;

    public $fillable = [
        'deliveryTime',
        'price',
        'vat_id',
        'freeAbove',
        'type',
        'sequence',
        'delivery_provider',
        'addressUrl',
    ];
    public $metaClass = __NAMESPACE__ . '\DeliveryMeta';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'method_data',
    ];

    public $with = [
        'metaData',
    ];

    public static function boot()
    {
        static::addGlobalScope('order', function(Builder $builder) {
            $builder->orderBy('sequence', 'asc');
        });

        parent::boot();
    }

    public function vat() {
        return $this->belongsTo(Vat::class, 'vat_id', 'id');
    }

    public function getMarketDayListAttribute()
    {
        $mdays = [];
        $md = $this->marketDays;

        foreach ($md as $selectedMd) {
            $mdays[] = $selectedMd->marketDay[language()];
        }

        return implode(", ", $mdays);
    }

    public function marketDays()
    {
        return $this->belongsToMany(MarketDay::class);
    }

    public function getPriceAttribute($originalPrice)
    {
        $defaultPrice     = $this->attributes['price'] ?? null;
        $deliveryProvider = $this->delivery_provider_class;

        //calcualte price only in last step in Payment page
        $pageName = Route::current()->getName();

        if (!\str_contains($pageName, 'payment')) {
            return $defaultPrice;
        }

        if ($deliveryProvider && $deliveryProvider->hasDynamicPrice()) {
            $priceWithVat = $deliveryProvider->getDynamicPrice( $this->getCart() );
            $priceNoVat   = $this->removeVat($priceWithVat);

            // dd($priceNoVat, $defaultPrice, $priceNoVat ?? $defaultPrice, $this->getCart());
            return $priceNoVat ?? $defaultPrice;
        }

        return $defaultPrice;
    }


    /**
     * How to calcualte VAT. Never could remember the correct way :)
     * @see https://vatcalconline.com/
     *
     * @param null|bool|int|float|string $price
     *
     * @return float
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    private function removeVat($price)
    {
        if ($price) {
            $vatPercent = $this->vat->amount ?? null;
            $withoutVat = round($price / ( (100+$vatPercent) / 100 ), 2);

            return $withoutVat;
        }

        return null;
    }
}
