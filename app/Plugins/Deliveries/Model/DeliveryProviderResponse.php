<?php

namespace App\Plugins\Deliveries\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Model
 */
class DeliveryProviderResponse extends Model
{
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $jsonable = [
        'data',
        'delivery_method_id',
    ];

    public static function findLatestFromOrder($order, $provider)
    {
        return self::orderBy('id', 'desc')
            ->where('order_id', $order->id)
            ->where('provider', get_class($provider))
            ->first();
    }
}
