<?php

namespace App\Plugins\Deliveries\Model;

use App\BaseModel;
use App\Plugins\Deliveries\Classes\DeliveryProvider;
use App\Plugins\Vat\Model\Vat;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Builder;
use App\Plugins\MarketDays\Model\MarketDay;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryProviderSettings extends BaseModel
{
    private $ramCache = null;

    public $fillable = [
        'provider',
        'key',
        'value',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_key',
    ];

    // public static function boot()
    // {
    //     static::addGlobalScope('order', function(Builder $builder) {
    //         $builder->orderBy('sequence', 'asc');
    //     });

    //     parent::boot();
    // }

    public static function resetCache()
    {
        return Cache::forget(str_slug(self::class));
    }

    public static function getForProvider(DeliveryProvider $provider, string $key, $default = null, $fromCache = true)
    {
        $full_key = self::makeFullKey($provider, $key);
        $key      = str_slug($key);

        if ($fromCache) {
            $data = self::getCachedSettings();

            return $data[$full_key]['value'] ?? $default;
        } else {
            $setting = self::query()
                ->where($provider)
                ->where($key)
                ->first();

            return $setting->value ?? $default;
        }
    }

    public static function setForProvider(DeliveryProvider $provider, string $key, $value)
    {
        $key     = str_slug($key);

        $setting = self::updateOrCreate([
            'provider' => $provider,
            'key'      => $key,
        ], [
            'value'    => $value,
        ]);

        self::resetCache();

        return $setting;
    }

    public function getfullKeyAttribute()
    {
        return self::makeFullKey($this->provider, $this->key);
    }


    private static function getCachedSettings()
    {
        $data = Cache::rememberForever(str_slug(self::class), function () {
            $settings = self::get()
                ->mapWithKeys(function($setting, $key) {
                    return [$setting->full_key => $setting];
                })
                ->toArray();

            dd($settings);
        });

        return $data;
    }

    private static function makeFullKey($provider, $key)
    {
        $provider = str_slug($provider);
        $key      = str_slug($key);

        return "$provider.$key";
    }
}
