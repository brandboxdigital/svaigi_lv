<?php namespace App\Plugins\Deliveries\Classes\DeliveryProviders;

use Cache;
use Validator;
use GuzzleHttp\Client;
use ValidationException;
use Mijora\Omniva\Shipment\Label;

// use Bboxdigi\Orders\Models\Order;
use Illuminate\Support\Collection;

use Mijora\Omniva\OmnivaException;
use Mijora\Omniva\Shipment\Shipment;
use Mijora\Omniva\Locations\PickupPoints;
use App\Plugins\Deliveries\Model\Delivery;
use Mijora\Omniva\Shipment\ShipmentHeader;
use Mijora\Omniva\Shipment\Package\Address;
use Mijora\Omniva\Shipment\Package\Contact;
use Mijora\Omniva\Shipment\Package\Package;
use Mijora\Omniva\Shipment\Package\Measures;
use App\Plugins\Deliveries\Classes\DeliveryProvider;
use App\Plugins\Orders\Model\OrderHeader as Order;
use Mijora\Omniva\Shipment\Package\AdditionalService;

class OmnivaParcelProvider extends DeliveryProvider
{
    const IDENTIFYER = 'omniva-parcel';

    const LOCATIONS_URL = 'https://www.omniva.ee/locations.json';
    const CACHE_KEY = 'bboxdigi.deliveries.omniva.locations';

    const API_USERNAME = '7105248';
    const API_PASSWORD = 'o4cB!qPUm';

    private $locations = null;

    public function __construct($noCache = false)
    {
        $days = $this->days(10);

        if ($noCache) {
            Cache::forget(self::CACHE_KEY);
        }

        $this->locations = Cache::remember(self::CACHE_KEY, $days, function () {
            return $this->getLocationsFromApi();
        });

        // dd($this->filterCountryCode('ee')->pluck('ZIP'), $this->filterCountryCode('ee')->pluck('ZIP')->unique());
    }

    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function name(): string
    {
        return 'Omniva Parcel';
    }


    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function identifier(): string
    {
        return self::IDENTIFYER;
    }

    public function needsData()
    {
        return true;
    }

    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function settings(): array
    {
        return [
            // 'username' => [
            //     'label' => "Username",
            //     'type'    => 'text',
            //     'span' => 'auto',
            // ],
            // 'password' => [
            //     'label' => "Password",
            //     'type'    => 'text',
            //     'span' => 'auto',
            // ],

            'postcode' => [
                'label' => "Postcode",
                'type'    => 'text',
                'comment' => 'In format `LV-0000`',
                'span' => 'auto',
            ],
            'city' => [
                'label' => "City",
                'type'    => 'text',
                'span' => 'auto',
            ],
            'street' => [
                'label' => "Street",
                'type'    => 'text',
                'span' => 'auto',
            ],
            'mobile' => [
                'label' => "Mobile",
                'type'    => 'text',
                'span' => 'auto',
            ],
            'personname' => [
                'label' => "Personname",
                'placeholder' => "SIA Uzņēmums",
                'type'    => 'text',
                'span' => 'auto',
            ],
        ];
    }

    /**
     * @inheritDoc
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function getData($code = 'lv')
    {
        // if ( $cart = CartHelper::findCart() ) {
        // $code = \strtolower($cart->country_code ?? 'lv');
        // }

        return [
            'locations' => $this->getDataItemsByCountry($code),
        ];
    }

    /**
     * @inheritDoc
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function castDataToSaveToOrder($data)
    {
        $location = null;

        if ($this->isDataArrayKey($data)) {
            $location = (array) $this->getDataItemWhere('array_key', $data);
        }

        if (!$location) {
            $parsed = $this->parseDataIfArray($data);

            if ($parsed) {
                $location = $parsed['location'] ?? (array) $this->getDataItemWhere('ZIP', $parsed['ZIP'] ?? null);
            }
        }

        return $location;

        // return [
        //     'country_code' => ($location) ? $this->getCountryCode($location) : null,
        //     'address'      => ($location) ? $this->getAddress($location) : null,
        //     'comment'      => ($location) ? $this->getComment($location) : null,
        //     'location'     => $location,
        // ];
    }

    public function unCastDataSavedToOrder($data)
    {
        if ($this->isDataArrayKey($data)) {
            return $data;
        }

        $parsed = $this->parseDataIfArray($data);

        if ($parsed) {
            return $this->getDataItemWhere('array', $parsed ?? null);
        }

        return null;
    }

    /**
     * @inheritDoc
     *
     * @param [type] $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function formatSavedData($data)
    {
        $location = null;

        if ($this->isDataArrayKey($data)) {
            $location = (array) $this->getDataItemWhere('array_key', $data);
        }

        if (!$location) {
            $parsed = $this->parseDataIfArray($data);

            if ($parsed) {
                $location = $parsed['location'] ?? (array) $this->getDataItemWhere('ZIP', $parsed['ZIP'] ?? null);
            }
        }

        if ($location) {
            return implode(" ", [
                // "Pakomāts",
                $location["NAME"],
                $location["ZIP"],
                $this->getAddress($location),
                $this->getComment($location),
            ]);
        }

        return null;
    }

    /**
     * @inheritDoc
     *
     * @param int|json_stirng $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function formatSavedDataObject($data)
    {
        $location = null;

        if ($this->isDataArrayKey($data)) {
            $location = (array) $this->getDataItemWhere('array_key', $data);
        }

        if (!$location) {
            $parsed = $this->parseDataIfArray($data);

            if ($parsed) {
                $location = $parsed['location'] ?? (array) $this->getDataItemWhere('ZIP', $parsed['ZIP'] ?? null);
            }
        }

        return [
            'country_code' => ($location) ? $this->getCountryCode($location) : null,
            'address'      => ($location) ? $this->getAddress($location) : null,
            'city'         => ($location) ? $this->getCity($location) : null,
            'zipcode'      => ($location) ? $this->getZipcode($location) : null,
            'comment'      => ($location) ? $this->getComment($location) : null,
            'location'     => $location,
        ];
    }

    public function hasAddressOverride()
    {
        return true;
    }


// --------------------------------------------------------------
    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function hasCreateShipment()
    {
        return true;
    }

    public function createShipment($order = null)
    {
        if (!$order) {
            return null;
        }

        $deliveryProvider = $order->delivery->delivery_provider_class ?? null;

        if (!$deliveryProvider) {
            return null;
        }

        $barcodes = $this->createBarcode($order, $deliveryProvider);
        // $barcodes = ['CE370286538EE'];

        $username = self::API_USERNAME;
        $password = self::API_PASSWORD;

        $label = new Label(); //new label object
        $label->setAuth($username, $password); //set auth data
        //return labels pdf or thow OmnivaException on error
        //default function attributes downloadLabels($barcodes, $combine = true, $mode = 'I', $name = 'Omniva labels')
        //$barcodes - string or array of strings
        //$combine - if true, will add 4 labels per page, else 1 label per page
        //$mode - I: return directly to browser preview, S: return pdf as string data, D: force browser to download
        //$name - name of file

        return $this->saveOrderLabel($order->id . '.pdf', $label->downloadLabels($barcodes, true, 'S'));
    }

    private function createBarcode($order, $deliveryProvider)
    {
        $result = null;

        $deliveryProviderData = $deliveryProvider->formatSavedDataObject($order->delivery_provider_data_for_order);

        $deliveryDataObject = [
            'order_number'      => $order->order_number_string,
            'zip'               => $deliveryProviderData['zipcode'],
            'short_zip'         => $deliveryProviderData['location']['ZIP'],
            'country_code'      => $deliveryProviderData['country_code'],
            'city'              => $deliveryProviderData['city'],
            'client_email'      => $order->user_email,
            'client_phone'      => $order->user_phone,
            'client_full_name'  => $order->user_full_name,
        ];

        // dd($deliveryDataObject, $deliveryProviderData);

        \Log::debug("OmnivaParcelProvider::createBarcode", $deliveryDataObject);

        try {
            $username = self::API_USERNAME;
            $password = self::API_PASSWORD;

            //create new shipment object
            $shipment = new Shipment();
            $shipment
                    ->setComment('')  //set comment, optional
                    ->setShowReturnCodeEmail(false) //return code in receiver email, optional
                    ->setShowReturnCodeSms(false); //return code in receiver sms, optional

            //new shipment header object, required
            $shipmentHeader = new ShipmentHeader();
            $shipmentHeader
                    ->setSenderCd($username) //set username
                    ->setFileId(date('Ymdhis')); //set date of shipment creation

            //assign header to shipment
            $shipment->setShipmentHeader($shipmentHeader);
            $terminal = $deliveryDataObject['short_zip'];

            $service = 'PA';
            //new shipment package object, required
            $package = new Package();
            $package->setId($deliveryDataObject['order_number']) //id number, optional
                    ->setService($service); //service code of package

            //create additional services and add to package, optional
            $additionalService = (new AdditionalService())->setServiceCode('ST');
            $package->setAdditionalServices([$additionalService]);
            //for available service codes, you can view at https://www.omniva.lt/public/files/failid/omniva-service-codes-lt-eng.pdf

            //set package size and weight
            $measures = new Measures();
            $measures
                    ->setWeight(15); //weight in kg, required
                    // ->setLength(9) //dimension in meter, optional
                    // ->setHeight(2) //dimension in meter, optional
                    // ->setWidth(3); //dimension in meter, optional
            $package->setMeasures($measures); //set package measurements

            //receiver contact object
            $receiverContact = new Contact();
            //receiver address object

            // $fixedAddressZip = $this->fixZip($order['delivery_meta']['location']['ZIP'], $order['delivery_meta']['country_code']);

            $address = new Address();
            $address->setCountry( $deliveryDataObject['country_code'] ); //set country code
            $address->setPostcode( $deliveryDataObject['short_zip'] ); //set postcode
            $address->setDeliverypoint( $deliveryDataObject['city'] ); //set city
            $address->setOffloadPostcode( $deliveryDataObject['short_zip'] ); //set terminal post code if sending to parcel terminal
                    // ->setStreet($order['shipping_address']['street_addr']); //set street
            $receiverContact
                    ->setAddress($address) //assign address to receiver
                    ->setEmail( $deliveryDataObject['client_email'] ) //set receiver email
                    ->setMobile( $deliveryDataObject['client_phone'] ) //set receiver phone
                    ->setPersonName( $deliveryDataObject['client_full_name'] ); //set receiver full name
            $package->setReceiverContact($receiverContact); //assign receiver to package

            $senderContact = new Contact(); //sender contact object
            $s_address = new Address(); //sender address object
            $s_address
                ->setCountry('LV') //set country code
                ->setPostcode( 'LV-1046' ) //set postcode
                ->setDeliverypoint( 'Rīga' ) //set city
                ->setStreet( 'Margrietas iela 7' ); //set street
            $senderContact
                    ->setAddress($s_address) //assign address to sender
                    ->setMobile( '+37124335225' ) //set sender phone
                    ->setPersonName( 'SIA "Svaigi"' ); //set sender full name
            $package->setSenderContact($senderContact); //assign sender to package

            //set packages to shipment, in this case we assign 2 same packeges for shipment
            // $shipment->setPackages([$package, $package]);
            $shipment->setPackages([$package]);

            //set auth data
            $shipment->setAuth($username, $password);

            //register shipment to Omniva, on success, will return $result['barcodes'], else throw OmnivaException exception with error message
            $result = $shipment->registerShipment();

            $this->saveShipmentResponse($order, __METHOD__, $result);

            return $result['barcodes'] ?? null;

        } catch (OmnivaException $th) {
            \Log::error([
                "Omniva parcel createBarcode failed",
                $result,
                $th->getMessage(),
                $order->toArray(),
            ]);

        } catch (\Throwable $th) {
            \Log::error($th);
        }

        return $result;
    }

// --------------------------------------------------------------
    private function isDataArrayKey($data)
    {
        return is_numeric($data);
    }

    /**
     * Parses data var to array. Understands array and json
     *
     * @param string|json $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    private function parseDataIfArray($data)
    {
        if (is_string($data)) {
            try {
                $data = json_decode($data, true);
            } catch (\Throwable $th) {}
        }

        if (is_array($data)) {
            return (array) $data;
            // return $data['location'] ?? $data;
        }

        return null;
    }

    /**
     * ????
     * @return Collection
     */
    public function getDataItemsByCountry($code = null)
    {
        // $cart = CartModel::byUser(Auth::getUser());
        // $code = null;

        // if ($country = $cart->shipping_address->country) {
        //     $code = str_slug($country->code);
        // }

        $locations = collect($this->locations);

        if($code) {
            $locations = $locations
                ->filter(function ($location) use ($code) {
                    return str_slug($location->A0_NAME) == $code;
                });
        }

        return $locations->map(function($loc) {
            $loc->address = $this->getAddress($loc);
            $loc->comment = $this->getComment($loc);

            return $loc;
        });
    }

    public function getDataItemWhere($field, $value)
    {
        // Return if $field is array key
        if ($field == 'array_key') {
            return $this->locations[$value];
        }

        // !! Special case: Returns Only key.
        if ($field == 'array') {
            $zip = $value['ZIP'] ?? null;

            if ($zip == null) {
                return null;
            };

            return collect($this->locations)
                ->filter(function ($location) use ($field, $zip) {
                    return $location->ZIP == $zip;
                })
                ->keys()
                ->first();
        }

        // Search by field
        return collect($this->locations)
            ->filter(function ($location) use ($field, $value) {
                return $location->$field == $value;
            })
            ->first();
    }

    public function getAddress($item, $join = true)
    {
        $item = (object) $item;

        $collection = collect([
            ($item->A1_NAME != 'NULL') ? $item->A1_NAME : null,
            ($item->A2_NAME != 'NULL') ? $item->A2_NAME : null,
            ($item->A3_NAME != 'NULL') ? $item->A3_NAME : null,
            ($item->A4_NAME != 'NULL') ? $item->A4_NAME : null,
            ($item->A5_NAME != 'NULL') ? $item->A5_NAME : null,
            ($item->A6_NAME != 'NULL') ? $item->A6_NAME : null,
            ($item->A7_NAME != 'NULL') ? $item->A7_NAME : null,
            ($item->A8_NAME != 'NULL') ? $item->A8_NAME : null,
        ])->filter();

        if ($join) {
            return $collection->implode(', ');
        } else {
            return $collection;
        }
    }

    public function getCity($item)
    {
        $item = (object) $item;

        $city = null;

        try {
            $city = ($item->A1_NAME != 'NULL') ? $item->A1_NAME : null;
        } catch (\Throwable $th) {}

        return $city;
    }

    public function getZipcode($item)
    {
        $item = (object) $item;

        $zipcode = null;

        try {
            $zipcode = ($item->ZIP != 'NULL') ? $item->ZIP : null;
        } catch (\Throwable $th) {}

        return 'LV-' . ltrim($zipcode, 'LV-');
    }

    public function getCountryCode($item)
    {
        $item = (object) $item;

        $countryCode = null;

        try {
            $countryCode = ($item->A0_NAME != 'NULL') ? $item->A0_NAME : null;
        } catch (\Throwable $th) {}

        return $countryCode;
    }

    public function getComment($item, $join = true)
    {
        $item = (object) $item;

        return $item->comment_lav ?? null;
    }

    // public function updateTrackingInfo(Order $order = null)
    // {
    //     $providerResponse = DeliveryProviderResponse::findLatestFromOrder($order, $this);

    //     if (!$providerResponse) {
    //         return null;
    //     }

    //     foreach ($providerResponse->data['barcodes'] ?? [] as $key => $value) {
    //         $barcode = $value;
    //     }

    //     $countryCode = 'lv';
    //     if ($order->country) {
    //         $countryCode = $order->country->code ?? 'lv';
    //     }

    //     $order->tracking_number = $barcode;
    //     $order->tracking_url    = "https://mana.omniva.lv/track/$barcode/?language=$countryCode";

    //     $order->save();
    // }


    private function getLocationsFromApi()
    {
        $client = new Client();

        $response = $client->get(self::LOCATIONS_URL);

        $json = json_decode($response->getBody());

        return $json;
    }

    protected function days(int $days)
    {
        $secondsInDay = 60 * 60 * 24;
        return $days * $secondsInDay;
    }

    private function onlyDigits($string)
    {

        return preg_replace("/[^0-9]/", "",$string);

    }
    private function fixPhone($number, $countryid = null)
    {
        $country = Country::find($countryid);

        if ( $country && $country->calling_code ?? null) {
            $number = ltrim($number, $country->calling_code);
            $number = ltrim($number, "+" . $country->calling_code);
        }

        return $this->onlyDigits($number);
    }

    private function fixZip($string, $countryCode)
    {
        $zipParts = [
            strtoupper($countryCode),
            $this->onlyDigits($string),
        ];

        return \implode('-', $zipParts);
    }
}
