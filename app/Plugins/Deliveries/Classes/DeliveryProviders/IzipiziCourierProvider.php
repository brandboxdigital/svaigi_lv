<?php namespace App\Plugins\Deliveries\Classes\DeliveryProviders;

use Cache;
use Validator;
use GuzzleHttp\Client;
use ValidationException;
use Illuminate\Support\Collection;
use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Deliveries\Model\Delivery;
use App\Plugins\Deliveries\Classes\DeliveryProvider;

class IzipiziCourierProvider extends DeliveryProvider
{
    const IDENTIFYER = 'izipizi-courier';

    const API_URL_BASE = "https://staging.izipiziapp.com/";
    // const API_USERNAME = '7105248';
    // const API_PASSWORD = 'o4cB!qPUm';

    private $client = null;

    public function __construct($noCache = false)
    {
        // $days = $this->days(10);

        // if ($noCache) {
        //     Cache::forget(self::CACHE_KEY);
        // }

        // $this->locations = Cache::remember(self::CACHE_KEY, $days, function () {
        //     return $this->getLocationsFromApi();
        // });

        // dd($this->filterCountryCode('ee')->pluck('ZIP'), $this->filterCountryCode('ee')->pluck('ZIP')->unique());
    }

    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function name(): string
    {
        return 'Izi Pizi Courier';
    }


    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function identifier(): string
    {
        return self::IDENTIFYER;
    }

    public function needsData()
    {
        return false;
    }

    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function settings(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function getData($code = 'lv')
    {

        $colorArray = [
            1 => '#FFFFFF',
            2 => '#9B90AD',
            3 => '#4B9068',
            4 => '#7DAE92',
            5 => '#D5D1DC',
            6 => '#DBECE2',
            7 => '#685884',
            // 8 => '#685884',
        ];

        $prices = $this->getApiPrices()->map(function($price, $zone) use ($colorArray) {
            if (isset($colorArray[$zone])) {
                return [
                    'price'     => \number_format(intval($price) / 100, 2, ',', ''),
                    'zonecolor' => $colorArray[$zone],
                ];
            }

            return null;

        })->filter();


        return [
            'prices' => $prices
        ];

        return [];
    }

    /**
     * @inheritDoc
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function castDataToSaveToOrder($data)
    {
        return null;
    }

    public function unCastDataSavedToOrder($data)
    {
        return null;
    }

    /**
     * @inheritDoc
     *
     * @param [type] $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function formatSavedData($data)
    {
        return null;
    }

    /**
     * @inheritDoc
     *
     * @param int|json_stirng $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function formatSavedDataObject($data)
    {
        return null;
    }

    public function hasAddressOverride()
    {
        return false;
    }


// --------------------------------------------------------------
    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function hasCreateShipment()
    {
        return false;
    }

// --------------------------------------------------------------
    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function hasDynamicPrice(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function getDynamicPrice(OrderHeader $order)
    {
        if ( $priceInCents = $this->getPriceFromZip($order->postcode) ) {
            $additionCoeficientInCents = 50;

            $price = intval($priceInCents + $additionCoeficientInCents) / 100;

            return $price;
        }

        return null;
    }
// --------------------------------------------------------------

    private function getCurlClient()
    {
        if (!$this->client) {

            $this->client = new Client([
                'base_uri' => self::API_URL_BASE,
                'timeout'  => 2.0,
            ]);
        }

        return $this->client;
    }

    private function getApiPrices()
    {
        $client = $this->getCurlClient();

        try {
            $response = $client->get('/getPrices');

            $body = json_decode($response->getBody());

            return collect($body)
                ->pluck('price', 'zone');

        } catch (\Throwable $th) {
            \Log::error($th);
        }

        return null;
    }
    private function getApiZones()
    {
        $client = $this->getCurlClient();

        try {
            $response = $client->get('/getAvailableZones');

            $body = json_decode($response->getBody());

            return $body;
        } catch (\Throwable $th) {
            \Log::error($th);
        }

        return null;
    }

    private function getPriceFromZip($zip)
    {
        preg_match('/\d{4}/m', $zip, $matches);

        $shortZip = $matches[0] ?? null;

        if (!$shortZip) {
            return null;
        }

        $zones = $this->getApiZones();
        $zone  = collect($zones)
            ->filter(function($zonePostcodes) use ($shortZip) {
                return collect($zonePostcodes)->search($shortZip) !== false;
            })
            ->keys()
            ->first();

        if ($zone) {
            $prices = $this->getApiPrices();

            return $prices[$zone] ?? null;
        }

        return null;
    }
}
