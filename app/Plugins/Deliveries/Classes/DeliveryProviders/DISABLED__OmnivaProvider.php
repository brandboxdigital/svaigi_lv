<?php namespace App\Plugins\Deliveries\Classes\DeliveryProviders;

use Validator;
use ValidationException;
use App\Plugins\Orders\Model\OrderHeader as Order;

use Mijora\Omniva\Shipment\Label;
// use Mijora\Omniva\Shipment\Package\AdditionalService;
use Mijora\Omniva\OmnivaException;
use Mijora\Omniva\Shipment\Shipment;
// use Mijora\Omniva\Shipment\Package\Cod;
use Mijora\Omniva\Locations\PickupPoints;
use Mijora\Omniva\Shipment\ShipmentHeader;
use Mijora\Omniva\Shipment\Package\Address;
use Mijora\Omniva\Shipment\Package\Contact;
use Mijora\Omniva\Shipment\Package\Package;
use Mijora\Omniva\Shipment\Package\Measures;
use Mijora\Omniva\Shipment\Package\AdditionalService;
use App\Plugins\Deliveries\Classes\DeliveryProvider;

class OmnivaProvider extends DeliveryProvider
{
    const IDENTIFYER = 'omniva';

    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function name(): string
    {
        return 'Omniva Courier';
    }


    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function identifier(): string
    {
        return self::IDENTIFYER;
    }


    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function settings(): array
    {
        return [
            // 'username' => [
            //     'label' => "Username",
            //     'type'    => 'text',
            //     'span' => 'auto',
            // ],
            // 'password' => [
            //     'label' => "Password",
            //     'type'    => 'text',
            //     'span' => 'auto',
            // ],

            'postcode' => [
                'label' => "Postcode",
                'type'    => 'text',
                'comment' => 'In format `LV-0000`',
                'span' => 'auto',
            ],
            'city' => [
                'label' => "City",
                'type'    => 'text',
                'span' => 'auto',
            ],
            'street' => [
                'label' => "Street",
                'type'    => 'text',
                'span' => 'auto',
            ],
            'mobile' => [
                'label' => "Mobile",
                'type'    => 'text',
                'span' => 'auto',
            ],
            'personname' => [
                'label' => "Personname",
                'placeholder' => "SIA Uzņēmums",
                'type'    => 'text',
                'span' => 'auto',
            ],
        ];
    }


// --------------------------------------------------------------
    /**
     * @inheritDoc
     *
     * @return string
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function hasCreateShipment()
    {
        return true;
    }

    public function createShipment($order = null)
    {
        if (!$order) {
            return null;
        }

        $barcodes = $this->createBarcode($order);
        // $barcodes = 'CE370069944EE';
        // CE370069944EE
        // CE370205551EE

        $username = '7100758';
        $password = 'Tkh9Kk8y';

        $label = new Label(); //new label object
        $label->setAuth($username, $password); //set auth data
        //return labels pdf or thow OmnivaException on error
        //default function attributes downloadLabels($barcodes, $combine = true, $mode = 'I', $name = 'Omniva labels')
        //$barcodes - string or array of strings
        //$combine - if true, will add 4 labels per page, else 1 label per page
        //$mode - I: return directly to browser preview, S: return pdf as string data, D: force browser to download
        //$name - name of file

        return $this->saveOrderLabel($order->id . '.pdf', $label->downloadLabels($barcodes, true, 'S'));
    }

    private function createBarcode($order = null)
    {
        $result = null;

        // try {
            $username = '7100758';
            $password = 'Tkh9Kk8y';

            //create new shipment object
            $shipment = new Shipment();
            $shipment
                    ->setComment('')  //set comment, optional
                    ->setShowReturnCodeEmail(false) //return code in receiver email, optional
                    ->setShowReturnCodeSms(false); //return code in receiver sms, optional

            //new shipment header object, required
            $shipmentHeader = new ShipmentHeader();
            $shipmentHeader
                    ->setSenderCd($username) //set username
                    ->setFileId(date('Ymdhis')); //set date of shipment creation

            //assign header to shipment
            $shipment->setShipmentHeader($shipmentHeader);

            //new shipment package object, required
            $package = new Package();
            $package->setId($order->order_number) //id number, optional
                    ->setService('QH'); //service code of package

            //set package size and weight
            $measures = new Measures();
            $measures
                    ->setWeight(1); //weight in kg, required
                    // ->setLength(9) //dimension in meter, optional
                    // ->setHeight(2) //dimension in meter, optional
                    // ->setWidth(3); //dimension in meter, optional
            $package->setMeasures($measures); //set package measurements

            //receiver contact object
            $receiverContact = new Contact();
            //receiver address object
            $terminal = null;
            $address = new Address();


            $fixedAddressZip = $this->fixZip($order->shipping_address['zip'], $order->shipping_address['country']['code']);

            $address
                    ->setCountry($order->shipping_address['country']['code']) //set country code
                    ->setPostcode( $fixedAddressZip ) //set postcode
                    ->setDeliverypoint($order->shipping_address['city']) //set city
                    // ->setOffloadPostcode($terminal) //set terminal post code if sending to parcel terminal
                    ->setStreet($order->shipping_address['street_addr']); //set street
            $receiverContact
                    ->setAddress($address) //assign address to receiver
                    ->setEmail($order->shipping_address['email']) //set receiver email
                    ->setMobile( $this->fixPhone($order->phone, $order->country_id ?? null) ) //set receiver phone
                    ->setPersonName($order->shipping_address['name'] . ' ' . $order->shipping_address['surname']); //set receiver full name
            $package->setReceiverContact($receiverContact); //assign receiver to package

            $senderContact = new Contact(); //sender contact object
            $s_address = new Address(); //sender address object
            $s_address
                ->setCountry('LV') //set country code
                ->setPostcode( $this->getSetting('postcode', 'LV-1050') ) //set postcode
                ->setDeliverypoint( $this->getSetting('city', 'Rīga') ) //set city
                ->setStreet( $this->getSetting('street', 'Grēcinieku iela 11') ); //set street
            $senderContact
                    ->setAddress($s_address) //assign address to sender
                    ->setMobile( $this->getSetting('mobile') ) //set sender phone
                    ->setPersonName( $this->getSetting('personname', 'SIA Baltu Rotas') ); //set sender full name
            $package->setSenderContact($senderContact); //assign sender to package

            //set packages to shipment, in this case we assign 2 same packeges for shipment
            // $shipment->setPackages([$package, $package]);
            $shipment->setPackages([$package]);

            //set auth data
            $shipment->setAuth($username, $password);

            //register shipment to Omniva, on success, will return $result['barcodes'], else throw OmnivaException exception with error message
            $result = $shipment->registerShipment();

            $this->saveShipmentResponse($order, __METHOD__, $result);

            return $result['barcodes'] ?? null;

        // } catch (OmnivaException $th) {
        //     \Log::error([
        //         "Omniva Courier createBarcode failed",
        //         $result,
        //         $th->getMessage(),
        //         $order->toArray(),
        //     ]);

        // } catch (\Throwable $th) {
        //     \Log::error($th);
        // }

        return $result;
    }

    /**
     * @inheritDoc
     *
     * @return bool
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function hasDynamicPrice(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     *
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function getDynamicPrice($cart)
    {
        return 9999;
    }

    // public function updateTrackingInfo(Order $order = null)
    // {
    //     $providerResponse = DeliveryProviderResponse::findLatestFromOrder($order, $this);

    //     if (!$providerResponse) {
    //         return null;
    //     }

    //     foreach ($providerResponse->data['barcodes'] ?? [] as $key => $value) {
    //         $barcode = $value;
    //     }

    //     $countryCode = 'lv';
    //     if ($order->country) {
    //         $countryCode = $order->country->code ?? 'lv';
    //     }

    //     $order->tracking_number = $barcode;
    //     $order->tracking_url    = "https://mana.omniva.lv/track/$barcode/?language=$countryCode";

    //     $order->save();
    // }

    private function onlyDigits($string)
    {
        return preg_replace("/[^0-9]/", "",$string);

    }

    private function fixPhone($number, $countryid = null)
    {
        $country = Country::find($countryid);

        if ( $country && $country->calling_code ?? null) {
            $number = ltrim($number, $country->calling_code);
            $number = ltrim($number, "+" . $country->calling_code);
        }

        return $this->onlyDigits($number);
    }

    private function fixZip($string, $countryCode)
    {
        $zipParts = [
            strtoupper($countryCode),
            $this->onlyDigits($string),
        ];

        return \implode('-', $zipParts);
    }
}
