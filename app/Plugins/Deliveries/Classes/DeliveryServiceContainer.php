<?php

namespace App\Plugins\Deliveries\Classes;

use App\Plugins\Orders\Model\OrderHeader as Order;
use App\Plugins\Orders\Model\OrderHeader;

// use Bboxdigi\Deliveries\Models\DeliveryGatewaySettings;
// use Bboxdigi\Deliveries\Models\DeliveryProviderResponse;

class DeliveryServiceContainer
{
    private $providers = [];

    /**
     * Class constructor.
     */
    public function __construct()
    {
        $scandir = __DIR__ . '/DeliveryProviders';

        $this->providers = collect(scandir($scandir))
            ->filter(function($item) {
                return !str_contains(strtolower($item), 'disabled');
            })
            ->filter(function($item) {
                return str_contains(strtolower($item), 'php');
            })
            ->mapWithKeys(function($file) {
                $name = str_replace('.php', '', $file);
                $class = '\\App\\Plugins\\Deliveries\\Classes\\DeliveryProviders\\' . $name;

                if (class_exists($class)) {
                    $class = new $class;
                    return [$class->identifier() => $class];
                }

                return null;
            })
            ->filter();

        // $this->providers->each(function ($provider) {
        //     $this->encryptable = array_merge($this->encryptable, $provider->encryptedSettings());
        // });
    }

    public function getAllDeliveryProviders()
    {
        return $this->providers;
    }

    public function getDeliveryProvider($identifier = null)
    {
        if ($this->providers[$identifier] ?? null) {
            return $this->providers[$identifier];
        }

        return null;
    }

    public function getAllDeliveryProvidersOptions()
    {
        $toRet = [];

        foreach ($this->providers ?? [] as $key => $provider) {
            $toRet[$key] = (object)[
                'id' => $key,
                'name' => $provider->name()
            ];
        }

        return $toRet;
    }
}
