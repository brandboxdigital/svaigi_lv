<?php

namespace App\Plugins\Deliveries\Classes;

use Illuminate\Support\Facades\File;

// use Bboxdigi\Deliveries\Models\DeliveryGatewaySettings;
// use Bboxdigi\Deliveries\Models\DeliveryProviderResponse;


use Illuminate\Support\Facades\Log;
use App\Plugins\Deliveries\Model\Delivery;
use App\Plugins\Orders\Model\OrderHeader as Order;
use App\Plugins\Deliveries\Model\DeliveryProviderResponse;
use App\Plugins\Deliveries\Model\DeliveryProviderSettings;
use Symfony\Component\HttpFoundation\File\File as FileObject;

abstract class DeliveryProvider
{
    protected $parentDeliveryModel = null;

    /**
     * Return the display name of this payment provider.
     *
     * @return string
     */
    abstract public function name(): string;


    /**
     * Return a unique identifier for this payment provider.
     *
     * @return string
     */
    abstract public function identifier(): string;


    /**
     * Return any custom backend settings fields configuration.
     *
     * @return array
     */
    abstract public function settings(): array;

    /**
     * Get the settings of this PaymentProvider.
     *
     * @return \October\Rain\Support\Collection
     */
    public function getSettings()
    {
        return collect($this->settings())->mapWithKeys(function ($settings, $key) {
            // return [$key => config("payment.klix.$key")];
            return [$key => DeliveryProviderSettings::getForProvider($this, $key)];
        });
    }

    /**
     * Get the settings of this PaymentProvider.
     *
     * @return \October\Rain\Support\Collection
     */
    public function getSetting($key = null, $default = null)
    {
        $settings = $this->getSettings();

        if ( isset($settings[$key]) && !empty($settings[$key]) ) {
            return $settings[$key];
        }

        return $default;
    }

    public function needsData()
    {
        return false;
    }

    /**
     * This function returns delivery method specific data accessable
     * trough \Bboxdigi\Deliveries\Models\DeliveryMethod->method_data attribute
     *
     * Override this function to add specific data
     *
     * @return null
     */
    public function getData()
    {
        return null;
    }

    /**
     * Format data received from frontend to save Order.
     *
     * So that afterwards formatSavedData and formatSavedDataObject
     * can interpret the data.
     *
     * Override this function to add specific data
     *
     * @return null
     */
    public function castDataToSaveToOrder($data)
    {
        return null;
    }

    public function unCastDataSavedToOrder($data)
    {
        return null;
    }

    /**
     * Function formats data saved in Order
     *
     * @param [mixed] $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function formatSavedData($data)
    {
        return null;
    }

    /**
     * Function returns data saved in Order
     *
     * @param [mixed] $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function formatSavedDataObject($data)
    {
        return null;
    }

    /**
     * Has Label creation functionality
     *
     * @return bool
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function hasCreateShipment()
    {
        return false;
    }

    public function hasAddressOverride()
    {
        return false;
    }


    public function createShipment($order = null)
    {
        return null;
    }

    /**
     * Returns info about price property of the Delivery
     * provider beeing dynamic ar API based.
     *
     * @return bool
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function hasDynamicPrice(): bool
    {
        return false;
    }

    /**
     * Gets the price value.
     * By default it's not dynamic so it is based on the Delivery model.
     *
     * @param Cart $cart
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function getDynamicPrice(Order $order)
    {
        return null;
    }

    /**
     * Save Order label in correct place
     *
     * @param [type] $filename
     * @param [type] $content
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function saveOrderLabel($filename, $content = null)
    {
        $deliveryIdentifyer = $this->identifier();

        $path = storage_path("app/delivery/$deliveryIdentifyer");
        $fullPath = "$path/$filename";

        if ( !File::isDirectory($path) ) {
            File::makeDirectory($path, 0755, true);
        }

        try {
            file_put_contents($fullPath, $content);

            $fileObject = new FileObject($fullPath);

            return $fileObject;
        } catch (\Throwable $th) {
            Log::error([
                "Could not save Order Label",
                $filename,
                $th->getMessage(),
                $th,
            ]);
        }

        return null;
    }

    public function saveShipmentResponse(Order $order, $type, $data)
    {
        DeliveryProviderResponse::unguard();

        $deliveryResponse = DeliveryProviderResponse::create([
            "delivery_method_id"  => $order->delivery_id,
            "order_id"            => $order->id,
            "provider"            => get_class($this),
            "type"                => $type,
            "data"                => json_encode($data),
        ]);

        return $deliveryResponse;
    }

    public function updateTrackingInfo(Order $order = null)
    {
        return null;
    }
}
