<?php

namespace App\Plugins\Deliveries\Classes;

use App\Plugins\Orders\Model\OrderHeader as Order;
use App\Plugins\Orders\Model\OrderHeader;

// use Bboxdigi\Deliveries\Models\DeliveryGatewaySettings;
// use Bboxdigi\Deliveries\Models\DeliveryProviderResponse;

trait HasDeliveryProviders
{

    public function getDeliveryProviderClassAttribute()
    {
        if ($this->type == 'has_provider') {

            $deliveryProviderObject = app('delivery-providers')
                ->getDeliveryProvider($this->attributes['delivery_provider'] ?? null);

            if ($deliveryProviderObject) {
                return $deliveryProviderObject;
            }
        }

        return null;
    }

    public function getMethodDataAttribute()
    {
        $provider = $this->getDeliveryProviderClassAttribute();

        if ($provider) {
            return $provider->getData();
        }
    }

    public function saveMethodDataToOrder($order, $data)
    {
        $provider = $this->getDeliveryProviderClassAttribute();

        if ($provider && $order instanceof OrderHeader) {
            $castedData = $provider->castDataToSaveToOrder($data);

            $order->update([
                'delivery_provider_data_for_order' => $castedData,
            ]);
        }
    }

    public function getMethodDataFromOrder($order)
    {
        $provider = $this->getDeliveryProviderClassAttribute();

        if ($provider && $order instanceof OrderHeader) {
            $data = $order->delivery_provider_data_for_order ?? null;

            return $provider->formatSavedData($data);
        }

        return null;
    }

    public function getMethodDataForFrontend($order)
    {
        $provider = $this->getDeliveryProviderClassAttribute();

        if ($provider && $order instanceof OrderHeader) {
            $data = $order->delivery_provider_data_for_order ?? null;

            return $provider->unCastDataSavedToOrder($data);
        }

        return null;
    }

    public function needAddess()
    {
        if ($this->type == 'local') {
            return false;
        }

        if ($this->type == 'has_provider') {
            $provider = $this->getDeliveryProviderClassAttribute();

            // dd($provider->hasAddressOverride(), $provider)

            if ($provider) {
                return $provider->hasAddressOverride() ? false : true;
            }
        }

        return true;
    }

    public function needsMethodData()
    {
        $provider = $this->getDeliveryProviderClassAttribute();

        if ($provider) {
            return $provider->needsData();
        }

        return false;
    }

    public function hasDynamicPrice()
    {
        $provider = $this->getDeliveryProviderClassAttribute();

        if ($provider) {
            return $provider->hasDynamicPrice();
        }

        return false;
    }
}
