<?php

namespace App\Plugins\Orders;


use App\User;
use App\Schedules;
use Carbon\Carbon;
use Dompdf\Dompdf;
use function MongoDB\BSON\toJSON;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use App\Console\Commands\OrderImport;
use Illuminate\Support\Facades\Route;
use App\Plugins\Admin\AdminController;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use App\Plugins\Orders\Functions\Orders;
use App\Plugins\Orders\Model\OrderLines;
use App\Console\Commands\SendOrderEmails;
use App\Mail\OrderStatusUpdated;
use App\Plugins\Orders\Model\OrderHeader;
use Illuminate\Database\Eloquent\Builder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Mail;

use App\Plugins\Orders\Model\OriginalOrder;
use App\Plugins\DiscountCodes\Model\DiscountCode;


class OrderController extends AdminController {

    use Orders;

    public function index($search = false) {
        $cr = explode(".", Route::currentRouteName());

        if (!$search && ($cr[1] ?? false) == 'search') {
            return redirect()->route($cr[0]);
        }

        $orders = $this->getFilteredResult($search, $trashed ?? false);

        // code for test from front
        if(isset($_GET["KU"])) {
            $schedule = Schedules::find(355);
            $import = new OrderImport();
            //$export = new SendOrderEmails();
            //$export->sendEmails($schedule);
            $import->createPDF($schedule);
            //$import->import($schedule);


        }
        return view('admin.elements.table',
            [
                'tableHeaders' => $this->getList(),
                'header' => 'Orders',
                'list' => $orders,
                'idField' => 'name',
                'destroyName' => 'Order',
                'filters' => method_exists($this, 'getFilters') ? $this->getFilters() : [],
                'logButton' => view('admin.partials.log', ['logTypes' => 'orderExport,orderExportToPdf,orderImport,orderExport,createPDF,sendOrderEmails,orderSummary'])->render(),
                'currentFilters' => session('order_filters'),
                'operations' => view("Orders::partials.extraButtons")->render(),
                'js' => [
                    'js/orderlist.js',
                ],
                'massActions' => [
                    (object)['url' => route('orders.summary'), 'class' => 'btn-info', 'icon' => 'fas fa-book', 'label' => 'Order Sumary'],
                    (object)['url' => route('orders.exportOrderstopdf'), 'class' => 'btn-warning', 'icon' => 'fas fa-download', 'label' => 'Export Orders To PDF'],

                    (object)['url' => route('orders.exportOrders'), 'class' => 'btn-warning', 'icon' => 'fas fa-download', 'label' => 'Export Orders'],
                    (object)['url' => route('orders.destroy'), 'class' => 'btn-danger', 'callback' => 'replaceTable', 'icon' => 'fas fa-trash', 'label' => 'Delete'],
                ],
            ]);
    }

    public function setPaid($id) {
        request()->validate(['amount' => 'numeric']);
        /** @var OrderHeader $oh */
        $oh = OrderHeader::find($id);
        $oh->update(['paid' => request('amount')]);

        $items = $oh->items()->get();

        return ['status' => true, 'message' => 'Payment registered', "amount" => $oh->paid, 'orderContent' => view('Orders::products',
            [
                'order' => $oh,
                'items' => $items,
                'displayValues' => $this->getDisplayValuesOrderInfo($oh, $items),
                'totalPrices' => getCartTotals($oh, [], $original??false),
            ])->render()];
    }

    public function setCoupon($id) {
        /** @var OrderHeader $oh */
        $oh = OrderHeader::find($id);

        /** @var DiscountCode $discount_code */
        $discount_code = DiscountCode::where('code', request('amount'))->first();
        if (!is_null($discount_code->uses)) {
            $discount_code->increment('uses', -1);
        }
        $oh->update([
            'discount_code'             => request('amount'),
            'discount_amount'           => $discount_code->amount,
            'discount_target'           => $discount_code->applied,
            'discount_type'             => $discount_code->unit,
            'discount_items'            => json_decode($discount_code->getOriginal('items')),
            'discount_applicable_from'  => $discount_code->discount_applicable_from ?? null,
        ]);

        $items = $oh->items()->get();

        return ['status' => true, 'message' => 'Coupon updated', "amount" => $oh->discount_code, 'orderContent' => view('Orders::products',
            [
                'order' => $oh,
                'items' => $items,
                'displayValues' => $this->getDisplayValuesOrderInfo($oh, $items),
                'totalPrices' => getCartTotals($oh, [], $original??false),
            ])->render()];
    }

    public function changeState($id) {
        $appendStirng = '';

        request()->validate(['newState' => 'required']);
        /** @var OrderHeader $o */
        $o = OrderHeader::find($id);
        $o->setState(request('newState'));

        if (request('newState') == 'accepted') {
            // Mail::to($o->user_email)->send(new OrderStatusUpdated($o));
            $appendStirng = " and Email sent!";
        }

        return [
            'status' => true,
            'message' => 'Order Status Changed'.$appendStirng,
            'state' => $o->state,
        ];
    }

    public function destroy($id = false, $action = "delete", $route = 'orders') {

        $id = $id ? [$id] : request('massAction');

        if (!count($id)) {
            return ['status' => false, 'message' => 'No Order specified'];
        }
        $forceDelete = OrderHeader::onlyTrashed()->whereIn('id', $id)->forceDelete();
        $delete = OrderHeader::whereIn('id', $id)->delete();

        if ($forceDelete) {
            OrderLines::whereIn('order_header_id', $id)->delete();
            OriginalOrder::where('id', $id)->delete();
            $result = $forceDelete;
        }
        else {
            $result = $delete;
        }

        /** @var array $repTable */
        $repTable = view('admin.elements.table',
            [
                'tableHeaders' => $this->getList(),
                'header' => 'Orders',
                'list' => $this->getFilteredResult(request()->get('search'), $trashed ?? false)->withPath(route($route)),
                'idField' => 'name',
                'destroyName' => 'Order',
                'js' => [
                    'js/orderlist.js',
                ],
                'massActions' => [
                    (object)['url' => route('orders.summary'), 'class' => 'btn-info', 'icon' => 'fas fa-book', 'label' => 'Order Sumary'],
                    (object)['url' => route('orders.exportOrderstopdf'), 'class' => 'btn-warning', 'icon' => 'fas fa-download', 'label' => 'Export Orders To PDF'],

                    (object)['url' => route('orders.exportOrders'), 'class' => 'btn-warning', 'icon' => 'fas fa-download', 'label' => 'Export Orders'],
                    (object)['url' => route('orders.destroy'), 'class' => 'btn-danger', 'callback' => 'replaceTable', 'icon' => 'fas fa-trash', 'label' => 'Delete'],
                ],
            ])->renderSections();

        return ['status' => $result ?? true, 'message' => 'Order Deleted', 'replaceTable' => $repTable['content']];
    }


    public function updateField($id, $original) {

        request()->validate([
            'update' => 'required',
        ]);

        $field = request()->get('update');
        OrderHeader::findOrFail($id)->update([$field => request()->get($field)]);

        $names = $this->getViewFields();

        return ['status' => true, 'message' => $names[$field] . " Updated"];
    }

    public function removeOrder($id = false) {
        $this->destroy($id, 'forceDelete', 'orderHistory');
    }

    public function showOrder($order_id, $original = false) { //
        if ($original == 'original') {
            $orderData = OriginalOrder::find($order_id);

            /** @var OrderHeader $orderHeaders */
            $orderHeaders = new OrderHeader($orderData->headers);
            foreach ($orderData->items as $item) {
                $orderItems[] = $i = new OrderLines($item);
            }

        }
        else {
            /** @var OrderHeader $orderHeaders */
            $orderHeaders = OrderHeader::findOrFail($order_id);
            $orderItems = $orderHeaders->items()->orderBy('product_name', 'asc')->get();
        }

        $orderHeaders->isOriginal = $original;

        $orderUser = User::find($orderHeaders->user_id) ?? new User;

        return view('Orders::viewOrder', [
            'orderUser' => $orderUser,
            'order' => $orderHeaders,
            'displayValues' => $this->getDisplayValuesOrderInfo($orderHeaders, $orderItems),
            'totalPrices' => getCartTotals($orderHeaders, [], $original??false),
            'items' => $orderItems,
            'fields' => $this->getViewFields($orderHeaders),
            'canEdit' => "editable", // ($original=='original') ? false :
            'original' => $original
        ]);
    }

    public function changeDelivery($id) {
        $oh = OrderHeader::findOrFail($id);
        $oh->update(["delivery_id" => request('delivery')]);

        $this->checkFreeDelivery($id, 1);

        $oh = OrderHeader::findOrFail($id);
        $items = $oh->items()->get();

        return ['status' => true, 'message' => 'Delivery Changed', 'orderContent' => view('Orders::products',
            [
                'order' => $oh,
                'items' => $items,
                'displayValues' => $this->getDisplayValuesOrderInfo($oh, $items),
                'totalPrices' => getCartTotals($oh, [], $original??false),
            ])->render(),

        ];
    }

    private function checkFreeDelivery($cartId, $orderStep = 3) {
        $cart        = OrderHeader::find($cartId);
        $totalAmount = getCartTotals($cart) ?? 0;

        if ($cart->delivery_id) {
            $delivery = $cart->delivery;

            if ( $orderStep < 2 && $delivery->hasDynamicPrice() ) {
                $r = $cart->update(['delivery_amount' => "0"]);
                return null;
            }

            if ($totalAmount->toPay >= ($delivery->freeAbove ?? 0)) {
                $r = $cart->update(['delivery_amount' => "0"]);

                return true;
            } else {

                if ($cart->discount_target == 'all' || $cart->discount_target == 'delivery') {
                    $deliveryPrice = number_format(round($delivery->price / (1 + ($cart->discount_amount / 100)), 2), 2);
                } else {
                    $deliveryPrice = $delivery->price;
                }

                $cart->update(['delivery_amount' => $deliveryPrice]);

                return true;
            }
        }

        return false;
    }

    public function setAmount($order, $item) {
        /** @var OrderHeader $order */
        $order = OrderHeader::findOrFail($order);

        /** @var OrderLines $item */
        $item = $order->items()->where('id', $item)->firstOrFail();

        $item->update(['real_amount' => request('amount'), 'amount' => request('amount')]);
        $items = $order->items()->get();

        return ['status' => true, 'message' => 'Amount Changed', 'orderContent' => view('Orders::products',
            [
                'order' => $order,
                'items' => $items,
                'displayValues' => $this->getDisplayValuesOrderInfo($order, $items),
                'totalPrices' => getCartTotals($order, [], $original??false),
            ])->render()];
    }

    public function setFilters() {

        $filters = session('order_filters');

        foreach (request()->get('filter') as $filter => $filterValue) {
            $filters[$filter] = $filterValue;
        }

        session()->put('order_filters', $filters);

        session()->save();

        return ['status' => true, 'noMessage' => true];
    }

    public function clearFilters() {
        session()->put('order_filters', []);

        return ['status' => true, 'noMessage' => true];
    }

    public function exportOrders() {
        if (request()->has('massAction')) {
            $orders = OrderHeader::findMany(request('massAction'))->pluck('id')->toArray();
        }
        else {
            if (!session('order_filters.market_day')) {
                return ['status' => false, 'message' => 'Please select market Day'];
            }

            $orders = $this->getFilteredResult(request()->route('search'), false, true)->pluck('id')->toArray();
        }

        Schedules::create([
            'filename' => json_encode($orders),
            'type' => 'orderExport',
        ]);

        return [
            'status' => true,
            'message' => 'Export Scheduled, please consult import/export log for a download link in few minutes'
        ];
    }

    public function exportOrdersToPdf() {
        if (request()->has('massAction')) {
            $orders = OrderHeader::findMany(request('massAction'))->pluck('id')->toArray();
        }
        else {
            $orders = OrderHeader::all()->pluck('id')->toArray();
        }

        // make "accepted" and send emails to clients!

        Schedules::create([
            'filename' => json_encode($orders),
            'type' => 'orderExportToPdf',
        ]);

        return ['status' => true, 'message' => 'Export To Pdf Scheduled, please consult import/export log for a download link in few minutes'];
    }

    public function createSummary() {
        if (request()->has('massAction')) {
            $orders = OrderHeader::findMany(request('massAction'))->pluck('id')->toArray();
        }
        else {
            if (!session('order_filters.market_day')) {
                return ['status' => false, 'message' => 'Please select market Day'];
            }

            $orders = $this->getFilteredResult(request()->route('search'), false, true)->pluck('id')->toArray();
        }

        Schedules::create([
            'filename' => json_encode($orders),
            'type' => 'orderSummary',
        ]);

        return [
            'status' => true,
            'message' => 'Summary Scheduled, please consult import/export log for a download link in few minutes'
        ];

    }

    public function importOrders() {
        request()->validate([
            'importFile' => 'file|required',
        ]);

        $zip = new \ZipArchive();
        $file = request()->file('importFile');
        $zresult = $zip->open($file);

        if ($zresult === true && $file->extension() == 'zip') {
            $zip->extractTo(storage_path('app/imports/ordersUpdate'));
            $zip->close();
            if(is_dir(storage_path('app/imports/ordersUpdate')."/__MACOSX/"))
                File::deleteDirectory(storage_path('app/imports/ordersUpdate')."/__MACOSX/");

            $result = ['status' => true, 'message' => 'Order update uploaded, task scheduled'];

            Schedules::create([
                'filename' => 'orders',
                'type' => 'orderImport',
                'total_lines' => count(\Storage::files('imports/ordersUpdate')),
            ]);

        }
        else {
            $result = ['status' => false, 'message' => 'Uploaded file is not an archive'];
        }

        return $result;
    }

    public function createFarmerPdfs() {
        request()->validate([
            'importFile' => 'file|required',
        ]);

        /**
         * Ensure that dir exist!
         */
        $storageclient = Storage::createLocalDriver(['root' => storage_path()]);
        $storageclient->makeDirectory('app/imports/ordersSend');
        $storageclient->makeDirectory('app/imports/ordersSendPdf');


        $zip = new \ZipArchive();
        $file = request()->file('importFile');
        $zresult = $zip->open($file);

        if ($zresult === true) {

            foreach (File::files(storage_path('app/imports/ordersSend')) as $file) {
                unlink($file->getPathname());
            }
            foreach ($storageclient->directories('app/imports/ordersSend') as $dir) {
                $storageclient->deleteDirectory($dir);
            }
            foreach (File::files(storage_path('app/imports/ordersSendPdf')) as $file) {
                unlink($file->getPathname());
            }

            $zip->extractTo(storage_path('app/imports/ordersSend'));
            $zip->close();

            if(is_dir(storage_path('app/imports/ordersSend')."/__MACOSX/"))
                File::deleteDirectory(storage_path('app/imports/ordersSend')."/__MACOSX/");

            $result = ['status' => true, 'message' => 'Orders uploaded, task scheduled'];

            Schedules::create([
                'filename' => 'orders',
                'type' => 'createPDF',
                'total_lines' => count(\Storage::files('imports/ordersSend')),
            ]);

        }
        else {
            $result = ['status' => false, 'message' => 'Uploaded file is not an archive'];
        }

        return $result;
    }

    public function doSendEmails() {
        Schedules::create([
            'filename' => 'orders',
            'type' => 'sendOrderEmails',
            'total_lines' => count(\Storage::files('imports/ordersSendPdf')),
        ]);

        $result = ['status' => true, 'message' => 'Send orders Task scheduled'];

        return $result;
    }

    public function createAndDownloadDeliveryLabel($order_id)
    {
        $order = OrderHeader::find($order_id);

        if (!$order) {
            throw new \Exception("No such Order!", 1);
        }

        $deliveryMethod   = $order->delivery;
        $deliveryProvider = $deliveryMethod->delivery_provider_class;

        if ( \method_exists($deliveryProvider, 'createShipment') ) {
            $file = $deliveryProvider->createShipment($order);

            if ($file) {
                $order->delivery_label = $file->getRealPath();
                $order->save();
            } else {
                dd($file);
            }
        }

        if ( \method_exists($deliveryProvider, 'updateTrackingInfo') ) {
            $deliveryProvider->updateTrackingInfo($order);
        }

        return response()->download($order->delivery_label);
    }
}
