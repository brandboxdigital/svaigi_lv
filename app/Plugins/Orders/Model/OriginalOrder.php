<?php

namespace App\Plugins\Orders\Model;

use Illuminate\Database\Eloquent\Model;
use App\Plugins\Orders\Model\OrderNumber;

class OriginalOrder extends Model
{
    public $fillable = ['id', 'headers', 'items'];
    public $incrementing = false;
    public $casts = [
        'headers' => 'array',
        'items' => 'array',
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            OrderNumber::createFromOriginalOrder($model);
        });

        // self::updating(function($model){
        //     // ... code here
        // });

        // self::updated(function($model){
        //     // ... code here
        // });

        // self::deleting(function($model){
        //     // ... code here
        // });

        // self::deleted(function($model){
        //     // ... code here
        // });
    }

    /**
     */
    public function order_number()
    {
        return $this->hasOne(OrderNumber::class, 'order_id');
    }
}
