<?php

namespace App\Plugins\Orders\Model;

use Illuminate\Database\Eloquent\Model;
use App\Plugins\Orders\Model\OriginalOrder;

/**
 * Basically this models purpose is to create correct order numbering.. nothing else.
 */
class OrderNumber extends Model
{
    public $fillable = [
        'order_id',
    ];

    public $appends = [
        'string',
    ];

    // public $casts = [
    // ];

    public function original_order()
    {
        return $this->belongsTo(OriginalOrder::class, 'order_id');
    }

    public function order()
    {
        return $this->belongsTo(OrderHeader::class);
    }

    public static function createFromOriginalOrder(OriginalOrder $ordr)
    {
        $model = new self;
        $model->order_id = $ordr->id;
        $model->save();
    }
    public static function createFromOrderHeader(OrderHeader $ordr)
    {
        $model = new self;
        $model->order_id = $ordr->id;
        $model->save();
    }

    public function getStringAttribute()
    {
        return sprintf('%05d', $this->id);
    }
}
