<?php

namespace App\Plugins\Orders;

use App\User;
use App\Paysera;
use Carbon\Carbon;
use WebToPayException;
use App\Mail\OrderCreated;
use App\Mail\NewsletterConfirmation;
use Illuminate\Http\Request;
use App\Http\Requests\Profile;
use Illuminate\Validation\Rule;
use WebToPay_Exception_Callback;
use Antcern\Paysera\PayseraManager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Query\Builder;
use App\Plugins\Products\Model\Product;
use Illuminate\Support\Facades\Session;
use App\Plugins\Orders\Functions\Orders;
use App\Plugins\Orders\Functions\Invoice;
use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Deliveries\Model\Delivery;
use App\Plugins\Orders\Model\OriginalOrder;
use Illuminate\Validation\ValidationException;
use App\Plugins\Orders\Functions\CartFunctions;
use App\Plugins\DiscountCodes\Model\DiscountCode;
use App\Plugins\MarketDays\MarketDaysController;
use App\Plugins\Categories\Model\CategoryMeta;
use App\Plugins\Suppliers\Model\SupplierMeta;

use Illuminate\Support\Facades\Log;


/**
 * Class CartController
 *
 * @package App\Plugins\Orders
 */
class CartController extends Controller
{

    use CartFunctions;
    use Orders;
    use Invoice;


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->updateMarketDayIfCartHasOld();

        $cart = $this->getCart();

        $this->checkFreeDelivery($cart->id, 1);

        return view('Orders::frontend.cart', [
            'cart'        => $cart,
            'step'        => 1,
            // 'stepInclude' => 'freeDelivery',
            'stepInclude' => '',
            'pageTitle'   => _t('translations.cart')
        ]);
    }

    public function setDelivery($deliveryId)
    {
        $cart = $this->getCart();

        /** @var OrderHeader $origCart */
        $origCart = OrderHeader::find($cart->id);
        $delivery = Delivery::findOrFail($deliveryId);
        $origCart
            ->delivery()
            ->associate($delivery)
            ->save();

        // Remove delivery provider data on delivery change
        $delivery->saveMethodDataToOrder($origCart, null);

        $this->checkFreeDelivery($cart->id, 1);



        $cart = $cart->refresh();

        if (request()->ajax()) {
            $contents = $this->getCartContents($cart, true);

            if ( $delivery->hasDynamicPrice() ) {
                $contents['cartTotals']->delivery = "Pēc adreses norādes";
            }

            return [
                'status'   => true,
                'message'  => 'Delivery Updated',
                'contents' => $contents,
                'delivery' => $delivery,
            ];
        }

        return redirect(r('cart'));
    }

    public function setDeliveryProviderData()
    {
        $request = request();

        $deliveryProviderData = $request->input('delivery_provider_data', null);

        if ($deliveryProviderData) {
            $cart    = $this->getCart();

            /** @var OrderHeader $origCart */
            $origCart = OrderHeader::find($cart->id);
            $delivery = Delivery::findOrFail($origCart->delivery_id);

            $deliveryProvider = $delivery->delivery_provider_class;

            if ($deliveryProvider) {

                $delivery->saveMethodDataToOrder($origCart, $deliveryProviderData);

                $cart = $cart->refresh();

                if ($request->ajax()) {
                    return [
                        'status'   => true,
                        'message'  => 'Delivery Data Updated',
                        'delivery' => $delivery,
                        'origCart' => $origCart,
                    ];
                }
            }

            // throw new \Exception("No such delivery provider");
        }

        return redirect(r('cart'));
    }

    public function recreateCart($orderId)
    {
        if ($order = Auth::user()->orders($orderId)->first()) {
            foreach ($order->items()->get() as $item) {

                if ($item->isAvailableToBuy()) {

                    $req = new Request();
                    $req->setMethod('POST');
                    $req->request->add([
                        'product_id' => $item->product_id,
                        'variation_id' => $item->variation_id,
                        'amount' => $item->getFixedAmount(),
                    ]);
                }

                $this->changeCartItem($req);
            }

            return redirect(r('cart'));
        }
        abort(404);
    }

    /**
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function changeCartItem(Request $request)
    {
        $request->validate([
            'product_id'   => 'required|numeric',
            'variation_id' => 'required|numeric',
            'amount'       => 'numeric',
            'line'         => 'numeric',
        ]);

        $lineData = [
            'id' => null,
        ];

        /** @var OrderHeader $cart */
        $cart = $this->getCart();

        if ($cartItemId = $request->get('line')) {
            $item = $cart->items()->where(['id' => $cartItemId, 'product_id' => $request->get('product_id')])->first();
            if (!$item) abort(404);
            $lineData = [
                'id' => $item->id,
            ];
        } else {
            $item = $cart->items()
                ->where(
                    [
                        'product_id'   => $request->get('product_id'),
                        'variation_id' => $request->get('variation_id'),
                    ]
                )->first();
            if ($item) {
                $lineData = [
                    'id' => $item->id,
                ];
            }
        }
        if (($item ?? false) && $item->variation_id == $request->get('variation_id')) {
            $amount = $request->get('amount');

            if (!is_null($item->products->storage_amount) && $item->products->storage_amount < ($amount * $item->variation->amount)) {
                return ['status' => false, 'message' => 'Not enough Item', 'contents' => $this->getCartContents($cart, true)];
            }


            $origSize = $item->total_amount / $item->amount;
            if ($request->get('line')) {
                $item->update(['amount' => $amount]);
            } else {
                $item->increment('amount', ($amount ?? 1));
            }
            $item->update(['total_amount' => $origSize * ($amount ?? 1), 'real_amount' => $origSize * ($amount ?? 1)]);
        } else {
            $amount = $request->get('amount');

            /** @var \App\Cache\ProductCache $product */
            $product = $this->cache()->getProduct($request->get('product_id'));
            $variation = $product->getVariationPrice($request->get('variation_id'));
            if (!$item) {
                $productAmount = Product::find($product->id)->storage_amount;
                if (!is_null($productAmount) && $productAmount < ($amount * $variation->size)) {
                    //return redirect()->back()->with(['message' => 'Not Enough Item']);
                    return ['status' => false, 'message' => 'Not enough Item', 'contents' => $this->getCartContents($cart, true)];

                }
            } elseif (!is_null($item->products->storage_amount) && $item->products->storage_amount < ($amount * $variation->size)) {
                return ['status' => false, 'message' => 'Not enough Item', 'contents' => $this->getCartContents($cart, true)];
            }

            $itemData = [
                'supplier_id'    => $product->supplier_id,
                'supplier_name'  => __('supplier.name.' . $product->supplier_id),
                'product_id'     => $product->id,
                'product_name'   => __("product.name.{$product->id}"),
                'vat_id'         => $product->price->vat_id,
                'vat_amount'     => $product->price->vat,
                'vat'            => $variation->vat,
                'price'          => $variation->price,
                'display_name'   => $variation->display_name,
                'variation_size' => $variation->size,
                'discount'       => $product->discount(),
                'variation_id'   => $request->get('variation_id'),
                'total_amount'   => $variation->amountinpackage * ($request->get('amount') ?? 1),
                'real_amount'    => $variation->amountinpackage * ($request->get('amount') ?? 1),
                'amount_unit'    => $variation->amountUnit,
                'amount'         => $request->get('amount') ?? 1,
                'price_raw'      => $variation->price_raw,
                'vat_raw'        => $variation->vat_raw,
                'cost'           => $variation->cost,
                'markup'         => $variation->markup,
                'markup_amount'  => $variation->markup_amount,
                'discount_name'  => $product->getDiscountName(),
                'deposit_amount'  => ($variation->has_deposit) ? $variation->deposit_amount : 0,
            ];

            if ($item) {
                if ($amount != $item->amount) {
                    $itemData['amount'] = $amount;
                }
            }

            $cartItem = $cart->items()->updateOrCreate($lineData, $itemData);
        }

        $this->checkFreeDelivery($cart->id, 1);

        $cart = $cart->refresh();

        if ($request->ajax()) {
            return ['status' => true, 'message' => 'Item Updated', 'contents' => $this->getCartContents($cart, true)];
        } else {
            return redirect(r('cart'));
        }

    }

    private function checkFreeDelivery($cartId, $orderStep = 3)
    {
        /** @var OrderHeader $cart */
        $cart = OrderHeader::find($cartId);
        $totalAmount = getCartTotals($cart) ?? 0;

        if($cart->items()->count()==0) {
            $cart->update([
                'delivery_amount' => 0,
                'delivery_id'     => null
            ]);

            return true;
        }

        if ($cart->delivery_id) {
            $delivery = $cart->delivery;

            if ( $orderStep < 2 && $delivery->hasDynamicPrice() ) {
                $r = $cart->update(['delivery_amount' => "0"]);
                return null;
            }

            if ($delivery->freeAbove && $totalAmount->productSum >= $delivery->freeAbove) {
                $r = $cart->update(['delivery_amount' => "0"]);

                return true;
            } else {
                $deliveryPrice = $delivery->price;
                $cart->update(['delivery_amount' => $deliveryPrice]);

                return true;
            }
        }

        return false;
    }

    /**
     * @param null $edit
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     *
     * @throws ValidationException
     */
    public function checkout($edit = null)
    {
        $cart = $this->getCart();

        if (!$cart->delivery_id) {
            throw ValidationException::withMessages([
                // ['delivery' => 'Please select Delivery option'],
                // ['delivery' => 'Lūdzu norādiet piegādes veidu'],
                ['delivery' => _t('translations.errorNoDeliverySelected')],
            ]);
        }

        if ($cart->delivery->needsMethodData() && !$cart->delivery->getMethodDataFromOrder($cart)) {

            throw ValidationException::withMessages([
                // ['delivery' => 'Please select Delivery option'],
                ['delivery' => 'Lūdzu norādiet visus piegādes datus!'],
                // ['delivery' => _t('translations.errorNoDeliverySelected')],
            ]);
        }

        $bDayFound = false;
        foreach ( (new MarketDaysController)->getClosestMarketDayList() as $timestamp => $m_day ) {
            $cartMd = Carbon::parse($cart->market_day_date)->startOfDay();
            $timestamp = Carbon::createFromTimestamp($timestamp)->startOfDay();

            $bDayFound = $bDayFound || $cartMd->eq($m_day->deliver_to_date);
        }

        if (!$bDayFound) {

            throw ValidationException::withMessages([
                // ['delivery' => 'Please select Delivery option'],
                // ['delivery' => 'Lūdzu norādiet piegādes veidu'],
                ['marketday' => 'Kļūda tirgus dienas izvelē. Lūdzu vēlreiz izvēlieties savu vēlamo tirgus dienu!'],
            ]);
        }


        $cartItemCount = $cart->items()->count();

        if ($cart->currentDayItems()->count() !== $cartItemCount || $cartItemCount == 0) {

            throw ValidationException::withMessages([
                ["cartError" => _t('translations.errorHasUndeliverableItems')],
            ]);
        }

        /**
         * @edit Don't skip userinfo page..
         */
        if (Auth::user() && Auth::user()->registered && !$edit) {
            \Session::put("email",Auth::user()->email);
            // return redirect(r('payment'));

            $edit = true;
        }

        $step = 2;
        // if (Auth::user() && !Auth::user()->registered && !$edit) {
        //     Auth::logout();
        // }

        return view('Orders::frontend.userinfo', [
            'edit'                => $edit,
            'cart'                => $cart,
            'step'                => $step,
            'stepInclude'         => 'loginToSave',
            'user'                => Auth::user() ?? new User,
            'pageTitle'           => _t('translations.checkoutForm'),
            'showLoactionFields'  => $cart->delivery->needAddess(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function payment()
    {
        $step = 3;
        $cart = $this->getCart();

        $this->checkFreeDelivery($cart->id, $step);

        if ($cart->items()->count() == 0) {
            return redirect()->route('cart.default');
        }

       // handle Paysera accepturl
        if (!is_null(request()->get('data'))) return $this->saveOrder();

        session()->forget(['paymetMethod', 'order_header_id']);

        return view('Orders::frontend.payment', [
            'cart' => $this->getCart(),
            'step' => $step,
            'user' => Auth::user(),
            'pageTitle' => _t('translations.payments'),
        ]);
    }

    /**
     * Initiate Paysera payment
     *
     * @return void
     */
    public function payseraMake()
    {
        $cart = $this->getCart();
        $cart_totals = getCartTotals($cart);

        Session::put('paymetMethod', 'paysera');
        Session::put('order_header_id', $cart->id);
        Session::save();
        $user = Auth::user();

        $this->saveOrder('payment_pending');

        if (config('paysera.test')) {
            $cartId = $cart->id . '---' . str_random(5);
        } else {
            $cartId = $cart->id;
        }

        PayseraManager::makePayment($cartId, $cart_totals->toPay, [
            'p_email' => $user ? $user->email : '',
            'developerid' => '5107791',
        ]);
    }

    /**
     * Validate Paysera response
     *
     * return void
     */
    public function payseraValidate()
    {
        // Stupid way to overcome echo in Paysera plugin and still collect errors
        ob_start();
        $response = PayseraManager::parsePayment(request());
        $content = ob_get_contents();
        ob_end_clean();

        $orderid = array_get($response, 'orderid', null);
        $amount  = array_get($response, 'amount', null);

        if ($response == null || $amount == null) {

            Paysera::create([
                'status'           => 'failed',
                'order_header_id'  => $orderid ?? 0,
                'amount'           => $amount ?? 0,

                'response_data'    => $response,
                'request_data'     => json_encode(request()),
                'log'              => $content,
            ]);

            if (config('paysera.test')) {
                throw new \Exception($content, 1);
            } else {
                throw new \Exception("Error processing order.", 1);
            }
        }

        if (config('paysera.test')) {
            $orderid = collect(\explode('--', $orderid))->first();
        } else {}

        Paysera::create([
            'status'           => 'success',
            'order_header_id'  => $orderid,
            'amount'           => number_format(round($amount / 100, 2), 2),

            'response_data'    => $response,
            'request_data'     => json_encode(request()),
            'log'              => $content,
        ]);

        try {
            $order = OrderHeader::where('id', $orderid)->first();

            OrderHeader::where('id', $orderid)
                ->update([
                    'paid' => number_format(round($amount / 100, 2), 2),
                    'state' => 'ordered',
                ]);

            if ($order->state != 'ordered') {
                $order->refresh();
                Mail::to($order->user_email)->send(new OrderCreated($order->buyer, $order));
            }

            $toRet = "OK";
        } catch (\Throwable $th) {
            $toRet = $th->getMessage();
        }

        session()->put('cartId', $orderid);

        if (request()->method() == "POST") {
            die($toRet);
        }

        return redirect(r('thankyou', ['id' => $orderid]));
        // die($toRet);
        // return $this->saveOrder();
    }

    /**
     * Check payment status
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function payseraSuccess()
    {
        if (request()->ajax()) {
            if (
                session()->get('paymetMethod') == 'paysera' &&
                is_int($order_header_id = session()->get('order_header_id')) &&
                $paymentSuccess = !empty(Paysera::where(['order_header_id' => $order_header_id, 'status' => 'success'])->first())
            ) {
                session()->forget(['paymetMethod', 'order_header_id']);

                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return redirect(r('checkout'));
        }
    }

    /**
     * @param Profile $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveUserInfo(Profile $request)
    {
        $request->validated();

        $user = Auth::user() ?? null;

        /** @var OrderHeader $cart */
        $cart = $this->getCart();

        $deliveryProvider = $cart->delivery->delivery_provider_class ?? null;


        if (!$user) {
            $user = User::where('email', request()->get('email'))->first() ?? new User;
        }

        $action = $request->input('action');

        if($action == "password"){

            $user = User::where(['email'=> request()->get('email'),'registered'=>'1'])->first() ?? null;
            if($user) {

                if(!Hash::check(request()->input('check_password'), $user->password)){
                    throw ValidationException::withMessages([
                        ["check_password" => __('translations.InvalidPassword')],
                    ]);
                }

            }else{
                throw ValidationException::withMessages([
                    ["check_password" => __('translations.InvalidPassword')],
                ]);
            }
        }

        //if(Auth::user() && $user->registered == "0")
        //$user->registered = 1;

        if($action != "password") {
            $userSaveData = array_merge( request($user->getFillable()), [
                'is_legal' => $request->input('is_legal') ?? 0,
            ]);

            $user = $user->updateOrCreate([
                'id' => $user->id ?? null
            ], $userSaveData);

            \Session::put("newsletter", $request->input('newsletter') ?? null);
        }

        if (!Auth::user()) {
            Auth::login($user);
            $cart->update(['user_id' => $user->id, 'comments' => $request->get('comments')]);
        }

        if($user->isSocialAuthedCount() != 0)
            $user = $user->updateOrCreate(['id' => $user->id ?? null], ['registered'=>1]);



        \Session::put("email",$user->email);

        $updateOrderData = [
            'comments' => request('comments'),
            'city'     => request('city', $user->city),
            'address'  => request('address', $user->address),
            'postcode' => request('postal_code', $user->postal_code),
        ];

        if ($deliveryProvider && $deliveryProvider->hasAddressOverride()) {
            $data = $deliveryProvider->formatSavedDataObject($cart->delivery_provider_data_for_order);

            $updateOrderData['address']  = $data['address'];
            $updateOrderData['city']     = $data['city'];
            $updateOrderData['postcode'] = $data['zipcode'];
        }

        $cart->update($updateOrderData);

        $this->checkFreeDelivery($cart->id);

        return redirect(r('payment'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveOrder($state = 'ordered')
    {
        /** @var OrderHeader $cart */
        $cart = $this->getCart();

        $pt = request()->get('payment_type')??session()->get('paymetMethod');

        if(!$pt) {
            abort(404, "Nezināma maksājuma metode! Lūdzu mēģiniet vēlreiz.");
        }

        if ($cart->items()->count() == 0) {
            return redirect()->route('cart.default');
        }

        $user = Auth::user() ?? User::where('email', (session()->get('email') ?? "temp@user.local"))->first();

        if (!$user) {
            return redirect()->route('checkout');
        }

        $updateOrderData = [
            'state'                          => $state,
            'ordered_at'                     => Carbon::now(),
            'payment_type'                   => $pt,

            'city'                           => $user->city,
            'address'                        => $user->address,
            'postcode'                       => $user->postal_code,

            'invoice'                        => $cart->id."-u".$user->id,
            // Слепок данных по данным юзера
            'user_full_name'                 => $user->full_name,
            'user_email'                     => $user->email,
            'user_phone'                     => $user->phone,
            'user_comment'                   => $user->address_comments,
            'user_is_legal'                  => $user->is_legal,
            'user_legal_name'                => $user->legal_name,
            'user_legal_address'             => $user->legal_address,
            'user_legal_reg_nr'              => $user->legal_reg_nr,
            'user_legal_vat_reg_nr'          => $user->legal_vat_reg_nr,
        ];

        $deliveryProvider = $cart->delivery->delivery_provider_class ?? null;
        if ($deliveryProvider && $deliveryProvider->hasAddressOverride()) {
            $data = $deliveryProvider->formatSavedDataObject($cart->delivery_provider_data_for_order);

            $updateOrderData['city']     = $data['city'];
            $updateOrderData['address']  = $data['address'];
            $updateOrderData['postcode'] = $data['zipcode'];
        }

        $cart->update($updateOrderData);

        foreach ($cart->items()->get() as $cartItem) {
            $cartItem->products()->increment('storage_amount', -($cartItem->amount * $cartItem->variation_size));
            if ($cartItem->products->storage_amount < 0) {
                $cartItem->products()->update(['storage_amount' => 0]);
            }

            /**
             * Incremet popularity counter
             */
            try {
                $cartItem->products()->increment(['times_bought' => $cartItem->amount]);
            } catch (\Throwable $th) {}
        }

        try {
            OriginalOrder::create([
                'id'      => $cart->id,
                'headers' => $cart->getOriginal(),
                'items'   => $cart->items->toArray(),
            ]);
        } catch (\Throwable $th) {

        }


        if ($state == 'ordered') {
            Mail::to($user->email)->send(new OrderCreated($user, $cart->refresh()));

            if (session()->get('newsletter')) {
                Mail::to($user->email)->send(new NewsletterConfirmation($user));
            }
        }

        // session()->forget('cart');
        //if (Auth::user() && !Auth::user()->registered) {
            //Auth::logout();
        //}

        session()->put('cartId', $cart->id);

        return redirect(r('thankyou'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thankyou()
    {
    	$user = Auth::user();
		if ($user && !$user->hasPassword()) {
			Auth::logout();
		}

        if (session()->get('paymetMethod') == 'paysera') $paymentMethod = 'paysera';
        else $paymentMethod = null;

        $googleAnalytics = null;

        if (session()->get('cartId')) {
            $orderHeader = OrderHeader::find(session()->get('cartId'));
            $cartItems = $orderHeader->items->toArray();
            $items = [];
            $price = floatval($orderHeader->delivery_amount);

            foreach ($cartItems as $idx => $item) {
                $items[$idx]['item_name'] = $item['product_name'];
                $items[$idx]['item_id'] = $item['product_id'];
                $items[$idx]['price'] = $item['price'];
                $product = Product::find($item['product_id']);
                $categoryId = $product->main_cat->id;
                $categoryMeta = CategoryMeta::where(['owner_id' => $categoryId, 'meta_name' => 'name'])->first();
                $supplierMeta = SupplierMeta::where(['owner_id' => $item['supplier_id'], 'meta_name' => 'name'])->first();
                $items[$idx]['item_category'] = $categoryMeta->meta_value;
                $items[$idx]['item_brand'] = $supplierMeta->meta_value;
                $items[$idx]['item_variant'] = $item['display_name'];
                $items[$idx]['quantity'] = $item['amount'];

                $price = $price + floatval($item['price']);
            }

            $googleAnalytics = [
                'event' => 'purchase',
                'ecommerce' => [
                    'transaction_id' => $orderHeader->id,
                    'value' => $price,
                    'shipping' => $orderHeader->delivery_amount,
                    'currency' => 'EUR',
                    'items' => $items,
                ],
            ];
        }

       session()->forget('cart');
       session()->forget('cartId');

        return view('Orders::frontend.thankyou', compact('paymentMethod', 'googleAnalytics'));
    }

    public function removeFromCart($itemId)
    {

        $cart = $this->getCart();

        $item = $cart->items()->where('id', $itemId)->first();

        if (request()->ajax()) {
            if ($item) {
                $item->delete();
                $this->checkFreeDelivery($cart->id, 1);
                $cart = $cart->refresh();

                return ['status' => true, 'message' => 'Item Removed', 'contents' => $this->getCartContents($cart, true)];
            }

            return ['status' => false, 'message' => 'Item Couldn\'t be foundRemoved'];
        } else {
            if (request('goTo') && $item) {
                $item->delete();
                $urlParts = explode("/", request('goTo'));
                unset($urlParts[0]);
                $this->checkFreeDelivery($cart->id, 1);
                $cart = $this->getCart();

                return redirect(r('url', $urlParts));
            }

            return redirect()->back();
        }
    }

    public function discount_code()
    {

        request()->merge(['code' => strtoupper(request('code'))]);

        request()->validate([
            'code' => [
                'required',
                Rule::exists('discount_codes')->where(function (Builder $q) {
                    $q->where('uses', '>', '0')
                        ->orWhereNull('uses');
                })
                    ->where(function (Builder $q) {
                        $q->where('valid_from', '<=', Carbon::now()->format('Y-m-d'))
                            ->where(function ($qq) { $qq->where('valid_to', '>=', Carbon::now()->format('Y-m-d'))->orWhereNull('valid_to'); });
                    }),
            ],
        ], [
            'code.required' => 'Ievadi akcijas kodu!',
            'code.*' => 'Nepareizs akcijas kods!',
        ]);

        /** @var DiscountCode $discount_code */
        $discount_code = DiscountCode::where('code', request('code'))->first();
        if (!is_null($discount_code->uses)) {
            $discount_code->increment('uses', -1);
        }
        $cart = $this->getCart();

        $result = $cart->update([
            'discount_code'             => request('code'),
            'discount_amount'           => $discount_code->amount,
            'discount_target'           => $discount_code->applied,
            'discount_type'             => $discount_code->unit,
            'discount_items'            => json_decode($discount_code->getOriginal('items')),
            'discount_applicable_from'  => $discount_code->discount_applicable_from ?? null,
        ]);

        if(request()->ajax()) {
            $cart = $cart->refresh();
            return ['status' => true, 'message' => 'Code Added', 'contents' => $this->getCartContents($cart, true)];
        }
        return redirect()->back()->with(['message' => 'Discount Code Added']);
    }

    public function discount_code_remove()
    {
        $cart = $this->getCart();

        /** @var DiscountCode $discount_code */
        $discount_code = DiscountCode::where('code', $cart->discount_code)->first();
        $discount_code->increment('uses', 1);

        $cart->update([
            'discount_code'   => null,
            'discount_amount' => null,
            'discount_target' => null,
            'discount_type'   => null,
            'discount_applicable_from'   => null,
        ]);
        if(request()->ajax()) {
            $cart = $cart->refresh();
            return ['status' => true, 'message' => 'Code removed', 'contents' => $this->getCartContents($cart, true)];
        }
        return redirect()->back()->with(['message' => 'Discount Code Removed']);
    }

}
