<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersAddDiscountApplicableFrom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_headers', function (Blueprint $table) {
            $table->decimal('discount_applicable_from', 8, 2)->nullable();
        });

        // DB::statement("ALTER TABLE  `order_headers` CHANGE  `paid`  `paid` DECIMAL( 8, 2 ) NULL DEFAULT  '0';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_headers', function (Blueprint $table) {
            $table->dropColumn('discount_applicable_from');
        });

        // DB::statement("ALTER TABLE  `order_headers` CHANGE  `paid`  `paid` DECIMAL( 8, 2 ) NULL;");
    }
}
