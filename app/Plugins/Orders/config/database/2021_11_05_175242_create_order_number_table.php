<?php

use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Orders\Model\OrderNumber;
use App\Plugins\Orders\Model\OriginalOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('order_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->string('number')->nullable();
            $table->timestamps();
        });

        OrderHeader::where('state', '!=', 'draft')->orderBy('id','asc')->get()->each(function($order) {
            OrderNumber::createFromOrderHeader($order);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_numbers');
        Schema::dropIfExists('order_number');
    }
}
