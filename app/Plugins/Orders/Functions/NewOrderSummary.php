<?php

namespace App\Plugins\Orders\Functions;

use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Orders\Model\OrderLines;
use App\Schedules;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\User;

trait NewOrderSummary
{

    protected $summaryFields = [];
    protected $summaryFieldsBase = [
        ['value' => 'id'                   , 'label' => 'ID' ],
        ['value' => 'order_number_string'  , 'label' => 'Order ID' ],

        // ['value' => 'supplier_id'          , 'label' => 'Supplier slug'          , 'translate' => 'supplier_slug' ],
        // ['value' => 'supplier_name'        , 'label' => 'Supplier Name' ],
        ['value' => 'ordered_at'           , 'label' => 'Order Date'],

        ['fn' => 'getDeliveryDate'         , 'label' => 'Delivery Date'          , 'value' => ["market_day_date"      , 'delivery'] ],


        // ['value' => 'sku'               , 'label' => 'SKU'                    , 'relation' => 'products'],
        // ['value' => 'product_name'      , 'label' => 'Product Name' ],
        // ['value' => 'amount'            , 'label' => 'Ordered Amount' ],
        // ['value' => 'display_name'      , 'label' => 'Display' ],
        // ['value' => 'cost'              , 'label' => 'Cost' ],
        // ['value' => 'markup'            , 'label' => 'Markup' ],


        ['label' => 'Discount Type'        , 'value' => "discount_type"],
        ['label' => 'Discount Amount'      , 'value' => "discount_amount"],
        ['label' => 'Discount Name'        , 'value' => 'discount_code'],

        ['label' => 'User ID'              , 'value' => 'user_id'],
        ['label' => 'User Name/Last Name'  , 'value' => 'user_full_name'],
        ['label' => 'User Phone'           , 'value' => 'user_phone'],
        ['label' => 'User email'           , 'value' => 'user_email'],


        ['fn' => 'getPriceWOvat'        , 'label' => 'Price w/o vat'          , 'value' => null],
        ['fn' => 'getPriceVat'          , 'label' => 'Vat'                    , 'value' => null],
        ['fn' => 'getPriceWVat'         , 'label' => 'Price w/ vat'           , 'value' => null],
        ['label' => 'Payment Type'         , 'value' => 'payment'],
        ['label' => 'Paid'                 , 'value' => 'paid'],

        // ['fn' => 'getTotalDiscount'    , 'label' => 'Discount Total'         , 'value' => null],

        ['label' => 'Delivery Type'       , 'value' => null                     , 'fn' => 'getDeliveryDescription',],
        ['label' => 'Delivery City'       , 'value' => 'city'],
        ['label' => 'Delivery Address'    , 'value' => 'address'],
        ['label' => 'Delivery Postcode'   , 'value' => ''],
        ['label' => 'Delivery Comment'    , 'value' => 'user_comment'],
        ['label' => 'Delivery Price'      , 'value' => 'delivery_Amount'],

        ['label' => 'Comment'             , 'value' => 'comments'               , 'fn' => 'getComments'],
        // ['label' => 'Svaigi Comment'      , 'value' => 'svaigi_comment_stats'   , 'fn' => 'getSvaigiCommentStats'],
        ['label' => 'Svaigi Comment'      , 'value' => 'global_user_comment'   , 'fn' => 'getGlobalUserComment'],
    ];

    /**
     * @var int
     */
    private $index = 2;

    /**
     * @var Schedules
     */
    private $schedule;

    /**
     * @var Worksheet
     */
    private $sheet;

    /**
     * @var array
     */
    private $summaryData;

    /**
     * @var object
     */
    private $price;

    /**
     * @var float
     */
    private $discount;

    private $lastLine;

    /**
     * @param Schedules|null $schedule
     * @param array          $oids
     *
     * @return bool
     */

    public function createOrderSummary(Schedules $schedule = null, $oids = [])
    {
        $this->setExcelColumns();

        // try {
            if (!$schedule && empty($oids)) {
                return false;
            }

            if ($schedule) {
                $this->schedule = $schedule;
            }

            $excel = new Spreadsheet();
            $this->sheet = $excel->getActiveSheet();
            $this->createHeaders();

            $this->getSummaryData($oids);

            foreach ($this->summaryData as $index => $orderHeader) {
            	$this->lastLine = false;

            	$next = $this->summaryData->get(++$index);

            	if (!$next) {
					$this->lastLine = true;
                }

                $this->createSummaryLine($orderHeader);

                $this->index++;

                $this->price = null;
                $this->discount = null;

                echo "{$orderHeader->id}-Done || ";
            }

            $this->autoSizeColumns();

            $xls = new Xlsx($excel);

            $filename = 'export/summary' . Carbon::now()->timestamp . ".xlsx";

            $xls->save(storage_path("$filename"));

            $this->schedule->update(['result_state' => true, 'running' => false, 'finished' => true, 'result_message' => 'Summary created: <a href="' . route('download', explode("/",$filename)) . '">Download</a>']);
        // } catch( \Exception $e) {
        //     $this->schedule->update(['result_state' => false, 'running' => false, 'finished' => true, 'result_message' => 'Error creating summary: '.$e->getMessage().' On Line:'.$e->getLine()]);
        // }
        return true;
    }

    private function setExcelColumns()
    {
        $summaryFields = [];
        foreach ($this->summaryFieldsBase as $key => $value) {
            $letter = $this->columnLetter($key+1);
            $summaryFields[$letter] = $value;
        }

        $this->summaryFields = $summaryFields;
    }

    private function columnLetter($c){

        $c = intval($c);
        if ($c <= 0) return '';

        $letter = '';

        while($c != 0){
           $p = ($c - 1) % 26;
           $c = intval(($c - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }

        return $letter;

    }

    private function autoSizeColumns() {
        foreach($this->summaryFields as $col => $undef) {
            $this->sheet->getColumnDimension($col)->setAutoSize(true);
        }
    }

    private function createHeaders()
    {
        foreach ($this->summaryFields as $column => $summaryField) {
            $this->sheet->setCellValue($column . "1", $summaryField['label']);
        }
        $this->sheet->freezePane('A1');
    }

    private function getDeliveryDescription($order)
	{
        $id = $order->delivery->id ?? null;

        if ($id) {
            return strip_tags(__("deliveries.description.{$id}"));
        }

        return 'N/A delivery id: ' . ($order->delivery_id ?? 'n/a');
	}

    private function getSummaryData($oids = [])
    {
        if (empty($oids)) {
            $oids = json_decode($this->schedule->filename);
        }
        $this->summaryData = OrderHeader::with(['items','delivery'])->whereIn('id', $oids)->get();
    }

    private function runFN($data, $summaryField) {
        if (method_exists($this, $summaryField['fn'])) {
            // echo "Method {$summaryField['fn']} Found \n";
            $fnData = [];
            if (is_array($summaryField['value'])) {
                foreach ($summaryField['value'] as $fieldVal) {
                    $fnData[$fieldVal] = $data->$fieldVal;
                }
            }
            return $this->{$summaryField['fn']}(count($fnData) ? $fnData : $data);
        }
        return "";
    }

    private function runRelation($data, $summaryField) {
        $dataRel = $data;
        if (is_array($summaryField['relation'])) {
            foreach ($summaryField['relation'] as $rel) {
                $dataRel = $dataRel->$rel;
            }
        } else {
            $dataRel = $dataRel->{$summaryField['relation']};
        }

        $dataRel->isOriginal = true;

        if($summaryField['fn']??false) {
            return $this->runFN($dataRel, $summaryField);
        }

        $fieldName = $summaryField['value'];

        if(is_array($fieldName)) {
            $fData = [];
            foreach($fieldName as $fname) {
                $fData[] = $dataRel->$fname;
            }
            return implode(" ", $fData);
        }

        return $dataRel->$fieldName;
    }

    private function createSummaryLine($data)
    {
        foreach ($this->summaryFields as $column => $summaryField) {
            $fieldValue = null;
            switch ($summaryField) {
                /**
                 * Relation fixes
                 */
                case ($summaryField['relation']??false) ? $summaryField : false:
                    $fieldValue = $this->runRelation($data, $summaryField);
                    break;

                case ($summaryField['fn']??false) ? $summaryField : false:
                    $fieldValue = $this->runFN($data, $summaryField);
                    break;

                default:
                    if(is_array($summaryField['value'])) {
                        $fdata = [];
                        foreach($summaryField['value'] as $fval) {
                            $fdata[] = $data->$fval;
                        }
                        $fieldValue = implode(" ", $fdata);
                    } else {
                        $fieldValue = $data->{$summaryField['value']};
                    }
                    break;
            }

            //if ($fieldValue) {
                if (($summaryField['translate'] ?? false)) {
                    $fieldValue = __($summaryField['translate']. "." . $fieldValue);
                }

                $this->sheet->setCellValue($column . $this->index, $fieldValue);
            //}

        }
    }

    /**
     *
     * @param $data
     *
     * @return array|null|string
     */
    private function getDeliveryDate($data)
    {
        $marketDay = $data['market_day_date'];
        $delivery = $data['delivery'];

        $md = new Carbon($marketDay);

        $dt = $md->copy();
        $modifiedMD = $dt->addDays($delivery->deliveryTime ?? 0);
        $mdDate = $modifiedMD->format('j');

        $month = __("translations." . $modifiedMD->format('F'));
        $dayName = __("translations." . $modifiedMD->format('l'));

        return __('translations.marketDayDeliveryText', ["dayname" => $dayName, 'date' => $mdDate, 'month' => $month]);
    }

    private function getDiscount($data)
    {
        if($this->discount) {
            return $this->discount;
        }
        $this->discountClass = "Sales";
        $discount = $lineDiscount = $data->discount;
        $orderDiscount = $data->order->discount_amount;

        if ($orderDiscount) {
            $discount_target = $data->order->discount_target;
            $discount_items = $data->order->discount_items;

            switch($discount_target) {
                case "product":
                    if(in_array($data->product_id, $discount_items)) {
                        $discount = $orderDiscount>$discount?$orderDiscount:$discount;
                        $this->discountClass = "DiscountCode";
                    }
                    break;

                case "cateogry":
                    $prodCat = $data->products->extra_categories()->pluck('id')->toArray();
                    if(in_array($prodCat,$discount_items)) {
                        $discount = $orderDiscount>$discount?$orderDiscount:$discount;
                        $this->discountClass = "DiscountCode";
                    }
                    break;
            }
        }
        $this->discount = $discount;

        return $discount;
    }

    private function getPrices($data) {
		if (!$this->price) {
			$this->price = $this->getDisplayValuesOrderInfo($data, $data->items);
			$this->price['total'] = getCartTotals($data);
		}

		/*if (!$this->price) {
			$discount = $this->getDiscount($data);
			$this->price = calcPrice($data->cost, $data->vat_amount, $data->markup, $discount, $data->amount);
		}*/
	}

    private function getPriceWOvat($data)
    {
        $this->getPrices($data);

        try {
            $collection = collect($this->price['vatTotals']);
        } catch (\Throwable $th) {

            \Log::error('getPriceWOvat', [
                'price' => $this->price,
            ]);

            throw $th;
        }
        return $collection->sum('summWoVat');

        // return current($this->price['vatTotals'])['summWoVat'];
        //return $this->price->wdiscount->pricewovat;
    }

    private function getPriceVat($data) {
        $this->getPrices($data);

        $collection = collect($this->price['vatTotals']);
        return $collection->sum('summVat');

		// return current($this->price['vatTotals'])['summVat'];
        //return $this->price->wdiscount->pricevat;
    }

    private function getPriceWVat($data) {
        $this->getPrices($data);

		return $this->getPriceWOvat($data) + $this->getPriceVat($data);
        //return $this->price->wdiscount->price;
    }

    /**
     * @todo NO worky worky. Order header does not have price_raw
     *
     * @param [type] $data
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    private function getTotalDiscount($data) {
        $this->getPrices($data);

        dd('disc', $this->getPriceWVat($data) );

		return ($data->price_raw - $data->price) * $data->amount;
        //return $this->price->sum->wdiscount->wvat-$this->price->sum->wodiscount->wvat;
    }

    private function getDeliveryAmount($data) {
    	if (!$this->lastLine) return "";
        $this->getPrices($data);
        return $this->price['total']->delivery + $this->price['total']->delivery_vat;
        //return $this->price->sum->wdiscount->wvat-$this->price->sum->wodiscount->wvat;
    }

    private function getComments($data)
    {
        return strip_tags($data->comments);
    }
    private function getSvaigiCommentStats($data)
    {
        return strip_tags($data->svaigi_comment_stats);
    }
    private function getGlobalUserComment($data)
    {
        $user = User::find($data->user_id);

        return $user->comment;
    }

}
