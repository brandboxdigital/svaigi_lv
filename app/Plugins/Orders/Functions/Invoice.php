<?php

namespace App\Plugins\Orders\Functions;


use App\User;
use Dompdf\Dompdf;
use Dompdf\Options;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\View;
use App\Plugins\Orders\Model\OrderHeader;

trait Invoice
{
    public function invoice($invoice)
    {
        $order = OrderHeader::where('invoice', $invoice)->first();

        if (!$order) {
            abort(404);
        }

        // собираем значения на вывод в пдф, key=0 так как заказ всегда один
        $arrDisplayValues[0]['items'] = $order->items()->orderBy('product_name', 'asc')->get();
        $arrDisplayValues[0]['displayValues'] = $this->getDisplayValuesOrderInfo($order, $arrDisplayValues[0]['items']);
        $arrDisplayValues[0]['totalPrices'] = getCartTotals($order, [], $original ?? false);

        // instantiate and use the dompdf class
        $view = View::make('Orders::pdf.invoiceList', [
            'orders' => [$order], //  'order' => $order
            'arrDisplayValues' => $arrDisplayValues,
        ]);

        $options = new Options();
        $options->set('isRemoteEnabled', true);

        $dompdf = new Dompdf($options);
        $dompdf->setPaper('A4', 'portrait');

        if (request()->input('html')) {
            return $view->render();
        }

        $dompdf->loadHtml($view->render());
        $dompdf->render();

        /**
         * To test uncomment this!
         */
        if (request()->input('test')) {
            echo $view; die();
        }


        /**
         * Output the generated PDF to Browser
         */
        $dompdf->stream("invoice.pdf", [
            "Attachment" => 0,
        ]);

        exit();


        /**
         * No idea what this is ... it was commented out!!
         */
        // $user = (\Auth::user() ?? new User());
        // $curUserOrder = "{$order->id}-u{$user->id}";
        // if ($user->isAdmin || $curUserOrder == $order->invoice) {
        //     return view('Orders::pdf.invoice', ['order' => $order]);
        //     return PDF::loadView('Orders::pdf.invoice', ['order' => $order]);
        // }
    }
}
