<?php

namespace App\Plugins\Orders\Functions;


use App\Schedules;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Console\Command;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use App\Plugins\Orders\Model\OrderHeader;

trait OrderExportToPdf {


    /**
     * @var Schedules
     */
    private $schedule;

    public function createPdf($schedule) {
        /** @var Command $this */

        $orders = [];

        if ($schedule instanceof Schedules) {
            $this->schedule = $schedule;
            $orders = json_decode($this->schedule->filename);
        }
        else {
            $this->schedule = null;
            if (is_array($schedule)) {
                $orders = $schedule;
            }
            else {
                $this->error('Unknown Data passed');
            }
        }

        $orderObjects = OrderHeader::whereIn('id', $orders)->orderBy('created_at', 'asc')->get();

        $orderListClear = [];
        $orderErrors = [];
        foreach ($orderObjects as $order) {
            $bFindError = false;
            foreach ($order->items()->get() as $item) {
                if ($item->total_amount == "0") {
                    $orderErrors[] = $order->id;
                    $bFindError = true;
                    continue;
                }
            }

            $order->setState('accepted');

            if(!$bFindError)
                $orderListClear[] =  $order;
        }
        $orderObjects = $orderListClear;

        // собираем значения на вывод в пдф
        $arrDisplayValues = [];
        foreach ($orderObjects as $key=>$order)
        {
            $arrDisplayValues[$key]['items'] = $order->items()->orderBy('product_name', 'asc')->get();;
            $arrDisplayValues[$key]['displayValues'] = $this->getDisplayValuesOrderInfo($order, $arrDisplayValues[$key]['items']);
            $arrDisplayValues[$key]['totalPrices'] = getCartTotals($order, [], $original??false);
        }

        try {
            $view = View::make('Orders::pdf.invoiceList', [
                'orders' => $orderObjects,
                'arrDisplayValues' => $arrDisplayValues,
            ]);

            $options = new Options();
            $options->set('isRemoteEnabled', true);

            $dompdf = new Dompdf($options);
            $dompdf->loadHtml($view->render());
            $dompdf->render();

            $output = $dompdf->output();
            $filename = "order_export_" . date('dmYHis') . ".pdf";
            $filePath = storage_path('files') . "/" . $filename;
            file_put_contents($filePath, $output);

            $additionMessage = "";
            if (!empty($orderErrors)) {
                $additionMessage = "; Errors in orders with ID: " . implode(",", $orderErrors);
            }

            foreach ($orderObjects as $order) {
                $order->setState('accepted');
            }

            if ($schedule) {
                $schedule->update([
                    'result_state' => 1,
                    'running' => 0,
                    'finished' => 1,
                    'result_message' => 'Export created: <a href="' . route('download', ['files', $filename]) . '">Download</a>' . $additionMessage,
                ]);
            }
        }catch( \Exception $e) {
            $this->schedule->update([
                'result_state' =>0,
                'running' => 0,
                'finished' => true,
                'result_message' => 'Error creating export to pdf: '.$e->getMessage()
            ]);
        }
        $this->info('ExportToPdf Created');
    }


}
