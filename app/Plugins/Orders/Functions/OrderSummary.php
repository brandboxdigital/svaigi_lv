<?php

namespace App\Plugins\Orders\Functions;


use App\Plugins\Orders\Model\OrderLines;
use App\Schedules;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

trait OrderSummary
{

    protected $summaryFields = [
        'A'  => ['value'     => 'id',                    'label'    => 'ID',            ],
        'B'  => ['translate' => 'supplier_slug',         'label'    => 'Supplier slug',         'value' => 'supplier_id', ],
        'C'  => ['value'     => 'supplier_name',         'label'    => 'Supplier Name'  ],
        'D'  => ['value'     => 'ordered_at',            'label'    => 'Order Date',            'relation' => 'order', ],
        'E'  => ['fn'        => 'getDeliveryDate',       'label'    => 'Delivery Date',         'value'    => ['market_day_date', 'delivery'], 'relation' => 'order', ],
        'F'  => ['value'     => 'sku',                   'label'    => 'SKU',                   'relation' => 'products'],
        'G'  => ['value'     => 'product_name',          'label'    => 'Product Name'   ],
        'H'  => ['value'     => 'amount',                'label'    => 'Ordered Amount' ],
        'I'  => ['value'     => 'display_name',          'label'    => 'Display'        ],
        'J'  => ['value'     => 'cost',                  'label'    => 'Cost'           ],
        'K'  => ['value'     => 'markup',                'label'    => 'Markup'         ],
        'L'  => ['fn'        => 'getPriceWOvat',         'label'    => 'Price w/o vat',         'value' => null],
        'M'  => ['fn'        => 'getPriceVat',           'label'    => 'Vat',                   'value' => null],
        'N'  => ['fn'        => 'getPriceWVat',          'label'    => 'Price w/ vat',          'value' => null],
        'O'  => ['fn'        => 'getDiscount',           'label'    => 'Discount Percent',      'value' => null],
        'P'  => ['fn'        => 'getDiscountAmount',     'label'    => 'Discount Amount',       'value' => null],
        'Q'  => ['value'     => 'discount_name',         'label'    => 'Discount Name'  ],
        'R'  => ['relation'  => 'order',                 'label'    => 'User ID',               'value' => 'user_id'],
        'S'  => ['relation'  => 'order',                 'label'    => 'User Name/Last Name',   'value' => 'user_full_name'],
        'T'  => ['relation'  => 'order',                 'label'    => 'User Phone',            'value' => 'user_phone'],
        'U'  => ['relation'  => 'order',                 'label'    => 'User email',            'value' => 'user_email'],
        'V'  => ['relation'  => 'order',                 'label'    => 'Svaigi Comment',        'value' => 'svaigi_comment_stats'],
        'W'  => ['value'     => 'order_number_string',   'label'    => 'Order ID'       ],

        'X'  => ['fn'        => 'getLinePrice',          'label'    => 'Sum',                   'value' => null],
        'Y'  => ['relation'  => 'order',                 'label'    => 'Payment Type',          'value' => 'payment_type'],

        'Z'  => ['relation'  => 'order',                 'label'    => 'Discount Code',         'value' => 'discount_code', ],
        'AA'  => ['fn'        => 'getTotalDiscount',      'label'    => 'Discount Total',       'value' => null],

        'AB' => ['fn'        => 'getDeliveryDescription','label'    => 'Delivery Type',         'value' => null],
        'AC' => ['relation'  => 'order',                 'label'    => 'Delivery City',         'value' => 'city', ],
        'AD' => ['relation'  => 'order',                 'label'    => 'Delivery Address',      'value' => 'address', ],
        'AE' => ['relation'  => 'order',                 'label'    => 'Delivery Postcode',     'value' => 'city', ],
        'AF' => ['relation'  => 'order',                 'label'    => 'Delivery Comment',      'value' => 'user_comment', ],
		'AG' => ['fn'       => 'getDeliveryAmount',      'label'    => 'Delivery Price',        'value' => null],

        'AH' => ['relation'  => 'order',                 'label'    => 'Comment',               'value' => 'comments',],
    ];

    /**
     * @var int
     */
    private $index = 2;

    /**
     * @var Schedules
     */
    private $schedule;

    /**
     * @var Worksheet
     */
    private $sheet;

    /**
     * @var array
     */
    private $summaryData;

    /**
     * @var object
     */
    private $price;

    /**
     * @var float
     */
    private $discount;

    private $lastLine;

    /**
     * @param Schedules|null $schedule
     * @param array          $oids
     *
     * @return bool
     */
    public function createOrderSummary(Schedules $schedule = null, $oids = [])
    {

        try {
            if (!$schedule && empty($oids)) {
                return false;
            }


            if ($schedule) {
                $this->schedule = $schedule;
            }


            $excel = new Spreadsheet();
            $this->sheet = $excel->getActiveSheet();
            $this->createHeaders();

            $this->getSummaryData($oids);

            foreach ($this->summaryData as $index => $summaryDataItem) {
            	$this->lastLine = false;
            	$next = $this->summaryData->get(++$index);
            	if (!$next || $next->order_header_id != $summaryDataItem->order_header_id)
					$this->lastLine = true;
                $this->createSummaryLine($summaryDataItem);
                $this->index++;
                $this->price = null;
                $this->discount = null;
                echo "{$summaryDataItem->order_header_id}-Done || ";
            }

            $this->autoSizeColumns();

            $xls = new Xlsx($excel);

            $filename = 'export/summary' . Carbon::now()->timestamp . ".xlsx";

            $xls->save(storage_path("$filename"));

            $this->schedule->update(['result_state' => true, 'running' => false, 'finished' => true, 'result_message' => 'Summary created: <a href="' . route('download', explode("/",$filename)) . '">Download</a>']);
        } catch( \Exception $e) {
            $this->schedule->update(['result_state' => false, 'running' => false, 'finished' => true, 'result_message' => 'Error creating summary: '.$e->getMessage().' On Line:'.$e->getLine()]);
        }
        return true;
    }

    private function autoSizeColumns() {
        foreach($this->summaryFields as $col => $undef) {
            $this->sheet->getColumnDimension($col)->setAutoSize(true);
        }
    }

    private function createHeaders()
    {
        foreach ($this->summaryFields as $column => $summaryField) {
            $this->sheet->setCellValue($column . "1", $summaryField['label']);
        }
        $this->sheet->freezePane('A1');
    }

    private function getDeliveryDescription($data)
	{
		return strip_tags(__("deliveries.description.{$data->order->delivery->id}"));
	}

    private function getSummaryData($oids = [])
    {
        if (empty($oids)) {
            $oids = json_decode($this->schedule->filename);
        }

        $this->summaryData = OrderLines::with('order')->whereIn('order_header_id', $oids)->get();
    }

    private function runFN($data, $summaryField) {
        if (method_exists($this, $summaryField['fn'])) {
            // echo "Method {$summaryField['fn']} Found \n";
            $fnData = [];
            if (is_array($summaryField['value'])) {
                foreach ($summaryField['value'] as $fieldVal) {
                    $fnData[$fieldVal] = $data->$fieldVal;
                }
            }
            return $this->{$summaryField['fn']}(count($fnData) ? $fnData : $data);
        }
        return "";
    }

    private function runRelation($data, $summaryField) {
        $dataRel = $data;
        if (is_array($summaryField['relation'])) {
            foreach ($summaryField['relation'] as $rel) {
                $dataRel = $dataRel->$rel;
            }
        } else {
            $dataRel = $dataRel->{$summaryField['relation']};
        }

        $dataRel->isOriginal = true;

        if($summaryField['fn']??false) {
            return $this->runFN($dataRel, $summaryField);
        }

        $fieldName = $summaryField['value'];

        if(is_array($fieldName)) {
            $fData = [];
            foreach($fieldName as $fname) {
                $fData[] = $dataRel->$fname;
            }
            return implode(" ", $fData);
        }

        return $dataRel->$fieldName;
    }

    private function createSummaryLine($data)
    {
        foreach ($this->summaryFields as $column => $summaryField) {
            $fieldValue = null;
            switch ($summaryField) {
                /**
                 * Relation fixes
                 */
                case ($summaryField['relation']??false) ? $summaryField : false:
                    $fieldValue = $this->runRelation($data, $summaryField);
                    break;

                case ($summaryField['fn']??false) ? $summaryField : false:
                    $fieldValue = $this->runFN($data, $summaryField);
                    break;

                default:
                    if(is_array($summaryField['value'])) {
                        $fdata = [];
                        foreach($summaryField['value'] as $fval) {
                            $fdata[] = $data->$fval;
                        }
                        $fieldValue = implode(" ", $fdata);
                    } else {
                        $fieldValue = $data->{$summaryField['value']};
                    }
                    break;
            }

            //if ($fieldValue) {
                if (($summaryField['translate'] ?? false)) {
                    $fieldValue = __($summaryField['translate']. "." . $fieldValue);
                }

                $this->sheet->setCellValue($column . $this->index, $fieldValue);
            //}

        }
    }

    /**
     *
     * @param $data
     *
     * @return array|null|string
     */
    private function getDeliveryDate($data)
    {

        $marketDay = $data['market_day_date'];

        $delivery = $data['delivery'];

        $md = new Carbon($marketDay);

        $dt = $md->copy();
        $modifiedMD = $dt->addDays($delivery->deliveryTime ?? 0);
        $mdDate = $modifiedMD->format('j');
        $month = __("translations." . $modifiedMD->format('F'));
        $dayName = __("translations." . $modifiedMD->format('l'));
        return __('translations.marketDayDeliveryText', ["dayname" => $dayName, 'date' => $mdDate, 'month' => $month]);
    }

    private function getDiscount($data)
    {
        if($this->discount) {
            return $this->discount;
        }
        $this->discountClass = "Sales";
        $discount = $lineDiscount = $data->discount;
        $orderDiscount = $data->order->discount_amount;

        if ($orderDiscount) {
            $discount_target = $data->order->discount_target;
            $discount_items = $data->order->discount_items;

            switch($discount_target) {
                case "product":
                    if(in_array($data->product_id, $discount_items)) {
                        $discount = $orderDiscount>$discount?$orderDiscount:$discount;
                        $this->discountClass = "DiscountCode";
                    }
                    break;

                case "cateogry":
                    $prodCat = $data->products->extra_categories()->pluck('id')->toArray();
                    if(in_array($prodCat,$discount_items)) {
                        $discount = $orderDiscount>$discount?$orderDiscount:$discount;
                        $this->discountClass = "DiscountCode";
                    }
                    break;
            }
        }
        $this->discount = $discount;

        return $discount;
    }

    private function getPrices($data) {
		if (!$this->price) {
			$this->price = $this->getDisplayValuesOrderInfo($data->order, [$data]);
			$this->price['total'] = getCartTotals($data->order);
		}
		/*if (!$this->price) {
			$discount = $this->getDiscount($data);
			$this->price = calcPrice($data->cost, $data->vat_amount, $data->markup, $discount, $data->amount);
		}*/
	}

    private function getPriceWOvat($data)
    {
        $this->getPrices($data);
        return current($this->price['vatTotals'])['summWoVat'];
        //return $this->price->wdiscount->pricewovat;
    }

    private function getPriceVat($data) {
        $this->getPrices($data);
		return current($this->price['vatTotals'])['summVat'];
        //return $this->price->wdiscount->pricevat;
    }

    private function getPriceWVat($data) {
        $this->getPrices($data);
		return $this->getPriceWOvat($data) + $this->getPriceVat($data);
        //return $this->price->wdiscount->price;
    }

    private function getDiscountAmount($data) {
        $this->getPrices($data);
		return $data->price_raw - $data->price;
        //return $this->price->wdiscount->discount;
    }

    private function getLinePrice($data) {
        $this->getPrices($data);
		return $this->getPriceWOvat($data) + $this->getPriceVat($data);
        //return $this->price->sum->wdiscount->wvat;
    }

    private function getTotalDiscount($data) {
        $this->getPrices($data);
		return ($data->price_raw - $data->price) * $data->amount;
        //return $this->price->sum->wdiscount->wvat-$this->price->sum->wodiscount->wvat;
    }

    private function getDeliveryAmount($data) {
    	if (!$this->lastLine) return "";
        $this->getPrices($data);
        return $this->price['total']->delivery + $this->price['total']->delivery_vat;
        //return $this->price->sum->wdiscount->wvat-$this->price->sum->wodiscount->wvat;
    }

}
