<?php

namespace App\Plugins\Orders\Functions;


use App\Mail\OrderSender;
use App\Plugins\Suppliers\Model\Supplier;
use App\Schedules;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Finder\SplFileInfo;

trait SendOrders
{
    private $folder;
    private $schedule;
    private $suppliers;

    public function sendEmails(Schedules $schedule = null) {
        $isDebug = config('app.debug', true);

        $this->folder = storage_path('app/imports/ordersSendPdf');
        $errors = [];

        if($schedule) {
            $this->schedule = $schedule;
        }

        $this->suppliers = __('supplier.slug');


        $arMails = [];

        /** @var SplFileInfo $file */
        foreach($this->getFiles() as $file) {
            $supplier_slug = pathinfo($file->getFilename(), PATHINFO_FILENAME);

            $supplier_id = array_search($supplier_slug, $this->suppliers);
            if($supplier = Supplier::find($supplier_id)) {
                $arMails[$supplier->email]["files"][] = $file;
                if(!isset($arMails[$supplier->email]["supplier"]))
                    $arMails[$supplier->email]["supplier"] = $supplier;

            } else {
                $errors[] = "Supplier with slug '$supplier_slug' doesn't exist";
            }
        }

        foreach($arMails as $supplierMail=>$arData) {

            try {
                Mail::to($supplierMail)->send(new OrderSender($arData["files"], $arData["supplier"]));
            } catch (\Throwable $th) {
                $errors[] = "Error sending email to: $supplierMail";
                Mail::to('saimniecibas@svaigi.lv')->send(new OrderSender($arData["files"], $arData["supplier"]));
            }

        }


        if(count($this->getFiles()) && $this->schedule) {

            $this->schedule->update([
                'finished' => true,
                'running' => false,
                'result_state' => true,
                'result_message' => ($isDebug)
                    ? implode("<br />", $errors)
                    : 'Emails Sent'
            ]);

        } elseif($this->schedule && $errors) {
            if(count($this->getFiles())) {
                $update = ['finished' => false, 'result_state' => false, 'running' => true];
            } else {
                $update = ['finished' => true, 'result_state' => false, 'running' => false];
            }
            $this->schedule->update(array_merge(['result_message' => implode("<br />", $errors)], $update));
        }
        $this->deleteOldFiles();
    }


    private function getFiles() {
        return File::files($this->folder);
    }

    private function deleteOldFiles(){
        File::delete($this->folder);
    }
}
