<?php

namespace App\Plugins\Orders\Functions;


use App\Plugins\MarketDays\Model\MarketDay;
use App\Plugins\Orders\Model\OrderHeader;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Route;

/**
 * Trait Orders
 *
 * @package App\Plugins\Orders\Functions
 */
trait Orders
{
    /**
     * @return array
     */
    public function getList()
    {
        return [
            ['field' => 'check', 'label' => 'checkall'],
            ['field' => 'order_number_string', 'label' => 'Nr'],
            ['field' => 'invoice', 'label' => 'Invoice'],
            ['field' => 'ordered_at_formatted', 'label' => 'Order Time'],
            ['field' => 'user_full_name', 'label' => 'Name, Last Name'],
            ['field' => 'user_email', 'label' => 'Email'],
            ['field' => 'payment', 'label' => 'Payment Type'],
            ['field' => 'paid_amount', 'label' => 'Paid'],
            ['field' => 'state_selector', 'label' => 'State' ,"class"=>"order-state-field", 'classbyfield' => "state"],
            ['field' => 'order_sum', 'label' => 'Sum of order'],
            ['field' => 'market_day', 'label' => 'Market Day'],
            ['field' => 'buttons', 'label' => '', 'buttons' => ['view', 'delete']],
        ];
    }


    /**
     * Get Orders based on filters and search
     *
     * @param      $search
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getFilteredResult($search, $trashed = false, $fullResults = false)
    {
        /** @var OrderHeader $orders */
        $orders = new OrderHeader;

        $orders = $orders->where('state', '!=', 'draft')->filters();

        if ($search) {
            $orders = $orders
                ->whereHas('buyer', function (Builder $q) use ($search) {
                    $q->where('name', 'like', "%$search%")
                        ->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('email', 'like', "%$search%");
                })
                ->orWhereHas('order_number', function (Builder $q) use ($search) {
                    $q->where('id', $search);
                })
                ->orWhere('invoice', 'like', "%$search%");
        }
        if($fullResults) {
            return $orders->get();
        }

        return $orders->paginate(100);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Routing\Route|object|string
     */
    public function getEditName($id)
    {
        $r = explode(".", Route::currentRouteName());
        if (($r[1] ?? "") == 'search') {
            return request()->route('search');
        }

        return OrderHeader::find($id)->invoice??"";
    }

    public function getViewFields($order = null)
    {
        $fieldList = [
            'state_selectpicker'                => 'Order State',
            'invoice_url'                       => 'Invoice Number',
            'ordered_at_formatted'              => 'Order Created',
            'startuser'                         => 'hr',
            'user_full_name'                    => 'Klients',
            'user_email'                        => 'Klienta e-pasts',
            'user_phone'                        => 'Klienta Tel.',
            'user_group'                        => 'Klienta Grupa',
            'comments'                          => 'Klienta komentārs',
            'enduser_startDelivery'             => 'hr',
            'selected_delivery'                 => 'Delivery',
            'selected_delivery_provider'        => 'Delivery provider',
            'selected_delivery_provider_data'   => 'Delivery provider Data',
            'delivery_provider_label_download'  => 'Delivery Label Download',
            'address'                           => 'Address',
            'city'                              => 'City',
            'postcode'                          => 'Postal code',
            'user_comment'                      => 'Delivery Comment',
            'endDelivery'                       => 'hr',
            'paid_amount'                       => 'Paid Amount',
            'coupon_code'                       => 'Coupon code',
            'payment'                           => 'Payment',
            'market_day'                        => 'Market Day',
            'market_day_date_formatted'         => 'Market Day Date',
            'svaigi_comment_invoice'            => 'Svaigi Komentārs rēķinam',
            'svaigi_comment_stats'              => 'Svaigi Komentārs atskaitei',
        ];

        // if ($order && $deliveryProvider = $order->delivery->delivery_provider_class) {
        //     $fieldList
        // }

        return $fieldList;
    }

    public function getFilters()
    {

        $marketDays = MarketDay::all()->pluck('marketDay', 'id')->toArray();
        $market_days = [];

        foreach ($marketDays as $mdid => $md) {
            $market_days[$mdid] = $md[language()];
        }

        return [
            ['type' => 'select', 'name' => 'market_day', 'label' => 'Market Day', 'options' => $market_days],
            ['type' => 'select', 'name' => 'trashed', 'label' => 'Trashed', 'options' => ['No', 'Yes']],
            ['type' => 'select', 'name' => 'state', 'label' => 'Order State', 'options' => ['ordered' => 'New', 'finished' => 'Finished']],
            ['type' => 'text', 'name' => 'ordered_at', 'label' => 'Ordered at range', 'class' => 'daterangepicker']
        ];
    }

    public function getDisplayValuesOrderInfo($order, $items)
    {
        $DiscountItemsId    = getDiscountItemsId($order);
        $itemsDisplayValues = [];
        $arrDisplayValues   = [
            'discountPriceNotCoupon' => 0,
            'vatTotals' => [],
        ];

        foreach($items as $item) {
            // dump($item->total_amount);
            // убрать подозрительный код
            if ($item->total_amount == 0) {
                $item->total_amount = 1;
            }
            //$sum = round((($item->price*$item->amount)/$item->total_amount)*$item->real_amount ,2);
            $itemPriceAndDiscount = getDiscountAndPrice($order, $item, $DiscountItemsId);

            // Prepare the conclusion of the price per unit.goods.For coupons - initial, for promo - already with a discount
            // if ($itemPriceAndDiscount['codeBetterThanPromo'] > 0 || true)
            if ($item->discount_name != "0" && $item->product_id != $itemPriceAndDiscount['codeBetterThanPromo']) {
                if ($item->price_raw > $item->price) {
                    $itemsDisplayValues["price"] = $item->price;
                    $itemsDisplayValues["vat"] = $item->vat;
                    $itemsDisplayValues["sum"] = round((($item->price * $item->amount)) , 2);
                } else {
                    $itemsDisplayValues["price"] = $item->price_raw;
                    $itemsDisplayValues["vat"] = $item->vat_raw;
                    $itemsDisplayValues["sum"] = round((($item->price_raw * $item->amount)) , 2);
                }
            } else {
                $itemsDisplayValues["price"] = $item->price_raw;
                $itemsDisplayValues["vat"] = $item->vat_raw;
                // $itemsDisplayValues["sum"] = round((($itemPriceAndDiscount['itemResultPrice'] * $item->amount)) , 2);
                $itemsDisplayValues["sum"] = round($item->price_raw * $item->amount, 2);

                $arrDisplayValues['discountPriceNotCoupon'] += ($item->price_raw - $item->price) * $item->amount;
            }

            $itemsDisplayValues["depositSum"] = round($item->deposit_amount * $item->amount, 2);

            if (!isset($arrDisplayValues["vatTotals"][$item->vat_id])) {
                $arrDisplayValues["vatTotals"][$item->vat_id] = [
                    'summWoVat' => 0,
                    'summVat' => 0,
                    'amount' => $item->products->vat->amount ?? 0,
                    'count' => 0,
                ];
            }

            $arrDisplayValues["vatTotals"][$item->vat_id]['summWoVat'] += $itemsDisplayValues["sum"] - $itemsDisplayValues["vat"] * $item->amount;
            $arrDisplayValues["vatTotals"][$item->vat_id]['summVat'] += $itemsDisplayValues["vat"] * $item->amount;
            $arrDisplayValues["vatTotals"][$item->vat_id]['count']++;

            $arrDisplayValues[$item->id] = $itemsDisplayValues;
        }


        return $arrDisplayValues;
    }
}
