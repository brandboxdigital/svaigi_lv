<div class="row viewRow">
    <div class="col-md-1 checkall">
        <a href="javascript:void(0)">All</a>
    </div>
    <div class="col-md-3"><h5>Product</h5></div>
    <div class="col-md-2"><h5>Variation</h5></div>
    <div class="col-md-1"><h5>Price w/o vat</h5></div>
    <div class="col-md-1"><h5>Vat</h5></div>
    <div class="col-md-1"><h5>Amount</h5></div>
    <div class="col-md-1"><h5>Corrected Amount</h5></div>
    <div class="col-md-1 text-right"><h5>Sum</h5></div>
</div>
@foreach($items as $item)
    <div class="row viewRow prodrow">
        <div class="col-md-1">
            <input type="checkbox" name="massAction" value="{{$item->id}}" class="massAction">
        </div>
        <div class="col-md-3">{{ $item->product_name ?? 'Error' }}
            <br/><small>
                {{ $item->supplier_name }}
            </small></div>
        <div class="col-md-2">
            {{ $displayValues[$item->id]["price"] ?? 'Error' }} &euro; / {{ $item->display_name ?? 'Error' }}
            <br />
            <small>+{{ number_format($displayValues[$item->id]["depositSum"] ?? 0, 2, ".", "") }}&nbsp;&euro; (deposit)</small>
        </div>

        <div class="col-md-1">{{ ($displayValues[$item->id]["price"] ?? 0 - $displayValues[$item->id]["vat"]) ?? 0 }}</div>
        <div class="col-md-1">{{ $displayValues[$item->id]["vat"] ?? 0 }} ({{ $item->products->vat->amount ?? 0 }}%)</div>

        <div class="col-md-1">{{ $item->amount ?? 'error'}} ({{$item->total_amount ?? 'error'}} {{$item->amount_unit ?? 'error'}})</div>
        <div class="col-md-1">
            @if(!($original??false))
                <input type="text" class="amountinput" data-origvalue="{{$item->amount ?? 'error'}}" size="5"
                       name="real_amount" value="{{ $item->amount ?? 'error'}}"/> <a
                        href='{{route('orders.setAmount', [$order->id, $item->id])}}'
                        class='setCorrectAmount btn btn-xs btn-success invisible'><i class='fas fa-check'></i></a>
            @else
                {{$item->total_amount ?? 'error'}} {{$item->amount_unit ?? 'error'}}
            @endif
        </div>
        <div class="col-md-1 text-right">{{ number_format($displayValues[$item->id]["sum"] ?? 0,2, ".","") }} &euro;</div>
    </div>
@endforeach

<div class="row viewRow">
    <div class="col-md-12">
        <hr style="border:1px solid black;">
    </div>
</div>

<div class="row viewRow">
    <div class="col-md-9 text-right font-weight-bold">Deposit:</div>
    <div class="col-md-3 text-right">{{ number_format($totalPrices->depositSum,2, ".","") }} &euro;</div>
</div>

<div class="row viewRow">
    <div class="col-md-9 text-right font-weight-bold">Delivery w/o VAT:</div>
    <div class="col-md-3 text-right">{{ number_format($totalPrices->delivery,2, ".","") }} &euro;</div>
</div>

<br>

<div class="row viewRow">
    <div class="col-md-9 text-right font-weight-bold">Delivery VAT ({{$totalPrices->delievery_vat_amount}}%):</div>
    <div class="col-md-3 text-right">{{ number_format($totalPrices->delivery_vat,2, ".","") }} &euro;</div>
</div>

@foreach($displayValues["vatTotals"] as $vat_id => $vatTotals)
    <div class="row viewRow">
        <div class="col-md-9 text-right font-weight-bold">Sum w/o VAT(products count {{$vatTotals['count']}})
            ({{ $vatTotals['amount'] }}%):
        </div>
        <div class="col-md-3 text-right">{{ number_format($vatTotals['summWoVat'],2, ".","") }} &euro;</div>
    </div>
    <div class="row viewRow">
        <div class="col-md-9 text-right font-weight-bold">VAT ({{ $vatTotals['amount'] }}%):</div>
        <div class="col-md-3 text-right">{{ number_format($vatTotals['summVat'],2, ".","") }} &euro;</div>
    </div>
@endforeach

<br>

{{--
@foreach($totalVat as $vat_id => $vat_amount)
    @php
        $vatAmount = \App\Plugins\Vat\Model\Vat::find($vat_id)->amount;
        $totals = getCartTotals($order, $vatItems[$vat_id], $original??false, true);
        //print_r($totals);
        $totalWoVat[$vat_id] = $twv = ($totals->productSum) - $totals->vatSum;
        $totalVat[$vat_id] = $tv = $totals->vatSum;
    @endphp
    <div class="row viewRow">
        <div class="col-md-9 text-right font-weight-bold">Sum w/o VAT(products count {{count($vatItems[$vat_id])}})
            ({{ $vatAmount }}%):
        </div>
        <div class="col-md-3 text-right">{{ number_format($twv,2, ".","") }} &euro;</div>
    </div>
    <div class="row viewRow">
        <div class="col-md-9 text-right font-weight-bold">VAT ({{ $vatAmount }}%):</div>
        <div class="col-md-3 text-right">{{ number_format($tv,2, ".","") }} &euro;</div>
    </div>
@endforeach
--}}

<div class="row viewRow">
    <div class="col-md-9 text-right font-weight-bold">Promotions:</div>
    <div class="col-md-3 text-right">- {{ number_format($displayValues["discountPriceNotCoupon"],2, ".","") }} &euro;</div>
</div>
@if($order->discount_target)
    <div class="row viewRow">
        <div class="col-md-9 text-right font-weight-bold">Discount ({{$order->discount_code}}
            - {{ $order->discount_amount }}{!! $order->discount_type=='percent'?"%":" &euro;" !!}
            ):
        </div>
        <div class="col-md-3 text-right">- {{ number_format($totalPrices->discount,2, ".","") }} &euro;</div>
    </div>
@endif
<div class="row viewRow">
    <div class="col-md-9 text-right font-weight-bold">Total To Pay:</div>
    <div class="col-md-3 text-right">{{ number_format($totalPrices->toPay,2, ".","") }} &euro;</div>
</div>
<div class="row viewRow">
    <div class="col-md-9 text-right font-weight-bold">Total Paid:</div>
    <div class="col-md-3 text-right">{{ number_format($order->paid,2, ".","") }} &euro;</div>
</div>
<div class="row viewRow">
    <div class="col-md-9 text-right font-weight-bold">To pay(or return):</div>
    <div class="col-md-3 text-right">{{ number_format($totalPrices->toPay - $order->paid,2) }} &euro;</div>
</div>
