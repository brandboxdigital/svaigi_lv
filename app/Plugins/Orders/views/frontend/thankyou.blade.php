@extends('layouts.app')

@section('content')
    @include("Orders::frontend.partials.step")

    @includeIf("Orders::frontend.partials.".($stepInclude??"noInclude"))

    <div class="sv-blank-spacer medium"></div>

    {{-- <div class="vc_row wpb_row vc_row-fluid" style="">
        <div class="wpb_column vc_column_container">
            <div class="vc_column-inner">
                <div class="wpb_wrapper"> --}}


                    <div class="sv-text-page">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 col-md-push-1">
                                    <div class="sv-text-block">
                                        <h1 class="text text-center">
                                            @if($paymentMethod == 'paysera')
                                                {!! _t('translations.orderPaymentProcessingText') !!}
                                            @else
                                                {!! _t('translations.orderCompleteThankYouText') !!}
                                            @endif
                                        </h1>


                                        <br>

                                        <div
                                            class="hidden-success"
                                            {{ ($paymentMethod == 'paysera') ? 'style="display:none;"' : '' }}
                                        >
                                            <p>Tavs pasūtījums virtuālajā tirgū ir svaigi veikts, un to apstiprinās arī ziņa Tavā e-pastā. Tavs pasūtījums tiks nodots saimniecībām produktu sagatavošanai: vākšanai, lasīšanai, griešanai, šmorēšanai un piegādei.</p>
                                            <p>Tevis izvēlētajā Tirgus dienā sagatavosim pasūtījumu kopā ar saimniekiem. Tu pasūtījumu saņemsi ar piegādi uz mājām vai nāksi pēc sava pasūtījuma Margrietas ielā 7, Rīgā laikā no plkst. 17:30 līdz 19:00 Vairāk informāciju uzzini: <a href="/ka-tas-notiek">Kā tas notiek?</a>.</p>
                                            <p>Ja vēl neesi veicis apmaksu, maksā ar pārskaitījumu uz bankas kontu LV96HABA0551040281516 līdz pasūtījuma saņemšanai vai norēķinies skaidrā naudā pasūtījuma saņemšanas brīdī.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sv-blank-spacer medium"></div>

                    <div class="btn-row btn-row--center">
                        <a href="/" class="btn btn-green">
                            Uz sākumu
                        </a>
                    </div>


                {{-- </div>
            </div>
        </div>
    </div> --}}

    <div class="sv-blank-spacer medium"></div>

    <div class="vc_row wpb_row vc_row-fluid sv-thankyou-image">
        <div class="wpb_column vc_column_container">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="bg-parallax">
                        <div class="image" style="background-image: url({{asset('assets/img/tmp/photo-18.jpg')}});"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@if($paymentMethod == 'paysera')
    @push('scripts')
        <script>
            var payseraInterval;
            $(document).ready(function () {
                payseraInterval = setInterval(function(){
                    $.get('{{ route('paysera.success') }}', function( data ) {
                        if(data) {
                            clearInterval(payseraInterval);
                            $('.sv-text-block .text').html('{!! str_replace( array( "\n", "\r" ), array( "\\n", "\\r" ), _t("translations.orderCompleteThankYouText") ) !!}');
                            $('.sv-text-block .hidden-success').show();
                        }
                    });
                }, 500);
            });
        </script>
    @endpush
@endif

@if($googleAnalytics !== null)
    @push('scripts')
        <script>
            let array = @json($googleAnalytics);
            dataLayer.push({ ecommerce: null });
            dataLayer.push(array)
        </script>
    @endpush
@endif
