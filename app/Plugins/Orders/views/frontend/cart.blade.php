@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
<div class="col-sm-12">

    @forelse($cart->items(true)->get() as $item)
        @push('items')
            @include("Orders::frontend.partials.item")
        @endpush
    @empty
        @php
            $noItems = true;
        @endphp
    @endforelse

    @php
        $cartTotals = getCartTotals($cart);
    @endphp

    @if(!($noItems??false))

        @includeIf("Orders::frontend.partials.$stepInclude")

        <div class="sv-blank-spacer small"></div>

        <div class="sv--title">
            <h3>{!! _t('translations.cartChooseTimeAndType') !!}</h3>
        </div>

        <div class="sv-blank-spacer very-small"></div>

        @include("Orders::frontend.partials.deliveries")

        <div class="sv-blank-spacer small hidden-xs"></div>
        <hr class="hidden-xs">
        <div class="sv-blank-spacer small"></div>

        <div class="sv-cart row">
            <div class="list">
                <div class="item header">
                    <h3 class="product">
                        {!! _t('translations.cartProducts') !!}
                    </h3>
                    <div class="price">
                        {!! _t('translations.cartPrice') !!}
                    </div>
                    <div class="quantity">
                        {!! _t('translations.cartQuantity') !!}
                    </div>
                    <div class="total">
                        {!! _t('translations.cartSum') !!}
                    </div>
                </div>

                @stack('items')

                <div class="clear">
                    <a href="{{ route('clearCart') }}"
                       class="sv-filters-cancel">{!! _t('translations.clearCart') !!}</a>
                </div>

                <div class="sv-blank-spacer small"></div>

                <div class="coupon">
                    <form method="post" action="{{ r('addDiscountCode') }}" id="discountCodeForm">
                        <div class="enter">
                            {{ @csrf_field() }}
                            <input type="text" name="code" class="nr enderDiscountCode"
                                   placeholder="{!! __('translations.cartDiscountCode') !!}" value="{{ $cart->discount_code }}" />
                        </div>
                        <input type="submit" class="sv-btn" value="OK" />
                    </form>
                </div>

            </div>
            <div class="sidebar sidebar--sticky">
                <div class="totals help--sticky">
                    <h3 class="cart-overview-btn">{!! _t('translations.cartTotals') !!}</h3>
                    <div class="list">
                        <div class="item">
                            <div>
                                {!! _t('translations.cartProducts') !!}
                            </div>

                            <div class="productSum">
                                {{ $cartTotals->productSum }}&nbsp;€
                            </div>
                        </div>
                        <div class="item" {{ $cart->delivery_id?"":"style=display:none;" }}>
                            <div>
                                {!! _t('translations.cartDelivery') !!}
                            </div>

                            @if ($cart->delivery && $cart->delivery->hasDynamicPrice())
                                <div class="delivery"> Pēc adreses norādes </div>
                            @else
                                <div class="delivery">
                                    @if($cart->delivery_amount === null) ? $cart->delivery_amount = 0.00 :  $cart->delivery_amount @endif
                                    {{number_format($cart->delivery_amount + $cartTotals->delivery_vat, 2, '.', '')}}&nbsp;€
                                </div>
                            @endif
                        </div>

                        <div class="item" {{ $cartTotals->depositSum?"":"style=display:none;" }}>
                            <div>
                                {!! _t('translations.cartDeposit') !!}
                            </div>
                            <div class="depositSum">
                                {{number_format($cartTotals->depositSum, 2, '.', '')}}&nbsp;€
                            </div>
                        </div>


                        <div class="item" {{ $cart->discount_code?"":"style=display:none;" }}>
                            <div class="discount-wrap">
                                {!! _t('translations.cartDiscount') !!}
                                (<span style="text-transform: uppercase; font-weight:bold; " class="dcode">{{$cart->discount_code}}</span>)
                                <a href="{{ r('removeDiscountCode') }}" class="remove" id="removeDiscountCode"></a>

                                <small class="discount-description" {{ $cartTotals->discount_description?"":"style=display:none;" }}  > {!! $cartTotals->discount_description !!} </small>
                            </div>
                            <div class="discount">
                                -&nbsp;{{ $cartTotals->discount }}&nbsp;€
                            </div>
                        </div>

                    </div>

                    <div class="checkout">
                        <div class="list">
                            <div class="item">
                                <div>
                                    {!! _t('translations.cartToPay') !!}
                                </div>
                                <div class="toPay">
                                    {{ $cartTotals->toPay }}&nbsp;€
                                </div>
                            </div>
                        </div>
                        <div class="btn-row btn-row--fill">
                            <a href="{{ r('checkout') }}" class="btn btn-green">{!! _t('translations.cartFormOrder') !!}</a>
                        </div>
                        <div class="btn-row btn-row--fill mt-1" style="border: 1px solid #e2e2e2;">
                            <a href="/partika" class="btn btn-secondary border border-secondary">Atgriezties uz produktu sarakstu</a>
                        </div>
                    </div>
                </div>
                {{-- <div class="clear">
                    <a href="{{ route('clearCart') }}"
                       class="sv-filters-cancel">{!! _t('translations.clearCart') !!}</a>
                </div> --}}
            </div>
        </div>

        <div class="sv-blank-spacer small"></div>

    @else

    <div class="sv-message is-empty-cart">
      <h2 style="padding: 20px; background-color: #f8ddc4;">
        Grozs ir tukšs!
      </h2>
    </div>

    @endif


</div>
</div>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/cart.js') }}"></script>
@endpush
