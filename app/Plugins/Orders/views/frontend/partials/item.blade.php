@php
    /** @var \App\Cache\ProductCache $product */
    try {
        $product = $cache->getProduct($item->product_id);
    } catch (\Throwable $th) {
        return null;
    }

    $random_id = str_random(20);

    $itemPriceAndDiscount = getDiscountAndPrice($cart, $item, getDiscountItemsId($cart));

    $crossPrice = false;
    if ($product->hasManyPrices())
    {
        $price = $itemPriceAndDiscount['itemResultPrice'];
        $price_raw = $item->price_raw;
    } else {
        $price = $product->prices()->price;
        $price_raw = $product->prices()->oldPrice;
    }

    $displayInlinePrice = $price;
    $total = $item->amount * $price;
    if ($itemPriceAndDiscount['codeBetterThanPromo'] > 0 )
    {
        // $total = $item->amount * $price_raw;
        $total = $item->amount * $price;
        $displayInlinePrice = $price_raw;
    } else if ($product->isSale()) {
        $crossPrice = true;
    }
@endphp

@if($product->hasManyPrices())
    @push("variations-$random_id")
        <select name="variation_id" class="selectpicker" data-href="{{ r('cartAdd') }}">
            @foreach($product->prices() as $priceID => $price)
                <option data-wrap="true" {{ $product->isAvailable($priceID, $item->products->storage_amount)?"":"disabled" }}
                        value="{{ $priceID }}" {{$item->variation_id==$price->id?"selected":""}}
                        {{ $crossPrice ? "data-origprice={$price->oldPrice}&nbsp;€":"" }}>
                    @if($crossPrice)
                        {!! implode(" / ",[$price->price."&nbsp;€", $price->display_name]) !!}
                    @else
                        {!! implode(" / ",[$price->oldPrice."&nbsp;€", $price->display_name]) !!}
                    @endif
                </option>
            @endforeach
        </select>
    @endpush
@else
    @push("variations-$random_id")
        <input type="hidden" value="{{$product->prices()->id}}" name="variation_id" />

        @if($crossPrice)
            <s>{{ $product->prices()->oldPrice }}&nbsp;€</s>
        @endif
        {!! implode(" / ",[$displayInlinePrice."&nbsp;€", $product->prices()->display_name]) !!}
    @endpush
@endif

@if( in_array($cart->market_day_id, $product->marketDays) && !empty($product->getVariationPrice($item->variation_id)) && $product->product->state == 1)
    <form>
        <div class="item">
            <input type="hidden" name="product_id" value="{{ $product->id }}" />
            <input type="hidden" name="line" value="{{ $item->id }}" />
            <div class="product">
                <div class="image">
                    <a href="{{ $product->getUrl() }}">
                        <img loading="lazy" src="{{ $product->image(config('app.imageSize.product_image.list')) }}"
                             style="max-width:105px;" />
                    </a>
                </div>
                <div class="title">
                    <a href="{{ $product->getUrl() }}" class="name">{{ $item->product_name }}</a>
                    <a href="{{ r('supplierOpen', [getSupplierSlugs(true),__("supplier.slug.{$product->supplier_id}")]) }}"
                       class="farmer">{{ $item->supplier_name }}</a>



                    {{-- @if($product->isSale())
                        <div>
                            <s>{{ $product->prices()->oldPrice }}&nbsp;€</s>
                            {!! implode(" / ",[$product->prices()->price."&nbsp;€", $product->prices()->display_name]) !!}
                        </div>
                    @else --}}
                        {{-- @stack("variations-$random_id") --}}
                    {{-- @endif --}}
                    <a href="{{ r('cart.removeItem', [$item->id]) }}" class="remove discard"></a>
                </div>
            </div>
            <div class="price">
                {{-- @if($product->isSale() && $product->prices())
                    <div>
                        <s>{{ $product->prices()->oldPrice }}&nbsp;€</s>
                        {!! implode(" / ",[$product->prices()->price."&nbsp;€", $product->prices()->display_name]) !!}
                    </div>
                @else --}}
                    @stack("variations-$random_id")
                {{-- @endif --}}

                @if ($item->deposit_amount)
                    <div class="sv-product--deposit-price is-small is-left"> +{{ number_format($item->deposit_amount, 2, '.', '') }}&nbsp;€ </div>
                @endif
            </div>
            <div class="quantity">
                <!-- <input class="spinner" name="amount" value="{{ $item->amount }}" /> -->
                <select class="selectpicker " name="amount" data-href="{{ r('cartAdd') }}">
                   @foreach(range(1,($item->amount+10)) as $amnt)
                       <option value="{{$amnt}}" {{ $item->amount==$amnt?"selected":"" }}>{{$amnt}}</option>
                   @endforeach
               </select>
            </div>
            <!-- <div>
                <a href="{{ r('cartAdd') }}" class="updateCartProduct">Update</a>
            </div> -->
            <div class="total">
                {{ number_format($total, 2) }}&nbsp;€

                @if ($item->deposit_amount)
                    <div class="sv-product--deposit-price is-small"> +{{ number_format($item->deposit_amount * $item->amount, 2, '.', '') }}&nbsp;€ </div>
                @endif
            </div>
        </div>
    </form>
@else
    <div class="item is-disabled">
        <div class="product">
            <div class="image">
                <a href="{{ $product->getUrl() }}">
                    <img loading="lazy" src="{{ $product->image(config('app.imageSize.product_image.list')) }}"
                         style="max-width:105px;" />
                </a>
            </div>
            <div class="title">
                <a href="{{ $product->getUrl() }}" class="name">{{ $item->product_name }}</a>
                <a href="{{ r('supplierOpen', [getSupplierSlugs(true),__("supplier.slug.{$product->supplier_id}")]) }}"
                   class="farmer">{{ $item->supplier_name }}</a>
                {{-- @stack("variations-$random_id") --}}
            </div>
        </div>
        <div class="price">
            @if($product->isSale() && $product->prices() && !is_array($product->prices()))
                <div>
                    <s>{{ $product->prices()->oldPrice }}&nbsp;€</s>
                    {!! implode(" / ",[$product->prices()->price."&nbsp;€", $product->prices()->display_name]) !!}
                </div>
            @else
                {!! implode(" / ",[$item->price."&nbsp;€", $item->display_name]) !!}
            @endif

        </div>
        <div class="controls">
            <a href="{{ r('cart.removeItem', [$item->id, 'goTo' => $product->getUrl(false, false)]) }}" class="change">
                <svg width="14px" height="14px">
                    <path d="M14.000,8.000 L8.000,8.000 L8.000,14.000 L6.000,14.000 L6.000,8.000 L0.000,8.000 L0.000,6.000 L6.000,6.000 L6.000,-0.000 L8.000,-0.000 L8.000,6.000 L14.000,6.000 L14.000,8.000 Z"></path>
                </svg>
                <span>{!! _t('translations.replace') !!}</span>
            </a>
            <a href="{{ r('cart.removeItem', [$item->id]) }}" class="discard">
                <svg width="12px" height="12px">
                    <path d="M11.657,1.757 L7.414,6.000 L11.657,10.243 L10.243,11.657 L6.000,7.414 L1.757,11.657 L0.343,10.243 L4.586,6.000 L0.343,1.757 L1.757,0.343 L6.000,4.586 L10.243,0.343 L11.657,1.757 Z"></path>
                </svg>
                <span>{!! _t('translations.refuse') !!}</span>
            </a>
        </div>
    </div>
@endif
