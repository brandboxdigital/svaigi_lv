<div class="sv-cart-tabs row">
@include("Deliveries::frontend.deliveries")
    {{--    <div class="tab active">
        <div class="icon"></div>
        <h3>Pirmdiena, 28. augusts</h3>
        <div class="text">
            <p>
                Produktu piegāde pirmdien no 18.00-20.00
            </p>
            <p>
                <a href="#">Margrietas ielā 21, Rīga</a>
            </p>
        </div>
        <div class="price">
            2.99&nbsp;€
        </div>
    </div>
    <div class="tab">
        <div class="icon"></div>
        <h3>Pirmdiena, 28. augusts</h3>
        <div class="text">
            <p>
                Produktu piegāde pirmdien no 18.00-20.00
            </p>
            <p>
                <a href="#">Margrietas ielā 21, Rīga</a>
            </p>
        </div>
        <div class="price">
            2.99&nbsp;€
        </div>
    </div>
    <div class="tab">
        <div class="icon"></div>
        <h3>Pirmdiena, 28. augusts</h3>
        <div class="text">
            <p>
                Produktu piegāde pirmdien no 18.00-20.00
            </p>
            <p>
                <a href="#">Margrietas ielā 21, Rīga</a>
            </p>
        </div>
        <div class="price">
            2.99&nbsp;€
        </div>
    </div>--}}
</div>

@php
    $MethodDataForFrontend = ($cart->delivery)
        ? $cart->delivery->getMethodDataForFrontend($cart) ?? '{}'
        : '{}'
@endphp

<delivery-providers
    :set-delivery-provider-data-route="'{{ r('setDeliveryProviderData') }}'"
    :initial-delivery="{{$cart->delivery ?? '{}'}}"
    :initial-delivery-data-model="{{$MethodDataForFrontend}}"
></delivery-providers>
