@extends('layouts.app')

@section('content')

    @include("Orders::frontend.partials.step")

    @includeIf("Orders::frontend.partials.".($stepInclude??"noInclude"))
    @php
        $cart->isOriginal = true;

    //$showPassword = true;
    //if(Auth::user() && Auth::user()->registered)
        $showPassword = false;
    @endphp
    @include("frontend.elements.profileForm", [
    'action' => r('checkout.post'), 'checkboxText' => _t('translations.orderAsLegalPerson'), 'showComments' => true, 'buttonText' => _t('translations.proceedToPayments'), 'showPasswordField' => true,'showPassword'=>$showPassword])
@endsection

@push('scripts')
    @if(!$edit)
    <script>
        $(document).ready(function () {
            chem();
            if($('#emailaddress').val()!='') {
                sm($('#emailaddress'));
            }
        });

        function chem() {
            $('#emailaddress').unbind().blur(function () {
                sm($(this));
            });
        }
        function profile_reset_values() {
            $('[name=phone]').val("");
            $('[name=name]').val("");
            $('[name=last_name]').val("");
            $('[name=address]').val("");
            $('[name=city]').val("");
            $('[name=postal_code]').val("");
            $('[name=address_comments]').val("");
            $('[name=is_legal]').prop('checked', false);
            $('#legalform').addClass('hidden');
            $('[name=legal_name]').val("");
            $('[name=legal_address]').val("");
            $('[name=legal_reg_nr]').val("");
            $('[name=legal_vat_reg_nr]').val("");
        }

        function sm(el) {
            console.log("SM");

            disableForm();

            $.post('{{route('checkEmail')}}', 'email=' + el.val(), function (response) {
                profile_reset_values();
                var container = $(el).closest("form");
                if (response.status == "2") {
                    $('[name=phone]').val(response.phone);
                    $('[name=name]').val(response.name);
                    $('[name=last_name]').val(response.last_name);
                    $('[name=address]').val(response.address);
                    $('[name=city]').val(response.city);
                    $('[name=postal_code]').val(response.postal_code);
                    $('[name=address_comments]').val(response.address_comments);
                    if (response.is_legal == 1) {
                        $('[name=is_legal]').prop('checked', true);
                        $('#legalform').removeClass('hidden');
                        $('[name=legal_name]').val(response.legal_name);
                        $('[name=legal_address]').val(response.legal_address);
                        $('[name=legal_reg_nr]').val(response.legal_reg_nr);
                        $('[name=legal_vat_reg_nr]').val(response.legal_vat_reg_nr);
                    }
                }
                if (response.status == "1") {

                    $("#password_for_login").val("password");
                    $(".sv-btn").show();
                    $(".form-comments").show();
                    $( "input", container).each(function( index ) {
                        if($(this).attr("name") != "email" && $(this).attr("name")!= "check_password" && $(this).attr("name")!= "_token"){
                            //$(this).parent().hide();
                        }
                    });
                    //$(".sv-line-spacer").hide();
                    $(".form-toogle-fields").hide();
                    $('#checkoutpassword').show();
                    $("#facebook_checkout").hide();

                } else if(response.status == "0") {


                    $( "input", container).each(function( index ) {
                        //$(this).parent().show();

                    });
                    $(".sv-btn").show();
                    $(".form-comments").show();
                    $("#password_for_login").val("");
                    //$(".sv-line-spacer").show();
                    $(".form-toogle-fields").show();
                    $('#checkoutpassword').hide();
                    $("#facebook_checkout").hide();
                } else if(response.status == "fb"){
                    $(".sv-btn").hide();
                    $(".form-comments").hide();
                    $(".form-toogle-fields").hide();
                    $('#checkoutpassword').hide();
                    $("#facebook_checkout").show();
                }

                enableForm()
            });

        }

        function enableForm() {
            console.log('enableForm');
            $('.sv-user-data-forms .loading').removeClass('is-active');
            $('.sv-user-data-forms form input').attr('disabled', false);
        }
        function disableForm() {
            console.log('disableForm');
            $('.sv-user-data-forms .loading').addClass('is-active');
            $('.sv-user-data-forms form input').attr('disabled', 'disabled');
        }
    </script>
    @endif
@endpush
