<table>
    <tr>
        <th colspan="{{ $colcount }}">{{ $marketday }}</th>
    </tr>
    <tr>
        <th>{{  __('translations.supplier') }}</th>
        <th colspan="{{ $colcount-1 }}"> <h1>{{ $farmer }} </h1></th>
    </tr>
    <tr>
        <th>{{  __('translations.email') }}</th>
        <th colspan="{{ $colcount-1 }}">{{ $email }}</th>
    </tr>
    <tr>
        <th>{{  __('translations.comment') }}</th>
        <th colspan="{{ $colcount-1 }}">{{ $comment??"" }}</th>
    </tr>

    <tr>
        @foreach($headers as $headerLine)
            <th>{{$headerLine['name']}}</th>
        @endforeach
    </tr>
    @foreach($lines as $line)
        @include("Orders::pdf.line")
    @endforeach

    @if ($price_no_vat_summ ?? false)
        <tr>
            <td colspan="{{count($headers) - 1}}" align="right"> Total:</td>
            <td > {{ $price_no_vat_summ }}</td>
        </tr>
    @endif
</table>
