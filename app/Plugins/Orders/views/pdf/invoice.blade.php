@php
    /** @var Carbon\Carbon $dt */
    $delivery = $order->delivery;
    $dt = new Carbon\Carbon($order->market_day_date);
    $modifiedMD = $dt->addDays($delivery->deliveryTime??0);
    $mdDate = $modifiedMD->format('j');
    $month = __("translations.".$modifiedMD->format('F'));
    $dayName = __("translations.".$modifiedMD->format('l'));
    $order->isOriginal = true;
    $path = $_SERVER["DOCUMENT_ROOT"];
@endphp

        <!DOCTYPE html>

<html>

<head>
    <title>Svaigi.lv</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <style>
        @font-face {
            font-family: "DINPro";
            src: url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Regular.woff2") format("woff2"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Regular.woff") format("woff"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Regular.ttf") format("truetype");
            font-weight: normal;
        }
        @font-face {
            font-family: "DINPro";
            src: url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Bold.woff2") format("woff2"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Bold.woff") format("woff"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Bold.ttf") format("truetype");
            font-weight: bold;
        }

        table {
            width: 100%;
        }

        .sv-invoice {
            max-width: 1803px;
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
        }

        .title td {
            width: 50%;
            height: 60px;
            padding: 0px 20px;
        }

        .title img {
            height: 30px;
        }

        .header-details {
            background: #f4f4f4;
        }

        .header-details td {
            padding: 0px 20px 15px;
            vertical-align: top;
        }

        .header-details td table {
            width: 100%;
        }

        .header-details td table tr td {
            padding: 0;
            width: 65%;
        }

        .header-details td table tr td:first-of-type {
            width: 35%;

        }

        .header-details td table thead tr td {
            padding: 15px 0px 15px;
        }

        .header-details td table tbody tr td {
            color: #9c9c9c;
        }

        .header-details td table thead tr td:first-of-type {
            font-weight: 600;
            font-size: 16px;
            color: #686868;
        }

        .header-details td table thead tr td:nth-of-type(2) {
            text-align: right;
            color: #9c9c9c;
            font-size: 12px;
        }

        .order-summary td {
            padding: 20px 20px;
        }

        .order-summary td h3 {
            font-size: 12px;
            margin: 0;
            margin-bottom: 10px;
            color: #686868;
        }

        .order-summary td h4 {
            font-size: 12px;
            font-weight: normal;
            margin: 0;
            color: #686868;
            margin-bottom: 20px;
        }

        .order-summary td p {
            color: #9c9c9c;
            margin: 5px 0px;
        }

        .products-list table {
            width: 96%;
            text-align: left !important;
        }

        .products-list table thead tr {
            background: #1f9363;
        }

        .products-list table,
        .products-list thead,
        .products-list tbody,
        .products-list tfoot,
        .products-list tr,
        .products-list td,
        .products-list th {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .products-list table thead tr th {
            color: #fff;
            height: 28px;
            text-align: left;
            padding-left: 20px;
            width: 63%;
        }

        .products-list table thead tr th:nth-child(2),
        .products-list table thead tr th:nth-child(3) {
            width: 20%;
        }

        .products-list table thead tr th:last-child {
            width: 5%;
            padding-right: 20px;
        }

        .products-list table tbody tr:nth-child(even) {
            background: #f4f4f4;
        }

        .products-list table tbody tr td,
        .products-list table tfoot tr td {
            height: 28px;
            padding-left: 20px;
            color: #9c9c9c;
        }

        .products-list table tbody tr td:nth-child(3),
        .products-list table tfoot tr td:nth-child(3) {
            padding-right: 20px;
            padding-left: 28px;
        }

        .products-list table tbody tr td:last-child,
        .products-list table tfoot tr td:last-child {
            text-align: right;
            padding-right: 20px;
        }

        .products-list table tbody tr td table tr td {
            padding-left: 0;
        }

        .products-list table tbody tr td table tr td:last-child {
            text-align: right;
            padding-left: 0;
            padding-right: 20px;
        }

        .order-totals td {
            padding: 20px;
        }

        .order-totals td:last-child {
            background: #f4f4f4;
        }

        .order-totals td table {
            width: 100%;
        }

        .order-totals td table tr td {
            padding: 0;
        }

        .order-totals td table tr td:first-child {
            width: 65%;
        }

        .order-totals td table tr td {
            color: #9c9c9c;
        }

        .order-totals td table tfoot tr td {
            padding-top: 20px;
            font-size: 12px;
            font-weight: bold;
            color: #686868;
        }

        .order-summary p {
            color: #9c9c9c;
        }
    </style>
</head>

<body style="font-family: DINPro, DejaVu Sans, sans-serif;">

<table class="sv-invoice">
    <tr class="title">
        <td>
            {{-- <img src="{{ public_path('/assets/img/logo-svaigilv-invoice.png') }}"/> --}}
            <img src="https://svaigi.lv/assets/img/logo-svaigilv-invoice.png" />
        </td>
        {{-- <td>
            Nr. {{ $order->order_number_string }}
            <span> {{ $order->ordered_at_formatted }}</span>
        </td> --}}
        <td style="text-align: right;">
            <h1 style="margin:0; display: block;">Rēķina Nr. {{ $order->order_number_string }}</h1>
            <div style="margin:0 0 10px;">
                <span style="font-size: 10px;">({{ $order->invoice }})</span>
                <span style="margin:0 0 10px 10px;"> {{ $order->ordered_at_formatted }}</span>
            </div>
        </td>
    </tr>
    <tr class="header-details">
        <td>
            @if($order->buyer->is_legal)
                <table>
                    <thead>
                    <tr>
                        <td>
                            Maksātājs
                        </td>
                        <td>
                            Piezīme: {{ $order->buyer->group->name }}
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            Nosaukums:
                        </td>
                        <td>
                            {{ $order->buyer->legal_name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Reģ.Nr.:
                        </td>
                        <td>
                            {{ $order->buyer->legal_reg_nr }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            PVN reģ. Nr.:
                        </td>
                        <td>
                            {{ $order->buyer->legal_vat_reg_nr }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Juridiskā adrese:
                        </td>
                        <td>
                            {{ $order->buyer->legal_address }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            E-pasts:
                        </td>
                        <td style="overflow:hidden;">
                            {{ $order->buyer->email }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tālrunis
                        </td>
                        <td>
                            {{ $order->buyer->phone }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            @else
                <table style="table-layout: fixed;">
                    <thead>
                    <tr>
                        <td>
                            Maksātājs
                        </td>
                        <td>
                            Piezīme: {{ $order->buyer->group->name }}
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            Nosaukums:
                        </td>
                        <td>
                            {{ $order->buyer->name." ".$order->buyer->last_name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            E-pasts:
                        </td>
                        <td style="overflow:hidden;">
                            {{ $order->buyer->email }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tālrunis
                        </td>
                        <td>
                            {{ $order->buyer->phone }}
                        </td>
                    </tr>
                    </tbody>
                </table>

            @endif
        </td>
        <td>
            <table>
                <thead>
                <tr>
                    <td>
                        Saņēmējs
                    </td>
                    <td>
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        Nosaukums:
                    </td>
                    <td>
                        SIA “Svaigi”
                    </td>
                </tr>
                <tr>
                    <td>
                        Reģ. Nr.:
                    </td>
                    <td>
                        40103915568
                    </td>
                </tr>
                <tr>
                    <td>
                        PVN reģ. Nr.:
                    </td>
                    <td>
                        LV40103915568
                    </td>
                </tr>
                <tr>
                    <td>
                        Juridiskā adrese:
                    </td>
                    <td>
                        Margrietas iela 7, Rīga, LV-1046
                    </td>
                </tr>
                <tr>
                    <td>
                        Banka:
                    </td>
                    <td>
                        Swedbank, AS
                    </td>
                </tr>
                <tr>
                    <td>
                        Konta Nr.:
                    </td>
                    <td>
                        LV96HABA0551040281516
                    </td>
                </tr>
                <tr>
                    <td>
                        Tālrunis:
                    </td>
                    <td>
                        24335225
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    @php

            @endphp

    <tr class="order-summary">
        <td colspan="2">
            <h3>Produktu saņemšanas datums: {{ $modifiedMD->format('d.m.Y') }}</h3>
            <h4>
                @if(isset($delivery->id))
                    {!! __("deliveries.description.{$delivery->id}", ['freeAbove' => (isset($delivery->freeAbove))?number_format($delivery->freeAbove,2):"No", 'dayName' => substr($dayName,0, -1)]) !!}
                    -
                @endif
                {{ $order->address." ".$order->city." ".$order->postcode }}</h4>
            {{ $order->user_comment }}
        </td>
    </tr>
    <tr class="products-list">
        <td colspan="2">
            @php
                $totalPrices = getCartTotals($order, [], $original??false);
                $total = 0;
                $totalVat = $totalWoVat = $vatItems = [];
                $discountPriceNotCoupon = 0;
            @endphp
            <table class="prl">
                <thead>
                <tr>
                    <th>Produkts</th>
                    <th style="width:20%;text-align: center;">Ražotājs</th>
                    <th style="width:20%;text-align: center;">Cena</th>
                    <th style="width:10%;">Skaits</th>
                    <th style="width:10%">Svars</th>
                    <th style="width:10%">Summa</th>
                </tr>
                </thead>
                <tbody>

                @php
                    $counter = 0;
                    $linesInPage = 9;
                @endphp
                @foreach($order->items()->orderBy('product_name', 'asc')->get() as $item)
                    @php

                        if($item->total_amount == "0")
                           $item->total_amount = 1;


                        $product = $item->product;
                        //$sum = round((($item->price*$item->amount)/$item->total_amount)*$item->real_amount ,2);
                        $itemPriceAndDiscount = getDiscountAndPrice($order, $item, getDiscountItemsId($order));
                        $sum = round((($itemPriceAndDiscount['itemResultPrice'] * $item->amount)) , 2);
                        $total+=$sum;
                        //$vat = round($sum*($item->vat_amount/100),2);
                        $vat = $item->vat * $item->real_amount;
                        if(!($totalVat[$item->vat_id]??false)) { $totalVat[$item->vat_id] = 0; }
                        if(!($totalWoVat[$item->vat_id]??false)) { $totalWoVat[$item->vat_id] = 0; }
                        $totalVat[$item->vat_id]+=$vat;
                        $totalWoVat[$item->vat_id]+=$sum-$vat;
                        $vatItems[$item->vat_id][] = $item->product_id;
                        if($item->price < $item->price_raw)
                            $discountPriceNotCoupon += ($item->price_raw - $item->price)* $item->real_amount;

                        if ($loop->index == 10) {
                            $counter = 0;
                            $linesInPage = 16;
                        }

                        $breakStyle = (($counter)%($linesInPage+1) == $linesInPage)
                                ? 'page-break-after: always; display:block; width:100%;height:1px;'
                                : '';
                    @endphp


                    <tr>
                        <td>{{ $item->product_name }}</td>
                        <td>{{ $item->supplier_name }}</td>
                        <td>{{ $item->price }}&euro; / {{ $item->display_name }}</td>
                        <td>{{ $item->amount }}</td>
                        <td>{{ $item->real_amount }}{{$item->amount_unit}}</td>
                        <td>
                            {{ $sum }} &euro;
                            <div style="{{$breakStyle}}"></div>
                        </td>
                    </tr>
                @endforeach

                <tr>
                    <td>
                        Produktu piegāde
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        {{ $totalPrices->delivery + $totalPrices->delivery_vat }} &euro;
                    </td>
                </tr>

                </tbody>
                <tfoot>
                <tr>
                    <td>
                    </td>

                </tr>
                </tfoot>
            </table>
        </td>
    </tr>
    <tr class="order-totals">
        <td>
        </td>
        <td>
            <table>
                @foreach($totalVat as $vat_id => $vat_amount)
                    @php
                        $vatAmount = \App\Plugins\Vat\Model\Vat::find($vat_id)->amount;
                        $totals = getCartTotals($order, $vatItems[$vat_id], $original??false);

                        $totalWoVat[$vat_id] = $twv = ($totals->productSum)-$totals->vatSum;
                        $totalVat[$vat_id] = $tv = $totals->vatSum;
                    @endphp
                    <tr>
                        <td>Kopā bez PVN ({{ $vatAmount }}%):</td>
                        <td colspan="2" style="text-align: right;">{{ number_format($twv,2, ".","") }} &euro;
                        </td>
                    </tr>
                    <tr>
                        <td>PVN ({{ $vatAmount }}%):</td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($tv,2, ".","") }} &euro;
                        </td>
                    </tr>
                    <tr>
                        <td>Kopā ar PVN ({{ $vatAmount }}%):</td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($tv+$twv,2, ".","") }} &euro;
                        </td>
                    </tr>

                @endforeach

                    <tr>
                        <td>Piegade bez VAT: </td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($totals->delivery, 2) }} &euro;
                        </td>
                    </tr>
                    <tr>
                        <td>Piegades VAT ({{ $totals->delievery_vat_amount }}%):</td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($totals->delivery_vat, 2) }} &euro;
                        </td>
                    </tr>

                    @php
                        $sum = array_sum($totalWoVat)+array_sum($totalVat)+$totalPrices->delivery+$totalPrices->delivery_vat-$totalPrices->discount;
                    @endphp

                @if($order->discount_target)
                    <tr>
                        <td>Atlaide ({{$order->discount_code}})</td>
                        <td colspan="2" style="text-align: right;">
                           - {{ number_format($totalPrices->discount,2) }} &euro;
                        </td>
                    </tr>
                @endif

                <tr>
                    <td style="font-size: 12px;">Kopā apmaksai</td>
                    <td colspan="2" style="text-align: right;font-size: 12px;">{{ number_format($sum,2) }} &euro;</td>
                </tr>
                    @if($order->paidAmountText > 0)
                    <tr>
                        <td style="font-size: 12px;">{!! _t('translations.total_paid')!!}:</td>
                        <td colspan="2" style="text-align: right;font-size: 12px;">{{ number_format($order->paidAmountText,2, ".","") }} &euro;</td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px;">{!! _t('translations.to_pay_or_return')!!}:</td>
                        <td colspan="2" style="text-align: right;font-size: 12px;">{{ number_format($sum-$order->paidAmountText,2) }} &euro;</td>
                    </tr>
                    @endif
            </table>
        </td>
    </tr>
    <tr class="order-summary">
        <td colspan="2">
            Rēķins ir sagatavots elektroniski un derīgs bez paraksta.<br/>
            Ja apmaksu jau veici un par pasūījumu ir izveidojusies pārmaksa, mēs pārmaksāto naudas summu atmaksāsim uz
            Tavu bankas kontu 7 dienu laikā no pasūtījuma saņemšanas dienas.
        </td>
    </tr>
</table>
</body>

</html>
