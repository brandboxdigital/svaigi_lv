

        <!DOCTYPE html>

<html>

<head>
    <title>Svaigi.lv</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}">
<style>

    @page {
        margin: 20px 30px 0;
    }

    @font-face {
        font-family: "DINPro";
        src: url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Regular.woff2") format("woff2"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Regular.woff") format("woff"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Regular.ttf") format("truetype");
        font-weight: normal;
    }
    @font-face {
        font-family: "DINPro";
        src: url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Bold.woff2") format("woff2"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Bold.woff") format("woff"), url("https://svaigi.brandbox.digital/assets/fonts/DINPro-Bold.ttf") format("truetype");
        font-weight: bold;
    }

    body {
        font-size:14px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    table td {
        width: 100%;
        border-collapse: collapse;
    }

    /* table tr {
        width: 100%;
    } */

    /* table td {
        border: 1px solid red;
    } */

	.sv-invoice {
		max-width: 1803px;
		width: 100%;
		margin: 0 auto;
		font-size: 10px;
	}

	.title h1 {
        font-size: 14px;
    }

	.title td {
		width: 50%;
		padding: 0px 10px 0px;
	}

    .title td.logo {
        padding-left: 10px;
    }

	.title img {
		height : 20px;
        margin-top: -10px;
	}

	.header-details {
		background: #f4f4f4;
	}

    .header-details .spacer{
		background: #ffffff;
	}
    .header-details .qrcode{
		background: #ebf4ef;
	}

	.header-details * {
		line-height: 1;
	}

	.header-details > td {
		padding: 10px 10px;
		vertical-align: top;
        border: 2px solid #fff;
	}

	/* .header-details td.left {
		padding-right: 10px;
	}
	.header-details td.right {
		padding-left: 10px;
	} */

	.header-details td table {
		width: 100%;
	}

	.header-details td table tr td {
		padding: 0;
		width: 65%;
	}

	.header-details td table tr td:first-of-type {
		width: 35%;

	}

	.header-details td table thead tr td {
		padding: 0px 0px 5px;
	}

	.header-details td table tbody tr td {
		color: #9c9c9c;
	}

	.header-details td table thead tr td:first-of-type {
		font-weight: bold;
		font-size:10px;
		color:#686868;
	}

	.header-details td table thead tr td:nth-of-type(2) {
		text-align: right;
		color: #9c9c9c;
		font-size:10px;
	}

	.order-summary td {
		padding: 10px 10px;
	}

	.order-summary td h3 {
		font-size:10px;
		margin: 0 0 5px;
		color:#686868;
	}

	.order-summary td h4 {
		font-size:10px;
		font-weight: normal;
		margin: 0;
		color:#686868;
	}

	.order-summary td p {
		color: #9c9c9c;
		margin: 0px 0px;
	}

	.products-list {
        border-bottom: 10px solid #fff;
    }
	.products-list table {
		width: 100%;
        padding-right: 0;
	}

	.products-list table thead tr {
		background: #1f9363;
	}

	.products-list table,
	.products-list thead,
	.products-list tbody,
	.products-list tfoot,
	.products-list tr,
	.products-list td,
	.products-list th {
		border-collapse: collapse;
		border-spacing: 0;
        line-height: 1;
	}

	.products-list table thead tr th {
		color: #fff;
		height: 28px;
		text-align: left;
		/* padding-left: 20px;
		padding-right: 20px; */
	}

	.products-list table tbody tr {
        border-bottom: 1px solid #f4f4f4;
    }

	.products-list table tbody tr:nth-child(even) {
		background: #f4f4f4;
	}

	.products-list table tbody tr td,
	.products-list table tfoot tr td {
		/* padding: 4px 20px 7px; */
		color: #9c9c9c;
	}

	.products-list table thead tr th:first-child {
        padding-left: 10px;
		padding-right: 0px;
    }

    .products-list table thead tr th:last-child,
	.products-list table tbody tr td:last-child,
	.products-list table tfoot tr td:last-child {
		text-align: right;
        padding-left: 0;
        padding-right: 10px;
	}

	.products-list table tbody tr td table tr td {
		padding-left: 0;
	}

	.products-list table tbody tr td table tr td:first-child {
        text-align: left;
		padding-left: 10px;
		padding-right: 0px;
	}

	.products-list table tbody tr td table tr td:last-child {
        text-align: right;
		padding-left: 0;
		padding-right: 10px;
	}

    .products-list .product-name {
        padding: 5px 10px;
    }
    .products-list .price {
        white-space: nowrap;
    }

    .products-list table tfoot {
		border-top: 1px solid #f4f4f4;
	}


	.order-totals {
        border-top: 10px solid #fff;
    }

    .order-totals .add-some-border {
        border: 2px solid #fff;
    }

	.order-totals td {
		padding: 5px 10px;
	}

    .vat-order-details,
    .vat-order-details > td,
    .vat-order-details td table {
        padding: 0;
        background: #fff;
    }

    .order-totals.vat-order-details table table td {
        padding: 0px 10px;
        background-color: #f4f4f4;
    }

	.order-totals td:last-child {
		background: #f4f4f4;
	}

	.order-totals td table {
		width: 100%;
	}
	.order-totals td table tr {
        vertical-align: top;
    }

	.order-totals td table tr td {
		padding: 0;
	}

	.order-totals td table tr td{
		color: #9c9c9c;
	}

	.order-totals td table tfoot tr td{
		padding-top: 10px;
		font-size:10px;
		font-weight: bold;
		color: #686868;
	}

	.order-totals .total-totals{
        font-weight: bold;
        /* vertical-align: bottom; */
	}
    .order-totals .total-totals td {
        font-size: 11px;
        line-height: 1;
        color: #686868;
        font-weight: bold;
        vertical-align: bottom;
    }

    .order-totals .total-totals .smalltext {
        padding-bottom: 0.1rem;
    }

    .order-totals .total-totals .bigtext{
        padding-bottom: 0;
        font-size: 13px;
        line-height: 1;
        text-align: right;
    }

	.order-summary p{
		color: #9c9c9c;
	}

	.page_break { page-break-before: always !important; }
	.page_break_top { page-break-before: always !important; }

    .page_break,
    .page_break_top {
        background-color: red;
    }

	@media print {
        .page_break { page-break-before: always !important; }
		.page_break_top { page-break-before: always !important; }
	}

}
</style>

</head>

<body style="font-family: DINPro, DejaVu Sans, sans-serif;">
@foreach($orders as $key=>$order)

@php
    /** @var Carbon\Carbon $dt */
    $delivery = $order->delivery;
    $dt = new Carbon\Carbon($order->market_day_date);
    $modifiedMD = $dt->addDays($delivery->deliveryTime??0);
    $mdDate = $modifiedMD->format('j');
    $month = __("translations.".$modifiedMD->format('F'));
    $dayName = __("translations.".$modifiedMD->format('l'));
    $order->isOriginal = true;
    $path = $_SERVER["DOCUMENT_ROOT"];

    $feedback_qrcode_svg = \App\Plugins\Feedback\Model\Feedback::getQrCodeLink($order);
@endphp
@if($key !== 0)
<div class="page_break_top"></div>
@endif

<table class="sv-invoice">
    <tr >
        <td colspan="2">
        <table>
            <tr class="title">
                <td class="logo">
                    {{-- <img src="./assets/img/logo-svaigilv-invoice.png" alt=""> --}}
                    {{-- {{public_path('/assets/img/logo-svaigilv-invoice.png')}} --}}
                    {{-- <img src="{{ public_path('/assets/img/logo-svaigilv-invoice.jpeg') }}" /> --}}
                    <img src="https://svaigi.lv/assets/img/logo-svaigilv-invoice.png" />
                </td>
                <td style="text-align: right;">
                    <h1 style="margin:0; display: block;">Rēķina Nr. {{ $order->order_number_string }}</h1>
                    <div style="margin:0 0 10px;">
                        <span style="font-size: 10px;">({{ $order->invoice }})</span>
                        <span style="margin:0 0 10px 10px;"> {{ $order->ordered_at_formatted }}</span>
                    </div>
                </td>
            </tr>
        </table>
        </td>
    </tr>

    <tr >
        <td colspan="2">
            <table>
                <tr class="header-details">
                    <td class="left" style="width:37%;">
                        @if($order->user_is_legal)
                            <table>
                                <thead>
                                <tr>
                                    <td>
                                        Maksātājs
                                    </td>
                                    {{-- <td>
                                        Piezīme: {{ $order->buyer->group->name }}
                                    </td> --}}
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Nosaukums:
                                    </td>
                                    <td>
                                        {{ $order->user_legal_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Reģ.Nr.:
                                    </td>
                                    <td>
                                        {{ $order->user_legal_reg_nr }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        PVN reģ. Nr.:
                                    </td>
                                    <td>
                                        {{ $order->user_legal_vat_reg_nr }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Juridiskā adrese:
                                    </td>
                                    <td>
                                        {{ $order->user_legal_address }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        E-pasts:
                                    </td>
                                    <td style="overflow:hidden;">
                                        {{ $order->user_email }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tālrunis
                                    </td>
                                    <td>
                                        {{ $order->user_phone }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @else
                            <table style="table-layout: fixed;">
                                <thead>
                                <tr>
                                    <td>
                                        Maksātājs
                                    </td>
                                    <td>
                                        {{-- Piezīme: {{ $order->buyer->group->name }} --}}
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Nosaukums:
                                    </td>
                                    <td>
                                        {{ $order->user_full_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        E-pasts:
                                    </td>
                                    <td style="overflow:hidden;">
                                        {{ $order->user_email }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tālrunis
                                    </td>
                                    <td>
                                        {{ $order->user_phone }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        @endif
                    </td>
                    <td class="right" style="width:37%;">
                        <table>
                            <thead>
                            <tr>
                                <td>
                                    Saņēmējs
                                </td>
                                <td>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    Nosaukums:
                                </td>
                                <td>
                                    SIA “Svaigi”
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Reģ. Nr.:
                                </td>
                                <td>
                                    40103915568
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    PVN reģ. Nr.:
                                </td>
                                <td>
                                    LV40103915568
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Juridiskā adrese:
                                </td>
                                <td>
                                    Margrietas iela 7, Rīga, LV-1046
                                </td>
                            </tr>
                            {{-- <tr>
                                <td>
                                    Banka:
                                </td>
                                <td>
                                    Swedbank, AS
                                </td>
                            </tr> --}}
                            <tr>
                                <td>
                                    Konta Nr.:
                                </td>
                                <td>
                                    LV96HABA0551040281516
                                </td>
                            </tr>
                            {{-- <tr>
                                <td>
                                    Tālrunis:
                                </td>
                                <td>
                                    24335225
                                </td>
                            </tr> --}}
                            </tbody>
                        </table>
                    </td>

                    @if ($feedback_qrcode_svg ?? null)
                    <td class="spacer" style="width:0%; padding:0;"></td>
                    <td class="qrcode" style="width:25%;">
                        <table >
                            <thead>
                            <tr>
                                <td colspan="2">
                                    Mums ir svarīgi zināt!
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="width:55%;">
                                    Noskenē kodu un sniedz atsauksmi, ieteikumu vai sūdzību.
                                </td>
                                <td style="width:45%; text-align:right;">
                                    <img src="data:image/svg+xml;base64,{!! $feedback_qrcode_svg !!}" width="60px" />
                                </td>
                            </tr>
                            </tbody>
                        </table>


                    </td>
                    @endif
                </tr>
            </table>
        </td>
    </tr>
    @php

    @endphp

    <tr class="order-summary">
        <td colspan="2">
            <h3>Produktu saņemšanas datums: {{ $modifiedMD->format('d.m.Y') }}</h3>
            <h4>
                @if(isset($delivery->id))
                    {!! __("deliveries.description.{$delivery->id}", ['freeAbove' => (isset($delivery->freeAbove))?number_format($delivery->freeAbove,2):"No", 'dayName' => substr($dayName,0, -1)]) !!}
                    -
                @endif

                @if ($delivery->getDeliveryProviderClassAttribute())
                    {{ $delivery->getMethodDataFromOrder($order) }}
                @else
                    {{ $order->address." ".$order->city." ".$order->postcode }}
                @endif
            </h4>

            {{ $order->user_comment }}
        </td>
    </tr>
    <tr class="products-list" >
        <td colspan="2" >
            <table >
                <thead >
                <tr>
                    <th style="wwidth:10px">Produkts</th>
                    {{-- <th style="wwidth:10px">Skaits</th> --}}
                    <th style="wwidth:10px">Cena ar PVN</th>
                    <th style="wwidth:10px">Cena bez PVN</th>
                    <th style="wwidth:10px">PVN %</th>
                    <th style="wwidth:10px">PVN €</th>
                    <th style="wwidth:10px">Daudzums</th>
                    <th style="wwidth:10px">Kopā</th>
                </tr>

                </thead>
                <tbody>
                    @php
                        $counter = 0; $linesInPage = 18;  $breakAfterDelivery = false;
                    @endphp
                    @foreach($arrDisplayValues[$key]['items'] as $item)
                        @php
                            $counter++;

                            $displayValues = $arrDisplayValues[$key]['displayValues'];
                            $totalPrices = $arrDisplayValues[$key]['totalPrices'];

                            $price       = $item->price < $item->price_raw ? $item->price : $item->price_raw;
                            $vat         = $item->vat;
                            $pricenovat  = $price - $item->vat;
                            $vat_percent = number_format($item->vat_amount, 0);

                            $depositSum  = ($item->deposit_amount)
                                ? number_format($item->deposit_amount * $item->amount, 2, ".", "")
                                : 0;

                            if ($counter >= $linesInPage) {
                                $counter     = 0;
                                $linesInPage = 25;
                            }

                            $break = $counter % $linesInPage == 0;

                            if ($loop->last && $counter > 22) {
                                $breakAfterDelivery = true;
                            }

                        @endphp

                        <tr >
                            <td style="width:260px" class="product-name">{{ $item->product_name }} <small style="display:block">{{ $item->supplier_name }}</small> </td>
                            {{-- <td style="width:50px" class="total-amount">{{ $item->total_amount }}{{$item->amount_unit}}</td> --}}
                            <td style="width:50px" class="price">
                                {{ $price }}&nbsp;&euro; / {{ $item->display_name }}

                                @if ($depositSum)
                                    <br /> <small>+&nbsp;{{ $depositSum }}&nbsp;&euro; </small>
                                @endif
                            </td>
                            <td style="width:50px" class="pricenovat">{{ $pricenovat }}&nbsp;&euro;</td>
                            <td style="width:50px" class="vat">{{ $vat_percent }}%</td>
                            <td style="width:50px" class="vat">{{ $vat }}&nbsp;&euro;</td>
                            <td style="width:50px" class="total-amount">{{ $item->amount }}</td>
                            <td style="width:50px">
                                {{ number_format($price * $item->amount,2, ".","") }}&nbsp;&euro;

                                @if ($break)
                                    <div style="page-break-after: always; display:block; width:100%;height:1px;"></div>
                                @endif
                            </td>
                        </tr>

                    @endforeach

                    <tr>
                        @php
                            $deliveryTotals = [
                                "deliveryWithVat" => number_format($totalPrices->delivery + $totalPrices->delivery_vat, 2),
                                "deliveryNoVat"   => number_format($totalPrices->delivery - $totalPrices->delivery_vat, 2),
                                "justVat"         => number_format($totalPrices->delivery_vat, 2),
                                "justVatPercent"  => number_format($totalPrices->delievery_vat_amount, 0),
                                "total"           => number_format($totalPrices->delivery + $totalPrices->delivery_vat, 2),
                            ];
                        @endphp
                        <td class="product-name">Produktu piegāde</td>
                        <td>{{ $deliveryTotals['deliveryWithVat'] }}&nbsp;&euro;</td>
                        <td>{{ $deliveryTotals['deliveryNoVat'] }}&nbsp;&euro;</td>
                        <td>{{ $deliveryTotals['justVatPercent'] }}%</td>
                        <td>{{ $deliveryTotals['justVat'] }}&nbsp;&euro;</td>
                        <td></td>
                        <td>
                            {{ $deliveryTotals['total'] }}&nbsp;&euro;
                        </td>
                    </tr>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7"></td>
                    </tr>
                </tfoot>
            </table>

            @if ($breakAfterDelivery)
                <div style="page-break-after: always; display:block; width:100%;height:1px;"></div>
            @endif
        </td>
    </tr>

    <tr class="order-totals vat-order-details">
        <td colspan="2">

            <table> <tr>

            @foreach($displayValues["vatTotals"] as $vat_id => $vatTotals)
                <td width="200" class="add-some-border">
                    <table>

                            <tr>
                                <td>Kopā bez PVN ({{ $vatTotals['amount'] }}%):</td>
                                <td colspan="2" style="text-align: right;">{{ number_format($vatTotals['summWoVat'],2, ".","") }}&nbsp;&euro;
                                </td>
                            </tr>
                            <tr>
                                <td>PVN ({{ $vatTotals['amount'] }}%):</td>
                                <td colspan="2" style="text-align: right;">
                                    {{ number_format($vatTotals['summVat'],2, ".","") }}&nbsp;&euro;
                                </td>
                            </tr>
                            <tr>
                                <td>Kopā ar PVN ({{ $vatTotals['amount'] }}%):</td>
                                <td colspan="2" style="text-align: right;">
                                    {{ number_format( ($vatTotals['summVat'] + $vatTotals['summWoVat']) ,2, ".","") }}&nbsp;&euro;
                                </td>
                            </tr>

                    </table>
                </td>
            @endforeach

            </tr> </table>

        </td>
    </tr>

    <tr class="order-totals">
        <td class="add-some-border"></td>
        <td class="add-some-border">
            <table>
                @if($order->discount_target)
                    <tr>
                        <td>Atlaide ({{$order->discount_code}})</td>
                        <td colspan="2" style="text-align: right;">
                        - {{ number_format($totalPrices->discount,2) }}&nbsp;&euro;
                        </td>
                    </tr>
                @endif

                {{-- @if($displayValues["discountPriceNotCoupon"] > 0)
                    <tr>
                        <td>Promotions</td>
                        <td colspan="2" style="text-align: right;">
                            - {{ number_format($displayValues["discountPriceNotCoupon"],2, ".","") }}&nbsp;&euro;
                        </td>
                    </tr>
                @endif --}}

                <tr>
                    <td style="">{!! _t('translations.cartDeposit')!!}:</td>
                    <td colspan="2" style="text-align: right;">{{ number_format($totalPrices->depositSum,2, ".","") }}&nbsp;&euro;</td>
                </tr>

                @if($order->paidAmountText > 0)
                    <tr >
                        <td style="">Kopējā summa</td>
                        <td colspan="2" style="text-align: right;">{{ number_format($totalPrices->toPay,2) }}&nbsp;&euro;</td>
                    </tr>
                    <tr>
                        <td style="">{!! _t('translations.total_paid')!!}:</td>
                        <td colspan="2" style="text-align: right;">{{ number_format($order->paid,2, ".","") }}&nbsp;&euro;</td>
                    </tr>

                    {{-- <tr>
                        <td style="font-size: 0.8rem;">{!! _t('translations.to_pay_or_return')!!}:</td>
                        <td colspan="2" style="text-align: right;font-size: 0.8rem;"></td>
                    </tr> --}}
                    <tr class="total-totals">
                        <td >
                            <div class="smalltext">Kopā apmaksai</div></td>
                        <td colspan="2">
                            <div class="bigtext">{{ number_format($totalPrices->toPay - $order->paid,2) }}&nbsp;&euro;</div>
                        </td>
                    </tr>
                @else
                    <tr class="total-totals">
                        <td >
                            <div class="smalltext">Kopā apmaksai</div>
                        </td>
                        <td colspan="2">
                            <div class="bigtext">{{ number_format($totalPrices->toPay,2) }}&nbsp;&euro;</div>
                        </td>
                    </tr>
                @endif

            </table>
        </td>
    </tr>

    <tr class="order-summary">
        <td colspan="2">
            {{-- Rēķins ir sagatavots elektroniski un derīgs bez paraksta.<br /> --}}
            {{-- Ja apmaksu jau veici un par pasūījumu ir izveidojusies pārmaksa, mēs pārmaksāto naudas summu atmaksāsim uz Tavu bankas kontu 7 dienu laikā no pasūtījuma saņemšanas dienas. --}}
            Ja apmaksu veici un par pasūtijumu ir izveidojusies pārmaksa, pārmaksāto summu atmaksāsim uz bankas kontu 7 darba dienu laikā no pasūtījuma saņemšanas dienas.
        </td>
    </tr>
</table>


@php
    //$isLast = (isset($order[$key+1]))?false:true; // !$isLast
@endphp
@if(isset($order[$key+1]))
<div class="page_break"></div>
@endif
@endforeach

</body>

</html>
