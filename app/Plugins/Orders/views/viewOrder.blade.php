@extends('layouts.admin')

@section('content')
    {{--<form action="{{ route($currentRoute.".store",request()->route()->parameters) }}" method="post">--}}
        {{ @csrf_field() }}
        <div class="tab-vertical">
            <div class="row">
                <div class="col-md-2">
                    <ul class="nav nav-tabs nav-pills" id="productTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="general-tab" data-toggle="tab"
                               href="#general" role="tab" aria-controls="general"
                               aria-selected="true">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="orderContent-tab" data-toggle="tab"
                               href="#orderContent" role="tab" aria-controls="orderContent"
                               aria-selected="false">Products</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="tab-content" id="productTabContent">
                        <!-- general tabl -->
                        <div class="tab-pane fade show active" id="general" role="tabpanel"
                             aria-labelledby="general-tab">
                            <div class="row viewRow">
                                <div class="col-md-10"></div>
                                <div class="col-md-2"><a href="{{ route('orders.view', $order->isOriginal?[$order->id]:[$order->id,"original"]) }}" class="btn btn-info btn-mini">{{ $order->isOriginal?"Show Current":"Show Original" }}</a></div>
                            </div>

                            @if($order->user_is_legal)
                                <div class="row viewRow">
                                    <div class="col-md-3"><h5 style="font-family: firefly, DejaVu Sans, sans-serif;color:#71748d;">Juridiskās personas nosaukums</h5></div>
                                    <div class="col-md-9 vertical-content">{{$order->user_legal_name}}</div>
                                </div>
                                <div class="row viewRow">
                                    <div class="col-md-3"><h5 style="font-family: firefly, DejaVu Sans, sans-serif;color:#71748d;">Juridiskā adrese</h5></div>
                                    <div class="col-md-9 vertical-content"> {{$order->user_legal_address}}</div>
                                </div>
                                <div class="row viewRow">
                                    <div class="col-md-3"><h5 style="font-family: firefly, DejaVu Sans, sans-serif;color:#71748d;">Reģistrācijas Nr.</h5></div>
                                    <div class="col-md-9 vertical-content">{{$order->user_legal_reg_nr}}</div>
                                </div>
                                <div class="row viewRow">
                                    <div class="col-md-3"><h5 style="font-family: firefly, DejaVu Sans, sans-serif;color:#71748d;">PVN reģistrācijas Nr.</h5></div>
                                    <div class="col-md-9 vertical-content">{{$order->user_legal_vat_reg_nr}}</div>
                                </div>



                                <div class="row viewRow">
                                    <div class="col-md-12">
                                        <hr style="border:1px solid black;"/>
                                    </div>
                                </div>
                            @endif
                            @php

                            //print_r($order);
                                //print_r($fields)
                            @endphp
                            @foreach($fields as $fieldName => $label)

                                @php
                                    // dd($label, $fields);

                                //if($fieldName == "user_comment")
                                    //$fieldName = "comments";
                               // elseif($fieldName == "comments")
                                    //$fieldName = "user_comment";
                                    //print_r($fields)

                                //echo $fieldName;

                                @endphp



                                @if($fieldName=='delivery_provider_label_download')

                                    @if ($order->delivery->delivery_provider_class && $order->delivery->delivery_provider_class->hasCreateShipment())
                                        <div class="row viewRow">
                                            <div class="col-md-3">
                                                <h5 style="font-family: firefly, DejaVu Sans, sans-serif;color:#71748d;">{{ $label }}</h5>
                                            </div>

                                            <div class="col-md-9 vertical-content">
                                                <a
                                                    href="{{ route('orders.createAndDownloadDeliveryLabel', $order->id) }}"
                                                    class="btn btn-info btn-xs"
                                                    {{-- class="btn btn-info btn-xs isAjax" --}}
                                                    target="_blank"
                                                >
                                                    Open Label
                                                </a>
                                            </div>
                                        </div>
                                    @endif

                                    @continue
                                @endif

                                @if($label=='hr')
                                    <div class="row viewRow">
                                        <div class="col-md-12">
                                            <hr style="border:1px solid black;"/>
                                        </div>
                                    </div>
                                    @continue
                                @endif

                                <div class="row viewRow">
                                    <div class="col-md-3"><h5 style="font-family: firefly, DejaVu Sans, sans-serif;color:#71748d;">{{ $label }}</h5></div>
                                    <div class="col-md-9 vertical-content {{ $canEdit??"" }}">{!! $order->$fieldName !!}</div>
                                </div>
                            @endforeach
                        </div>

                        <!-- product tab -->
                        <div class="tab-pane fade" id="orderContent" role="tabpanel"
                             aria-labelledby="orderContent-tab">
                            @include("Orders::products")
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--@include('admin.partials.formFooter')--}}
    {{--</form>--}}
@endsection

@push('scripts')
    <script src="{{ asset('js/orderlist.js') }}"></script>
    <script>
        var orderUpdateUrl = "{{ route('orders.updateOrderField', [$order->id, 'ori']) }}";
    </script>

    <style>
        .bootstrap-select {
            margin-top: -10px !important;
            width: 100% !important;
        }
    </style>
@endpush
