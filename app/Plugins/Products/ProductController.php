<?php

namespace App\Plugins\Products;

use Psy\Util\Str;
use App\Schedules;
use App\Functions\General;
use Illuminate\Http\Request;
use App\Plugins\Vat\Model\Vat;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Plugins\Admin\AdminController;
use App\Plugins\Products\Model\Product;
use Illuminate\Support\Facades\Storage;
use App\Plugins\Orders\Model\OrderLines;
use App\Http\Controllers\CacheController;
use Illuminate\Database\Eloquent\Builder;
use App\Plugins\Products\Model\ProductRating;
use App\Plugins\Products\Model\ProductVariation;
use App\Plugins\Products\Functions\ProductImport;
use Illuminate\Support\Facades\Cache;

class ProductController extends AdminController
{
    use Functions\Products;
    use General;

    public function index($search = false)
    {
        $cr = explode(".", Route::currentRouteName());

        if (!$search && ($cr[1] ?? false) == 'search') {
            return redirect()->route($cr[0] . ".list");
        }

        return view('admin.elements.table',
            [
                'tableHeaders' => $this->getList(),
                'header'       => 'Products',
                'list'         => $this->getProducts(),
                'idField'      => 'name',
                'destroyName'  => 'Product',
                'operations'   => view("Products::partials.extraButtons")->render(),
                'logButton'    => view('admin.partials.log', ['logTypes' => 'productImport,productExport,productImageImport'])->render(),
                'js'           => [
                    'js/productIO.js',
                ],
            ]);
    }

    public function getProducts()
    {

        /** @var Product $products */
        $products = Product::withTrashed();

        // Sort by Name
        $products = $products->select('products.*')->addSelect(DB::raw('product_metas.meta_value as metaName'))->join('product_metas', function ($join) {
            $join->on('products.id', 'product_metas.owner_id')
                ->where('meta_name', 'name');
        })->addSelect(DB::raw('supplier_metas.meta_value as supplierName'))->join('supplier_metas', function ($query) {
            $query->on('products.supplier_id', 'supplier_metas.owner_id')
                ->where('supplier_metas.meta_name', 'name');
        })->orderBy('metaName', 'asc');


        if ($search = request()->route('search')) {
            $products = $products
                ->whereHas('metaData', function (Builder $q) use ($search) {
                    $q->whereIn('meta_name', ['name', 'slug'])->where('meta_value', 'like', "%$search%")
                    ->orWhereIn('supplier_metas.meta_name', ['name', 'slug'])->where('meta_value', 'like', "%$search%")
                    ;
                })
                ->orWhere('sku', $search)
                ;
        }

        return $products->with('metaData')->paginate(40);
    }

    public function add()
    {
        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => new Product(), 'modalId' => ["attributes" => str_random(10), "variations" => str_random(10)]]);
    }

    public function store(Request $request, $id = false)
    {
        if ($id) {
            $unique = ",$id";
        }

        $val = [
            // 'main_category' => 'required',
            'sku'           => 'required|unique:products,sku' . ($unique ?? ""),
            'supplier_id'   => 'required',
            'mark_up'       => 'required',
            'cost'          => 'required',
            'vat_id'        => 'required',
            'unit_id'       => 'required',
        ];

        $msg = [
            'main_category.required' => 'Main Category can not be empty',
            'sku.required'           => 'Product Code can not be empty',
            'sku.unique'             => 'Product with this Product Code already exists',
            'supplier_id.required'   => 'Supplier can not be empty',
            'cost.required'          => 'Product Cost can not be empty',
            'vat_id.required'        => 'Vat can not be empty',
            'unit_id.required'       => 'Measurement Unit can not be empty',
            'mark_up.required'       => 'Mark-up can not be empty',
        ];

        foreach (languages() as $lang) {
            $val["name.{$lang->code}"] = "required";
            $msg["name.{$lang->code}.required"] = "Category Name in {$lang->name} Should Be Filled";
        }


        $request->validate($val, $msg);


        // dd($request->input(), $this->switch(request('has_deposit')));


        $metas = [
            'name',
            'slug',
            'description',
            'ingredients',
            'expire_date',
            'google_keywords',
            'google_description',
            'nutrition',
        ];

        try {
            DB::beginTransaction();

            /** @var Product $product */
            $product = Product::updateOrCreate(['id' => $id], [
                'sku'            => request('sku'),
                'state'          => $this->switch(request('state')),
                'is_bio'         => $this->switch(request('is_bio')),
                'is_lv'          => $this->switch(request('is_lv')),
                'is_suggested'   => $this->switch(request('is_suggested')),
                'is_highlighted' => $this->switch(request('is_highlighted')),
                'mark_up'        => request('mark_up'),
                'cost'           => request('cost'),
                'vat_id'         => request('vat_id'),
                'unit_id'        => request('unit_id'),
                'main_category'  => request('main_category', 1),
                'supplier_id'    => request('supplier_id'),

                // 'has_deposit'    => $this->switch(request('has_deposit')),
                // 'deposit_amount' => request('deposit_amount'),
            ]);

            $this->handleMetas($product, $metas, 'name-id');
            $this->handleImages($product);

            $res = $this->setVariations($product) ?? $product->createVariation();

            $this->addCategories($product);
            $this->addMarketDays($product);

            $product->attributeValues()->sync(request('attributeValues'));
            $product->attributes()->sync(request('attribute'));

            DB::commit();

            $product->forgetMeta(['slug', 'name']);

            (new CacheController)->createProductCache($product->id, true);
        } catch (\PDOException $e) {
            DB::rollBack();

            \Log::error($e);
            session()->flash("message", ['msg' => $e->getMessage(), 'isError' => true]);

            return redirect()->back()->withInput();
        }

		$this->updateDraftOrderLines($product);

        return redirect(route('products.list'))->with(['message' => ['msg' => "Product Saved"]]);
    }

	private function updateDraftOrderLines(Product $product)
	{
        return null;
		$lines = OrderLines::query()
				->whereHas('order', function($q) {
					$q->where('state', 'draft');
				})
				->where('product_id', $product->id)
				->get();

		foreach ($lines as $line)
		{
			$product = $this->cache()->getProduct($product->id);
			$variation = $product->getVariationPrice($line->variation->id);
			$line->update([
				'supplier_id'    => $product->supplier_id,
				'supplier_name'  => __('supplier.name.' . $product->supplier_id),
				'product_id'     => $product->id,
				'product_name'   => __("product.name.{$product->id}"),
				'vat_id'         => $product->price->vat_id,
				'vat_amount'     => $product->price->vat,
				'vat'            => $variation->vat,
				'price'          => $variation->price,
				'display_name'   => $variation->display_name,
				'variation_size' => $variation->size,
				'price_raw'      => $variation->price_raw,
				'vat_raw'        => $variation->vat_raw,
				'cost'           => $variation->cost,
				'markup'         => $variation->markup,
				'markup_amount'  => $variation->markup_amount,
			]);
		}
	}

    public function destroy($id)
    {
        /** @var Product $cc */
        $cc = Product::findOrFail($id);

        try {
            DB::beginTransaction();
            $cc->extra_categories()->detach();
            $cc->metaData()->delete();
            $cc->variations()->delete();
            $cc->forceDelete();
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            abort(500);
        }

        return ['status' => true, "message" => "Product Deleted"];
    }

    public function edit($id)
    {
        /**
         * If search is used..
         * I dont know why the search form URL is so malformed but...
         */
        $input = request()->input();
        if (count($input) > 0) {
            $id = collect(array_keys($input))->first();
            return redirect("/admin/shop/products/edit/$id");
        }

        // Correct url response
        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => Product::findOrFail($id), 'modalId' => ["attributes" => str_random(10), "variations" => str_random(10)]]);
    }

    public function getEditName($id)
    {
        $r = explode(".", Route::currentRouteName());
        if ((array_pop($r) ?? "") == 'search') {
            return request()->route('search');
        }

        return Product::findOrFail($id)->name;
    }

    public function state($id)
    {
        $state = [];
        /** @var Product $md */
        if ($md = Product::find($id)) {
            $md->state = !$md->state;
            $md->save();
            // $md->delete();

            $msg = $md->state ? 'Product set as Active' : 'Product set as Draft';

            // $state = ['status' => true, 'newState' => true, "message" => 'Product set as Draft'];
            $state = ['status' => true, 'newState' => true, "message" => $msg];
        }
        // else if ($md = Product::onlyTrashed()->findOrFail($id)) {
        //     $md->restore();
        //     $md->state = true;
        //     $md->save();

        //     $state = ['status' => true, 'newState' => true, "message" => 'Product set as Active'];
        // }

        (new CacheController)->forgetCache("product$id");

        return $state;
    }


    // Variations
    public function calculatePrice()
    {

        request()->validate([
            'vat_id' => 'required',
            'cost'   => 'required',
        ]);

        $vat = Vat::findOrFail(request('vat_id')) ?? (object)['amount' => 0];
        $price = calcPrice(request('cost'), $vat->amount, request('mark_up') ?: 0, 0);

        return ['status' => true, 'noMessage' => true, 'result' => $price];
    }

    public function makeDisplayString()
    {
        request()->validate([
            'unit_id' => 'required',
            'amount'  => 'required',
        ]);


        $result = $this->makeDisplayName(request(['unit_id', 'price', 'amount']));

        return ['status' => true, 'noMessage' => true, 'result' => $result['display_name']];
    }

    public function storeVariation()
    {
        $id = request('id');

        $data = request((new ProductVariation)->getFillable());
        $data['has_deposit'] = $this->switch($data['has_deposit'] ?? 0);

        $variation = ProductVariation::updateOrCreate(
            [
                'id' => $id,
            ],

            $this->withoutID($data)
        );

        if ($variation) {
            return ['status' => true, "message" => 'Product Variation Saved', "variation" => $variation->id, "result" => view('Products::variation', ['variation' => $variation, 'content' => Product::findOrFail(request('product_id'))])->render()];
        } else {
            return ['status' => false, "message" => "Some Error happened"];
        }
    }

    public function loadVariation()
    {
        $id = request('id');

        return ['status' => true, "noMessage" => true, "result" => ProductVariation::findOrFail($id)];
    }

    public function import()
    {
        return $this->googleDocsImport();
        // return $this->importOld();
    }

    /**
     * @deprecated This is old version. See googleDocsImport()
     */
    public function importOld()
    {
        request()->validate([
            'importFile' => 'file|required',
        ]);

        $file = request()->file('importFile');
        $filename = $file->storeAs('imports/products', str_random(40) . "." . $file->getClientOriginalExtension());

        Schedules::create([
            'filename' => basename($filename),
            'type'     => 'productImport',
        ]);

        return ['status' => true, 'message' => "Product Import Scheduled"];
    }

    /**
     * Import products from google sheet!
     */
    private function googleDocsImport()
    {
        $googleFileId  = '1ylV1SaPJ9JHdKCltogYsef9hp9gr61aLVaibN_iS7mo';
        $googleSheetId = '276998627';

        if (config('app.env') == 'local') {
            $googleFileId  = '1ylV1SaPJ9JHdKCltogYsef9hp9gr61aLVaibN_iS7mo';
            $googleSheetId = '1760492156';
        }


        // $googleFileId  = '1d_9P5iH8SyzumIwrgIsQx9q3nfOFXX3xjRPZfSJQxAo';
        // $googleSheetId = '276998627';

        $gSheetsFile = "https://docs.google.com/spreadsheets/d/" . $googleFileId . "/export?format=csv&id=" . $googleFileId . "&gid=" . $googleSheetId;

        $filename = 'imports/products/'. str_random(40) . '.csv';

        try {

            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            $cont = file_get_contents($gSheetsFile, false, stream_context_create($arrContextOptions));

            Storage::put($filename, $cont);

        } catch (\Throwable $th) {
            throw new \Exception("Error Downloading Google file! URL: $gSheetsFile | Error:" . $th->getMessage());
        }

        Schedules::create([
            'filename' => basename($filename),
            'type'     => 'productImport',
        ]);

        return ['status' => true, 'message' => "Product Import from Google Sheet Scheduled"];
    }

    public function export()
    {
        Schedules::create([
            'filename' => '',
            'type'     => 'productExport',
        ]);

        return ['status' => true, 'message' => "Product Export Scheduled"];


        app('debugbar')->disable();
        /** @var ProductImport $export */
        $export = new ProductImport();

        return $export->exportdata();

    }

    public function importImages()
    {
        request()->validate([
            'importFile' => 'file|required',
        ]);

        $zip = new \ZipArchive();
        $file = request()->file('importFile');
        $zresult = $zip->open($file);

        if ($zresult === true) {
            $zip->extractTo(storage_path('app/imports/product_images'));
            $zip->close();
            $result = ['status' => true, 'message' => 'Images uploaded, task scheduled'];

            Schedules::create([
                'filename'    => 'images',
                'type'        => 'productImageImport',
                'total_lines' => count(Storage::files('imports/product_images')),
            ]);

        } else {
            $result = ['status' => false, 'message' => 'Uploaded file is not an archive'];
        }


        return $result;

    }


    public function storage($search = false)
    {

        $cr = explode(".", Route::currentRouteName());

        if (!$search && ($cr[2] ?? false) == 'search') {
            return redirect()->route(implode(".", [$cr[0], $cr[1], "list"]));
        }

        return view('admin.elements.table',
            [
                'tableHeaders' => $this->storageList(),
                'header'       => 'Storage',
                'list'         => $this->getProducts(),
                'idField'      => 'name',
                'destroyName'  => 'Product',
                'operations'   => view("Products::partials.extraButtonsStorage")->render(),
                'js'           => [
                    'js/productIOStorage.js',
                ],
            ]);
    }

    public function storeStorage()
    {
        $product = '';

        request()->validate([
            'product_id' => 'required|exists:products,id',
        ]);

        try {
            DB::beginTransaction();
            /** @var Product $product */
            $product = Product::withTrashed()->where('id', request()->get('product_id'));
            if (request()->has('info')) {
                $product->update(['info' => request('info')]);
            } elseif (request()->has('amount')) {
                if (!is_null($product->firstOrFail()->storage_amount)) {
                    $product->increment('storage_amount', request()->get('amount'));
                } else {
                    $product->update(['storage_amount' => request()->get('amount')]);
                }
            } elseif (request()->has('reset')) {
                $product->update(['storage_amount' => null]);
            }
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
        }
        $product = $product->first();

        return ['status' => true, 'message' => 'Product Updated', 'data' => ['product_id' => $product->id, 'info' => $product->info, 'storage_amount' => $product->storage_amount]];
    }


    // Frontend
    public function favProduct(Request $request)
    {
        $user = Auth::user();
        $product = Product::find(request('product_id'));

        $result = null;
        $message = null;

        if ($user && $product) {
            $result = $user->favouriteProducts()->toggle(request('product_id'));
        }

        if ($result) {
            if ($result['attached']) {
                $message       = _t("translations.product_faved_thanks");
                $newButtonText = _t("translations.removeFromFav");
            }

            if ($result['detached']) {
                $message       = _t("translations.product_unfaved");
                $newButtonText = _t("translations.addToFav");
            }
        }

        if ($request->ajax()) {
            return [
                'status' => $result !== null,
                'message' => $message,
                'newButtonText' => $newButtonText,
            ];
        } else {
            return redirect()->back();
        }
    }

    public function rateProduct(Request $request)
    {
        $user = Auth::user();
        if (request('id')) {
            $pr = ProductRating::find(request('id'));
            $pr->review = request('review');
            $pr->save();
        } else {
            $pr = ProductRating::create([
                'rating'      => request('rating'),
                'product_id'  => request('product_id'),
                'user_id'     => $user ? $user->id : null,
            ]);
        }

        if ($request->ajax()) {
            return [
                'status' => true,
                'collectReview' => $user ? true : false,
                'message' => _t("translations.rated_thank_you"),
                'data' => $pr,
            ];
        } else {
            return redirect()->back();
        }
    }

    public function rateSupplier(Request $request)
    {

    }
}
