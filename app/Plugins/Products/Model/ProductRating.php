<?php namespace App\Plugins\Products\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;

class ProductRating extends Model
{
    public $fillable = [
        'rating','product_id','user_id','review','supplier_review',
    ];

    // public static function boot()
    // {
    //     static::addGlobalScope('order', function(Builder $builder) {
    //         $builder->orderBy('amount', 'asc');
    //     });

    //     parent::boot();
    // }


    // public function getPriceAttribute() {
    //     return calcPrice($this->cost?:0, $this->product->vat->amount, $this->mark_up, (Auth::user()?Auth::user()->discount():0));
    // }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getUserNameAttribute()
    {
        if ($this->user) {
            return $this->user->name . ' ' . $this->user->last_name;
        }

        return null;
    }

    public function scopeApprovedSupplierReviews($query)
    {
        return $query
            ->where('is_approved',true)
            ->where('supplier_review', '!=', null);
    }
    public function getSupplierReviewObject()
    {
        if ($this->user) {
            return (object) [
                'short'=> strtoupper(substr($this->user->name,0,1) . substr($this->user->last_name,0,1)),
                'name' => $this->user->name,
                'surname' => $this->user->last_name,
                'title' => null,
                'body' => $this->supplier_review,
            ];
        }

        return null;
    }

    public function scopeApprovedProductReviews($query)
    {
        return $query
            ->where('is_approved',true)
            ->where('review', '!=', null);
    }
    public function getProductReviewObject()
    {
        if ($this->user) {
            return (object) [
                'short'=> strtoupper(substr($this->user->name,0,1) . substr($this->user->last_name,0,1)),
                'name' => $this->user->name,
                'surname' => $this->user->last_name,
                'title' => $this->review,
                'body' => null,
            ];
        }

        return null;
    }
}
