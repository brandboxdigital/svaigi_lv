<?php

namespace App\Plugins\Products\Functions;


use App\Functions\General;
use App\Http\Controllers\CacheController;
use App\Plugins\Attributes\Model\Attribute;
use App\Plugins\Categories\Model\Category;
use App\Plugins\Products\Model\Product;
use App\Plugins\Products\Model\ProductVariation;
use App\Plugins\Units\Model\Unit;
use App\Plugins\Vat\Model\Vat;
use App\Schedules;
use Illuminate\Database\Eloquent\Builder;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Plugins\Sales\Model\Sale;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

use Exception;
use Illuminate\Support\Facades\Storage;


// The code snippet imports a product from an external source or file into the current program or system.
class ProductImport
{

    use General;

    public $attributes = [];
    public $attribute_values = [];
    public $suppliers = [];
    public $units = [];
    public $vat = [];
    public $categories = [];
    public $required = [
        'sku',
        'state',
        'supplier_id',
        'marketdays',
        'categories',
        'is_suggested',
        'is_highlighted',
        'unit_id',
        'price',
        'mark_up',
        'vat_id',
    ];
    public $metas = [
        'name',
        'description',
        'ingredients',
        'expire_date',
        'google_keywords',
        'google_description',
    ];

    public static function runImport($scheduled) {
        return (new self)->readImportData($scheduled);
    }

    public function __construct()
    {
        $this->attributes = __('attributes.slug');
        $this->attribute_values = __('attributevalues.slug');
        $this->vat = Vat::all()->pluck('amount', 'id')->toArray();
        $this->units = Unit::all()->pluck('unit', 'id')->toArray();
        $this->suppliers = __('supplier.slug');
        $this->categories = Category::get()->pluck('unique_name', 'id')->toArray();
    }

    public function readImportData($schedule)
    {
        try {

            if (!ini_get("auto_detect_line_endings")) {
                ini_set("auto_detect_line_endings", '1');
            }

            $filename = storage_path("app/imports/products/" . $schedule['filename']);
            if(!\Storage::exists("imports/products/" . $schedule['filename'])) {
                return ['status' => false, 'message' => 'Import File not present'];
            }
            $csv = Reader::createFromPath($filename, 'r');
            $csv->setHeaderOffset(0);

            if(!empty(array_diff($csv->getHeader(), $this->importFieldData())) || !empty(array_diff($this->importFieldData(),$csv->getHeader()))) {
                \Log::error([
                    'csvheader' => $csv->getHeader(),
                    'importFieldData' => $this->importFieldData(),
                ]);
                return ['status' => false, 'message' => 'Missing fields in import (or import has renamed headers)'];
            }

            Schedules::find($schedule['id'])->update(['total_lines' => $csv->count()]);

            $linereader = (new Statement())->offset($schedule['stopped_at']);

            $previousProduct = false;
            foreach ($linereader->process($csv) as $l => $importLine) {

                $sku = $this->getLineData($importLine, 'search');

                if($previousProduct==$sku['sku']) {
                    $product['metas'] = array_merge($product, $this->getLineData($importLine, 'metaData', $product['metas']));

                    if ($productCollection ?? false) {
                        $this->handleMetas($productCollection, array_merge($this->metas, ['slug']), 'name-id', $product['metas']);
                        Schedules::find($schedule['id'])->increment('stopped_at', 1);
                    }
                    continue;
                } else {
                    unset($product, $productCollection);
                }

                $product['search'] = $sku;

                $product['update'] = $this->getLineData($importLine, 'product');



                $product['metas'] = $this->getLineData($importLine, 'metaData');

                $product['promotion'] = $this->getLineData($importLine, 'promotion');

                $product = array_merge($product, $this->getLineData($importLine, 'marketDays'));
                $product = array_merge($product, $this->getLineData($importLine, 'variations'));
                $product = array_merge($product, $this->getLineData($importLine, 'attributes'));
                $product = array_merge($product, $this->getLineData($importLine, 'categories'));



                $productCollection = $this->saveProduct($product);
                $this->handleMetas($productCollection, array_merge($this->metas, ['slug']), 'name-id', $product['metas']);
                Schedules::find($schedule['id'])->increment('stopped_at', 1);
                $previousProduct = $sku['sku'];

                $productCollection->forgetMeta(['slug', 'name']);

                if ($productCollection->deleted_at != NULL) {
                    try {
                        (new CacheController)->createProductCache($productCollection->id, true);
                    } catch (\Throwable $th) {}
                }

            }

            $csv = null;

            (new CacheController)->getPromotions(true);
            Cache::flush();

            return ['status' => true, 'message' => 'Import Successful'];
        } catch (\Throwable $th) {
            $sh = Schedules::find($schedule['id']);
            $encoded = json_encode($importLine ?? []);

            \Log::error($th, $importLine ?? null);

            // dump($importLine ?? null, $product ?? null);

            return ['status' => false, 'message' => 'Error @ line ' . ($sh->stopped_at+2) . " | " . $th->getMessage() . ' | ' . $encoded];
        }
    }

    public function saveProduct($product)
    {
        // dump($product);

        /** @var Product $productCardData */
        $productCardData = Product::withTrashed()->updateOrCreate($product['search'], $product['update']);

        $productId = $productCardData->id ?? null;


        /**
         * sheet url: https://docs.google.com/spreadsheets/d/1ylV1SaPJ9JHdKCltogYsef9hp9gr61aLVaibN_iS7mo/edit#gid=276998627
         */
        if (count($product['promotion'])) {
            $promotion = $product['promotion'][0] ?? null;

            if ($promotion) {

                $data = [
                    'name'             => $product['metas']['name']['lv'],
                    'amount'           => $product['promotion'][0],
                    'discount_to'      => 'product',
                    'discount_target'  => [$productId],
                    'group'            => null,
                    'valid_from'       => Carbon::now(),
                    'valid_to'         => Carbon::now()->addWeeks(2),
                    'user_group'       => []
                ];

                Sale::updateOrCreate([
                    'discount_target' => "[$productId]"
                ], $data);
            }

        } else {
            $sales = Sale::where('discount_target', "[$productId]")
                ->get()
                ->each(function ($sale) {
                    $sale->delete();
                });
        }

        $productId = $productCardData->id;

        try {
            $productCardData->attributeValues()->sync($product['attributeValues']);
        } catch (\Throwable $th) {}

        try {
            $productCardData->attributes()->sync($product['attributes']);
        } catch (\Throwable $th) {}

        if ($product['categories']) {
            $productCardData->extra_categories()->sync($product['categories']);
        }

        if ($product['search']['sku'] == 347) {
            // dd($productCardData, $this->categories);
        }

        $importedVariations = [];
        foreach ($product['variations']??[1 => ''] as $size => $comment) {
            $deposit = 0;

            if ( !empty($product['deposit_variations'][$size] ?? null) ) {
                $deposit = $this->valueToFloatOrNull($product['deposit_variations'][$size]) ?? 0;
            }

            $pv = ProductVariation::updateOrCreate(['amount' => (float)$size, 'product_id'   => $productId], [
                'for_supplier' => $comment,
                'display_name' => $comment,

                'has_deposit'    => ($deposit>0) ? true : false,
                'deposit_amount' => $deposit,
                // 'display_name' => $this->makeDisplayName((float)$size, $productCardData),
            ]);

            $importedVariations[] = $pv->id;
        }

        $productCardData->variations()->whereNotIn('id', $importedVariations)->delete();

        $productCardData->market_days()->sync($product['marketdays']);

        return $productCardData;

    }


    public function makeDisplayName($amount, $product)
    {
        /** @var Unit $unit */
        $unit = $product->unit;
        if ($unit->subUnit()->count() && $amount < 1) {
            $unit = $unit->subUnit;
            $displayName = ($amount * $unit->parent_amount);
        }

        return ($displayName ?? $amount) . $unit->unit;
    }

    public function getLineData($line, $dataType, $response = [])
    {

        $fields = $this->importFieldData();

        switch ($dataType) {
            case "search":
                $response = [
                    'sku' => trim($line[$fields['sku']]),
                ];
                break;

            case "product":

                $deposit = 0;

                if ( !empty($line[$fields['deposit']] ?? null) ) {
                    $deposit = \floatval( $line[$fields['deposit']] ) ?? 0;
                }

                $response = [
                    'supplier_id'    => array_search($line[$fields['supplier_id']], $this->suppliers),
                    // 'deleted_at'     => !$line[$fields['deleted_at']] ? now() : null,
                    'state'          => $line[$fields['state']],
                    'is_bio'         => $line[$fields['is_bio']],
                    'is_lv'          => $line[$fields['is_lv']],
                    'is_suggested'   => $line[$fields['is_suggested']],
                    'is_highlighted' => $line[$fields['is_highlighted']],
                    'unit_id'        => array_search($line[$fields['unit_id']], $this->units),
                    'cost'          => $line[$fields['price']],
                    'mark_up'        => $line[$fields['mark_up']],
                    'vat_id'         => array_search($line[$fields['vat_id']], $this->vat),
                    'sequence'       => $line[$fields['sequence']] ?? 0,
                    'main_category'  => array_search(current(explode("|", $line[$fields['categories']])), $this->categories),


                ];

                break;

            case "metaData":
                $metas = $this->metas;

                $language = $line[$fields['language']];

                foreach ($metas as $field) {
                    $response[$field][$language] = trim($line[$fields[$field]]);
                }

                break;

            case "marketDays":
                $response['marketdays'] = explode('|', trim($line[$fields['marketdays']]));
                break;

            case "variations":
                $variations         = explode("|", trim($line[$fields['variations']]));
                $deposit_variations = explode("|", trim($line[$fields['deposit']]));

                foreach ($variations as $key => $variation) {
                    list($size, $comment) = array_pad(explode(":", $variation),2, "");
                    if(!$size) { break; }

                    $response['variations'][$size] = $comment ?? "";
                    $response['deposit_variations'][$size] = $deposit_variations[$key] ?? 0;
                }

                break;

            case "attributes":
                $attrText = $line[$fields['attributes']];
                if (empty($attrText)) {
                    $attrText = 'svars:100g';
                }

                $attributes = explode("|", trim($attrText));

                foreach ($attributes as $attribute) {
                    if($attribute) {
                        list($attributeSlug, $values) = array_pad(explode(":", $attribute),2, "");

                        if(empty($values)) {  break; }

                        $response['attributes'][] = array_search($attributeSlug, $this->attributes);
                        $response['attributeValues'] = array_map( function ($attributeValue) {
                            return array_search($attributeValue, $this->attribute_values);
                        }, explode(",", $values));
                    }
                }
                break;

            case "categories":
                $categories = explode("|", trim($line[$fields['categories']]));

                $response['categories'] = collect($categories)
                    ->map(function ($category) {
                        return array_search($category, $this->categories);
                    })
                    ->filter()
                    ->values()
                    ->all();

                // dd($response);

                break;

            case "promotion":
                if ($line[$fields['promotion']]) {
                    $response[] = $line[$fields['promotion']];
                }
            break;

        }

        // dump($response, $line);
        return $this->checkRequired($response);
    }

    public function checkRequired($fields)
    {

        foreach ($this->required as $fieldName) {

            if (($fields[$fieldName] ?? "unset") == "") {
                throw new Exception("$fieldName is Required!");
                return null;
            }
        }

        return $fields;
    }

    /**
     * Define Product import fields
     *
     * @return array
     */
    public function importFieldData()
    {
        return [
            'sku'                => "SKU",
            'supplier_id'        => "Farmer",
            'state'              => "Active",
            'marketdays'         => "Market Days",
            'name'               => "Product Name",
            'categories'         => "Categories",
            'is_bio'             => "BIO",
            'is_lv'              => "LV",
            'is_suggested'       => "Suggested",
            'is_highlighted'     => "Highlighted",
            'description'        => "Description",
            'ingredients'        => "Ingridients",
            'expire_date'        => "Expiration Date",
            'unit_id'            => "Measurement Unit",
            'variations'         => "Variations",
            'price'              => "Price",
            'mark_up'            => "Mark Up",
            'vat_id'             => "Vat",
            'language'           => "Language",
            'sequence'           => "Sequence",
            'attributes'         => "Attributes",
            'google_keywords'    => "SEO Keywords",
            'google_description' => "SEO Description",
            'promotion'          => "Promotion",
            'deposit'            => "Deposit",
        ];
    }

    public function exportData()
    {
        $csv = Writer::createFromString();

        $fieldOrder = $this->importFieldData();

        $csv->insertOne($fieldOrder);

        $products = Product::withTrashed()->get();

        /** @var Product $product */
        foreach ($products as $product) {
            foreach (languages() as $language) {
                if (language() == $language->code) {
                    $insertData = [
                        'language'           => $language->code,
                        'name'               => $product->meta['name'][$language->code] ?? " ",
                        'description'        => $product->meta['description'][$language->code] ?? " ",
                        'ingredients'        => $product->meta['ingredients'][$language->code] ?? " ",
                        'expire_date'        => $product->meta['expire_date'][$language->code] ?? " ",
                        'google_keywords'    => $product->meta['google_keywords'][language()] ?? " ",
                        'google_description' => $product->meta['google_description'][language()] ?? " ",
                        'sequence'           => $product->sequence ?? 0,
                        'sku'                => $product->sku ?? " ",
                        'price'              => number_format($product->cost, 2),
                        'mark_up'            => $product->mark_up ?? 0,
                        'vat_id'             => $product->vat->amount ?? 0,
                        'supplier_id'        => $this->suppliers[$product->supplier_id],

                        // 'deleted_at'         => ($product->deleted_at ? 0 : 1),
                        'state'              => $product->state,

                        'categories'         => implode("|", $this->getCategorySlugs($product->extra_categories->pluck('id')->toArray())),
                        'is_bio'             => $product->is_bio,
                        'is_lv'              => $product->is_lv,
                        'is_suggested'       => $product->is_suggested,
                        'is_highlighted'     => $product->is_highlighted,
                        'unit_id'            => $product->unit->unit,
                        'variations'         => implode("|", $this->getVariations($product->variations->toArray())),
                        'marketdays'         => implode("|", $product->market_days->pluck('id')->toArray()),
                        'attributes'         => implode("|", $this->getAttributes($product->attributes()->get(), $product)),
                    ];
                } else {
                    $insertData = [
                        'sku'                => $product->sku,
                        'language'           => $language->code,
                        'name'               => $product->meta['name'][$language->code] ?? "",
                        'description'        => $product->meta['description'][$language->code] ?? "",
                        'ingredients'        => $product->meta['ingredients'][$language->code] ?? "",
                        'google_keywords'    => $product->meta['google_keywords'][language()] ?? "",
                        'google_description' => $product->meta['google_description'][language()] ?? "",
                    ];
                }

                $insertDataLine = [];
                foreach ($fieldOrder as $orderID => $unused) {
                    $insertDataLine[] = $insertData[$orderID] ?? " ";
                }
                $csv->insertOne($insertDataLine);
            }
        }

        $csvOutput = function () use ($csv) {
            echo $csv->getContent();
        };

        $fileName = 'product_export_' . now()->format('dmyHis') . '.csv';

        $response = new StreamedResponse();
        $response->headers->set('Content-Encoding', 'none');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Description', 'File Transfer');
        $response->setCallback($csvOutput);
        $response->send();
    }

    public static function runExportDataFile()
    {
        $storage = Storage::createLocalDriver(['root' => storage_path()]);
        $filename = 'product_export_' . now()->format('dmyHis') . '.csv';
        $path = $storage->path("export/");

        $me = new self;

        try {
            // create empty file
            $storage->put('export/' . $filename, null);

            $csv = Writer::createFromPath($path . $filename);

            $fieldOrder = $me->importFieldData();

            $csv->insertOne($fieldOrder);

            $products = Product::withTrashed()->get();

            /** @var Product $product */
            foreach ($products as $product) {
                foreach (languages() as $language) {
                    if (language() == $language->code) {
                        $insertData = [
                            'language'           => $language->code,
                            'name'               => $product->meta['name'][$language->code] ?? " ",
                            'description'        => $product->meta['description'][$language->code] ?? " ",
                            'ingredients'        => $product->meta['ingredients'][$language->code] ?? " ",
                            'expire_date'        => $product->meta['expire_date'][$language->code] ?? " ",
                            'google_keywords'    => $product->meta['google_keywords'][language()] ?? " ",
                            'google_description' => $product->meta['google_description'][language()] ?? " ",
                            'sequence'           => $product->sequence ?? 0,
                            'sku'                => $product->sku ?? " ",
                            'price'              => number_format($product->cost, 2),
                            'mark_up'            => $product->mark_up ?? 0,
                            'vat_id'             => $product->vat->amount ?? 0,
                            'supplier_id'        => $me->suppliers[$product->supplier_id],
                            'deleted_at'         => ($product->deleted_at ? 0 : 1),
                            'categories'         => implode("|", $me->getCategorySlugs($product->extra_categories->pluck('id')->toArray())),
                            'is_bio'             => $product->is_bio,
                            'is_lv'              => $product->is_lv,
                            'is_suggested'       => $product->is_suggested,
                            'is_highlighted'     => $product->is_highlighted,
                            'unit_id'            => $product->unit->unit,
                            'variations'         => implode("|", $me->getVariations($product->variations->toArray())),
                            'marketdays'         => implode("|", $product->market_days->pluck('id')->toArray()),
                            'attributes'         => implode("|", $me->getAttributes($product->attributes()->get(), $product)),
                        ];
                    } else {
                        $insertData = [
                            'sku'                => $product->sku,
                            'language'           => $language->code,
                            'name'               => $product->meta['name'][$language->code] ?? "",
                            'description'        => $product->meta['description'][$language->code] ?? "",
                            'ingredients'        => $product->meta['ingredients'][$language->code] ?? "",
                            'google_keywords'    => $product->meta['google_keywords'][language()] ?? "",
                            'google_description' => $product->meta['google_description'][language()] ?? "",
                        ];
                    }

                    $insertDataLine = [];
                    foreach ($fieldOrder as $orderID => $unused) {
                        $insertDataLine[] = $insertData[$orderID] ?? " ";
                    }
                    $csv->insertOne($insertDataLine);
                }
            }
        } catch (\Throwable $th) {
            return ['status' => false, 'message' => "Error " . $th->getMessage()];
        }

        $msg = 'Product Export created: <a href="'.route('download', ["export", $filename]).'">Download</a>';

        return ['status' => true, 'message' => $msg];
    }

    public function getCategorySlugs($categories)
    {
        return array_map(function ($item) { return $this->categories[$item]; }, $categories);
    }

    public function getVariations($variations)
    {
        return array_map(function ($item) { return $item['amount'] . ":" . $item['for_supplier']; }, $variations);
    }

    public function getAttributes($attributes, $product)
    {
        $attributeList = [];
        /** @var Attribute $attribute */
        foreach ($attributes as $attribute) {
            $attributeValues = $attribute->values()->whereHas('product', function (Builder $q) use ($product) { $q->where('product_id', $product->id); })->get()->pluck('id')->toArray();
            $attributeList[] = implode(":", [$this->getAttributeSlug($attribute->id), implode(",", $this->getAttributeValues($attributeValues))]);
        }

        return $attributeList;
    }

    public function getAttributeSlug($attribute)
    {
        return $this->attributes[$attribute] ?? "";
    }

    public function getAttributeValues($values)
    {
        return array_map(function ($value) { return $this->attribute_values[$value]; }, $values) ?? [];
    }

    private function valueToFloatOrNull($value = 0)
    {
        try {
            $value = str_replace([',', '.'] , ['.', '.'], $value);

            return floatval( $value ) ?? null;

        } catch (\Throwable $th) {
            \Log::error($th->getMessage());
        }

        return null;
    }
}
