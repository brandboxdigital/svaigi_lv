<?php

//namespace App\Plugins\Products;
namespace App\Plugins\Products\Functions;

use App\Plugins\Attributes\Model\Attribute;
use App\Plugins\Attributes\Model\AttributeValue;
use App\Plugins\MarketDays\Model\MarketDay;
use App\Plugins\Products\Model\Product;
use App\Plugins\Products\Model\ProductVariation;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Units\Model\Unit;
use App\Plugins\Vat\Model\Vat;
use App\Schedules;

/**
 * Trait Products
 *
 * @package App\Plugins\Products
 */
trait Products
{

    public function storageList()
    {
        return [
            ['field' => 'sku', 'label' => 'Product Code'],
            ['field' => 'name', 'label' => 'Product Name', 'translate' => 'product.name', 'key' => 'id'],
            ['field' => 'info_window', 'label' => 'Info'],
            ['field' => 'available', 'label' => 'Current Amount'],
            ['field' => 'amount', 'label' => 'Add Amount'],
        ];
    }


    /**
     * @return array
     */
    public function form()
    {
        $selectedItems = old('main_category') ?? (new Product)->formatSelected('main_category');
        $categories = \App\Plugins\Categories\Model\Category::with('nodes.nodes.nodes.nodes')
            ->where('parent_id', null)
            ->get();

        // $this->categoryRecursiveLooper($categories, $selectedItems);

        // dd($categories->toArray());
        $suppliers = Supplier::all();
        $languages = languages()->pluck('name', 'code');

        return [
            [
                'Label'     => 'Display',
                'languages' => $languages,
                'data'      => [
                    'name'        => ['type' => 'text', 'class' => 'slugify', 'label' => 'Product Name', 'meta' => true,],
                    'slug'        => ['type' => 'text', 'class' => '', 'label' => 'Product Slug', 'readonly' => 'readonly', 'meta' => true],
                    'description' => ['type' => 'textarea', 'class' => '', 'label' => 'Description', 'meta' => true],
                    'ingredients' => ['type' => 'textarea', 'class' => '', 'label' => 'Ingredients', 'meta' => true],
                    'expire_date' => ['type' => 'text', 'class' => '', 'label' => 'Expire Info', 'meta' => true],
                    'nutrition' => ['type' => 'text', 'class' => '', 'label' => 'Nutrition', 'meta' => true],
                ],
            ],
            [
                'Label'     => 'Google SEO',
                'languages' => $languages,
                'data'      => [
                    'google_keywords'    => ['type' => 'text', 'class' => '', 'label' => 'Keywords', 'meta' => true],
                    'google_description' => ['type' => 'textarea', 'class' => '', 'label' => 'Google Description', 'meta' => true],
                ],
            ],

            [
                'Label' => 'Categories',
                'data'  => [
                    'main_category'    => ['type' => 'select', 'label' => 'Main Category', 'options' => $categories, 'comment' => '!! Šo nemainam. Šis lauks ar laiku pazudīs!'],
                    'extra_categories' => ['type' => 'categoryselect', 'label' => 'Extra Categories', 'options' => $categories],
                ],
            ],
            [
                'Label' => 'Parameters',
                'data'  => [
                    'sku'              => ['type' => 'text',   'class' => '', 'label' => 'Product Code'],
                    'state'            => ['type' => 'switch', 'class' => '', 'label' => 'State',       'comment' => 'Rādās/Nerādās'],
                    'is_bio'           => ['type' => 'switch', 'class' => '', 'label' => 'Bio',         'comment' => 'Produkta atvērumā parādās "Bio" uz produkta bildes'],
                    'is_lv'            => ['type' => 'switch', 'class' => '', 'label' => 'Latvian',     'comment' => 'Neko nedara šobrīd'],
                    'is_suggested'     => ['type' => 'switch', 'class' => '', 'label' => 'Suggested',   'comment' => 'Produktu sarakstos parādās birka "Ieteikts"'],
                    'is_highlighted'   => ['type' => 'switch', 'class' => '', 'label' => 'Highlighted', 'comment' => 'Šis produkts parādīsies pirmajā lapā, izcelto produktu sarakstā'],
                    'market_days'      => ['type' => 'chosen', 'label' => 'Market Day(-s)', 'options' => MarketDay::all()],
                    'supplier_id'      => ['type' => 'select', 'label' => 'Supplier', 'options' => $suppliers],
                    'product_image'    => ['type' => 'image', 'label' => 'Product Image', 'preview' => true],
                ],
            ],

            [
                'Label' => 'Prices',
                'data'  => [
                    'cost'    => ['type' => 'text', 'class' => '', 'label' => 'Cost'],
                    'mark_up' => ['type' => 'text', 'class' => '', 'label' => 'Mark Up (%)'],
                    'vat_id'  => ['type' => 'select', 'class' => '', 'label' => 'Vat', 'options' => Vat::all()],
                    'unit_id' => ['type' => 'select', 'class' => 'set_unit_id', 'label' => 'Measurement Unit', 'options' => Unit::all()],
                ],
            ],
            [
                'Label' => 'Attributes',
                'data'  => [
                    'filters' => ['type' => 'view', 'class' => 'Products::attributes'],
                ],
            ],
            [
                'Label' => 'Prices/Variations',
                'data'  => [
                    'price' => ['type' => 'view', 'class' => 'Products::specifications'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getList()
    {
        return [
            ['field' => 'sku', 'label' => 'Product Code'],
            ['field' => 'state', 'label' => 'Is&nbsp;Active', 'type' => 'yesno'],
            ['field' => 'name', 'label' => 'Product Name', 'translate' => 'product.name', 'key' => 'id'],
            ['field' => 'slug', 'label' => 'Product Slug', 'translate' => 'product.slug', 'key' => 'id'],
            ['field' => 'supplier_id', 'label' => 'Supplier', 'translate' => 'supplier.name', 'key' => 'supplier_id'],
            ['field' => 'category_list_names', 'label' => 'Product Categories'],
            ['field' => 'buttons', 'buttons' => ['edit', 'state', 'delete'], 'label' => '',],
        ];
    }

    /**
     * @param Product $collection
     *
     * @return bool|null
     */
    public function setVariations(Product $collection)
    {
        $variations = request('variation') ?? false;

        if (!$variations) return null;

        $DBvariations = ProductVariation::findMany($variations);

        $removeVariations = array_diff($collection->variations()->pluck('id')->toArray(), $variations);

        foreach ($removeVariations as $rVariation) {
            ProductVariation::find($rVariation)->delete();
        }

        foreach ($DBvariations as $variation) {
            $variation->product_id = $collection->id;
            $variation->save();
        }

        return true;
    }

    /**
     * @param Product $collection
     */
    public function addCategories(Product $collection)
    {
        $extra_categories = [];

        if (request('extra_categories')) {
            $extra_categories = explode(',',request('extra_categories'));
        }

        $categories = collect((array_merge([request('main_category')], ($extra_categories ?? []))))
            ->unique()
            ->filter()
            ->toArray();

        $collection->extra_categories()->sync($categories);
    }

    /**
     * @param Product $collection
     */
    public function addMarketDays(Product $collection)
    {
        $marketdays = request('market_days');
        $collection->market_days()->sync($marketdays);
    }


    /**
     * @param array $variation
     *
     * @return array
     */
    public function withoutID(array $variation)
    {
        unset($variation['id']);

        return $this->makeDisplayName($variation);
    }

    /**
     * @param array $variation
     *
     * @return array
     */
    public function makeDisplayName(array $variation)
    {
        if (!($variation['display_name'] ?? false)) {
            $unit = Unit::findOrFail(request('unit_id')) ?? "";

            if ($unit->subUnit()->count() && request('amount') < 1) {
                $unit = $unit->subUnit;
                $displayName = (request('amount') * $unit->parent_amount) . $unit->unit;
            }
            $variation['display_name'] = $displayName ?? request('amount') . $unit->unit;
        }

        return $variation;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        $options = [];
        $attribute = Attribute::findOrFail(request('attribute'));

        $allValues = $attribute->values;
        $selectedValues = $attribute->values()->whereHas('product', function ($q) {
            $q->where('product_id', request('product'));
        })->get()->pluck('id')->toArray();

        foreach ($allValues as $attributeValue) {
            $options[] = [
                'name'     => __("attributevalues.name.{$attributeValue->id}"),
                "id"       => $attributeValue->id,
                'selected' => in_array($attributeValue->id, $selectedValues),
            ];
        }

        return ['status' => true, 'noMessage' => true, 'options' => $options, "attribute" => $attribute->id];
    }

    /**
     * @return array
     */
    public function formatAttributes()
    {
        $attribute = Attribute::findOrFail(request('attributeSel'));

        $attributeValues = AttributeValue::findMany(request('attributeValuesSel'));

        if ($attributeValues->count() == 0) {
            return ['status' => true, 'remove' => true, 'attributeId' => $attribute->id];
        }

        return [
            'status'      => true,
            'attributeId' => $attribute->id,
            'data'        => view('Products::attribute',
                [
                    'attribute'          => $attribute,
                    'selectedAttributes' => $attributeValues,
                ])->render(),
            'message'     => 'Changes to Attribute have been made',
        ];

    }

    public function uploadImport()
    {

    }

    public function scheduleImport()
    {
        return Schedules::create([
            'plugin'     => 'Products',
            'class'      => 'ProductImport',
            'data'       => $importData,
            'progress'   => 0,
            'is_running' => false,
            'stopped_at' => 0,
            'result'     => 'Awaiting for Schedule',
        ]);
    }

    private function categoryRecursiveLooper(&$items, $selectedItems) {
        foreach ($items as $key => $item) {

            if (in_array($item->id, $selectedItems)) {
                $item->state = ['selected' => true];
            }

            $this->categoryRecursiveLooper($item, $selectedItems);
        }
    }
}
