@php
/** @var \App\Cache\ProductCache $item */
@endphp

@if ($item && $item->id)

<div class="sv-product-card {{ $item->isSale() }}">
    <form method="post" action="{{ r("cartAdd") }}" data-cart-form>
        {{ @csrf_field() }}
        <input type="hidden" name="product_id" value="{{ $item->id }}" />

        @if($item->isSale())
            <div class="sv-tag sale">
                {!! _t('translations.sale') !!}
            </div>
        @elseif($item->isSuggested)
            <div class="sv-tag sugg">
                {!! _t('translations.suggested') !!}
            </div>
        @elseif($item->isNew())
            <div class="sv-tag new">
                {!! _t('translations.new') !!}
            </div>
        @endif
        @if($item->isBio)
            <div class="sv-badge">
                {{-- <img src="{{ asset('assets/img/badge-bio-1.svg') }}"> --}}
                <img class="fix-size" src="{{ asset('assets/img/bio.png') }}">
            </div>
        @endif

        <a href="{{ $item->getUrl() }}" class="image" style="background-image: url({{ $item->image(config('app.imageSize.product_image.list')) }});">
            <div class="sizing">
                <div class="hovertext">
                    {{-- {!! _t('translations.seemore') !!} --}}
                    Apskatīt
                </div>
            </div>
        </a>
        <div class="text">
            <h2><a href="{{ $item->getUrl() }}">{{ __("product.name.{$item->id}") }}</a></h2>
            <div class="inner-spacer"></div>
            @if($item->hasManyPrices())
                <select name="variation_id" class="selectpicker">
                    @foreach($item->prices() as $priceID => $price)
                        <option
                            data-wrap="true"
                            {{ $item->isAvailable($priceID, ($itemAmount??null))?"":"disabled" }}
                            value="{{ $priceID }}" {{$loop->first?"selected":""}} {!! $item->isSale()?"data-origprice={$price->oldPrice}&nbsp;€":"" !!}
                            @if ($price->has_deposit)
                                data-appendhtml='<small class="">+&nbsp;depozīts {{ number_format($price->deposit_amount, 2, '.', '') }}&nbsp;€</small>'
                            @endif
                        >
                            {!! implode(" / ",[$price->price."&nbsp;€", $price->display_name]) !!}
                        </option>
                    @endforeach
                </select>
            @else
                @if ($item->prices())
                    <h3>
                        <input type="hidden" value="{{ $item->prices()->id }}" name="variation_id" />
                        @if($item->isSale())
                            <s>{{ $item->prices()->oldPrice }}&nbsp;€</s>
                        @endif
                        {!! implode(" / ",[$item->prices()->price."&nbsp;€", $item->prices()->display_name]) !!}
                        @if ($item->prices()->has_deposit)
                            <small class=""> +&nbsp;depozīts {{ number_format($item->prices()->deposit_amount, 2, '.', '') }}&nbsp;€ </small>
                        @endif
                    </h3>
                @endif
            @endif
            <h4><a href="{{ r('supplierOpen', [getSupplierSlugs(true),__("supplier.slug.{$item->supplier_id}")]) }}">{{ __("supplier.name.{$item->supplier_id}") }}</a></h4>
        </div>
        <button class="add-to-cart {{ in_array((new \App\Http\Controllers\CacheController)->getSelectedMarketDay()->id,$item->marketDays)?"":"is-disabled" }} add-to-cart-main-page">
                <span class="icon">
                    <s></s>
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="14" viewBox="0 0 26 14">
                        <path d="M25.91,2l0.081,0.046L21,13.538V14H6l-0.806-.014L0.009,2.046,0.09,2H0V0H26V2H25.91ZM2.234,2L6.577,12H19.423L23.766,2H2.234Z"></path>
                    </svg>
                </span>
        </button>
    </form>
</div>

@endif
