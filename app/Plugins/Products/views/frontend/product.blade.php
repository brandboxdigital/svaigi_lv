@extends('Categories::frontend.list')

@php
    $hideHeader = true;
    /** @var \App\Cache\ProductCache $product **/
@endphp

@section('wrapper')
    sv-product-single-wrapper
@endsection
<div class="sv-category-title-mobile">
    <div class="line">
        <a href="javascript:void()" onclick="window.history.go(-1); return false;" class="arrow arrow-back"></a>
        {{-- <h1 class="mobile-category-name toggle-sv-menu-mobile">Gatavi produktu grozi</h1> --}}
    </div>
</div>
@section('leftSide')
    @if (in_array((new \App\Http\Controllers\CacheController())->getSelectedMarketDay()->id, $product->marketDays) &&
            $product->state !== 0)

        <div class="sv-product-single-content">

            <div class="sv-product-single-header">

                <div class="sv-product-description">
                    <div class="sv-product-description--sticky">
                        <div class="help--ratio-1x1">
                            <div class="inner">
                                @if ($product->isBio)
                                    <div class="sv-badge">
                                        {{-- <img src="{{ asset('assets/img/badge-bio-1.svg') }}"> --}}
                                        <img class="fix-size" src="{{ asset('assets/img/bio.png') }}">
                                    </div>
                                @endif
                                <img src="{{ $product->image(config('app.imageSize.product_image.view')) }}"
                                    class="title-image">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sv-product-title">

                    <div class="title">
                        <a href="{{ r('supplierOpen', [getSupplierSlugs(true), __("supplier.slug.{$product->supplier_id}")]) }}"
                            class="farmer">
                            <img loading="lazy"
                                src="{{ $product->supplier()->image(config('app.imageSize.supplier_image.main')) }}">
                            <h3>{{ __('supplier.name.' . $product->supplier_id) }}</h3>
                        </a>

                        <div class="lower-part">
                            <h2>{{ __("product.name.{$product->id}") }}</h2>

                            <div class="breadcrumbs">
                                @foreach ($product->createBreadcrumbs() as $crumb)
                                    <a href="{{ $crumb['url'] }}">{{ $crumb['name'] }}</a>
                                @endforeach
                            </div>

                            {{-- <div class="text">
                                {!! $product->getMeta('description') !!}
                            </div>

                            @if ($ingredients = $product->getMeta('ingredients'))
                                <div class="text mt-2">
                                    {!! $ingredients !!}
                                </div>
                            @endif --}}

                            @if ($nutrition = $product->getMeta('nutrition'))
                                <div class="mt-5">
                                    <span>Uzturvērtība:</span>
                                    {{ $nutrition }}
                                </div>
                            @endif

                            {{-- @if ($expire = $product->getMeta('expire_date'))
                                <div class="sv-exp-date">
                                    <b>{!! _t('translations.expireDate', ['date' => 1]) !!}</b>
                                    {{$expire}}
                                </div>
                            @endif --}}
                        </div>
                    </div>

                    <form action="{{ r('cartAdd') }}" method="post" data-cart-form>
                        {{ csrf_field() }}
                        <input type="hidden" name="product_id" value="{{ $product->id }}" />

                        <div class="product-price-mobile">
                            @if ($product->hasManyPrices())
                                <select name="variation_id" class="selectpicker" title="variation">
                                    @foreach ($product->prices() as $priceID => $price)
                                        <option value="{{ $priceID }}"
                                            {{ $product->isAvailable($priceID, $productAmount) ? '' : 'disabled' }}
                                            {{ $loop->first ? 'selected' : '' }} {!! $product->isSale() ? "data-origprice={$price->oldPrice}&nbsp;€" : '' !!}
                                            @if ($price->has_deposit)
                                                data-appendhtml='<small class="">+&nbsp;depozīts {{ number_format($price->deposit_amount, 2, '.', '') }}&nbsp;€</small>'
                                            @endif
                                        >
                                            {!! implode(' / ', [$price->price . '&nbsp;€', $price->display_name]) !!}</option>
                                    @endforeach
                                </select>
                            @else
                                <input type="hidden" value="{{ $product->prices()->id }}" name="variation_id" />

                                <div class="amount">
                                    @if ($product->isSale())
                                        <s>{{ $product->prices()->oldPrice }}&nbsp;€</s>
                                    @endif
                                    {!! implode(' / ', [$product->prices()->price . '&nbsp;€', $product->prices()->display_name]) !!}

                                    @if ($product->prices()->has_deposit)
                                        <small class=""> +&nbsp;depozīts {{ number_format($product->prices()->deposit_amount, 2, '.', '') }}&nbsp;€ </small>
                                    @endif
                                </div>

                            @endif
                        </div>

                        <div class="text mb-2">
                            {!! $product->getMeta('description') !!}
                        </div>

                        @if ($ingredients = $product->getMeta('ingredients'))
                            <div class="text mb-2">
                                {!! $ingredients !!}
                            </div>
                        @endif

                        @if ($expire = $product->getMeta('expire_date'))
                            <div class="text mb-2">
                                <span>{!! _t('translations.expireDate', ['date' => 1]) !!}</span>
                                {{ $expire }}
                            </div>
                        @endif

                        <div class="product-price-desktop">
                            @if ($product->hasManyPrices())
                                <select name="variation_id" class="selectpicker" title="variation">
                                    @foreach ($product->prices() as $priceID => $price)
                                        <option value="{{ $priceID }}"
                                            {{ $product->isAvailable($priceID, $productAmount) ? '' : 'disabled' }}
                                            {{ $loop->first ? 'selected' : '' }}
                                            {!! $product->isSale() ? "data-origprice={$price->oldPrice}&nbsp;€" : '' !!}
                                            @if ($price->has_deposit)
                                                data-appendhtml='<small class="">+&nbsp;depozīts {{ number_format($price->deposit_amount, 2, '.', '') }}&nbsp;€</small>'
                                            @endif
                                        >
                                            {!! implode(' / ', [$price->price . '&nbsp;€', $price->display_name]) !!}
                                        </option>
                                    @endforeach
                                </select>
                            @else
                                <input type="hidden" value="{{ $product->prices()->id }}" name="variation_id" />

                                <div class="amount">
                                    @if ($product->isSale())
                                        <s>{{ $product->prices()->oldPrice }}&nbsp;€</s>
                                    @endif
                                    {!! implode(' / ', [$product->prices()->price . '&nbsp;€', $product->prices()->display_name]) !!}
                                    @if ($product->prices()->has_deposit)
                                        <small class=""> +&nbsp;depozīts {{ number_format($product->prices()->deposit_amount, 2, '.', '') }}&nbsp;€ </small>
                                    @endif
                                </div>
                            @endif

                        </div>

                        <div class="cart-form-flex-wrapper mt-2">

                            <div class="input-wrapper quantity">
                                <span class="button minus disabled"></span>
                                <span class="button add"></span>
                                <input type="text" name="amount" value="1" class="qty" />
                            </div>

                            @php
                                $isDisabled = !in_array((new \App\Http\Controllers\CacheController())->getSelectedMarketDay()->id, $product->marketDays) || $product->state == 0 ? true : false;
                            @endphp

                            {{-- <button class="sv-btn add-to-cart-btn {{ in_array((new \App\Http\Controllers\CacheController)->getSelectedMarketDay()->id,$product->marketDays)?"":"is-disabled" }}"> --}}
                            <button class="sv-btn add-to-cart-btn {{ $isDisabled ? 'is-disabled' : '' }}">
                                {!! _t('translations.addToCart') !!}
                            </button>
                        </div>
                    </form>

                    {{-- <newsletter-subscriber-modal v-if="showNewsletterModal" id="nl-modal"></newsletter-subscriber-modal> --}}

                    @if ($user = Auth::user())
                        <div class="sv-product--favourite-group">
                            {{-- <form action="{{ r("rateProduct") }}" method="post" data-rate-form>
                                <div class="sv-product--rating">
                                    <span>
                                        {!! _t('translations.rate') !!}
                                    </span>
                                    <div class="rate">
                                        <input type="radio" id="star5" name="rating" value="5" {{$product->rating == 5 ? 'checked' : ''}}/>
                                        <label for="star5" title="5"></label>

                                        <input type="radio" id="star4" name="rating" value="4" {{$product->rating == 4 ? 'checked' : ''}}/>
                                        <label for="star4" title="4"></label>

                                        <input type="radio" id="star3" name="rating" value="3" {{$product->rating == 3 ? 'checked' : ''}}/>
                                        <label for="star3" title="3"></label>

                                        <input type="radio" id="star2" name="rating" value="2" {{$product->rating == 2 ? 'checked' : ''}}/>
                                        <label for="star2" title="2"></label>

                                        <input type="radio" id="star1" name="rating" value="1" {{$product->rating == 1 ? 'checked' : ''}}/>
                                        <label for="star1" title="1"></label>
                                    </div>
                                </div>
                                {{ csrf_field() }}
                                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                            </form> --}}

                            <form action="{{ r("favAdd") }}" method="post" data-fav-form class="btn-row btn-row--fill">
                                @if ($user->isProductFavourited($product->id))
                                    <button class="btn btn-outlined">
                                        {!! _t('translations.removeFromFav') !!}
                                    </button>
                                @else
                                    <button class="btn btn-outlined">
                                        {!! _t('translations.addToFav') !!}
                                    </button>
                                @endif

                                {{ csrf_field() }}
                                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                            </form>


                        </div>
                    @endif
                </div>

            </div>

            <div class="sv-blank-spacer medium"></div>

            <div class="sv-marketday-access">
                <h3>{!! _t('translations.productMarketDaysAvailableHeader') !!}</h3>
                <p>
                    {!! _t('translations.productMarketDaysAvailableText') !!}
                </p>
                <div class="sv-blank-spacer medium"></div>
                <div class="days">
                    @foreach ($cache->getClosestMarketDayList() as $marketDay)
                        <div class="sv-day {{ in_array($marketDay->id, $product->marketDays) ? '' : 'is-disabled' }}">
                            <div>
                                <div class="nr">
                                    <span>{{ $marketDay->date->format('d') }}</span>
                                    <span>{{ ucfirst(__('translations.' . $marketDay->date->format('F'))) }}</span>
                                </div>
                                <div class="name">
                                    {{ $marketDay->name }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="sv-blank-spacer medium"></div>

            <div class="sv-farmer-about">
                <a href="{{ r('supplierOpen', [getSupplierSlugs(true), __("supplier.slug.{$product->supplier_id}")]) }}"
                    class="farmer">
                    <img loading="lazy"
                        src="{{ $product->supplier()->image(config('app.imageSize.supplier_image.main')) }}">
                    <h3>{{ __('supplier.name.' . $product->supplier_id) }}</h3>
                    <h4>{{ ___('supplier.location.' . $product->supplier_id) }}</h4>
                </a>
                {!! $product->supplier()->getMeta('description') !!}
            </div>

            @if (count($product->getOtherProducts($product->id)))
                <div class="sv-blank-spacer medium"></div>
                <div class="sv-title">
                    <h3>{!! _t('translations.otherProducts') !!}</h3>
                </div>

                <div class="sv-blank-spacer medium"></div>

                <div class="sv-linked-products-slider">
                    <div class="owl-carousel">
                        @foreach ($product->getOtherProducts($product->id) as $otherProduct)
                            @php
                                $item = $cache->getProduct($otherProduct);
                            @endphp
                            @include('Products::frontend.listitem')
                        @endforeach
                    </div>
                </div>
            @endif

            @if (count($blogPosts = \App\Plugins\Products\Model\Product::find($product->id)->blogPosts) > 0)
                <div class="sv-blank-spacer medium"></div>
                <div class="sv-title">
                    <h3>{!! _t('translations.linked_blogposts') !!}</h3>
                </div>
                <div class="sv-blank-spacer medium"></div>
                <div class="sv-blog-list-slider">
                    <div class="owl-carousel">
                        @foreach ($blogPosts as $post)
                            <div class="item">
                                <a href="{{r('blog', [__('postcategory.slug.' . $post->main_category), __('posts.slug.' . $post->id)])}}"
                                    class="link">
                                    <div class="image help--overimage"><img loading="lazy"
                                            src="{{ $post->getImageByKey('blog_picture') }}"
                                            alt="{{ __('posts.name.' . $post->id) }}" class="image">
                                        <div class="overimage">
                                            <p>Lasīt vairāk</p>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h4>{{ __('posts.name.' . $post->id) }}</h4>
                                        {{-- <p>Par mums runā</p> --}}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    @else
        {!! _t('translations.nothinghasbeenfound') !!}
    @endif
@endsection
