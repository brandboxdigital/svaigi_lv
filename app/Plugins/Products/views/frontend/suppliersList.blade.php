@extends('layouts.app')


@section('content')


    <form class="sv-sidebar-search is-mobile" method="get" action="{{ r('url', ['search']) }}">
        <input type="text" name="search" value="{{ request()->get('search') }}" title="search">
        <input type="submit" value="{{ _t('translations.search') }}">
    </form>

    <div class="sv-category-wrapper">
        <div class="container">
            <div class="row mt-2">
                <div class="back-to-supplier">
                    {{-- <a class="sv-back-link" href="{{ $backLink }}"> --}}
                    <a class="sv-back-link" href="/saimniecibas-no-visas-latvijas">
                        Atpakaļ uz saimniecību sarakstu
                    </a>
                    {{-- <form class="sv-sidebar-search" method="get" action="{{ r('url', ['search']) }}">
                        <input type="text" name="search" value="{{request()->get('search')}}" title="search" />
                        <input type="submit" value="{{ _t('translations.search') }}" />
                    </form> --}}

                </div>


                <div class="sv-products">
                    @if(!($hideHeader??false && $category))
                        <div class="sv-category-title-banner">
                            <div class="title">
                                <h1>{{ __("supplier.name.{$supplier->id}") }}</h1>
                                {{-- <p style="margin-bottom: 1rem;">Visi produkti no saimniecības: </p> --}}
                            </div>
                        </div>
                    @endif

                    @if(count($products??[]) > 0)

                        @foreach($products as $itemId => $itemAmount)
                            @php
                                if (empty($itemId)) {
                                    continue;
                                }

                                /** @var \App\Cache\ProductCache $item */
                                $item = $cache->getProduct($itemId);
                                if((!$item->hasManyPrices() && ($itemAmount<1 && !is_null($itemAmount))) || ($item->hasManyPrices() && (!is_null($itemAmount) && $itemAmount<$item->lowestAmount()))) {
                                    continue;
                                }
                            @endphp
                            @include('Products::frontend.listitem')
                        @endforeach

                        @if ($pagination && $pagination->hasPages())
                            <div class="sv-pagination hidden-xs">
                                {{$pagination->onEachSide(5)->render() }}
                            </div>
                            <div class="sv-pagination visible-xs">
                                {{$pagination->onEachSide(1)->render() }}
                            </div>
                        @endif
                    @else
                        <div class="" style="padding: 0 2rem 3rem;">
                            {!! _t('translations.nothinghasbeenfound') !!}.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
