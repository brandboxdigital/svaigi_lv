@extends('Categories::frontend.list')

@section('leftSide')
    <div class="infinite-page">
        @foreach($products as $itemId => $itemAmount)
            @php
                if (empty($itemId)) {
                    continue;
                }

                /** @var \App\Cache\ProductCache $item */
                $item = $cache->getProduct($itemId);
                if((!$item->hasManyPrices() && ($itemAmount<1 && !is_null($itemAmount))) || ($item->hasManyPrices() && (!is_null($itemAmount) && $itemAmount<$item->lowestAmount()))) {
                    continue;
                }
            @endphp
            @include('Products::frontend.listitem')
        @endforeach
        {{-- <newsletter-subscriber-modal v-if="showNewsletterModal" id="nl-modal"></newsletter-subscriber-modal> --}}
    </div>

    <div class="infinite-page-status">
        <div class="inner infinite-scroll-request" style="display: none">
            <div class="inner">
                <div class="loading-ring is-sm"></div>
            </div>
        </div>
        <div class="inner infinite-scroll-last" style="display: none">
            <div class="inner">
                <div class="btn-row btn-row--center">
                    <button class="btn btn-green" data-scroll-top>Uz lapas sākumu <div class="arrow arrow-back is-small is-vertical is-white is-in-button-left"></div></button>
                </div>
            </div>
        </div>
    </div>
    {{-- <button class="load-next-button sv-btn" style="margin-left: 15px">Ielādēt vēl</button> --}}
    @if ($pagination && $pagination->hasPages())

        <div class="sv-pagination hidden-xs">
            {{$pagination->onEachSide(5)->render() }}
        </div>

        {{-- <div class="sv-pagination visible-xs">
            {{$pagination->onEachSide(1)->render() }}
        </div> --}}

    @endif

@endsection

@section('wrapper')
    sv-category-wrapper
@endsection
