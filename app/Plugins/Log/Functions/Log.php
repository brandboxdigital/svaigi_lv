<?php

namespace App\Plugins\Log\Functions;


use App\Schedules;

trait Log
{

    public function getList()
    {
        return [
            ['field' => 'created_at', 'label' => 'Job initiated', 'type' => 'tooltip', 'tooltip'=>'id' ],
            ['field' => 'user_name',   'label' => 'Created By'],
            ['field' => 'type_name',  'label' => 'Job'],
            ['field' => 'is_running_partial', 'label' => 'Is Running'],
            ['field' => 'result_state_switch', 'label' => 'State'],
            ['field' => 'result_message', 'type' => 'tooltip', 'label' => 'Message'],
        ];
    }

    public function getLog($types) {
        $schedule = new Schedules();
        if(!$types) {
            return $schedule->paginate(20);
        }

        return $schedule
            ->whereIn('type', explode(",", $types))
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
    }

    public function getEditName() {
        return "All Logs";
    }
}
