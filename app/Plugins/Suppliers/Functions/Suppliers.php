<?php
/**
 * Created by PhpStorm.
 * User: ya
 * Date: 1/24/19
 * Time: 10:22 AM
 */

namespace App\Plugins\Suppliers\Functions;

use App\User;

trait Suppliers
{
    public function form()
    {
        $languages = languages()->pluck('name', 'code');
        $users = User::all()->map(function($user) {
            $user->name = $user->name . ' ' . $user->last_name . ' | ' . $user->email;
            return $user;
        });

        return [
            [
                'Label'     => 'Display',
                'languages' => $languages,
                'data'      => [
                    'name'        => ['type' => 'text', 'label' => 'Supplier Name', 'meta' => true, 'class' => 'slugify'],
                    'slug'        => ['type' => 'text', 'label' => 'Supplier Slug', 'meta' => true, 'class' => 'slug', 'readonly' => 'readonly'],
                    'jur_name'    => ['type' => 'text', 'label' => 'Supplier Legal Name', 'meta' => true],
                    'location'    => ['type' => 'text', 'label' => 'Location', 'meta' => true, 'comment' => "Lokāciajs nosaukums, piem., `Sigulda`"],
                    'excerpt'     => ['type' => 'textarea', 'label' => 'Excerpt',     'meta' => true, 'comment' => "Īss aprraksts, kas parādās kartiņā"],
                    'description' => ['type' => 'textarea', 'label' => 'Description', 'meta' => true, 'comment' => "Pilns aprraksts, kas parādās atvērumā"],
                ],
            ],
            [
                'Label'     => 'Google SEO',
                'languages' => $languages,
                'data'      => [
                    'google_keywords'    => ['type' => 'text', 'class' => '', 'label' => 'Keywords', 'meta' => true],
                    'google_description' => ['type' => 'textarea', 'class' => '', 'label' => 'Google Description', 'meta' => true],
                ],
            ],
            [
                'Label' => 'Parameters',
                'data'  => [
                    'custom_id'      => ['type' => 'text', 'label' => 'Supplier ID'],
                    'email'          => ['type' => 'text', 'label' => 'E-mail'],
                    'coords'         => ['type' => 'text', 'label' => 'Google Maps Coordinates', "comment" => 'for example: `57.034947660600096, 26.45124003949112`'],
                    //'farmer'         => ['type' => 'switch', 'label' => 'Farmer'],
                    //'craftsman'      => ['type' => 'switch', 'label' => 'Craftsman'],
                    // 'featured'       => ['type' => 'switch', 'label' => 'Featured'],
                    'supplier_image' => ['type' => 'image', 'preview' => true, 'label' => 'Supplier image'],
                ],
            ],
            [
                'Label' => 'Login User',
                'data'  => [
                    'user_id'        => ['type' => 'select', 'label' => 'User', 'options' => $users],
                ],
            ],
        ];
    }

    public function getList()
    {
        return [
            ['field' => 'name', 'label' => 'Supplier Name', 'translate' => 'supplier.name', 'key' => 'id'],
            ['field' => 'user_name', 'label' => 'Linked User'],
            ['field' => 'buttons', 'buttons' => ['edit', 'delete'], 'label' => '',],
        ];
    }
}
