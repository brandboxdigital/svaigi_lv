<?php namespace App\Plugins\Suppliers\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SupplierRating extends Model
{
    public $fillable = [
        'rating','supplier_id','user_id','comment'
    ];

    public function supplier() {
        return $this->belongsTo(Product::class);
    }
}
