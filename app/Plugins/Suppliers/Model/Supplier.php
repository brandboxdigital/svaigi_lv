<?php

namespace App\Plugins\Suppliers\Model;

use App\User;
use App\BaseModel;
use App\Plugins\Admin\Model\File;
use App\Plugins\Blog\Model\Blog;
use App\Plugins\Products\Model\Product;

class Supplier extends BaseModel
{
    public $fillable = [
        'custom_id',
        'email',
        'location',
        'coords',
        'farmer',
        'craftsman',
        'featured',
        'user_id',
    ];

    public $metaClass = __NAMESPACE__ . '\SupplierMeta';

    public $appends = ['meta', 'image', 'url', 'name', 'productsUrl', 'slug'];

    public function getNameAttribute()
    {
        if (!$this->id) return;

        return __('supplier.name.' . $this->id);
    }

    public function getSlugAttribute()
    {
        if (!$this->id) return;

        return __('supplier.slug.' . $this->id);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }

    public function images() {
        return $this->hasMany(File::class, 'owner_id');
    }

    public function getCoordsArrayAttribute() {
        return explode(",",$this->coords);
    }

    public function blogPosts() {
        return $this->belongsToMany(Blog::class);
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getUserNameAttribute()
    {
        if ($this->user) {
            return $this->user->name . ' ' . $this->user->last_name . ' | ' . $this->user->email;
        }

        return null;
    }

    // public function getMetaAttribute()
    // {
    //     return $this->metaData()->get()->toArray();
    // }

    public function getProductsUrlAttribute()
    {
        return r('supplier-products', ['supplier' => __('supplier.slug.' . $this->id)]);
    }

    public function getImageAttribute()
    {
        $path = config("app.uploadFile.supplier_image","temp");
        $image = File::where(['owner' => 'supplier_image', 'owner_id' => $this->id])->first();
        if (null !== $image) {
            $image->toArray();
            $image['dir_path'] = "/$path/700x/";
        } else {
            $image['dir_path'] = '/';
        }

        return $image;
    }

    public function getUrlAttribute()
    {
        return r('supplierOpen', [getSupplierSlugs(true),__("supplier.slug.{$this->id}")]);
    }
}
