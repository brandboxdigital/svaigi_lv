<?php

namespace App\Plugins\Blog\Functions;


use App\Plugins\Blog\Model\BlogCategories;
use App\Plugins\Products\Model\Product;
use App\Plugins\Suppliers\Model\Supplier;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

trait Blog
{

    public function getList($type)
    {
        if ($type == 'posts') {
            return [
                ['field' => 'id', 'translate' => 'posts.name', 'label' => 'Post Title', 'key' => 'id'],
                ['field' => 'is_highlighted', 'label' => 'Is Highlighted', 'type' => 'yesno'],
                ['field' => 'buttons', 'buttons' => ['edit', 'delete'], 'label' => ''],
            ];
        }

        return [
            ['field' => 'id',   'label' => "ID", ],
            ['field' => 'name', 'translate' => 'postcategory.name', 'label' => 'Post Category Name', 'key' => 'id'],
            ['field' => 'sort_order',   'label' => "SortOrder", ],
            ['field' => 'parent_id',   'label' => "Parent ID", ],
            ['field' => 'buttons', 'buttons' => ['edit', 'delete'], 'label' => ''],
        ];
    }

    public function getData($type, $search = false)
    {
        if ($type == 'posts') {
            $posts = new \App\Plugins\Blog\Model\Blog();
            if ($search) {
                $posts = $posts->whereHas('metaData', function (Builder $q) use($search) {
                    $q->whereIn('meta_key', ['name'])
                        ->where('meta_value', 'like', '%' . $search . '%');
                });
            }

            return $posts->filters()->paginate(20);
        }

        $categories = new BlogCategories();

        if ($search) {
            $categories = $categories->whereHas('metaData', function (Builder $q) use($search) {
                $q->whereIn('meta_key', ['name'])
                    ->where('meta_value', 'like', '%' . $search . '%');
            });
        }

        return $categories->paginate(20);
    }

    public function form($type)
    {
        $languages = languages()->pluck('name', 'code');

        if ($type == 'posts') {
            return [
                [
                    'Label'     => 'Display',
                    'languages' => $languages,
                    'data'      => [
                        'name'    => ['type' => 'text', 'meta' => true, 'label' => 'Post Name', 'class' => 'slugify'],
                        'slug'    => ['type' => 'text', 'meta' => true, 'label' => 'Post Slug', 'class' => 'slug', 'readonly' => 'readonly'],
                        'excerpt' => ['type' => 'textarea', 'meta' => true, 'label' => 'Post Excerpt'],
                        'content' => ['type' => 'textarea', 'meta' => true, 'label' => 'Post Content'],
                    ],
                ],
                [
                    'Label'     => 'Google SEO',
                    'languages' => $languages,
                    'data'      => [
                        'google_keywords'    => ['type' => 'text', 'class' => '', 'label' => 'Keywords', 'meta' => true],
                        'google_description' => ['type' => 'textarea', 'class' => '', 'label' => 'Google Description', 'meta' => true],
                    ],
                ],
                [
                    'Label' => 'Data',
                    'data'  => [
                        'main_category'    => ['type' => 'select', 'options' => BlogCategories::all(), 'label' => 'Main Catetgory'],
                        'extra_categories' => ['type' => 'chosen', 'options' => BlogCategories::all(), 'label' => 'Extra Categories'],
                        'is_highlighted'   => ['type' => 'switch', 'label' => 'Is Highlighted in main page'],
                        'blog_picture'     => ['type' => 'image',  'preview' => true, 'label' => 'Post Image', 'comment' => 'Size: 600x600px'],
                        'linked_supplier'  => ['type' => 'select', 'label' => 'Linked Supplier(-s)', 'options' => Supplier::all(), 'multiple' => 'multiple', 'dataAttr' => ['live-search' => 'true', 'width' => 'fit', "selected-text-format"=>"count>5"]],
                        'linked_products'  => ['type' => 'select', 'label' => 'Linked Product(-s)', 'options' => Product::all(), 'multiple' => 'multiple', 'dataAttr' => ['live-search' => 'true', 'width' => 'fit', "selected-text-format"=>"count>5"]],
                    ],
                ],
                [
                    'Label' => 'If has video',
                    'data'  => [
                        'youtube_id'         => ['type' => 'text', 'class' => '', 'label' => 'Youtube link/id'],
                        'youtube_embed_html' => ['type' => 'text', 'class' => '', 'label' => 'Youtube link/id', 'readonly' => 'readonly'],
                    ],
                ],
                [
                    'Label' => 'INFO',
                    'data'  => [
                        'full_blog_url' => ['type' => 'text', 'label' => 'Full Blog URL',  'readonly' => 'readonly'],
                    ],
                ],
            ];
        }

        return [
            [
                'Label'     => 'Display',
                'languages' => $languages,
                'data'      => [
                    'name' => ['type' => 'text', 'meta' => true, 'label' => 'Category Name', 'class' => 'slugify'],
                    'slug' => ['type' => 'text', 'meta' => true, 'label' => 'Category Slug', 'class' => 'slug', 'readonly' => 'readonly'],
                    'view_template' => [
                        'type' => 'select',
                        'class' => 'show-tick',
                        'options' => $this->getCategoryTemplateOptions(),
                        'label' => 'Blog category page template'
                    ],
                    'parent_id' => [
                        'type' => 'select',
                        'class' => 'show-tick',
                        'options' => \App\Plugins\Blog\Model\BlogCategories::all(),
                        'label' => 'Parent Category',
                        'dataAttr' => [
                            'live-search' => "true",
                            'size' => '5'
                        ]
                    ],
                    'exclude_category_from_main_list' => ['type' => 'switch', 'label' => 'Exclude this category from main blogposts'],
                ],
            ],
            [
                'Label'     => 'Other',
                'data'      => [
                    'sort_order' => ['type' => 'text', 'meta' => false, 'label' => 'Sort Order'],
                ],
            ],
        ];
    }

    public function getFilters()
    {

        // $marketDays = MarketDay::all()->pluck('marketDay', 'id')->toArray();
        // $market_days = [];

        // foreach ($marketDays as $mdid => $md) {
        //     $market_days[$mdid] = $md[language()];
        // }

        return [
            // ['type' => 'select', 'name' => 'market_day', 'label' => 'Market Day', 'options' => $market_days],
            // ['type' => 'select', 'name' => 'trashed', 'label' => 'Trashed', 'options' => ['No', 'Yes']],
            // ['type' => 'select', 'name' => 'state', 'label' => 'Order State', 'options' => ['ordered' => 'New', 'finished' => 'Finished']],
            // ['type' => 'text', 'name' => 'ordered_at', 'label' => 'Ordered at range', 'class' => 'daterangepicker']
            ['type' => 'select', 'name' => 'is_highlighted', 'label' => 'Is Highlighted', 'options' => ['No', 'Yes']]
        ];
    }

    private function getCategoryTemplateOptions()
    {
        try {
            $fileList = scandir(base_path('resources/views/frontend/pages/blog_templates/'));
        } catch (\Throwable $th) {
            return [];
        }

        return collect($fileList)
            ->filter(function($file) {
                return Str::endsWith($file, '.blade.php');
            })
            ->map(function($file) {
                $templateName = \str_replace('.blade.php', '', $file);

                return (object) [
                    'id'   => $templateName,
                    'name' => $templateName,
                    // 'name' => Str::title($templateName),
                    'file' => $file,
                ];
            });
    }
}
