<?php

namespace App\Plugins\Blog\Functions;


use App\Plugins\Blog\Model\BlogCategories;
use App\Plugins\Blog\Model\Gallery;
use App\Plugins\Products\Model\Product;
use App\Plugins\Suppliers\Model\Supplier;
use Illuminate\Database\Eloquent\Builder;

trait Galleries
{

    public function getList()
    {
        return [
            ['field' => 'id',  'label' => 'ID' ],
            ['field' => 'title',  'label' => 'Title' ],
            // ['field' => 'buttons', 'buttons' => ['edit', 'delete'], 'label' => ''],
            ['field' => 'buttons', 'buttons' => ['edit'], 'label' => ''],
        ];
    }

    public function getData($search = false)
    {
        // if ($type == 'posts') {
        //     $posts = new \App\Plugins\Blog\Model\Blog();
        //     if ($search) {
        //         $posts = $posts->whereHas('metaData', function (Builder $q) use($search) {
        //             $q->whereIn('meta_key', ['name'])
        //                 ->where('meta_value', 'like', '%' . $search . '%');
        //         });
        //     }

        //     return $posts->filters()->paginate(20);
        // }

        // $categories = new BlogCategories();

        // if ($search) {
        //     $categories = $categories->whereHas('metaData', function (Builder $q) use($search) {
        //         $q->whereIn('meta_key', ['name'])
        //             ->where('meta_value', 'like', '%' . $search . '%');
        //     });
        // }

        return $galleries = Gallery::all();

        // return $galleries->paginate(20);
    }

    public function form()
    {
        $languages = languages()->pluck('name', 'code');

        return [
            [
                'Label'     => 'Display',
                'data'      => [
                    'title'    => ['type' => 'text', 'label' => 'Title'],
                ],
            ],
            [
                'Label' => 'Images',
                'data'  => [
                    'gallery_images'     => ['type' => 'image', 'preview' => true, 'label' => 'Gallery Images'],
                ],
            ],
        ];
    }

    public function getFilters()
    {

        // $marketDays = MarketDay::all()->pluck('marketDay', 'id')->toArray();
        // $market_days = [];

        // foreach ($marketDays as $mdid => $md) {
        //     $market_days[$mdid] = $md[language()];
        // }

        return [
            // ['type' => 'select', 'name' => 'market_day', 'label' => 'Market Day', 'options' => $market_days],
            // ['type' => 'select', 'name' => 'trashed', 'label' => 'Trashed', 'options' => ['No', 'Yes']],
            // ['type' => 'select', 'name' => 'state', 'label' => 'Order State', 'options' => ['ordered' => 'New', 'finished' => 'Finished']],
            // ['type' => 'text', 'name' => 'ordered_at', 'label' => 'Ordered at range', 'class' => 'daterangepicker']
            // ['type' => 'select', 'name' => 'is_highlighted', 'label' => 'Is Highlighted', 'options' => ['No', 'Yes']]
        ];
    }
}
