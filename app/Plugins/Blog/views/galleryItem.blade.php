@if ($gallery)
    <div class="blog-gallery-fullpage-slider">
        <div class="owl-carousel">
            @foreach ($gallery->gallery_images ?? [] as $image)
                <div class="blog-gallery-fullpage-slider--item">
                    <img src="{{ $gallery->getImageById($image->id, 'default') }}" alt="" class="image">
                </div>
            @endforeach
        </div>
    </div>
@endif
