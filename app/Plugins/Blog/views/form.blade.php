@php
    $title              = 'Piesakies jaunumiem!';
    $text               = 'Par saimniekošanu dārzā, mājās un virtuvē - iepazīstinot bērnus ar dzīvei noderīgām prasmēm, kas paveicamas pašu rokām.';
    $email              = 'E-pasta adrese';
    $nameAndSurname     = 'Vārds un uzvārds';
    $name               = 'Vārds';
    $surname            = 'Uzvārds';
    $phone              = 'Tālruņa numurs';
    $confirmationText   = 'Saņem jaunumu ziņas par Svaigi.lv saimes darbiem, aktuāliem notikumiem un vērtīgiem piedāvājumiem.';
    $btnText            = 'Pieteikties';
    $isMeistarklaseCat  = false;

    if ($cat == 'meistarklases') {
        $isMeistarklaseCat = true;
        $title = 'Piesakies meistarklasei!';
    }
@endphp

<div class="sv-form {{ $isMeistarklaseCat ? 'meistarklase-form' : '' }}" style="background-image: url({{ $isMeistarklaseCat ? '../../../images/rediss-orange.jpg' : '../../../images/rediss.png' }});">
    <div class="container">
        <div class="row">
            <div class="sv-form__text col-xs-12 col-md-6">
                <div class="sv-form__text__inner">
                    <h3 class="sv-form__text__title">{{ $title }}</h3>
                    <p class="sv-form__text__text">{{ $text }}</p>
                </div>
            </div>
            <div class="sv-form__form col-xs-12 col-md-6">
                <form class="sv-form__form__inner" method="POST" action="{{ route('api.subscribe-blog-post') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email">{{ $email }}</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="">
                    </div>

                    <!-- Second Input -->
                    @if ($isMeistarklaseCat)
                        <input type="hidden" name="type" value="masterclass">
                        <div class="form-group">
                            <label for="phone">{{ $phone }}</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="">
                        </div>
                        <div class="form-name-and-surname-row row">
                            <div class="form-name-col col-lg-6">
                                <div class="form-group">
                                    <label for="name">{{ $name }}</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="">
                                </div>
                            </div>
                            <div class="form-surname-col col-lg-6">
                                <div class="form-group">
                                    <label for="surname">{{ $surname }}</label>
                                    <input type="text" class="form-control" id="surname" name="surname" placeholder="">
                                </div>
                            </div>
                        </div>
                    @else
                        <input type="hidden" name="type" value="newsletter">
                        <div class="form-group">
                            <label for="nameAndSurname">{{ $nameAndSurname }}</label>
                            <input type="text" class="form-control" id="nameAndSurname" name="name" placeholder="">
                        </div>
                    @endif
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="agreeCheck" name="agree">
                        <label class="form-check-label" for="agreeCheck">{{ $confirmationText }}</label>
                    </div>
                    <!-- Button -->
                    <button type="submit" class="btn btn-purple">{{ $btnText }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
