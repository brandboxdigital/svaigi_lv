<?php

namespace App\Plugins\Blog;


use App\Functions\General;
use Illuminate\Http\Request;
use App\Plugins\Blog\Model\Blog;
use Illuminate\Support\Facades\DB;
use App\Plugins\Blog\Model\Gallery;
use Illuminate\Support\Facades\Route;
use App\Plugins\Admin\AdminController;
use App\Plugins\Blog\Model\BlogCategories;
use App\Plugins\Blog\Functions\Galleries as GalleriesFunctions;

class GalleriesController extends AdminController
{
    private $list;
    private $editId;

    use GalleriesFunctions;
    use General;

    public function index()
    {
        $list = $this->list;
        $search = request()->route('search') ?? false;

        $cr = explode(".", Route::currentRouteName());

        // if (!$search && ($cr[1] ?? false) == 'search') {
        //     return redirect()->route($cr[0] . ".list");
        // }

        return view('admin.elements.table',
            [
                'tableHeaders' => $this->getList($list),
                'header'       => "Galleries",
                // 'filters'      => method_exists($this, 'getFilters') ? $this->getFilters() : [],
                // 'currentFilters' => session('order_filters'),
                'list'         => $this->getData(),
                'idField'      => 'id',
                'destroyName'  => 'Gallery',
            ]);
    }

    public function view($id)
    {
        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => Gallery::findOrFail($id)]);
    }

    public function add()
    {
        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => new Gallery()]);
    }

    public function store(Request $request, $id = false)
    {
        $route = "galleries";

        try {

            DB::beginTransaction();
                $gallery = Gallery::updateOrCreate(['id' => $id],
                    [
                        'title'  => request('title'),
                    ]);

                $this->handleImages($gallery);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            session()->flash("message", ['msg' => $e->getMessage(), 'isError' => true]);

            return redirect()->back()->with(request()->all());
        }

        return redirect(route($route))->with(['message' => ['msg' => "Gallery Saved"]]);
    }

    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);

        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => $gallery]);
    }

    public function destroy($id)
    {
        $result = Gallery::findOrFail($id)->delete();

        $result = ['status' => $result, "message" => $result ? "Post Deleted" : "Error Deleting Post"];

        return $result;
    }
}
