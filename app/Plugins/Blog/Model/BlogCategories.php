<?php

namespace App\Plugins\Blog\Model;

use Kalnoy\Nestedset\NodeTrait;
use App\BaseModel;

class BlogCategories extends BaseModel
{
    use NodeTrait;

    public $fillable = ['id', 'view_template', 'parent_id', 'exclude_category_from_main_list', 'sort_order'];

    public $metaClass = __NAMESPACE__."\BlogCategoriesMeta";

    public function posts() {
        return $this->belongsToMany(Blog::class);
    }

    public function mainPost() {
        return $this->hasMany(Blog::class, 'main_category','id');
    }

    // public function nodes() {
    //     return $this->hasMany(Category::class, 'parent_id');
    // }


    public function getNameAttribute() {
        return $this->meta['name'][language()];
    }

}
