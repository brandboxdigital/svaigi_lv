<?php

use App\Model\Admin\Menu;
use App\Plugins\Blog\Model\BlogCategories;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowSubcategyField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::table('blog_categories', function (Blueprint $table) {
            $table->string('view_template')->nullable();

            $table->nestedSet();
        });

        BlogCategories::fixTree();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->tryToDrop('blog_categories', 'show_subcategories');
        $this->tryToDrop('blog_categories', 'view_template');

        try {
            Schema::table('blog_categories', function (Blueprint $table) {
                $table->dropNestedSet();
            });
        } catch (\Throwable $th) {}
    }

    private function tryToDrop($tableName, $colName)
    {
        try {
            Schema::table($tableName, function (Blueprint $table) use ($colName) {
                $table->dropColumn($colName);
            });
        } catch (\Throwable $th) {
            // dump($th->getMessage());
        }
    }
}
