<?php

use App\Model\Admin\Menu;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();

            $table->timestamps();
        });

        $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');

        $routeName = 'galleries';
        Menu::where('routeName', 'like', "$routeName%")
            ->get()
            ->each(function($menu) {
                try {
                    $menu->delete();
                } catch (\Throwable $th) {}
            });
    }

    public function seed()
    {
        $routeName = 'galleries';
        $menu      = Menu::where('slug', 'blog')->first();
        $lastSeq   = Menu::where('parent_id', $menu->id)->orderBy('sequence', 'DESC')->first()->sequence ?? 0;

        $lastSeq++;

        $menu = Menu::updateOrCreate(
            [
                'slug'      => $routeName,
                'routeName' => $routeName,
            ],
            [
                'icon'        => 'fas fa-chart-line',
                'displayName' => 'Galleries List',
                'action'      => '\App\Plugins\Blog\GalleriesController@index',
                'inMenu'      => '1',
                'sequence'    => $lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        $lastSeq = Menu::where('parent_id', $menu->id)->orderBy('sequence', 'DESC')->first()->sequence ?? 0;
        $lastSeq++;

        Menu::updateOrCreate(
            [
                'slug'      => "view/{id}",
                'routeName' => $routeName.".view",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'View',
                'action'      => '\App\Plugins\Blog\GalleriesController@view',
                'inMenu'      => 0,
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);
        Menu::updateOrCreate(
            [
                'slug'      => "edit/{id}",
                'routeName' => $routeName.".edit",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'Edit',
                'action'      => '\App\Plugins\Blog\GalleriesController@edit',
                'inMenu'      => 0,
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        Menu::updateOrCreate(
            [
                'slug'      => "store/{id?}",
                'routeName' => $routeName.".store",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'View',
                'action'      => '\App\Plugins\Blog\GalleriesController@store',
                'inMenu'      => 0,
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'POST',
            ]);

        Menu::updateOrCreate(
            [
                'slug'      => "add",
                'routeName' => $routeName.".add",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'View',
                'action'      => '\App\Plugins\Blog\GalleriesController@add',
                'inMenu'      => 0,
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        // Menu::updateOrCreate(
        //     [
        //         'slug'      => "refresh-from-file",
        //         'routeName' => $main.".refresh-from-file",
        //     ],
        //     [
        //         'icon'        => 'fas fa-calendar-times',
        //         'displayName' => 'Run',
        //         'action'      => '\App\Plugins\Reports\ReportsController@refreshFromFile',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $menu->id,
        //         'method'      => 'GET',
        //     ]);

        // Menu::updateOrCreate(
        //     [
        //         'slug'      => "edit/{id}",
        //         'routeName' => $main.".edit",
        //     ],
        //     [
        //         'icon'        => 'fas fa-edit',
        //         'displayName' => 'Edit',
        //         'action'      => '\App\Plugins\Vat\VatController@edit',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'GET',
        //     ]);

        // Menu::updateOrCreate(
        //     [
        //         'slug'      => "destroy/{id}",
        //         'routeName' => $main.".destroy",
        //     ],
        //     [
        //         'icon'        => 'far fa-window-close',
        //         'displayName' => 'Delete',
        //         'action'      => '\App\Plugins\Vat\VatController@delete',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'POST',
        //     ]);


        // Menu::updateOrCreate(
        //     [
        //         'slug'      => "store/{id?}",
        //         'routeName' => $main.".store",
        //     ],
        //     [
        //         'icon'        => 'far fa-window-close',
        //         'displayName' => 'Save',
        //         'action'      => '\App\Plugins\Vat\VatController@store',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'POST',
        //     ]);
    }
}
