<?php

use App\Model\Admin\Menu;
use App\Plugins\Blog\Model\BlogCategories;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExcludeBlogsFromMainPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::table('blog_categories', function (Blueprint $table) {
            $table->boolean('exclude_category_from_main_list')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->tryToDrop('blog_categories', 'exclude_category_from_main_list');
    }

    private function tryToDrop($tableName, $colName)
    {
        try {
            Schema::table($tableName, function (Blueprint $table) use ($colName) {
                $table->dropColumn($colName);
            });
        } catch (\Throwable $th) {
            // dump($th->getMessage());
        }
    }
}
