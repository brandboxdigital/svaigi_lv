<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYoutubeToBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::table('blogs', function (Blueprint $table) {
            $table->string('youtube_id')->nullable();
        });

        // Artisan::call("db:seed", ['--class' => 'BlogAdminMenu']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->tryToDrop('blogs', 'youtube_id');
    }

    private function tryToDrop($tableName, $colName)
    {
        try {
            Schema::table($tableName, function (Blueprint $table) use ($colName) {
                $table->dropColumn($colName);
            });
        } catch (\Throwable $th) {
            // dump($th->getMessage());
        }
    }
}
