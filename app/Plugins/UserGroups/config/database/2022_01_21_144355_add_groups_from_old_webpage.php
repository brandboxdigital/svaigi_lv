<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Plugins\UserGroups\Model\UserGroup;
use Illuminate\Database\Migrations\Migration;



class AddGroupsFromOldWebpage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('user_groups', function (Blueprint $table) {
                // $table->integer('min_orders')->change();
            });
        } catch (\Throwable $th) {
            //throw $th;
        }

        // $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_groups', function (Blueprint $table) {
            // $table->dropColumn('last_buy_at');
        });
    }

    public function seed()
    {
        foreach ([
            ['name' => "1 pirkums" , 'min_orders' =>1],
            ['name' => "2 pirkumi" , 'min_orders' =>2],
            ['name' => "3 pirkumi" , 'min_orders' =>3],
            ['name' => "4 pirkumi" , 'min_orders' =>4],
            ['name' => "5 pirkumi" , 'min_orders' =>5],
            ['name' => "6-10"      , 'min_orders' =>6],
            ['name' => "11-15"     , 'min_orders' =>11],
            ['name' => "16-20"     , 'min_orders' =>16],
            ['name' => "21-25"     , 'min_orders' =>21],
            ['name' => "26-30"     , 'min_orders' =>26],
            ['name' => "31-40"     , 'min_orders' =>31],
            ['name' => "41-50"     , 'min_orders' =>41],
            ['name' => "51-60"     , 'min_orders' =>51],
            ['name' => "61-70"     , 'min_orders' =>61],
            ['name' => "71-80"     , 'min_orders' =>71],
            ['name' => "81-90"     , 'min_orders' =>81],
            ['name' => "91-100"    , 'min_orders' =>91],
            ['name' => "101-150"   , 'min_orders' =>101],
            ['name' => "151-200"   , 'min_orders' =>151],
            ['name' => "201-250"   , 'min_orders' =>201],
            ['name' => "251-300"   , 'min_orders' =>251],
            ['name' => "301-400"   , 'min_orders' =>301],
            ['name' => "401-500"   , 'min_orders' =>401],
            ['name' => "501-600"   , 'min_orders' =>501],
            ['name' => "601-700"   , 'min_orders' =>601],
            ['name' => "701-800"   , 'min_orders' =>701],
            ['name' => "801-900"   , 'min_orders' =>801],
            ['name' => "901-1000"  , 'min_orders' =>901],
        ] as $value) {
            $ug = UserGroup::updateOrCreate([
                'name' => $value['name'],
            ],[
                'min_orders' => $value['min_orders'],
            ]);
        }
    }
}
