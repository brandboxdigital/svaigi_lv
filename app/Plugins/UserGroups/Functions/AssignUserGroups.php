<?php

namespace App\Plugins\UserGroups\Functions;

use DB;
use App\User;
use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\UserGroups\Model\UserGroup;

trait AssignUserGroups
{

    /**
     * Run the asigment procedure.
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function runUserAsigment()
    {

        DB::table('order_headers')
            ->selectRaw('count(id) as number_of_orders, user_id')
            ->groupBy('user_id')
            ->orderBy('number_of_orders', 'desc')
            ->where('state', '!=', 'draft')
            ->chunk(10, function($chunk) {

                foreach ($chunk as $item) {
                    $user = User::find($item->user_id);

                    if ($user) {
                        $user->timestamps = false;

                        $group = $this->findGroup($item->number_of_orders);

                        if ($group) {
                            $user->user_group_id = $group->id;
                            $user->save();
                        }
                    }
                }

            });
    }

    private function findGroup($count)
    {
        return UserGroup::orderBy('min_orders', 'asc')->where('min_orders', '>=', $count)->first();
    }
}
