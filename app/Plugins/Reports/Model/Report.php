<?php

namespace App\Plugins\Reports\Model;

use File;
use Artisan;
use Storage;
use App\Schedules;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App\Plugins\Reports\Model
 */
class Report extends Model
{
    // use General;

    public $fillable = [
        "slug",
        "title",
        "description",
        "class",
        "last_run_at",
    ];

    protected $appends = [];

    /**
     * Runs report
     *
     * @return bool Returns true if command exists..
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function shedule(): bool
    {
        if (class_exists($this->class)) {

            // $exitCode = Artisan::call($this->slug, []);

            Schedules::create([
                'filename' => '',
                'type'     => $this->slug,
            ]);

            return true;
        }

        return false;
    }

    // public $with = [];

    // public function __construct(array $attributes = [])
    // {
    //     parent::__construct($attributes);
    // }

    // public function main_cat()
    // {
    //     return $this->belongsTo(Category::class, 'main_category', 'id');
    // }

    public static function recreateFromFiles()
    {
        $path = app_path('Console/Commands/Reports');

        foreach (File::files($path) as $file) {
            // dd($file->getFilename());
            $class = "\\App\\Console\\Commands\\Reports\\" . $file->getBasename('.php');

            if (class_exists($class)) {
                $object = new $class;
                $title = ucwords(implode(' ',preg_split('/(?=[A-Z])/', class_basename($class))));

                self::updateOrCreate(
                    [
                        'slug'         => $object->getName(),
                    ],
                    [
                        'title'        => $title,
                        'description'  => $object->getDescription(),
                        'class'        => $class,
                        'last_run_at'  => null,
                    ]
                );
            }
        }
    }

}
