<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->unique();
            $table->string('title');
            $table->text('description')->nullable();

            $table->string('class');

            $table->timestamp('last_run_at')->nullable();
            $table->timestamps();
        });

        $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }

    public function seed()
    {
        $lastSeq = \App\Model\Admin\Menu::where('parent_id', null)->orderBy('sequence', 'DESC')->first()->sequence ?? 0;

        $menu = \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => 'reports',
                'routeName' => 'reports',
            ],
            [
                'icon'        => 'fas fa-chart-line',
                'displayName' => 'Reports',
                'action'      => '\App\Plugins\Reports\ReportsController@index',
                'inMenu'      => '1',
                'sequence'    => $lastSeq,
                'parent_id'   => null,
                'method'      => 'GET',
            ]);


        $lastSeq = \App\Model\Admin\Menu::where('parent_id', $menu->id)->orderBy('sequence', 'DESC')->first()->sequence ?? 0;
        $lastSeq++;

        $main = 'reports';

        // $vacations = \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => $main,
        //         'routeName' => $main,
        //     ],
        //     [
        //         'icon'        => 'fas fa-percent',
        //         'displayName' => 'Vat',
        //         'action'      => '\App\Plugins\Reports\ReportsController@index',
        //         'inMenu'      => '1',
        //         'sequence'    => $lastSeq,
        //         'parent_id'   => $menu->id,
        //         'method'      => 'GET',
        //     ]);

        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "run/{id}",
                'routeName' => $main.".run",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'Run',
                'action'      => '\App\Plugins\Reports\ReportsController@run',
                'inMenu'      => '0',
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "refresh-from-file",
                'routeName' => $main.".refresh-from-file",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'Run',
                'action'      => '\App\Plugins\Reports\ReportsController@refreshFromFile',
                'inMenu'      => '0',
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        // \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => "edit/{id}",
        //         'routeName' => $main.".edit",
        //     ],
        //     [
        //         'icon'        => 'fas fa-edit',
        //         'displayName' => 'Edit',
        //         'action'      => '\App\Plugins\Vat\VatController@edit',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'GET',
        //     ]);

        // \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => "destroy/{id}",
        //         'routeName' => $main.".destroy",
        //     ],
        //     [
        //         'icon'        => 'far fa-window-close',
        //         'displayName' => 'Delete',
        //         'action'      => '\App\Plugins\Vat\VatController@delete',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'POST',
        //     ]);


        // \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => "store/{id?}",
        //         'routeName' => $main.".store",
        //     ],
        //     [
        //         'icon'        => 'far fa-window-close',
        //         'displayName' => 'Save',
        //         'action'      => '\App\Plugins\Vat\VatController@store',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'POST',
        //     ]);
    }
}
