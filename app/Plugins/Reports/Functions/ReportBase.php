<?php

namespace App\Plugins\Reports\Functions;

use Illuminate\Support\Str;
use App\Schedules;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

abstract class ReportBase
{
    public $summaryFields = [];
    public $excel = null;
    public $schedule = null;

    public static function make($schedule)
    {
        return new self($schedule);
    }

    public function __construct($schedule)
    {
        $this->schedule = $schedule;
    }

    public function getSummaryFields()
    {
        return $this->summaryFields;
    }

    public function makeExcel()
    {
        $this->excel = new Spreadsheet();
    }

    public function saveExcel()
    {
        $xls = new Xlsx($this->excel);

        $filename = 'export/report' . Carbon::now()->timestamp . ".xlsx";

        $xls->save(storage_path("$filename"));

        return $filename;
    }

    public function getActiveSheet()
    {
        if (!$this->excel) {
            $this->makeExcel();
        }

        return $this->excel->getActiveSheet();
    }

    public function createHeaders($sheet)
    {
        foreach ($this->getSummaryFields() as $column => $summaryField) {
            $sheet->setCellValue($column . "1", $summaryField['label']);
        }

        $sheet->freezePane('A1');
    }

    public function createLine($model, $sheet, $line)
    {
        foreach ($this->getSummaryFields() as $column => $summaryField) {

            $fieldValue = $this->getFiledValue($model, $summaryField);
            $sheet->setCellValue($column . $line, $fieldValue);

        }
    }

    public function getFiledValue($model, $summaryField)
    {
        $attribute = $summaryField['value'];

        $functionName = Str::camel('get_' . $attribute);
        if (method_exists($this, $functionName)) {
            return $this->$functionName($model);
        }

        if ($model->$attribute) {
            return $model->$attribute;
        }

        return null;
    }


    public function finishShedule($filename)
    {
        $this->schedule->update([
            'result_state' => true,
            'running' => false,
            'finished' => true,
            'result_message' => 'Report created: <a href="' . route('download', explode("/",$filename)) . '">Download</a>'
        ]);
    }

    public function errorShedule($e)
    {
        $this->schedule->update([
            'result_state' => false,
            'running' => false,
            'finished' => true,
            'result_message' => 'Error creating summary: '.$e->getMessage()
        ]);
    }

    public function autoSizeColumns($sheet) {
        foreach ($this->getSummaryFields() as $column => $summaryField) {
            $sheet->getColumnDimension($column)->setAutoSize(true);
        }
    }
}
