<?php

namespace App\Plugins\Reports\Functions;


use App\User;
use App\Schedules;
use Carbon\Carbon;
use App\Plugins\Orders\Model\OrderLines;
use App\Plugins\Orders\Model\OrderHeader;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class UserReport extends ReportBase
{

    public $summaryFields = [
        'A'  => ['value'  => 'id',              'label'  => 'ID', ],
        'B'  => ['value'  => 'email',           'label'  => 'Email', ],
        'C'  => ['value'  => 'name',            'label'  => 'Name', ],
        'D'  => ['value'  => 'last_name',       'label'  => 'Last Name', ],
        'E'  => ['value'  => 'phone',           'label'  => 'Phone', ],
        'F'  => ['value'  => 'legal_name',      'label'  => 'Legal Name', ],
        'G'  => ['value'  => 'legal_address',   'label'  => 'Legal Address', ],
        // 'H'  => ['value'  => 'legal_reg_nr',    'label'  => 'Legal Register Number', ],
        'I'  => ['value'  => 'address',         'label'  => 'Address', ],
        'J'  => ['value'  => 'city',            'label'  => 'City', ],
        'K'  => ['value'  => 'postal_code',     'label'  => 'Postal Code', ],
        'L'  => ['value'  => 'group',           'label'  => 'Client Group', ],
        'M'  => ['value'  => 'last_buy',        'label'  => 'Last Buy', ],
    ];

    public function run()
    {
        try {
            $sheet = $this->getActiveSheet();

            $this->createHeaders($sheet);

            $models = User::where('isAdmin', false)->with(['group'])->get();
            $count = $models->count();

            foreach ($models as $key => $user) {
                $this->createLine($user, $sheet, $key+2);
            }

            $this->autoSizeColumns($sheet);

            $filename = $this->saveExcel();

            $this->finishShedule($filename);

        } catch (\Throwable $th) {
            $this->errorShedule($th);
        }
    }


    public function getLastBuy($user)
    {
        $order = OrderHeader::where('user_id', $user->id)
            ->where('ordered_at', '!=', NULL)
            ->orderBy('ordered_at', 'desc')
            ->first();

        if ($order) {
            return $order->ordered_at;
        }

        return null;
    }

    public function getGroup($user)
    {
        if ($user->group) {
            return $user->group->name;
        }

        return null;
    }
}
