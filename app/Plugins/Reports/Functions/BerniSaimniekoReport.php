<?php

namespace App\Plugins\Reports\Functions;

use App\BerniSaimniekoAnalytics;

class BerniSaimniekoReport extends ReportBase
{

    public $summaryFields = [
        'A' => ['value' => 'id',           'label' => 'id', ],
        'B' => ['value' => 'user_id',      'label' => 'user_id', ],
        'C' => ['value' => 'email',        'label' => 'email', ],
        'D' => ['value' => 'name',         'label' => 'name', ],
        'E' => ['value' => 'last_name',    'label' => 'last_name', ],
        'F' => ['value' => 'phone',        'label' => 'phone', ],
        'G' => ['value' => 'page',         'label' => 'page', ],
        'H' => ['value' => 'created_at',   'label' => 'created at', ],
        'I' => ['value' => 'updated_at',   'label' => 'updated at', ],
        'J' => ['value' => 'type',         'label' => 'Type', ],
    ];

    public function run()
    {
        try {
            $sheet = $this->getActiveSheet();

            $this->createHeaders($sheet);

            $models = BerniSaimniekoAnalytics::get();
            $count = $models->count();

            foreach ($models as $key => $user) {
                $this->createLine($user, $sheet, $key+2);
            }

            $this->autoSizeColumns($sheet);

            $filename = $this->saveExcel();

            $this->finishShedule($filename);

        } catch (\Throwable $th) {
            $this->errorShedule($th);
        }
    }
}
