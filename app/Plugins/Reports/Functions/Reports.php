<?php

//namespace App\Plugins\Products;
namespace App\Plugins\Reports\Functions;

use App\Schedules;
use App\Plugins\Vat\Model\Vat;
use App\Plugins\Units\Model\Unit;
use App\Plugins\Reports\Model\Report;
use App\Plugins\Products\Model\Product;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Attributes\Model\Attribute;
use App\Plugins\MarketDays\Model\MarketDay;
use App\Plugins\Attributes\Model\AttributeValue;
use App\Plugins\Products\Model\ProductVariation;

/**
 * Trait Products
 *
 * @package App\Plugins\Products
 */
trait Reports
{

    /**
     * @return array
     */
    public function getList()
    {
        return [
            ['field' => 'title',       'label' => 'Report'],
            ['field' => 'description', 'label' => 'Description'],
            ['field' => 'slug',        'label' => 'Code'],

            ['field' => 'buttons', 'buttons' => ['run'], 'label' => '',],
        ];
    }

    public function getReportTypes()
    {
        return Report::get()->pluck('slug')->implode(',');
    }

}
