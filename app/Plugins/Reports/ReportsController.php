<?php

namespace App\Plugins\Reports;

use Psy\Util\Str;
use App\Schedules;
use App\Functions\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Plugins\Reports\Model\Report;
use Illuminate\Support\Facades\Route;
use App\Plugins\Admin\AdminController;

class ReportsController extends AdminController
{
    use Functions\Reports;
    // use General;

    public function index()
    {
        return view('admin.elements.table',
            [
                'tableHeaders' => $this->getList(),
                'header'       => 'Available Reports',
                'list'         => $this->getItems(),
                'idField'      => 'name',
                'destroyName'  => 'Reports',
                // 'operations'   => view("Products::partials.extraButtons")->render(),
                'logButton'    => view('admin.partials.log', ['logTypes' => $this->getReportTypes()])->render(),
                // 'js'           => [
                //     'js/productIO.js',
                // ],
            ]);
    }

    public function getItems()
    {
        return Report::get();
    }

    /**
     * GET: Refresh Reports from files..
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function refreshFromFile()
    {
        Report::recreateFromFiles();
    }

    public function run($id)
    {
        $report = Report::find($id);

        $isSuccess = $report->shedule();

        session()->flash("message", [
            'msg' => 'Added to Queue',
            'isError' => !$isSuccess,
        ]);

        return redirect()->back();
    }
}
