<?php

namespace App\Plugins\Users\Functions;


trait Users
{
    public function form($changeAdmin = false)
    {
        $form = [
            [
                'Label'     => 'Display',
                'data' => [
                    "email"             => ['type'  => 'text' , 'label'      => 'Email'] ,
                    "name"              => ['type'  => 'text' , 'label'      => 'Name'] ,
                    "last_name"         => ['type'  => 'text' , 'label'      => 'Last Name'] ,
                    "phone"             => ['type'  => 'text' , 'label'      => 'Phone'] ,
                    "is_legal"          => ['type'  => 'switch' , 'label'    => 'Is Legal'] ,
                    "legal_name"        => ['type'  => 'text' , 'label'      => 'Legal Name'] ,
                    "legal_address"     => ['type'  => 'text' , 'label'      => 'Legal Address'] ,
                    "legal_reg_nr"      => ['type'  => 'text' , 'label'      => 'Legal Reg Nr'] ,
                    "legal_vat_reg_nr"  => ['type'  => 'text' , 'label'      => 'Legal Vat Reg Nr'] ,
                    "address"           => ['type'  => 'text' , 'label'      => 'Address'] ,
                    "city"              => ['type'  => 'text' , 'label'      => 'City'] ,
                    "postal_code"       => ['type'  => 'text' , 'label'      => 'Postal Code'] ,
                    // "address_comments"  => ['type'  => 'text' , 'label'      => 'Address Comments'] ,
                    "comment"  => ['type'  => 'text' , 'label'      => 'Comment'] ,
                ],
            ],

            [
                'Label'     => 'Resset Password',
                'data' => [
                    "new_password"               => ['type'  => 'password' , 'label'      => 'New Password'] ,
                    "new_password_confirmation"  => ['type'  => 'password' , 'label'      => 'Confirm new Password'] ,
                ],
            ],

        ];

        if ($changeAdmin) {
            $form[] = [
                'Label'     => 'Admin',
                'data' => [
                    "isAdmin"          => ['type'  => 'switch', 'label'    => 'Is Admin'] ,
                    "username"         => ['type'  => 'text' , 'label'    => 'Username'] ,
                ],
            ];
        }

        return $form;
    }

    public function getList()
    {
        return [
            ['field'     => 'id', 'label'          => 'ID'],
            ['field'     => 'name', 'label'        => 'Name'],
            ['field'     => 'last_name', 'label'   => 'Last Name'],
            ['field'     => 'email', 'label'       => 'Email'],
            ['field'     => 'is_legal', 'label'    => $this->nbsp('Is Legal'), 'type'           => 'yesno'],
            ['field'     => 'isAdmin', 'label'     => $this->nbsp('Is Admin'), 'type'           => 'yesno'],
            ['field'     => 'username', 'label'     => 'Admin Username'],
            // ['field'  => 'min_orders', 'label'  => 'Minimum Orders'],
            // ['field'  => 'user_count', 'label'  => 'Users in group'],

            ['field'     => 'buttons', 'buttons'   => ['edit', 'delete'], 'label'  => ''],
            // ['field'     => 'buttons', 'buttons'   => ['edit'], 'label'  => ''],
        ];
    }

    private function nbsp($string)
    {
        return \str_replace(' ', '&nbsp;', $string);
    }
}
