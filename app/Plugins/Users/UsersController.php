<?php

namespace App\Plugins\Users;

use App\EmailChange;
use App\Plugins\Admin\AdminController;
use App\Plugins\Users\Functions\Users as UsersFunctions;
use App\Plugins\Users\Model\UserGroup;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\Profile as ProfileRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class UsersController extends AdminController
{
    use UsersFunctions;

    public function index($search = false)
    {
        $cr = explode(".", Route::currentRouteName());

        if (!$search && ($cr[1] ?? false) == 'search') {
            return redirect()->route($cr[0] . ".list");
        }

        return view('admin.elements.table',
            [
                'tableHeaders' => $this->getList(),
                'header'       => 'Users',
                'list'         => $this->getUsers(),
                'idField'      => 'id',
                'destroyName'  => 'User',
            ]);
    }

    public function getUsers()
    {
        /** @var User $suppliers */
        $users = User::query();

        if ($search = request()->route('search')) {
            $users = $users->where('id', 'like', "%$search%");

            foreach ([
                "email",
                "name",
                "last_name",
                "phone",
                "legal_name",
                "legal_address",
                "legal_reg_nr",
                "address",
                "city",
                "postal_code",
            ] as $field) {
                $users = $users->orWhere($field, 'like', "%$search%");
            }
        }

        return $users->paginate(100);
    }

    public function add()
    {
        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => new User]);
    }

    public function edit($id)
    {
        /**
         * If search is used..
         * I dont know why the search form URL is so malformed but...
         */
        $input = request()->input();
        if (count($input) > 0) {
            $id = collect(array_keys($input))->first();
            return redirect("/admin/config/users/edit/$id");
        }

        return view('admin.elements.tabForm', ['formElements' => $this->form(true), 'content' => User::findOrFail($id)]);
        // return view('admin.elements.form', ['formElements' => $this->form(), 'content' => User::findOrFail($id)]);
    }

    public function store(Request $request, $id = false)
    {
        if ($request->input('new_password')) {

            $request->validate([
                'new_password' => 'required|confirmed|min:6',
            ]);

            $request->request->add(['password' => $request->input('new_password')]);
        }

        $profileRequest = ProfileRequest::createFrom($request);

        $request->validate($profileRequest->rules(),$profileRequest->messages());

        $user = User::findOrNew($id);

        // If Email Changes
        if ($user->email != request()->get('email')) {
            $changes = request()->all();

            $changes['registered'] = 1;

            $changes['is_legal'] = isset($changes['is_legal']);
            $changes['newsletter'] = isset($changes['newsletter']);

            // If there is unregistered user with changed email
            $emailUser = User::where(['email' => request()->get('email')])->first();
            if ($emailUser && $emailUser->id != $user->id) {
                $joinUser = $emailUser->id;
            }

            $user->fill($changes);
            $user->save();

            EmailChange::create([
                'invokedBy'    => $user->id,
                'changes'      => $changes,
                'joinUser'     => $joinUser ?? null,
                'verifyString' => str_random(60),
            ]);

            \Session::flash('message', [
                "msg"=>"User created",
                "isError"=>false
            ]);

        } else {
            $changes = request()->all();
            // $rn = explode(".", Route::currentRouteName());
            // if($rn[0] == "profile" && !Auth::user()->registered)
            $changes['registered'] = 1;

            $changes['is_legal'] = isset($changes['is_legal']);
            $changes['newsletter'] = isset($changes['newsletter']);

            /**
             * Don't let change admin mode for self.. this way
             * at least one user will stay admin..
             */
            if (Auth::user()->id != $user->id) {
                $changes['isAdmin'] = isset($changes['isAdmin']);
            } else {
                unset($changes['isAdmin']);
            }

            $user->update($changes);

            if($request->input('rules')){
                // Newsletter::subscribe($user->email);
            }

            \Session::flash('message', [
                "msg"=>"User updated",
                "isError"=>false
            ]);
        }

        return redirect()->route('users');
    }

    public function delete($id)
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        $result = $user->delete();

        return ['status' => $result, 'message' => ($result ? 'User Deleted' : "Error deleting User")];
    }
}
