<?php

namespace App\Plugins\Categories\Functions;


use App\Languages;
use App\Plugins\Admin\Model\File;
use App\Plugins\Attributes\Model\Attribute;

/**
 * Trait Category
 *
 * @package App\Plugins\Categories\Functions
 */
trait Category
{
    /**
     * @return array
     */
    public function getList()
    {
        return [
            ['field' => 'name', 'label' => 'Category Name', 'translate' => 'category.name', 'key' => 'id'],
            ['field' => 'unique_name', 'label' => 'Unique Name'],
            ['field' => 'name', 'label' => 'Parent Category', 'translate' => 'category.name', 'key' => 'parent_id', 'meta' => true],
            ['field' => 'buttons', 'buttons' => ['edit', 'delete'], 'label' => ''],
        ];
    }

    /**
     * @return array
     */
    public function form()
    {

        $categories = \App\Plugins\Categories\Model\Category::all();
        return [
            [
                'Label'     => 'Display',
                'languages' => Languages::all()->pluck('name', 'code'),
                'data'      => [
                    'name'        => ['type' => 'text', 'class' => '', 'label' => 'Category Name', 'meta' => true],
                    'slug'        => ['type' => 'text', 'class' => '', 'label' => 'Category Slug','readonly' => false, 'meta' => true, "comment" => "Unikāla kategorijas vērtība, kura paradās linkā uz kategoriju. Piem. https://svaigi.lv/partika/olas. Vārdi 'partika' un 'olas' ir tie kas nākno šī lauka. Šajā Laukā nedrīkst būt īpašie simboli un mīkstinājuma zīmes."],
                    'description' => ['type' => 'textarea', 'class' => '', 'label' => 'Description', 'meta' => true],
                ],
            ],
            [
                'Label'     => 'Google SEO',
                'languages' => Languages::all()->pluck('name', 'code'),
                'data'      => [
                    'google_keywords'    => ['type' => 'text', 'class' => '', 'label' => 'Keywords', 'meta' => true],
                    'google_description' => ['type' => 'textarea', 'class' => '', 'label' => 'Google Description', 'meta' => true],
                ],
            ],
            [
                'Label' => 'Parameters',
                'data'  => [
                    'unique_name' => ['type' => 'text', 'class' => '', 'label' => 'Unique Name',  'meta' => false, "comment" => "Unikāla kategorijas vērtība, kura būtība tiek izmantota tikai produktu importā/exportā"],
                    'parent_id'      => ['type' => 'select', 'class' => 'show-tick',
                                         'options' => $categories,
                                         'label' => 'Parent Category', 'dataAttr' => ['live-search' => "true", 'size' => '5']],

                    // 'category_image' => ['type' => 'image', 'label' => 'Category Header Image', 'preview' => true],
                    // 'category_dropdown_menu_image' => ['type' => 'image', 'label' => 'Category Dropdown Menu Image', 'preview' => true],
                    // 'filters'        => ['type' => 'chosen', 'label' => 'Attributes', 'options' => Attribute::all()],
                ],
            ],
        ];
    }

    /**
     * @param $collection
     */
    public function handleAttributes($collection) {
        $collection->filters()->sync(request('filters'));
    }

}
