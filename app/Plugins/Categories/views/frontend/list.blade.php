@extends('layouts.app')
@php
    /** @var \App\Plugins\Categories\Model\Category $category */
    $suppliersInCateogry = $frontController->getSupplierListInCategory($allProducts??null);
    $menu = $cache->getMenuCache($menuSlug??"main");
    $categories = $cache->getCategoryCache()
@endphp

@section('content')



@if($banners??false)
    @foreach($banners as $banner)
        @if($banner->type!='message')
            @continue
        @endif
        {{-- @php
            dd($banner->frequency);
        @endphp --}}
        @if($banner->frequency=='always' || ($banner->frequency=='once_per_session' && !session()->has('banner'.$banner->id)) || ($banner->frequency=='once_a_week' && !request()->cookie('banner'.$banner->id)))
            @push('css')
                <style type="text/css">
                    .sv-message#message-{{$banner->id}}      {
                        color: #{{$banner->color_text}};
                    }

                    .sv-message#message-{{$banner->id}}     > div {
                        background-color: #{{ $banner->color_background }};
                    }

                    .sv-message#message-{{$banner->id}} a {
                        color: #{{$banner->color_url }};
                    }
                </style>
            @endpush
            <div class="sv-message" id="message-{{ $banner->id }}">
                <div>
                    {!! $banner->meta['message'][language()]??"" !!}
                    <a href="#" class="sv-close reportClose" data-url="{{ route('closeBanner', [$banner->id]) }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="21" viewBox="0 0 22 21">
                            <path d="M21.253,19.339l-1.414,1.414L11,11.914,2.161,20.753,0.747,19.339,9.586,10.5,0.747,1.661,2.161,0.247,11,9.086l8.839-8.839,1.414,1.414L12.414,10.5Z"></path>
                        </svg>
                    </a>
                </div>
            </div>
        @endif
    @endforeach
@endif


@php
    // dd($slug_category->unique_name);
@endphp


        <div class="@yield('wrapper')">
            @if(!isset($slug_category))
                <div class="container">
                    <div class="sv-products">
                        @include('frontend.elements.breadcrumbs')
                        @if(!($hideHeader??false) && $category)
                            <div class="sv-category-title-banner">
                                <div class="title">
                                    <h1>
                                        {{ ___("category.name.".$category->id) }}
                                    </h1>
                                    <h4>{{ ___("category.description.".$category->id) }}</h4>
                                </div>
                            </div>
                        @endif
                        @if(count($products??[]) || ($product??false))
                            @yield('leftSide')
                        @else
                            {!! _t('translations.nothinghasbeenfound') !!}
                        @endif
                    </div>
                </div>
            @endif
            @if(isset($slug_category))
                <products-data category-slug="{{ $slug_category->unique_name }}" active-nav-slug="{{ $activeNavSlug }}"></products-data>
            @endif
        </div>
    </div>
@endsection
