<?php

namespace App\Plugins\Categories;

use App\Functions\General;
use App\Http\Controllers\CacheController;
use App\Languages;
use App\Plugins\Admin\AdminController;
use App\Plugins\Admin\Model\File;
use App\Plugins\Categories\Model\Category;
use App\Plugins\Categories\Model\CategoryMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Schedules;

/**
 * Class CategoryController
 *
 * @package App\Plugins\Categories
 */
class CategoryController extends AdminController
{
    use \App\Plugins\Categories\Functions\Category;
    use General;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.elements.treeTable',
            [
                'tableHeaders' => $this->getList(),
                'header'       => 'Categories',
                'list'         => Category::with(['metaData', 'descendants'])->defaultOrder()->get()->toTree(),
                'idField'      => 'name',
                'destroyName'  => 'Category',
                'operations'   => view("Categories::partials.extraButtons")->render(),
            ]);
    }

    public function metaImport()
    {
        return $this->googleDocsMetaImport();
    }

        /**
     * Import products from google sheet!
     */
    private function googleDocsMetaImport()
    {
        $googleFileId  = '1rbs80ZGv7cb7rBN8F5NisKNDv3Q9BhUv';
        $googleSheetId = '512097583';

        $gSheetsFile = "https://docs.google.com/spreadsheets/d/" . $googleFileId . "/export?format=csv&id=" . $googleFileId . "&gid=" . $googleSheetId;

        $filename = 'imports/category-meta/'. str_random(40) . '.csv';

        try {

            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            $cont = file_get_contents($gSheetsFile, false, stream_context_create($arrContextOptions));

            Storage::put($filename, $cont);

        } catch (\Throwable $th) {
            throw new \Exception("Error Downloading Google file! URL: $gSheetsFile | Error:" . $th->getMessage());
        }

        Schedules::create([
            'filename' => basename($filename),
            'type'     => 'categoryMetaImport',
        ]);

        return ['status' => true, 'message' => "Product Import from Google Sheet Scheduled"];
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => new Category()]);
    }

    /**
     * @param Request $request
     * @param bool    $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $id = false)
    {
        $val = $msg = [];

        /**
         * Find and set Unique slug
         */
        $nextFreeSlug = $this->findNextFreeSlug($request->input('slug.' . language()), $id);
        $request->request->add([
            'slug' => [
                language() => $nextFreeSlug,
            ],
        ]);

        $nextUniqueName = $this->findNextFreeUniqueName($request->input('unique_name'), $id);
        $request->request->add([
            'unique_name' => $nextUniqueName,
        ]);

        // dd($request->input());

        foreach (languages() as $lang) {
            $val["name.{$lang->code}"] = "required";
            $msg["name.{$lang->code}.required"] = "Category Name in {$lang->name} Should Be Filled";
            $val["slug.{$lang->code}"] = [
                'required',
                Rule::unique('category_metas', 'meta_value')->where(function ($query) use ($lang, $id) {
                    $query = $query->where(['meta_name' => 'slug', 'language' => $lang->code]);
                            if ($id) {
                                $query = $query->where('owner_id', '!=', $id);
                            }
                            return $query;
                    }),
            ];
            $msg["slug.{$lang->code}.unique"] = "Slug in {$lang->name} Already Exists";
            $msg["slug.{$lang->code}.required"] = "Slug in {$lang->name} Can not be empty";

            $msg["unique_name.required"] = "Unique name is required";
        }

        $request->validate($val, $msg);

        $metas = [
            'name',
            'slug',
            'description',
            'google_keywords',
            'google_description',
        ];

        try {
            DB::beginTransaction();
            /** @var Category $category */
            $category = Category::updateOrCreate(['id' => $id], [
                'parent_id' => $request->input('parent_id'),
            ]);

            $category->unique_name = $request->input('unique_name', $category->slug);
            $category->save();

            $this->handleMetas($category, $metas, false, $request->all($metas));
            $this->handleImages($category);
            $this->handleImages($category, "category_dropdown_menu_image");
            // dd($category);
            $this->handleAttributes($category);

            DB::commit();

            $category->forgetMeta();
            (new CacheController)->createCategoryCache(true);
        } catch(\PDOException $e) {
            DB::rollBack();
            session()->flash("message", ['msg' => $e->getMessage(), 'isError'=> true]);
            return redirect()->back();
        }
        return redirect(route('categories.list'))->with(['message' => ['msg' => "Category Saved"]]);
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function destroy($id)
    {

        /** @var Category $cc */
        $cc = Category::findOrFail($id);

        if($cc->products_main()->count()) {
            return ['status' => 'false', "message" => 'Category is set to main in some products, can not remove Category'];
        }

        try {
            DB::beginTransaction();
            $cc->products()->detach();
            $cc->metaData()->delete();

            $cc->delete();
            DB::commit();
        } catch(\PDOException $e) {
            DB::rollBack();
            abort(500);
        }

        return ['status' => true, "message" => "Category Deleted"];
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {

        return view('admin.elements.tabForm', ['formElements' => $this->form(), 'content' => Category::findOrFail($id)]);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getEditName($id)
    {
        return Category::findOrFail($id)->name;
    }


    private function findNextFreeSlug($slug, $current_id)
    {
        $isUnique = true;
        $count    = 0;
        $initialslug = Str::slug($slug, '-');
        $slug = $initialslug;

        do {
            $count++;
            $metasCount = CategoryMeta::where('meta_value', $slug)
                ->where('language', language())
                ->where('meta_name', 'slug')
                ->where('owner_id', '!=', $current_id)
                ->count();

            $isUnique = $metasCount == 0;

            if (!$isUnique) {
                $slug = Str::slug("$initialslug $count", '-');
            }

        } while (!$isUnique);

        return $slug;
    }

    private function findNextFreeUniqueName($unique_name, $current_id)
    {
        $isUnique = true;
        $count    = 0;
        $initial_unique_name = Str::slug($unique_name, '-');
        $unique_name = $initial_unique_name;

        do {
            $count++;
            $metasCount = Category::where('unique_name', $unique_name)
                ->where('id', '!=', $current_id)
                ->count();

            $isUnique = $metasCount == 0;

            if (!$isUnique) {
                $unique_name = Str::slug("$initial_unique_name $count", '-');
            }

        } while (!$isUnique);

        return $unique_name;
    }
}
