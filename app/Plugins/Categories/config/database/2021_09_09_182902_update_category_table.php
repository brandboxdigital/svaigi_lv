<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Kalnoy\Nestedset\NestedSet;

class UpdateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('categories', function (Blueprint $table) {
                $table->dropColumn('parent_id');
            });
        } catch (\Throwable $th) {}

        try {
            Schema::table('categories', function (Blueprint $table) {
                $table->integer('id_from_import_file')->nullable();
            });
        } catch (\Throwable $th) {}

        Schema::table('categories', function (Blueprint $table) {
            NestedSet::columns($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            NestedSet::dropColumns($table);
        });

        try {
            Schema::table('categories', function (Blueprint $table) {
                $table->dropColumn('id_from_import_file');
            });
        } catch (\Throwable $th) {}
    }
}
