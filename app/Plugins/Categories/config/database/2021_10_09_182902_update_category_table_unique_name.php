<?php

use App\Plugins\Categories\Model\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class UpdateCategoryTableUniqueName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('categories', 'unique_name'))
        {
            Schema::table('categories', function (Blueprint $table) {
                $table->string('unique_name')->nullable();;
            });

            Category::get()->each(function($cat) {
                $cat->unique_name = $cat->slug;
                $cat->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('unique_name');
        });
    }
}
