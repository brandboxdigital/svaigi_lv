<?php

namespace App\Plugins\Categories\Model;

use App\BaseModel;
use App\Plugins\Admin\Model\File;
use App\Plugins\Attributes\Model\Attribute;
use App\Plugins\Banners\Model\Banner;
use App\Plugins\Products\Model\Product;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\CacheController;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;

/**
 * Class Category
 *
 * @package App\Plugins\Products\Model\Categories
 */
class Category extends BaseModel {

    use NodeTrait;
    use SoftDeletes;

    public $fillable = ['parent_id'];
    public $metaClass = __NAMESPACE__ . '\CategoryMeta';

    public $appends = [
        'text',
        'url'
    ];

    protected $dates = ['deleted_at'];

    /******************** Meta Attributes *************************/
    public function setNameAttribute($value) {
        if (!$this->id)
            return false;
        $this->setMeta('name', $value);
    }

    /****************** Images ***************/
    public function getCategoryImageAttribute() {
        return $this->images()->where('owner', 'category_image')->limit(1)->get();
    }

    public function getCategoryDropdownMenuImageAttribute() {
        return $this->images()->where('owner', 'category_dropdown_menu_image')->limit(1)->get();
    }


    /**
     * Get image filename
     *
     * @param string $imageType
     *
     * @return null
     */
    public function getImage($imageType = 'main') {
        return $this->images()->where('owner', 'category_image')->latest()->first();
    }

    public function getDropdownMenuImage() {
        return $this->images()->where('owner', 'category_dropdown_menu_image')->latest()->first();
    }


    public function images() {
        return $this->hasMany(File::class, 'owner_id');
    }

    // public function parent() {
    //     return $this->belongsTo(Category::class, 'parent_id');
    // }

    public function getNameAttribute() {
        if (!$this->id)
            return;
        return __("category.name." . $this->id);
    }

    public function getSlugAttribute() {
        if (!$this->id)
            return;
        return __("category.slug." . $this->id);
    }

    public function filters() {
        return $this->belongsToMany(Attribute::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }

    public function getFullProductCountAttribute()
    {
        $model = $this;

        $count = Cache::rememberForever( self::class . '.FullProductCount.' . $this->id, function () use ($model){
            $cat_ids = array_merge([$model->id], $model->descendants->pluck('id')->toArray());

            $count = Product::whereHas('extra_categories', function($q) use ($cat_ids) {
                    $q->whereIn('id', $cat_ids);
                })
                // ->orWhereHas('main_cat', function($q) use ($cat_ids) {
                //     $q->whereIn('id', $cat_ids);
                // })
                ->whereHas('market_days', function ($q) {
                    $md = (new CacheController)->getSelectedMarketDay();
                    return $q->where('market_day_id', $md->id);
                })
                ->count();

            return $count;
        });

        return $count;
    }

    public function products_main() {
        return $this->hasMany(Product::class, 'main_category');
    }

    // public function children() {
    //     return $this->hasMany(Category::class, 'parent_id');
    // }

    public function nodes() {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function discount() {
        return 0;
    }

    public function banners() {
        return $this->belongsToMany(Banner::class);
    }

    public function getTextAttribute() {
        return trim($this->name);
    }

    public function getUrlAttribute() {
        return $this->getUrl(false);
    }

    public function ancestorCache() {
        $list = Cache::rememberForever( self::class . '.AncestorCache.' . $this->id, function () {
            return $this->ancestors->toArray();
        });

        return collect($list);
    }

    public function getUrl($absolute = true)
    {
        $slugArr = [];
        $counter = 1;

        foreach ($this->ancestorCache()->pluck('id') as $key => $id) {
            $slug = "slug" . $counter;
            $slugArr[$slug] = __('category.slug.' . $id);

            $counter++;
        }

        $slug = "slug" . $counter;
        $slugArr[$slug] = __('category.slug.' . $this->id);

        return $absolute ? r("url", $slugArr) : r("url", $slugArr, false);
    }

    public function getBreadcrumbs()
    {
        $result = $this->ancestorCache()->toArray();

        foreach ($result as $key => $res) {
            if ($key > 0) {
                $result[$key]['unique_name'] = $result[$key - 1]['unique_name'] . '/' . $res['unique_name'];
            }
        }


        return $result;
    }
}
