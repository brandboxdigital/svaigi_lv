<?php

namespace App\Plugins\Feedback;

use Psy\Util\Str;
use App\Schedules;
use App\Functions\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Route;
use App\Plugins\Admin\AdminController;
use App\Plugins\Feedback\Model\Feedback;
use App\Plugins\Orders\Model\OrderHeader;

class FeedbacksController extends AdminController
{
    use Functions\Feedbacks;
    // use General;

    public function index()
    {
        return view('admin.elements.table',
            [
                'tableHeaders' => $this->getList(),
                'header'       => 'Available Feedbacks',
                'list'         => $this->getItems(),
                'idField'      => 'name',
                'destroyName'  => 'Feedbacks',
                // 'operations'   => view("Products::partials.extraButtons")->render(),
                // 'logButton'    => view('admin.partials.log', ['logTypes' => $this->getReportTypes()])->render(),
                // 'js'           => [
                //     'js/productIO.js',
                // ],
            ]);
    }

    public function view($id)
    {
        return view('admin.elements.form', ['formElements' => $this->form(), 'content' => Feedback::findOrFail($id)]);
    }

    private function getItems()
    {
        return Feedback::query()
            ->orderBy('created_at', 'desc')
            ->get();
    }


    private function makeNewFeedbackTest()
    {
        return Feedback::create([
            'order_number'  => 40,
            'email'         => 'adx@adx.lv',
            'body'          => 'Nepatika!',
        ]);
    }
}
