<?php

namespace App\Plugins\Feedback\Observers;

use App\Mail\FeedbackAdded;
use Illuminate\Support\Facades\Mail;
use App\Plugins\Feedback\Model\Feedback;
use App\Plugins\Orders\Model\OrderNumber;

class FeedbackObserver
{
    /**
     * Handle the feedback "creating" event.
     *
     * @param  \App\Plugins\Feedback\Model\Feedback  $feedback
     * @return void
     */
    public function creating(Feedback $feedback)
    {
        $orderNumber = OrderNumber::find($feedback->order_number);

        if ($orderNumber) {
            $feedback->order_header_id = $orderNumber->order_id;
        }
    }

    /**
     * Handle the feedback "created" event.
     *
     * @param  \App\Plugins\Feedback\Model\Feedback  $feedback
     * @return void
     */
    public function created(Feedback $feedback)
    {

        //Send mail to admin
        Mail::to('svaigi@svaigi.lv')->send(new FeedbackAdded($feedback));
    }

    /**
     * Handle the feedback "updated" event.
     *
     * @param  \App\Plugins\Feedback\Model\Feedback  $feedback
     * @return void
     */
    public function updated(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the feedback "deleted" event.
     *
     * @param  \App\Plugins\Feedback\Model\Feedback  $feedback
     * @return void
     */
    public function deleted(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the feedback "restored" event.
     *
     * @param  \App\Plugins\Feedback\Model\Feedback  $feedback
     * @return void
     */
    public function restored(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the feedback "force deleted" event.
     *
     * @param  \App\Plugins\Feedback\Model\Feedback  $feedback
     * @return void
     */
    public function forceDeleted(Feedback $feedback)
    {
        //
    }
}
