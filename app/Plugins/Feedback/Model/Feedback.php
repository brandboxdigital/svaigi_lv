<?php

namespace App\Plugins\Feedback\Model;

use File;
use Artisan;
use Storage;
use App\Schedules;
use BaconQrCode\Writer;
use BaconQrCode\Renderer\Color\Rgb;
use Illuminate\Http\Request;

use BaconQrCode\Renderer\ImageRenderer;
use Illuminate\Database\Eloquent\Model;
use App\Plugins\Orders\Model\OrderHeader;
use BaconQrCode\Renderer\RendererStyle\Fill;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;

/**
 * Class Product
 *
 * @package App\Plugins\Feedback\Model
 */
class Feedback extends Model
{
    // use General;
    const TEMP_STORAGE_FOLDER = 'public/temp/feedback/';
    const STORAGE_FOLDER = 'public/feedback/';

    public $table = "feedbacks";

    public $fillable = [
        "order_header_id",
        "order_number",
        "email",
        "body",
        "is_read",
    ];

    protected $appends = [];


    /**
     * Relation to order
     *
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function order()
    {
        return $this->belongsTo(OrderHeader::class, 'order_header_id', 'id');
    }



    /**
     * Create new feedback and move files from tmp dir
     *
     * @param Request $request
     *
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public static function storeNewFeedback(Request $request)
    {
        $feedback = new self;

        $feedback->order_number  = $request->order_number ?? null;
        $feedback->email         = $request->email ?? null;
        $feedback->phone         = $request->phone ?? null;
        $feedback->body          = $request->body ?? null;

        $feedback->save();

        $feedback->grabTempFilesFromSession($request);

        return $feedback;
    }

    /**
     * Get a URL list of tmp files. Filter by session ID
     *
     * @param Request $request
     *
     * @return void
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public static function getTempFilesListFromSession(Request $request, $getPublicPath = true)
    {
        $sessionId = $request->session()->getId();
        $path = self::TEMP_STORAGE_FOLDER . $sessionId;

        $files = [];

        foreach (Storage::files($path) as $filePath) {

            $files[] = (object)[
                "publicPath" => ($getPublicPath) ? \str_replace("public/", '/storage/', $filePath) : $filePath,
                "sessionId"  => $sessionId,
                "fileName"   => \str_replace([$path, "/"], "", $filePath),
            ];
        }

        return $files;
    }

    /**
     * Make a QR code for specific order
     *
     * @param [type] $order
     * @param bool   $inBase64
     *
     * @return string QR code string. SVG or base64 encoded SVG
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public static function getQrCodeLink($order, $inBase64 = true)
    {
        if ($order->id) {

            $fill     = Fill::uniformColor(new Rgb(235,244,239), new Rgb(0,0,0));
            $renderer = new ImageRenderer(
                new RendererStyle(400, 0, null, null, $fill),
                new SvgImageBackEnd()
            );

            $writer = new Writer($renderer);

            $url = route('feedback-page-with-order', $order->id);
            $svg = $writer->writeString($url);

            if ($inBase64) {
                return base64_encode($svg);
            }

            return $svg;
        }

        return null;
    }


    public function getImagePublicPathListAttribute()
    {
        return $this->getFilesList(true);
    }



    public function getFilesList($getPublicPath = true)
    {
        $path  = self::STORAGE_FOLDER . $this->id;
        $files = [];

        foreach (Storage::files($path) as $filePath) {
            if ($getPublicPath) {
                $files[] = \str_replace("public/", '/storage/', $filePath);
            } else {
                $files[] = $filePath;
            }
        }

        return $files;
    }

    public function grabTempFilesFromSession(Request $request)
    {
        $sessionId = $request->session()->getId();

        $oldpath   = self::TEMP_STORAGE_FOLDER . $sessionId;
        $newpath   = self::STORAGE_FOLDER . $this->id;

        foreach (Storage::files($oldpath) as $filePath) {
            $newFilePath = \str_replace($oldpath, $newpath, $filePath);
            Storage::move($filePath, $newFilePath);
        }
    }
}
