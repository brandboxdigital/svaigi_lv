<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('order_header_id')->unsigned();
            $table->bigInteger('order_number')->unsigned();

            $table->string('email')->nullable();
            $table->string('phone')->nullable();

            $table->text('body')->nullable();
            $table->text('files')->nullable();

            $table->boolean('is_read')->default(false);

            $table->timestamps();
        });

        $this->seed();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
        Schema::dropIfExists('feedbacks');

        $routeName = 'feedbacks';

        \App\Model\Admin\Menu::where('routeName', 'like', "$routeName%")
            ->get()
            ->each(function($menu) {
                try {
                    $menu->delete();
                } catch (\Throwable $th) {}
            });
    }

    public function seed()
    {
        $lastSeq = \App\Model\Admin\Menu::where('parent_id', null)->orderBy('sequence', 'DESC')->first()->sequence ?? 0;

        $routeName = 'feedbacks';

        $menu = \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => $routeName,
                'routeName' => $routeName,
            ],
            [
                'icon'        => 'fas fa-chart-line',
                'displayName' => 'Feedback List',
                'action'      => '\App\Plugins\Feedback\FeedbacksController@index',
                'inMenu'      => '1',
                'sequence'    => $lastSeq,
                'parent_id'   => null,
                'method'      => 'GET',
            ]);


        $lastSeq = \App\Model\Admin\Menu::where('parent_id', $menu->id)->orderBy('sequence', 'DESC')->first()->sequence ?? 0;
        $lastSeq++;

        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "view/{id}",
                'routeName' => $routeName.".view",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'View',
                'action'      => '\App\Plugins\Feedback\FeedbacksController@view',
                'inMenu'      => 0,
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        \App\Model\Admin\Menu::updateOrCreate(
            [
                'slug'      => "store",
                'routeName' => $routeName.".store",
            ],
            [
                'icon'        => 'fas fa-calendar-times',
                'displayName' => 'View',
                'action'      => '\App\Plugins\Feedback\FeedbacksController@view',
                'inMenu'      => 0,
                'sequence'    => ++$lastSeq,
                'parent_id'   => $menu->id,
                'method'      => 'GET',
            ]);

        // \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => "refresh-from-file",
        //         'routeName' => $main.".refresh-from-file",
        //     ],
        //     [
        //         'icon'        => 'fas fa-calendar-times',
        //         'displayName' => 'Run',
        //         'action'      => '\App\Plugins\Reports\ReportsController@refreshFromFile',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $menu->id,
        //         'method'      => 'GET',
        //     ]);

        // \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => "edit/{id}",
        //         'routeName' => $main.".edit",
        //     ],
        //     [
        //         'icon'        => 'fas fa-edit',
        //         'displayName' => 'Edit',
        //         'action'      => '\App\Plugins\Vat\VatController@edit',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'GET',
        //     ]);

        // \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => "destroy/{id}",
        //         'routeName' => $main.".destroy",
        //     ],
        //     [
        //         'icon'        => 'far fa-window-close',
        //         'displayName' => 'Delete',
        //         'action'      => '\App\Plugins\Vat\VatController@delete',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'POST',
        //     ]);


        // \App\Model\Admin\Menu::updateOrCreate(
        //     [
        //         'slug'      => "store/{id?}",
        //         'routeName' => $main.".store",
        //     ],
        //     [
        //         'icon'        => 'far fa-window-close',
        //         'displayName' => 'Save',
        //         'action'      => '\App\Plugins\Vat\VatController@store',
        //         'inMenu'      => '0',
        //         'sequence'    => ++$lastSeq,
        //         'parent_id'   => $vacations->id,
        //         'method'      => 'POST',
        //     ]);
    }
}
