@extends('layouts.app')

@php
    // dd($order->order_number);
@endphp

@section('content')

    @if ($feedback ?? null)
        <div class="sv-user-data-forms" style="min-height: calc(100vh - 342px)">
            <div class="sv-page-title">
                <h1>Paldies, saņemts!</h1>
            </div>
        </div>
    @else
        <div class="sv-user-data-forms" style="min-height: calc(100vh - 342px)">
            <div class="sv-page-title">
                <h1>Mums ir svarīgi zināt!</h1>
            </div>

            <form id="feedbackForm" method="post" action="{{ $formAction }}">
                {{ csrf_field() }}

                <div class="section">
                    <p>
                        Iesniedz savu atsauksmi, ieteikumu vai sūdzību.
                    </p>
                </div>
                <div class="section">
                    @if ($order)
                        <label>Rēķina numurs: {{$order->order_number->id}}</label>
                        <input type="hidden" name="order_header_id" value="{{ $order->id }}" />
                        <input type="hidden" name="order_number" value="{{ $order->order_number->id }}" />
                        <div class="sv-blank-spacer very-small"></div>
                    @else
                        <div class="input-wrapper req">
                            <label>Rēķina numurs</label>
                            <input type="text" name="order_number" id="order_number" value="{{ old('order_number') }}" />
                        </div>
                    @endif


                    {{-- <div class="input-wrapper req">
                        <label>{!! _t('translations.profileFormPhone') !!}</label>
                        <input type="text" name="phone" value="{{ old('phone') ?? ($user->phone ?? '') }}" />
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-wrapper req">
                                <label>{!! _t('translations.profileFormName') !!}</label>
                                <input type="text" name="name" value="{{ old('name') ?? ($user->name ?? '') }}" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-wrapper req">
                                <label>{!! _t('translations.profileFormLastName') !!}</label>
                                <input type="text" name="last_name"
                                    value="{{ old('last_name') ?? ($user->last_name ?? '') }}" />
                            </div>
                        </div>
                    </div>

                    <div class="sv-blank-spacer very-small"></div>

                    <div class="sv-line-spacer"></div> --}}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-wrapper form-address-comments">
                                    <label>E-pasta adrese</label>
                                    <input type="text" name="email" id="email" value="{{ old('email') ?? ($user->email ?? '') }}" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-wrapper form-address-comments">
                                    <label>Tālruņa nummurs</label>
                                    <input type="text" name="phone" id="phone" value="{{ old('phone') ?? ($user->phone ?? '') }}" />
                                </div>
                            </div>
                        </div>

                        <div class="input-wrapper form-address-comments">
                            <label>Tava ziņa</label>
                            <textarea type="text" name="body" value="{{ old('body') }}" cols="30" rows="10"></textarea>
                        </div>

                        <div class="input-wrapper file-input">
                            <label>Pievieno attēlu</label>
                            <input type="file" id="fileupload" name="file" data-url="{{ $fileUploadAction }}">

                            <div class="uploaded-list">
                                <label for="fileupload">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"/></svg>
                                </label>

                                @foreach ($files as $file)
                                    <div class="uploaded-image" data-filename="{{$file->fileName}}">
                                        <img src="{{$file->publicPath}}" />
                                    </div>
                                @endforeach
                            </div>
                        </div>
                </div>

                <div class="section form-comments">
                    <div class="btn-row btn-row--fill">
                        <button type="submit" class="btn btn-green">Nosūtīt</button>
                    </div>
                </div>
            </form>

            <div class="loading"></div>
        </div>
    @endif

@endsection


@push('scripts')
<script for="order--feedback">

$(function () {

    $('#fileupload').fileupload({
        // dataType: 'json',
        done: function (e, data) {
            //response
            console.log(data.result.files);
            console.log(data);

            $.each(data.result.files, function (index, file) {
                $("<div>")
                    .addClass('uploaded-image')
                    .attr('data-filename', file.fileName)
                    .append( $('<img>').attr('src', file.publicPath) )
                    .appendTo( $('.file-input .uploaded-list') );
            });
        }
    });

    $('.uploaded-list').on('click', '.uploaded-image', function(event) {
        var imagetodelete = $(this);
        var filename = imagetodelete.data('filename') ?? null;
        if (filename) {
            $.ajax({
                method: "POST",
                url: "{{ $fileDeleteAction ?? null }}",
                data: { filename: filename },

                success: function(data) {
                    imagetodelete.remove();
                }
            })
        }
    })
});

</script>

@endpush
