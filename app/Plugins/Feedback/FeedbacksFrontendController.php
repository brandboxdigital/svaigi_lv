<?php

namespace App\Plugins\Feedback;

use App\User;
use Psy\Util\Str;
use App\Schedules;
use App\Functions\General;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use App\Plugins\Feedback\Model\Feedback;
use App\Plugins\Orders\Model\OrderHeader;

class FeedbacksFrontendController extends Controller
{
    public function index(Request $request, $order_header_id = null)
    {
        $order = OrderHeader::find($order_header_id);

        if ($order) {
            $user = User::find($order->user_id);
            // return abort(404);
        }

        return view('Feedback::frontend.feedback-page', [
            'formAction'       => route('feedback-page.store'),
            'fileUploadAction' => route('feedback-page.file-upload'),
            'fileDeleteAction' => route('feedback-page.file-delete'),
            'order'            => $order,
            'user'             => $user ?? null,
            'files'            => Feedback::getTempFilesListFromSession($request),
        ]);
    }

    public function storeTempFile(Request $request)
    {
        $request->validate([
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $sessionId = $request->session()->getId();
        $fileName   = implode("-", [
            time(),
            $request->file->hashName()
        ]);

        $filePath = $request->file->storeAs("public/temp/feedback/$sessionId", $fileName);
        $filePath = \str_replace("public/", '/storage/', $filePath);

        return [
            "files" => [
                ["sessionId"  => $sessionId, "publicPath" => $filePath, "fileName" => $fileName]
            ]
        ];
    }

    public function deleteTempFile(Request $request)
    {
        $request->validate([
            'filename' => 'required'
        ]);

        $sessionId = $request->session()->getId();
        $fileName  = $request->input('filename');

        $filePath = "public/temp/feedback/$sessionId/$fileName";

        if (Storage::exists($filePath)) {
            Storage::delete($filePath);
        }

        return [];
    }

    public function store(Request $request)
    {
        $request->validate([
            "order_number"  => 'required',
            "email"         => 'required',
            "phone"         => 'required',
            "body"          => 'required',
        ]);

        $feedback = Feedback::storeNewFeedback($request);

        return view('Feedback::frontend.feedback-page', [
            'feedback' => $feedback,
        ]);
    }




}
