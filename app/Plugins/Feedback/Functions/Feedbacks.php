<?php

//namespace App\Plugins\Products;
namespace App\Plugins\Feedback\Functions;

use App\Schedules;
use App\Plugins\Vat\Model\Vat;
use App\Plugins\Units\Model\Unit;
use App\Plugins\Reports\Model\Report;
use App\Plugins\Products\Model\Product;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Attributes\Model\Attribute;
use App\Plugins\MarketDays\Model\MarketDay;
use App\Plugins\Attributes\Model\AttributeValue;
use App\Plugins\Products\Model\ProductVariation;

/**
 * Trait Products
 *
 * @package App\Plugins\Products
 */
trait Feedbacks
{

    /**
     * @return array
     */
    public function getList()
    {
        return [
            ['field' => 'order_number',    'label' => 'Order Nr'],
            ['field' => 'body',            'label' => 'Feedback'],
            ['field' => 'created_at',      'label' => 'Created At'],
            ['field' => 'order_header_id', 'label' => 'Order ID'],

            // ['field' => 'is_read',     'label' => 'Is Read'],
            ['field' => 'buttons', 'buttons' => ['view'], 'label' => '',],
        ];
    }

    public function form()
    {
        return [
            'data' => [
                'order_number'  => ['type' => 'text',     'label' => 'Order Number', 'readonly' => true],
                'email'         => ['type' => 'text',     'label' => 'Email',    'readonly' => true],
                'phone'         => ['type' => 'text',     'label' => 'Phone',    'readonly' => true],
                'body'          => ['type' => 'textarea', 'label' => 'Body', 'readonly' => true],

                'image_public_path_list' => ['type' => 'read_only_image_list', 'label' => 'Images'],
            ],
        ];
    }


}
