@php
    $id = $id??str_random(5);

    $filelist = $content->$name ?? [];
@endphp

<div class="row">
    <div class="col-md-2">
        <label for="{{$id}}" class="col-form-label">{{ $label }}</label>
    </div>
    <div class="col-md-8 preview">
        @foreach ($filelist as $key => $filePath)
            <div class="preview_image_container" data-id="{{$key}}">
                <div class="preview_image">
                    <img src="{{ $filePath }}" />
                </div>

                {{-- @if (isset($comment)) <p class="help-block">{!! $comment !!}</p> @endif --}}
            </div>
        @endforeach
    </div>
</div>
