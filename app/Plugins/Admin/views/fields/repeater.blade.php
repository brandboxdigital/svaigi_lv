@php
    $id = $id ?? str_random(5);
    $lang = "";
    if(($language??false)) {
        $lang="[$language]";
        if($meta??false) {
            $savedContent = $content->meta[$name]??"";
        } else {
            $savedContent = is_array($content->$name) ? $content->$name : $content->{str_plural($name)};
        }
        $oldValues = old($name)[$language]??$savedContent[$language]??"";
    } else {
        $oldValues = old($name)??$content->$name??"";
    }

@endphp

<div class="row">
    <div class="col-md-2">
        <h5 style="color:#71748d;font-weight:normal">{{ $label }}</h5>
    </div>

    <div class="col-md-9">

        <div class="form-group repeater" style="margin-bottom:20px;">
            <div data-repeater-list="{{ $name }}{{ $lang??"" }}">
                @foreach ($oldValues as $oldValue)
                <div data-repeater-item>
                    <label for="">Title</label>
                    <input class="form-control" name="title" style="width: 100%" value="{{$oldValue['title']}}" />
                    <br>
                    <label for="">Body</label>
                    <textarea class="noEditor form-control" name="body" rows="3" style="width: 100%">{{$oldValue['body']}}</textarea>
                    <br>
                    <button
                        data-repeater-delete
                        class="btn btn-xs btn-danger destroyButton"
                    >
                        <i class="fas fa-trash-alt"></i>&nbsp; Remove This Item
                    </button>
                </div>
                @endforeach
            </div>

            <hr>

            <a
                data-repeater-create
                class="btn btn-xs destroyButton"
            >
                <i class="fas fa-plus"></i>&nbsp; Add new item
            </a>

        </div>
    </div>
</div>



@push('scripts')
<script for="{{$id}}-repeater">
$(document).ready(function () {
    console.log('Repeater stuff');
    $('.repeater').repeater({
        show: function () {
            $(this).show()

            $(this).find('textarea').css('visibility', 'visible');

            setTimeout(() => {
                // window._initTinyMCE($(this).find('textarea').first()[0]);
            }, 500);
            window._initTinyMCE();
        },
    })
});
</script>
<style>
    [data-repeater-item] {
        border: 1px solid #d2d2e4;
        padding: 20px;
        margin-bottom: 10px;
    }
</style>
@endpush
