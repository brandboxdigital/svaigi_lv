<div class="form-group row">
    <label class="col-xs-12 col-sm-2 col-form-label __text-sm-right">{{ $label }}</label>
    <div class="col-xs-12 col-sm-8 col-lg-6 pt-1">
        @php
            $id = $id??str_random(5);
        @endphp
        <div class="switch-button switch-button-success">
            <input type="checkbox" {{ ($content->$name??false)?"checked=checked":"" }} name="{{ $name }}"
                   id="{{$id}}" />
            <span>
                <label for="{{$id}}"></label>
            </span>
        </div>

        @if (isset($comment)) <p class="help-block">{!! $comment !!}</p> @endif
    </div>

</div>
