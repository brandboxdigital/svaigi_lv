@php
function categoryRecursiveLooper(&$items, $selectedItems) {

    try {
        foreach ($items as $key => $item) {
            if (in_array($item->id, $selectedItems)) {
                $item->state = ['selected' => true];
            }

            if (count($item->nodes) > 0) {
                // $item->selectable = false;
                // $item->icon = "glyphicon glyphicon-stop";
                // $item->selectedIcon = "glyphicon glyphicon-stop";

                categoryRecursiveLooper($item->nodes, $selectedItems);
            } else {
                unset($item->nodes);
            }
        }
    } catch (\Throwable $th) {
        dump("There was some error, please be carefull!", $items, $selectedItems);
    }

}

$id = $id ?? str_random(5);
$selectedItems = old($name)
    ? explode(',', old($name))
    : $content->formatSelected($name);

// dump(old($name), $content->formatSelected($name));
// $selectedItems = collect($selectedItems)->toArray();

categoryRecursiveLooper($options, $selectedItems);
$json = $options->toJson();

@endphp

<div class="row">
    <div class="col-md-2">
        <h5 style="color:#71748d;font-weight:normal">{{ $label }}</h5>
    </div>

    <div class="col-md-9">

        <div class="form-group" style="margin-bottom:20px;">
            {{-- <select class="multiselect {{ $class ?? '' }}" style="display:none;" {{ $data ?? '' }}
                id="{{ $id }}" multiple="multiple" name="{{ $name }}[]" {{ $data ?? '' }}
                tabindex="-98"
            >
            </select> --}}
            @if (isset($comment)) <p class="help-block">{!! $comment !!}</p> @endif
            <div id="{{$id}}-tree"></div>

            <input id="{{$id}}-input" type="hidden" name="{{$name}}">
        </div>
    </div>
</div>


@push('scripts')
<script for="{{$id}}-tree">

function updateSelected_{{$id}}() {
    var selected = $('#{{$id}}-tree').treeview('getSelected');
    var joined = selected.map(function(elem){
        return elem.id;
    }).join(",");

    console.log('a?', joined);

    $('#{{$id}}-input').val(joined);
}

$('#{{$id}}-tree').treeview({
    data: {!! $json !!},
    multiSelect: true,
    levels:4,
    onNodeSelected: function(event, data) {
        updateSelected_{{$id}}();
    },
    onNodeUnselected: function(event, data) {
        updateSelected_{{$id}}();
    },
});

updateSelected_{{$id}}();

</script>
@endpush
