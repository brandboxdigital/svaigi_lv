<?php

namespace App\Plugins\Pages\Functions;


use App\Plugins\Admin\Model\File;
use Illuminate\Support\Collection;

class PageComponentFactory
{
    public $metaData;
    public $images;
    public $pageimage;
    public $pageimage1;
    public $pageimage2;
    public $pageimage3;

    public function __construct($metaData, $images) {
        $this->meta = $metaData;
        $this->images = $images;
        $this->getPageImages();
    }

    public function getPageImages() {

        if ($this->images instanceof Collection) {
            $imageQuery = File::whereIn('id', $this->images->pluck('id')->toArray());
        } else if (is_array($this->images)) {
            $imageQuery = File::whereIn('id', $this->images);
        } else {
            $imageQuery = File::whereIn('id', []);
        }

        $this->pageimage  = with(clone $imageQuery)->where('owner', 'pageimage')->get();
        $this->pageimage1 = with(clone $imageQuery)->where('owner', 'pageimage1')->get();
        $this->pageimage2 = with(clone $imageQuery)->where('owner', 'pageimage2')->get();
        $this->pageimage3 = with(clone $imageQuery)->where('owner', 'pageimage3')->get();

        // dd($this);
    }

    public function __get($attribute)
    {
        if (\array_key_exists($attribute, $this->meta)) {
            $languages = languages()->pluck('code');

            /**
             * @author Guntis Šulcs <guntis@brandbox.digital>
             * Here I dont understand how this should work.. but i will
             * just take first lang code (lv)
             */
            $lang = $languages->first();


            if (\array_key_exists($lang, $this->meta[$attribute])) {
                return $this->meta[$attribute][$lang];
            }
        }

        return null;
    }
}
