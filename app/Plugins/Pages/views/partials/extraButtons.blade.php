<div class="btn-group">
    {{-- <label for="importOrderUpload" style="margin:0px;" class="btn btn-warning btn-xs importOrders">
        Import Order Update
        <i class="fas fa-upload"></i>
    </label>
    <form method="post" style="display:none;">
        <input type="file" name="importFile" accept=".zip,.rar" id="importOrderUpload">
        <input type="hidden" name="controller" value="OrderController" />
        <input type="hidden" name="plugin" value="Orders" />
        <input type="hidden" name="method" value="importOrders" />
    </form> --}}

    <a href="{{ route('pages.list') }}" class="btn btn-success btn-xs" data-question="{{ __('translations.SureToSend') }}">
        <i class="fas fa-long-arrow-alt-left"></i>
        Pack to page list
    </a>

</div>
