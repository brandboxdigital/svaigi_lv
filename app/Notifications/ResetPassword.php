<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends ResetPasswordNotification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Svaigi.lv paroles atjaunošana')
            ->line('Ja Tev rodas jautājumi par pieeju savam Svaigi.lv kontam, droši dod mums ziņu! Ja vēlies atjaunot pieeju Svaigi.lv kontam, klikšķini uz šo saiti:')
            ->action('Atjaunot paroli', url('password/reset', $this->token))
            // ->line('Paroles atjaunināšanas links pieejams 60 minūtes. Ja Jūs nēesat veicis šo pieprasījumu, jums nekas nav jādara')
            ->greeting('Sveiciens!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
