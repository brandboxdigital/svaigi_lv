<?php

namespace App\Components;

use App\BerniSaimniekoAnalytics;
use Illuminate\Support\Facades\Auth;

class emailSubscriptionForm
{
    public $componentName = "Email Subscription Form";

    public function form()
    {
        return [
            [
                'Label'     => "Display",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title' => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'text' => ['type' => 'text', 'label' => 'Text', 'meta' => true],
                    'confirmation_text' => ['type' => 'text', 'label' => 'Confirmation Text', 'meta' => true],
                    'btn_text' => ['type' => 'text', 'label' => 'Button Text', 'meta' => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.emailSubscriptionForm";
    }

    public function countPageView()
    {
        try {
            $url = url()->full();
        } catch (\Throwable $th) {}

        try {
            $user = Auth::user();

            if ($user) {
                BerniSaimniekoAnalytics::countUserPageview($user, $url ?? 'unknown');
            }
        } catch (\Throwable $th) {}
    }
}
