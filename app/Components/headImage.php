<?php

namespace App\Components;


class headImage
{

    public $componentName = "Head Image Banner";

    public function form()
    {
        return [
            [
                'Label'     => "Display",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title' => ['type' => 'text', 'label' => 'Main Title', 'meta' => true],
                    'content' => ['type' => 'textarea', 'label' => 'Sub Text', 'meta' => true],
                    'link_text' => ['type' => 'text', 'label' => 'Link Text', 'meta' => true],
                    'link' => ['type' => 'text', 'label' => 'Link URL', 'meta' => true],
                ],
            ], [
                'Label' => 'Background Image',
                'data'  => [
                    'pageimage' => ['type' => 'image', 'preview' => 'true', 'label' => 'Banner'],
                ],
            ],
        ];
    }

    public function template() {
        return "frontend.components.headImage";
    }

}
