<?php

namespace App\Components;


class popular
{
    public $componentName = "Featured Products";

    public function form()
    {

        return [
            [
                'Label'     => "Title",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title'      => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.popular";
    }
}
