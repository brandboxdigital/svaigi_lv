<?php

namespace App\Components;


class simpleImageColumns
{
    public $componentName = "3 Simple Image Columns";
    public $renderedComponent = null;

    public function form()
    {

        return [
            [
                'Label'     => "Column 1",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title1'      => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'content1'    => ['type' => 'textarea', 'label' => 'Text', 'meta' => true],
                    'link1'       => ['type' => 'text', 'label' => 'Link', 'meta' => true],
                ],
            ],
            [
                'Label'     => "Column 2",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title2'      => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'content2'    => ['type' => 'textarea', 'label' => 'Text', 'meta' => true],
                    'link2'       => ['type' => 'text', 'label' => 'Link', 'meta' => true],
                ],
            ],
            [
                'Label'     => "Column 3",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title3'      => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'content3'    => ['type' => 'textarea', 'label' => 'Text', 'meta' => true],
                    'link3'       => ['type' => 'text', 'label' => 'Link', 'meta' => true],
                ],
            ],
            [
                'Label'     => "Images",
                'data'      => [
                    'pageimage1'  => ['type' => 'image', 'preview' => 'true', 'label' => 'Image 1'],
                    'pageimage2'  => ['type' => 'image', 'preview' => 'true', 'label' => 'Image 2'],
                    'pageimage3'  => ['type' => 'image', 'preview' => 'true', 'label' => 'Image 3'],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.simpleImageColumns";
    }

    public function getColumns()
    {
        $component = $this->renderedComponent;

        foreach ([1,2,3] as $key) {
            $column = [
                'title'    => $component->getData('title' . $key),
                'content'  => $component->getData('content' . $key),
                'link'     => $component->getData('link' . $key),
                'image'    => $component->getComponentImage(false, 'original', 'pageimage' . $key),
            ];

            $columns[] = (object) $column;
        }

        return collect($columns);
    }
}
