<?php

namespace App\Components;


class imageManager
{
    public $componentName = "Image Manager";

    public function form()
    {
        return [
            [
                'Label'     => "Image manager",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'select' => [
                        'type' => 'select',
                        'label' => 'Size',
                        'selected' => '210x210',
                        'options' => [
                            (object) ["id" => '210x210', 'name' => 'Small (Shop product)'],
                            (object) ["id" => '369x369', 'name' => 'Medium (Blog main)'],
                            (object) ["id" => '700x', 'name' => 'Large (Open product)'],
                        ],
                    ],
                    'img_universal'      => ['type' => 'image', 'label' => 'Image', 'preview' => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.imageManager";
    }
}
