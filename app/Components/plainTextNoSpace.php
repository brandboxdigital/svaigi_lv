<?php

namespace App\Components;


class plainTextNoSpace
{
    public $componentName = "Plain Text No Space";

    public function form()
    {


        return [
            [
                'Label'     => "Plain Text No Space",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'isWide'  => ['type' => 'switch',   'label' => 'Is Wide', 'meta' => true],
                    'content' => ['type' => 'textarea', 'label' => 'Page Content', 'meta' => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.plainTextNoSpace";
    }
}
