<?php

namespace App\Components;


class centeredButton
{
    public $componentName = "Centered Button";

    public function form()
    {
        return [
            [
                'Label'     => "Display",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title' => ['type' => 'text', 'label' => 'Button Title', 'meta' => true],
                    'link' => ['type' => 'text', 'label' => 'Button Link', 'meta' => true],
                    'is_btn_wide' => ['type' => 'switch', 'label' => 'Is Button Wide', 'meta' => true],
                    'only_desktop' => ['type' => 'switch', 'label' => 'Only Desktop', 'meta' => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.centeredButton";
    }
}