<?php

namespace App\Components;


class carouselBanner
{
    public $componentName = "Carousel Banner";

    public function form()
    {

        return [
            [
                'Label'     => "Carousel Banner 1",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'is_enabled1' => ['type' => 'switch', 'label' => 'Is Enabled', 'meta' => true],
                    'label1'      => ['type' => 'text', 'label' => 'Label', 'meta' => true],
                    'title1'      => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'content1'    => ['type' => 'textarea', 'label' => 'Text', 'meta' => true],
                    'link1'       => ['type' => 'text', 'label' => 'Link', 'meta' => true],
                ],
            ],
            [
                'Label'     => "Carousel Banner 2",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'is_enabled2' => ['type' => 'switch', 'label' => 'Is Enabled', 'meta' => true],
                    'label2'      => ['type' => 'text', 'label' => 'Label', 'meta' => true],
                    'title2'      => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'content2'    => ['type' => 'textarea', 'label' => 'Text', 'meta' => true],
                    'link2'       => ['type' => 'text', 'label' => 'Link', 'meta' => true],
                ],
            ],
            [
                'Label'     => "Carousel Banner 3",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'is_enabled3' => ['type' => 'switch', 'label' => 'Is Enabled', 'meta' => true],
                    'label3'      => ['type' => 'text', 'label' => 'Label', 'meta' => true],
                    'title3'      => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'content3'    => ['type' => 'textarea', 'label' => 'Text', 'meta' => true],
                    'link3'       => ['type' => 'text', 'label' => 'Link', 'meta' => true],
                ],
            ],
            [
                'Label'     => "Images",
                'data'      => [
                    'pageimage1'  => ['type' => 'image', 'preview' => 'true', 'label' => 'Image 1', 'comment' => 'Size 1400x800px'],
                    'pageimage2'  => ['type' => 'image', 'preview' => 'true', 'label' => 'Image 2', 'comment' => 'Size 1400x800px'],
                    'pageimage3'  => ['type' => 'image', 'preview' => 'true', 'label' => 'Image 3', 'comment' => 'Size 1400x800px'],
                ],
            ],
            [
                'Label'     => "Is BerniSaimnieko",
                'data'      => [
                    'is_berni_saimnieko' => ['type' => 'switch', 'label' => 'Has Berni Saimnieko Styles', 'meta' => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.carouselBanner";
    }
}
