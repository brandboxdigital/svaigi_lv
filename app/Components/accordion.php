<?php

namespace App\Components;


class accordion
{
    public $componentName = "Accordion";

    public function form()
    {


        return [
            [
                'Label'     => "Accordion",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title'   => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'content' => ['type' => 'repeater', 'label' => 'Accordion Items', 'meta' => true],
                ],
            ],
            [
                'Label'     => "Image",
                'data'      => [
                    'pageimage'  => ['type' => 'image', 'preview' => 'true', 'label' => 'Image'],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.accordion";
    }
}
