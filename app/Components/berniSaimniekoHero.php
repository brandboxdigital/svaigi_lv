<?php

namespace App\Components;


class berniSaimniekoHero
{
    public $componentName = "Berni Saimnieko Hero";

    public function form()
    {
        return [
            [
                'Label'     => "Display",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'pretitle'         => ['type'  => 'text', 'label'  => 'Pre Title', 'meta'  => true],
                    'title'            => ['type'  => 'text', 'label'  => 'Title', 'meta'  => true],
                    'text'            => ['type'  => 'text', 'label'  => 'Text', 'meta'  => true],
                    'youtube_title'    => ['type'  => 'text', 'label'  => 'Youtube title', 'meta'  => true],
                    'youtube_url'      => ['type'  => 'text', 'label'  => 'Youtube url', 'meta'  => true],
                    'instagram_title'  => ['type'  => 'text', 'label'  => 'Instagram title', 'meta'  => true],
                    'instagram_url'    => ['type'  => 'text', 'label'  => 'Instagram url', 'meta'  => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.berniSaimniekoHero";
    }
}
