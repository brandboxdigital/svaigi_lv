<?php

namespace App\Components;


class spacer
{
    public $componentName = "A Spacer";

    public function template()
    {
        return "frontend.components.spacer";
    }

    public function form()
    {
        $languages = languages()->pluck('name', 'code');
        return [
            [
                'Label'     => "Spacer Type",
                'data'      => [
                    'type' => [
                        'type' => 'select',
                        'label' => 'Spacer Type',
                        'selected' => 'small',
                        'options' => [
                            (object) ["id" => 'very-big',    'name' => 'Very Big'],
                            (object) ["id" => 'big',         'name' => 'Big'],
                            (object) ["id" => 'medium',      'name' => 'Medium'],
                            (object) ["id" => 'small',       'name' => 'Small'],
                            (object) ["id" => 'very-small',  'name' => 'Very Small'],
                        ],
                    ],
                ],
            ],
        ];
    }
}
