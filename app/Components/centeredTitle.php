<?php

namespace App\Components;


class centeredTitle
{
    public $componentName = "Centered Title";

    public function form()
    {
        return [
            [
                'Label'     => "Display",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title' => ['type' => 'text', 'label' => 'Title', 'meta' => true],
                    'mobile_only' => ['type' => 'switch', 'label' => 'Show Only On Mobile Screens', 'meta' => true],
                    'is_berni_saimnieko' => ['type' => 'switch', 'label' => 'Has Berni Saimnieko Styles', 'meta' => true],
                ],
            ],
        ];
    }

    public function template()
    {
        return "frontend.components.centeredTitle";
    }
}