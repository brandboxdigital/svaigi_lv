<?php

namespace App\Components;

use App\Http\Controllers\FrontController;
use App\Plugins\Blog\Model\Blog;
use App\Plugins\Blog\Model\BlogCategories;

class blogposts
{
    public $componentName = "Blog Posts";
    public $renderedComponent = null;

    public function template()
    {
        return "frontend.components.blogposts";
    }

    public function form()
    {
        return [
            [
                'Label'     => "Blog Posts",
                'languages' => languages()->pluck('name', 'code'),
                'data'      => [
                    'title' => [
                        'type' => 'text',
                        'label' => 'Block title',
                        'meta' => true
                    ],
                    'is_title_disabled' => [
                        'type' => 'switch',
                        'label' => 'Is Title Hidden',
                    ],
                    'is_single_fullpage' => [
                        'type' => 'switch',
                        'label' => 'Is Single Fullpage',
                        'meta' => true
                    ],
                    'has_berni_saimnieko_styles' => [
                        'type' => 'switch',
                        'label' => 'Has Berni Saimnieko Styles',
                        'meta' => true
                    ],
                    'is_slider' => [
                        'type' => 'switch',
                        'label' => 'Is Slider',
                        'meta' => true
                    ],
                    'hide_mobile' => [
                        'type' => 'switch',
                        'label' => 'Hide Mobile',
                        'meta' => true
                    ],
                    'category' => [
                        'type' => 'select',
                        'label' => 'Only from Category',
                        'comment' => 'Select nothing to show from all categories',
                        'selected' => null,
                        'options' => $this->getBlogCategories(),
                    ],
                ],
            ],
        ];
    }

    private function getBlogCategories()
    {
        $categories = BlogCategories::get()
            // ->map(function($category){
            //     return [

            //     ]
            // })
            // ->pluck('name', 'id')
            ;

        return $categories;
    }

    public function getPosts()
    {
        $component = $this->renderedComponent;

        $postCount = $component->getData('is_single_fullpage' , null) == 'on'
            ? 1
            : 3;

        if ($categoryId = $component->getData('category' , null)) {
            $posts = Blog::query()
                // ->inRandomOrder()
                ->where('main_category', $categoryId)
                ->orWhereHas('extra_categories', function($extraCategoryQuery) use ($categoryId) {
                    return $extraCategoryQuery->where('blog_categories.id', $categoryId);
                })
                ->limit($postCount)
                ->get();

            return $posts;
        } else {

            $frontController = new FrontController;
            $posts = $frontController->getHighlightedPosts();
        }

        return $posts->slice(0, $postCount);
    }
}
