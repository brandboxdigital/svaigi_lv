<?php

namespace App\Components;


class reviewBanner
{
    public $componentName = "Review Banner";
    public $disableEdit = true;

    public function template()
    {
        return "frontend.components.reviewBanner";
    }
}
