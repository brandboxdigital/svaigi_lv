<?php

namespace App\Cache;


use App\Plugins\Admin\Model\File;
use App\Plugins\Categories\Model\Category;

class CategoryCache
{
    private $categories;
    private $categoryTree;
    private $products = [];
    private $image = [];
    private $dropdownMenuImage = [];
    private $filters = [];

    public function __construct() {
        $categoryList = [];
        $categories = Category::withDepth()->get();

        $this->categories = $categories;
        $this->categoryTree = $categories->toTree();
        // $this->categoryTree = $categories;
        // return $categories;

        // $this->categories = $categories->pluck('id')->toArray();

        // foreach($categories as $category) {
        //     /** @var Category $category */
        //     $this->filters[$category->id] = $category->filters()->pluck('id')->toArray();
        //     $this->products[$category->id] = $category->products()->pluck('id')->toArray();
        //     $categoryList[$category->id] = $category->parent_id??0;
        //     /** @var File|null $cimage */
        //     $cimage = $category->getImage();
        //     $this->image[$category->id] = $cimage?$cimage->filePath:null;

        //     $cimage = $category->getDropdownMenuImage();
        //     $this->dropdownMenuImage[$category->id] = $cimage?$cimage->filePath:null;
        // }

        // $this->categoryTree = $categoryList;
    }

    /**
     * @author Guntis Šulcs <guntis@brandbox.digital>
     * Used in \App\Cache\MenuCache __construct to get menu list from cache.. i think.. :/
     */
    public function getCategoryMenuItems($firstLevelId, $parentCategory, $squence = 0)
    {
        $counter = $squence;
        $menuItems = Category::descendantsOf($parentCategory)->map(function($cat) use (&$counter, $firstLevelId, $parentCategory) {
            $parentId = ($parentCategory == $cat->parent_id)
                ? $firstLevelId
                : $cat->parent_id;

            return (object)[
                'id' => $cat->id,
                'menu_owner' => 'category',
                'owner_id' => $cat->id,
                'frontend_menu_item_id' => $parentId,
                'sequence' => $counter++,
                'product_count' => $cat->full_product_count,
            ];
        });

        return $menuItems;
    }

    public function getPath($categoryId) {
        return $this->findParent($categoryId);
    }

    public function findParent($categoryId) {

        /**
         * @todo Correct parent behaviour
         */
        return [$categoryId];

        if($categoryId==0) return [];
        return array_merge($this->findParent($this->categoryTree[$categoryId]), [$categoryId]);
    }

    public function image($size, $categoryId)
    {
        $path = "/".implode("/", ["categories", $size, ($this->image[$categoryId] ?? '')]);
        if(\Storage::exists("public/$path")) {
            return $path;
        }

        return config("app.defaultCategoryImage");
    }

    public function dropDownMenuImage($size, $categoryId)
    {
        $path = "/".implode("/", ["categories", $size, ($this->dropdownMenuImage[$categoryId]??'')]);

        if(\Storage::exists("public/$path")) {
            return $path;
        }

        return config("app.defaultCategoryImage");
    }

    public function getFilters($categoryId) {
        $category = Category::find($categoryId);

        return $category->filters()->pluck('id')->toArray();
        return $this->filters[$categoryId];
    }
}
