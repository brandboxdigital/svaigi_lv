<?php

namespace App\Cache;


use App\Plugins\Menu\Model\FrontendMenu;

class MenuCache
{

    private $menuItems = [];
    private $tree = [];

    public function __construct($menuSlug)
    {
        $categories = (new \App\Http\Controllers\CacheController())->getCategoryCache();

        $menuItems = FrontendMenu::where("slug", $menuSlug)->first() ?? new FrontendMenu();
        $menuItems = $menuItems->menuItems;

        if ($menuSlug == 'shop') {
            /**
             * Fill category items with full category tree
             */
            $cats = collect();
            foreach ($menuItems as $menu) {
                if ($menu->menu_owner == 'category') {
                    $cats = $cats->merge($categories->getCategoryMenuItems($menu->owner_id, $menu->owner_id, $menu->sequence));
                }
            }
            $menuItems = $cats->merge($menuItems);
        } else {
            $menuItems = FrontendMenu::where("slug", $menuSlug)->first() ?? new FrontendMenu();
            $menuItems = $menuItems->menuItems;
        }

        foreach ($menuItems ?? [] as $item) {
            $itemId = ($menuSlug == 'shop')
                ? $item->owner_id
                : $item->id;


            /**
             * Only for Categoruy dropdown menu...
             * If category is empty, then dont show it!
             */
            if ($menuSlug == 'shop') {

                $count = isset($item->product_count)
                    ? $item->product_count
                    : null;

                if ($count === 0) {
                    continue;
                }
            }


            $this->menuItems[$itemId] = (object)[
                'slug'     => "{$item->menu_owner}.slug.{$item->owner_id}",
                'name'     => "{$item->menu_owner}.name.{$item->owner_id}",
                'parent'   => $item->frontend_menu_item_id,
                'urlType'  => $item->menu_owner,
                'id'       => $item->owner_id,
                'image'    => $categories->image('1473x247', $item->owner_id),
                'owner_id' =>  $item->owner_id,
                'dropdownMenuImage' => $categories->dropDownMenuImage('1473x247', $item->owner_id),
            ];

            if (!($item->frontend_menu_item_id ?? false)) {
                $this->tree['first'][$item->sequence] = $itemId;
            } else {
                $this->tree[$item->frontend_menu_item_id][$item->sequence] = $itemId;
            }
        }
    }

    public function getItems($itemLevel = 'first', $autoParam = 'slug1')
    {
        if ($itemLevel == 'auto') {
            $itemLevel = $this->getItemId(request()->route($autoParam)); // , 'menuid', false - альтернативный вариант меню
        }
        if(!is_array($this->tree[$itemLevel]??false)) return [];

        ksort($this->tree[$itemLevel]);

        return $this->tree[$itemLevel];
    }

    public function getItem($item)
    {
        return $this->menuItems;
        // return $this->menuItems[$item];
    }

    public function getItemsTreeJson($itemLevel = 'first', $autoParam = 'slug1')
    {
        if(!is_array($this->tree[$itemLevel]??false)) return [];

        ksort($this->tree[$itemLevel]);

        $menu = $this->__recursiveMenuMapper($this->getItems());

        return $menu->toJson();
    }

    private function __recursiveMenuMapper(array $tree, $deepness = 0)
    {
        // dump(['$tree'=> $tree]);

        $deepness++;
        $menu = collect();

        if ($deepness > 10) {
            // dump('a?');
            return [];
        }

        foreach ($tree as $menuId) {
            $children = null;

            if ($this->hasChildren($menuId)) {
                $children = $this->__recursiveMenuMapper($this->getItems($menuId), $deepness);
            }

            $item = collect([
                // 'redirect' => $this->getUrl($menuId),
                // 'external' => $this->getUrl($menuId),
                'path'     => $this->getSlug($menuId),
                'id'       => $menuId,
                'name'     => $this->getName($menuId),
            ]);

            if ($children) {
                $item->put('children', $children);
            }

            $menu->push($item);
        }

        return $menu;
    }

    public function hasItems($itemLevel) {
        if ($this->getType($itemLevel) === 'category') {
            return true;
        }

        return count($this->tree[$itemLevel]??[]);
    }

    public function getItemId($routeParam, $type = "menuid", $strict = true)
    {
        $slugs = array_map(
            function ($a) use ($routeParam, $strict) {
                if($strict) {
                    return (__($a->slug) == $routeParam && !($a->parent ?? false));
                }
                return __($a->slug) == $routeParam;
            },
            $this->menuItems
        );

        $result = array_search(true, $slugs);

        if($type=='menuid') {
            return $result;
        }
        return $this->menuItems[$result]->id??"";
    }

    public function getMenuImage($item = '') { //AK edits
        return $this->menuItems[$item]->image ?? '';
    }

    public function getMenuOwnerId($item = '') { //AK edits
        return $this->menuItems[$item]->owner_id ?? '';
    }

    public function getDropdownMenuImage($item = '') {
        return $this->menuItems[$item]->dropdownMenuImage ?? '';
    }

    public function hasChildren($item)
    {
        return count($this->tree[$item] ?? []) > 0;
    }

    public function getName($item)
    {
        return __($this->menuItems[$item]->name);
    }

    public function getSlug($item)
    {
        return __($this->menuItems[$item]->slug);
    }

    public function getUrl($item, $withDomain = true)
    {
        if($item==0) { return r('url', 'search'); }

        $menuItem = $this->menuItems[$item];

        /**
         * @author Guntis Šulcs <guntis@brandbox.digital>
         * I have no idea why hierachy should start from 7.. so if
         * item does not hav eparent then i forsed to 1
         */
        if ($menuItem->parent == null) {
            $menuItems = $this->getHierarchy($menuItem, 1);
        } else {
            $menuItems = $this->getHierarchy($menuItem);
        }

        if (!$withDomain) {
            $baseUrl = r("url");
            $menuUrl = r("url", $menuItems);

            return str_replace($baseUrl, '', $menuUrl);
        }

        return r("url", $menuItems);
    }

    public function getType($item)
    {
        return $this->menuItems[$item]->urlType;
    }

    private function getHierarchy($item, $level = 7)
    {

        if (!($item->slug ?? false)) return [];
        $nextLevel = $level - 1;

        return array_merge(["slug$level" => __($item->slug)], $this->getHierarchy($this->menuItems[$item->parent] ?? [], $nextLevel));
    }

    public function getCurrentId() {
        $x=8;
        do {
            $x--;
        } while(request()->route('slug'.$x)==null || $x==0);

        return $this->getItemId(request()->route('slug'.$x), 'menuid', false);
    }

    public function getCurrentCatId() {
        $x=8;
        do {
            $x--;
        } while(request()->route('slug'.$x)==null || $x==0);
        return $this->getItemId(request()->route('slug'.$x), 'catId', false);
    }
}
