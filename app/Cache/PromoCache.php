<?php

namespace App\Cache;


use App\Plugins\Sales\Model\Sale;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CacheController;

class PromoCache
{
    public $userGroups = [];
    public $category = [];
    public $product = [];
    public $promotions = [];
    public $promotionNames = [];
    public $promo;

    public function __construct()
    {
        // /** @var Sale $promo */
        // $promo = Sale::where(function (Builder $q) {
        //         $q->where('valid_to', '>=', Carbon::now())
        //             ->orWhereNull('valid_to');
        //     })
        //     ->where('valid_from', '<=', Carbon::now())
        //     ->get();

        // $this->promo = $promo;

        $promo = $this->getPromo();

        $this->promotions = $promo->pluck('amount', 'id')->toArray();
        $this->promotionNames = $promo->pluck('name', 'id')->toArray();

        foreach ($promo as $promoItem) {
            if (is_array($promoItem->user_group)) {
                foreach ($promoItem->user_group as $user_group) {
                    if (!($this->userGroups[$user_group]??false)) {
                        $this->userGroups[$user_group] = [];
                    }
                    $this->userGroups[$user_group][] = $promoItem->id;
                }
            }

            $targets = json_decode($promoItem->getOriginal()['discount_target']);

            if (is_array($targets)) {
                $discount_target = $promoItem->discount_to;

                foreach ($targets as $target) {
                    if (!($this->$discount_target[$target]??false)) {
                        $this->$discount_target[$target] = [];
                    }
                    $this->$discount_target[$target][] = $promoItem->id;
                }
            }

            // dd($targets, $promoItem, $this);
        }


    }

    public function getPromo()
    {
        /** @var Sale $promo */
        $promo = Sale::where(function (Builder $q) {
            $q->where('valid_to', '>=', Carbon::now())
                ->orWhereNull('valid_to');
        })
        ->where('valid_from', '<=', Carbon::now())
        ->get();

        return $promo;
    }

    public function getDiscount($target, $id, $ugroup = false) {
        return array_only($this->promotions, $this->hasDiscount($target, $id, $ugroup));
    }

    public function hasDiscount($target, $id, $userGroup) {
        if (empty($this->userGroups)) {
            $this->setCurrentUserGroupToAllPromotions($userGroup);
        }
        return array_intersect(($this->$target[$id]??[]), ($this->userGroups[$userGroup]??[]));
    }

    public function setCurrentUserGroupToAllPromotions($userGroup)
    {
        $promo = $this->getPromo();

        foreach ($promo as $promoItem) {
            $this->userGroups[$userGroup][] = $promoItem->id;
         }
    }

    /**
     * Extend original function.. now second par can be also array..
     *
     * @param [type] $t
     * @param [array|int] $d
     * @param [type] $u
     *
     * @author Guntis Šulcs <guntis@brandbox.digital>
     */
    public function getKeyValuePair($t, $d, $u)
    {
        try {

            //code...
        if ($d instanceof Collection) {
            $d = $d->toArray();
        }

        if (is_array($d) ) {
            $result = [];
            foreach ($d as $id) {
                $result[] = $this->_getKeyValuePair($t,$id,$u);
            }

            return collect($result)
                ->flatten()
                ->unique()
                ->values()
                ->toArray();

        } else {
            return $this->_getKeyValuePair($t,$d,$u);
        }

        } catch (\Throwable $th) {
            dd($th);
        }
    }

    /**
     * Original function getKeyValuePair() function.. see few lines up
     */
    public function _getKeyValuePair($t, $d, $u) {

        $disc = $this->getDiscount($t,$d,$u)?:[0 => 0];

        $discMax = max($disc);
        $discMaxKey = array_search($discMax, $disc);

        return [$discMaxKey => $discMax];
    }

}
