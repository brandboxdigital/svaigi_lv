<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paysera extends Model
{
	protected $table = 'paysera';

	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public $casts = [
        'response_data'   => 'array',
        // 'request_data'    => 'array',
        'log'             => 'array',
    ];
}
