<?php

namespace App\Http\Controllers;


use App\User;
use App\BaseModel;
use Carbon\Carbon;
use \Mailjet\Resources;
use App\BerniSaimniekoAnalytics;
use App\Plugins\Blog\Model\Blog;
use App\Plugins\Pages\Model\Page;
use App\Plugins\Sales\Model\Sale;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Plugins\Banners\Model\Banner;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cookie;
use App\Plugins\Products\Model\Product;
use App\Plugins\Orders\Model\OrderLines;
use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Suppliers\Model\Supplier;
use Illuminate\Database\Eloquent\Builder;
use App\Plugins\Blog\Model\BlogCategories;
use App\Plugins\Categories\Model\Category;
use App\Plugins\Products\Model\ProductMeta;
use App\Plugins\Categories\Model\CategoryMeta;
use App\Plugins\Orders\Functions\CartFunctions;
use App\Plugins\Featured\Model\FeaturedSupplier;

/**
 * Class FrontController
 *
 * @package App\Http\Controllers
 */
class FrontController extends Controller
{

    use CartFunctions;

    private $slugPath;


    public function clearCart() {
        $cart = $this->getCart();
        $cart->items()->delete();

        // $cart->update([
        //    'delivery_id' => null,
        //    'delivery_amount' => null
        // ]);
        $cart->delete();

        return redirect()->back();
    }

    public function cartHasItems() {
        return $this->getCart()->items()->count()?"has-items":"";
    }

    /**
     * Open page - detected by cache
     *
     * @return mixed
     */
    public function divert()
    {
        session()->flash("cu", (Auth::user() ?? User::find(99)));

        $cache = (new CacheController)->getSlugCache();

        $action = $cache->findAction();

        if (!($action ?? false)) abort(404);

        $this->removeDeliveryFromCart();

        $this->slugPath = $cache->getSlugs();

        return $this->{$action['action']}($action['slug'], $action['id']);
    }

    public function allProducts()
    {
        session()->flash("cu", (Auth::user() ?? User::find(99)));

        $cache = (new CacheController)->getSlugCache();

        // dd($action);

        $this->slugPath = $cache->getSlugs();
        return $this->showCategory('all');
    }

    public function search()
    {
        $this->showCategory("search", 0);
    }

    /**
     * Show Category products
     *
     * @param $categorySlug
     * @param $categoryId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCategory($categorySlug, $categoryId = null)
    {
        session()->put('lastCategory', request()->route()->parameters);
        $filters = false;
        $slug_category = null;
        $popularProducts = null;

        // dd($categoryId);
        if ($categorySlug != 'search') {


            $currentCategoryId = $categoryId;

            /**
             * @var \App\Plugins\Categories\Model\Category $category
             *
             * This is the category found in slug
             */
            $slug_category = Category::find($categoryId);

            /**
             * There are some rear cases where $slug_category does not exist...
             * ..so try different way to get it
             *
             * @todo find out why these cases happen
             */
            if (!$slug_category) {
                $meta = CategoryMeta::where([
                        "meta_value" => $categorySlug,
                        'meta_name' => 'slug'
                    ])
                    ->firstOrFail();

                $slug_category = optional($meta)->category;
            }

            /**
             * if it has parents then show the category that link points to
             */
            if ($slug_category->parent) {
                $category = $slug_category;

            }
            /**
             * If no parent then show first child category. Also in this case show popular products
             */
            else if ($slug_category->descendants->first()) {
                // $category = $slug_category->descendants->first();

                /**
                 * Updated to get as first gatavi-produktu-grozi
                 */
                if ($slug_category->unique_name === 'partika') {
                    $category = Category::where('unique_name', 'gatavi-produktu-grozi')->get()->first();
                } else {
                    $category = count($slug_category->products()->get()) ? $slug_category : $slug_category->descendants->first();
                }


                // $cat_ids = array_merge([$slug_category->id], $slug_category->descendants->pluck('id')->toArray());
                // $popularProducts = Product::whereHas('extra_categories', function($q) use ($cat_ids) {
                //     $q->whereIn('id', $cat_ids);
                // })
                //     ->whereHas('market_days', function (Builder $q) {
                //         $md = (new CacheController)->getSelectedMarketDay();
                //         // dd($md);

                //         return $q->where('market_day_id', $md->id);
                //     })
                //     ->orderBy('times_bought')
                //     ->take(6)
                //     ->get();
            }

            /**
             * If that does not exist, then again show currently selectect category by url
             */
            else {
                $category = $slug_category;
            }


            /** @var \App\Plugins\Products\Model\Product $products */
            if ($category) {
                $cat_ids = array_merge([$category->id], $category->descendants->pluck('id')->toArray());
                // $products = $category->products()
                $products = Product::whereHas('extra_categories', function($q) use ($cat_ids) {
                        $q->whereIn('id', $cat_ids);
                    })
                    // ->orWhereHas('main_cat', function($q) use ($cat_ids) {
                    //     $q->whereIn('id', $cat_ids);
                    // })
                    ->whereHas('market_days', function (Builder $q) {
                        $md = (new CacheController)->getSelectedMarketDay();
                        // dd($md);

                        return $q->where('market_day_id', $md->id);
                    });
            } else {
                $products = Product::whereHas('market_days', function (Builder $q) {
                    $md = (new CacheController)->getSelectedMarketDay();
                    // dd($md);

                    return $q->where('market_day_id', $md->id);
                });
            }


            if (session()->get('filters', null)) {
                $filters = session()->get('filters');
            }

            if (request()->input('supplier')) {
                $filters['suppliers'] = collect(request()->input('supplier'))->toArray();
                session()->put('filters', $filters);
            }

            /**
             * There are some cases where error happens when category does not exist... :/
             */
            if (!isset($filters['category']) && $category != null) {
                // $filters['category'] = $category->id;
                // session()->put('filters', $filters);
            }



            // if (($filters['filters'] ?? false) && $filters['category'] == $category->id) {
            if (($filters['filters'] ?? false) ) {
                $products = $products->whereHas('attributeValues', function (Builder $q) use ($filters) {
                    $q->whereIn('attribute_value_id', $filters['filters']);
                });
                $filters = true;
            }

            // if (($filters['suppliers'] ?? false) && $filters['category'] == $category->id) {
            if (($filters['suppliers'] ?? false) ) {
                $products = $products->whereIn('supplier_id', $filters['suppliers']);
            }

            /**
             * Put produktu-grozi as first item in collection
             */
            $slug_category->children = $slug_category->children->sortBy(fn($v) => $v->getAttributes()['unique_name'] !== 'produktu-grozi');

            // dd($filters, request()->input('supplier'));

            $banners = ($slug_category)
                ? $slug_category->banners()->whereRaw("'".Carbon::now()."' between dateFrom and dateTo")->get()
                : collect();

            $banners = $banners->merge(Banner::whereDoesntHave('categories')->whereRaw("'".Carbon::now()."' between dateFrom and dateTo")->get());

        } else {
            $products = new Product();

            $currentCategoryId = request()->get('category_id');
            if ($currentCategoryId) {
                $category = Category::find($currentCategoryId);
                $slug_category = Category::find($currentCategoryId);
                $banners = null;
            }

            if ($searchString = request()->get('search')) {
                $products = $products
                    ->whereIn('supplier_id', array_keys(preg_grep('/' . $searchString . '/i', __('supplier.name'))))
                    ->orWhereHas('metaData', function (Builder $q) use ($searchString) {
                        $q->where('meta_value', 'like', "%$searchString%")->where('language', language())->whereIn('meta_name', ['name'])->where('state', 1);
                    })
                    ->orWhereHas('extra_categories', function(Builder $q) use($searchString) {
                        $q->whereIn('category_id', array_keys(preg_grep("/$searchString/i", __('category.name'))))->where('state', 1);
                    });
            }
        }

        $products = $products->select('products.*')->where('state', 1);
        switch (request()->get('order')) {
            case "svaigi_order":
            default:
                $products = $products->addSelect(DB::raw('product_metas.meta_value as metaName'))->join('product_metas', function ($join) {
                    $join->on('products.id', '=', 'product_metas.owner_id')
                        ->where('meta_name', '=', 'name');
                })->orderBy('metaName', 'asc');

                // $products = $products->orderBy('sequence', 'desc');
                break;

            case "name_asc":
                $products = $products->addSelect(DB::raw('product_metas.meta_value as metaName'))->join('product_metas', function ($join) {
                    $join->on('products.id', '=', 'product_metas.owner_id')
                        ->where('meta_name', '=', 'name');
                })->orderBy('metaName', 'asc');
                break;

            case "name_desc":
                $products = $products->addSelect(DB::raw('product_metas.meta_value as metaName'))->join('product_metas', function ($join) {
                    $join->on('products.id', '=', 'product_metas.owner_id')
                        ->where('meta_name', '=', 'name');
                })->orderBy('metaName', 'desc');
                break;

            case "price_asc":
                $products = $products->addSelect(DB::raw('(cost*(1+mark_up/100))*(1+vats.amount/100) as price'))->join('vats', function ($join) {
                    $join->on('products.vat_id', '=', 'vats.id');
                })->orderBy('price', 'asc');
                break;

            case "price_desc":
                $products = $products->addSelect(DB::raw('(cost*(1+mark_up/100))*(1+vats.amount/100) as price'))->join('vats', function ($join) {
                    $join->on('products.vat_id', '=', 'vats.id');
                })->orderBy('price', 'desc');
                break;

            case "popularity":
                $products = $products->orderBy('times_bought', 'desc');
                break;

        }

        /**
         * Get pagination object and get product list that is used in frionted
         */
        $prodQuery = $products
            ->where(function($q) {
                $q->where('storage_amount', '>', 0)->orWhereNull('storage_amount');
            });
        $allProducts = clone $prodQuery;

        $pagination = $prodQuery->paginate(20);
        if (isset($searchString)) {
            $pagination->appends(['search' => $searchString]);
        }
        // dd($pagination);
        $allProducts = $allProducts->get()->pluck('storage_amount', 'id')->toArray();

        $products   = collect($pagination->items())->pluck('storage_amount', 'id')->toArray();
        // $pagination = $pagination->toArray();
        // $products = (object)array_unique($products);

        /**
         * Get only ID friom table
         */
        $suppliers = Supplier::select('id')->get()->pluck('id')->toArray();
        // $suppliers = Supplier::all()->pluck('id');

        $categoryPath = $this->slugPath;


        $meta = $this->getMeta($category);
        $breadcrumbs = $slug_category->getBreadcrumbs();

        if ($categorySlug === 'piedavajumi') {

            $salesProducts = [];
            $date = Carbon::now();
            $categorySales = Sale::where('discount_to', 'category')->where('valid_from', '<=', $date)->where('valid_to', '>=', $date)->get()->pluck('discount_target_id')->all();

            foreach ($categorySales as $sale) {
                foreach ($sale as $id) {
                    $salesCategory = Category::find($id);
                    foreach ($salesCategory->products()->get() as $p) {
                        $salesProducts[] = $p;
                    }
                }
            }

            $productSales = Sale::where('discount_to', 'product')->where('valid_from', '<=', $date)->where('valid_to', '>=', $date)->get()->pluck('discount_target_id')->all();
            foreach ($productSales as $sale) {
                foreach ($sale as $id) {
                    $salesProducts[] = Product::find($id);
                }
            }

            $pagination = new Paginator($salesProducts, 20);
            $pagination->setPath(url('/piedavajumi'));

            $products = collect($pagination->items())->pluck('storage_amount', 'id')->toArray();

        }

        $activeNavSlug = request()->route()->parameters['slug1'];
// dd(request()->route()->parameters['slug1']);
        return view("Products::frontend.listitems", compact([
            'category',
            'slug_category',
            'products',
            'suppliers',
            'categoryPath',
            'banners',
            'filters',
            'currentCategoryId',
            'pagination',
            'allProducts',
            'meta',
            'breadcrumbs',
            'activeNavSlug',
        ]));
    }

    /**
     * @return array
     * @param Category $category
     */
    public function getMeta(Category $category): array
    {
        $meta = [];

        $keywords = CategoryMeta::where([
            'owner_id' => $category->id,
            'meta_name' => 'google_keywords'
        ])->first();

        $description = CategoryMeta::where([
            'owner_id' => $category->id,
            'meta_name' => 'google_description'
        ])->first();

        if ($keywords) {
            $meta['keywords'] = $keywords->toArray();
            $meta['title'] = strtoupper($meta['keywords']['meta_value']) . ' | Svaigi.lv';
        }

        if ($description) {
            $meta['description'] = $description->toArray();
        }
// dd($meta);
        return $meta;
    }

    private function getSeoMetadata($model)
    {
        $meta = [
            "title"       => null,
            "description" => ['meta_value' => null],
            "keywords"    => ['meta_value' => null],
        ];

        if ($model instanceof BaseModel) {
            $metaContent = $model->getMetaAttribute();

            foreach ([
                // 'title'       => 'name',
                'description' => 'google_description',
                'keywords'    => 'google_keywords',
            ] as $mappedKeyFrom => $mappedKeyTo) {
                $meta[$mappedKeyFrom]['meta_value'] = $metaContent[$mappedKeyTo][language()] ?? null;
            }

            $meta['title'] = $metaContent['name'][language()] ?? null;
        }

        return $meta;
    }

    public function suplyerProducts($supplier_slug)
    {
        session()->put('lastCategory', request()->route()->parameters);

        $supplier = Supplier::query()
            ->whereHas('products', function($productQuery) {
                return $productQuery->isPublished();
            })
            ->whereHas('metaData', function($q) use ($supplier_slug) {
                return $q->where('meta_value', $supplier_slug)
                        ->where('meta_name', 'slug');
            })
            ->firstOrFail();

        $productsQuery = Product::whereHas('market_days', function (Builder $q) {
            $md = (new CacheController)->getSelectedMarketDay();
            // dd($md);

            return $q->where('market_day_id', $md->id);
        });

        $products = $productsQuery
            ->isPublished()
            ->where('supplier_id', $supplier->id);



        /**
         * Get pagination object and get product list that is used in frionted
         */
        $pagination = $products
            ->where('state', 1)
            ->where(function($q) {
                $q->where('storage_amount', '>', 0)->orWhereNull('storage_amount');
            })
            ->paginate(20);

        $products   = collect($pagination->items())->pluck('storage_amount', 'id')->toArray();

        $backLink = "/saimniecibas/$supplier_slug";


        // return view("Products::frontend.suppliersList", compact(['category', 'products', 'suppliers', 'categoryPath', 'banners', 'filters', 'currentCategoryId', 'pagination']));
        return view("Products::frontend.suppliersList", compact([
            'products',
            'pagination',
            'supplier',
            'backLink',
            // 'currentCategoryId',
            // 'banners',
            // 'category',
            // 'suppliers',
        ]));
    }

    /**
     * Show Product opening
     *
     * @param $productSlug
     * @param $productId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProduct($productSlug, $productId)
    {
        $product = (new CacheController)->getProduct($productId);

        $suppliers = Supplier::all()->pluck('id');
        $category = Category::findOrFail($product->getData('mainCategory'));

        $categoryPath = $this->slugPath;
        $productAmount = Product::findOrFail($productId)->storage_amount;
        $metaArr = ProductMeta::where('owner_id', $product->id)->where('meta_name', 'name')->get()->first()->toArray();
        $meta['title'] = isset($metaArr['meta_value']) ? $metaArr['meta_value'] : '';

        return View("Products::frontend.product", compact(['product', 'suppliers', 'categoryPath', 'category', 'productAmount', 'meta']));
    }

    /**
     * Get current category product id's
     *
     * @return mixed
     */
    public function getItems()
    {
        list($cat1, $cat2, $cat3) = [request()->route('category1'), request()->route('category2'), request()->route('category3')];
        $products = CategoryMeta::where(["meta_value" => $cat3 ?: $cat2 ?: $cat1, 'meta_name' => 'slug'])->firstOrFail()->category->products()->pluck('id')->toArray();

        return $products;
    }

    /**
     * Get cart total value
     *
     * @return object
     */
    public function getCartTotals()
    {
        return getCartTotals($this->getCart());
    }

    /**
     * Get cart items
     *
     * @return mixed
     */
    public function getCartItems()
    {
        return $this->getCart()->items;
    }

    /**
     * Get current page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function homepage()
    {
        if (!pageTable()) {
            abort(404);
        }
        $homepageId = Page::where("homepage", 1)->first()->id ?? 0;
        $slug = __("pages.slug.$homepageId");

        return (new PageController)->show($slug);
    }

    public function redrawCart()
    {
        /** @var OrderHeader $cart */
        $cart = $this->getCart(true);

        /** @var OrderLines $item */
        foreach ($cart->items as $item) {

            $lineData = ['id' => $item->id];

            $product = $this->cache()->getProduct($item->product_id);
            $variation = $product->getVariationPrice($item->variation_id);

            $itemData = [
                'supplier_id'    => $product->supplier_id,
                'supplier_name'  => __('supplier.name.' . $product->supplier_id),
                'product_id'     => $product->id,
                'product_name'   => __("product.name.{$product->id}"),
                'vat_id'         => $product->price->vat_id,
                'vat_amount'     => $product->price->vat,
                'vat'            => $variation->vat,
                'price'          => $variation->price,
                'display_name'   => $variation->display_name,
                'variation_size' => $variation->size,
                'discount'       => (Auth::user() ?? User::find(99))->discount(),
                'variation_id'   => $item->variation_id,
            ];
            $item->updateOrCreate($lineData, $itemData);
        }

        return $cart->id;
    }

    public function showImage($folder, $size, $image = null)
    {

        $cr = Route::currentRouteName();

        if ($cr === 'image') {
            $path = implode("/", ["public", $folder, $size, $image]);
        } elseif ($cr === 'image.nopath') {
            $path = implode("/", ["public", $folder, $image]);
        } else {
            //abort(404);
            $path = "public/pixel.png";
        }

        if (!\Storage::exists($path)) {
            $path = "public/pixel.png";
            //abort(404);
        }
        return response()->file(storage_path("app/$path"));
    }

    public function getFeaturedSupplier()
    {
        return FeaturedSupplier::inRandomOrder()->first();
    }

    public function showBlog($blogCategory = null, $blogItem = null)
    {

        $blogId     = array_search($blogItem, __('posts.slug'));
        $categoryId = array_search($blogCategory, __('postcategory.slug'));

        if($blogId && $categoryId) {

            $blogItem = Blog::where(['id' => $blogId, 'main_category' => $categoryId])
                ->first();

            $this->special_countUsersBerniSaimnieko($blogItem);

            if ($blogItem) {
                $toMatchCategoryId = array_search('berni-saimnieko', __('postcategory.slug'));
                $toMatchCategories = BlogCategories::descendantsAndSelf($toMatchCategoryId);

                $toMatchIds = $toMatchCategories
                        ->pluck('id')
                        ->unique();

                $postIds    = $blogItem
                    ->extra_categories
                    ->pluck('id')
                    ->push($blogItem->main_category ?? null)
                    ->filter()
                    ->unique();

                $intersect = $toMatchIds->intersect($postIds);

                $isBerniSaimniekoCat = $intersect->count() > 0;
            }

            return view('frontend.pages.blog_item', [
                'meta'       => $this->getSeoMetadata($blogItem),
                'item'       => $blogItem,
                'categories' => BlogCategories::get(),

                'isBerniSaimniekoCat' => $isBerniSaimniekoCat ?? false,
            ]);
        }

        $blogCategories = new BlogCategories;
        $blog           = new Blog;

        if($blogCategory) {
            $blog = $blog->where('main_category', $categoryId);
        } else {
            $blog = $blog
                ->whereHas('extra_categories', function($categoryQuery) {
                    return $categoryQuery->where('exclude_category_from_main_list', false);
                })
                ->whereHas('main_category_model', function($categoryQuery) {
                    return $categoryQuery->where('exclude_category_from_main_list', false);
                });
        }

        $pagination = $blog->paginate(20);
        $items = $pagination->items();

        return view('frontend.pages.blog', [
            'categories'        => $blogCategories->get(),
            'items'             => $items,
            'pagination'        => $pagination,
            'currentCategoryId' => $categoryId?:false,
            'currentCategory'   => BlogCategories::find($categoryId),
        ]);
    }

    public function getHighlightedPosts() {
        $postCount = 3;

        return Blog::where('is_highlighted', 1)
            ->whereHas('extra_categories', function($categoryQuery) {
                return $categoryQuery->where('exclude_category_from_main_list', false);
            })
            ->whereHas('main_category_model', function($categoryQuery) {
                return $categoryQuery->where('exclude_category_from_main_list', false);
            })
            ->limit($postCount)->get();

        return Blog::inRandomOrder()->where('is_highlighted', 1)->limit($postCount)->get();
    }

    /**
     * hides banner when clicked Close (based on frequency)
     *
     * @param Banner $banner
     *
     * @return mixed
     */
    public function hideBanner(Banner $banner) {
        switch($banner->frequency) {
            case "once_a_week":
                Cookie::queue('banner'.$banner->id, "1", 10080);
                return response('Done');
            break;

            case "once_per_session":
                session()->put('banner'.$banner->id, '1');
                session()->save();
                return response('Done');
                break;
            default:
                return response('Done');
                break;
        }
    }

    public function getSupplierListInCategory($items) {

        if($items) {
            return array_unique(Product::whereIn('id', array_keys($items))->pluck('supplier_id')->toArray());
        }
        return [];
    }

    /**
     * Subscribe for newsletter
     *
     * @return \Illuminate\View\View
     */
    public function confirmNewsletter()
    {
        $params = request()->route()->parameters;
        $user = User::find($params['user']);
        $user->newsletter = true;
        $user->save();

        $this->createMailjetNewsletterSubscriber($user);

        return redirect('/');
    }

    public function createMailjetNewsletterSubscriber($user)
    {
        $mj = new \Mailjet\Client(env('MAILJET_PUBLICKEY'), env('MAILJET_PRIVATEKEY'), true, ['version' => 'v3']);

        $contactBody = [
          'IsExcludedFromCampaigns' => "false",
          'Name' => $user->name,
          'Email' => $user->email
        ];
        $contactResponse = $mj->post(Resources::$Contact, ['body' => $contactBody]);

        if ($contactResponse->success()) {
          $contact = $contactResponse->getData();
          $body = [
            'IsUnsubscribed' => "false",
            'ContactID' => $contact[0]['ID'],
            'ContactAlt' => $user->email,
            'ListID' => env('MAILJET_GOOG_CONTACT_LIST_ID', 5353),
            // 'ListAlt' => "test contact list"
          ];
          $response = $mj->post(Resources::$Listrecipient, ['body' => $body]);
        }
    }


    private function special_countUsersBerniSaimnieko($blogItem)
    {
        $toMatchCategoryId = array_search('berni-saimnieko', __('postcategory.slug'));
        $toMatchCategories = BlogCategories::descendantsAndSelf($toMatchCategoryId);

        if ($toMatchCategories->count() < 1 || !$blogItem instanceOf Blog) {
            return;
        }

        $toMatchIds = $toMatchCategories
            ->pluck('id')
            ->unique();

        $postIds    = $blogItem
            ->extra_categories
            ->pluck('id')
            ->push($blogItem->main_category ?? null)
            ->filter()
            ->unique();

        $intersect = $toMatchIds->intersect($postIds);

        if ($intersect->count() < 1) {
            return;
        }

        try {
            $url = url()->full();
        } catch (\Throwable $th) {}

        try {
            $user = Auth::user();

            if ($user) {
                BerniSaimniekoAnalytics::countUserPageview($user, $url ?? 'unknown');
            }
        } catch (\Throwable $th) {}
    }

    private function removeDeliveryFromCart()
    {
        $cart = $this->getCart();

        if ($cart) {

            $cart->update([
                'delivery_amount' => 0,
                'delivery_id'     => null
            ]);
        }
    }
}
