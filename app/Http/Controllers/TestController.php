<?php

namespace App\Http\Controllers;


use App\Plugins\Banners\Model\Banner;
use App\Plugins\Blog\Model\Blog;
use App\Plugins\Blog\Model\BlogCategories;
use App\Plugins\Categories\Model\Category;
use App\Plugins\Categories\Model\CategoryMeta;
use App\Plugins\Featured\Model\FeaturedSupplier;
use App\Plugins\Orders\Functions\CartFunctions;
use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Orders\Model\OrderLines;
use App\Plugins\Pages\Model\Page;
use App\Plugins\Products\Model\Product;
use App\Plugins\Suppliers\Model\Supplier;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegistered;
use App\Mail\OrderSender;

/**
 * Class FrontController
 *
 * @package App\Http\Controllers
 */
class TestController extends Controller
{
    private $menuItems = [];

    public function index() {



        // Mail::to('adx@adx.lv')->send(new UserRegistered(\App\User::find(1)));

        $model = Supplier::find(1);

        Mail::to('adx@adx.lv')->send(new OrderSender([], $model));

        return 'Y';
        // print($this->roundPrice(1.57));


        // $categories = (new \App\Http\Controllers\CacheController())->getCategoryCache();
        // // foreach ($menuItems->menuItems ?? [] as $item) {
        // //     $this->menuItems[$item->id] = (object)[
        // //         'slug'    => "{$item->menu_owner}.slug.{$item->owner_id}",
        // //         'name'    => "{$item->menu_owner}.name.{$item->owner_id}",
        // //         'parent'  => $item->frontend_menu_item_id,
        // //         'urlType' => $item->menu_owner,
        // //         'id'      => $item->owner_id,
        // //         'image'   => $categories->image('1473x247', $item->owner_id),
        // //         'dropdownMenuImage' => $categories->dropDownMenuImage('1473x247', $item->owner_id)
        // //     ];
        // // }

        // // dd($categories);
        // // dd($this->menuItems);

        // $directories = \Storage::allDirectories("public");
        // echo "<pre>";
        // print_r($directories);
        // echo "</pre>";

        // if(\Storage::exists("/public/categories/1473x247/mjwwdTk22xeYrZfzhju81SXQbHnz39wabr2ZsS9A.jpeg")) {
        //     echo "Exist";
        // }
        // else {
        //     echo "Not exist";
        // }

        // dd($categories->dropDownMenuImage('1473x247', 1));
    }

    private function roundPrice($price)
    {
        $modulo = fmod($price, 0.05);

        if ($modulo != 0) {
            $price = $price + (0.05 - $modulo);
        }

        return $price;
    }

}
