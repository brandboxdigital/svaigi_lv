<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Profile;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\NewsletterConfirmation;

use App\Mail\UserRegistered;
use Illuminate\Support\Facades\Mail;

/**
 * Class RegisterController
 *
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @return string
     */
    public function redirectTo() {
        return r('page');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm()
    {
//        $exist = true;

        $newUser = session()->get('fbuser');

        $user = new User($newUser??[]);

        return view('auth.register', ['user' => $user, "pageTitle" => _t('translations.registerForm')]);
    }

    public function register(Profile $request)
    {

        $user = (new User)->updateUser($request->all());

        event(new Registered($user));

        $this->guard()->login($user);

        $result = Mail::to($user->email)->send(new UserRegistered($user));

        if ($request->input('newsletter')) {
            Mail::to($user->email)->send(new NewsletterConfirmation($user));
        }

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }


}
