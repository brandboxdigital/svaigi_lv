<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;
use Newsletter;

class EmailSubscriptionController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
    
        if ($validator->fails()) {
            $errors = $validator->errors();
            $emailErrors = implode('; ', $errors->get('email'));

            return response()->json([
                'result' => 'error',
                'message' => $emailErrors
            ], 422);
        }
    
        $email = $request->email;

        if (Newsletter::hasMember($email)) {
            return response()->json([
                'result' => 'success',
                'message' => __('emailSubscription.alreadySubscribed')
            ]);
        }
        else {
            $result = Newsletter::subscribe($email);

            if ($result) {
                return response()->json([
                    'result' => 'success',
                    'message' => __('emailSubscription.subscribed'),
                ]);
            }
            else {
                return response()->json([
                    'result' => 'error',
                    'message' => __('emailSubscription.somethingHappend')
                ], 422);
            }
        }
    }
}
