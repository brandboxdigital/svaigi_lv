<?php

namespace App\Http\Controllers;


use App\EmailChange;
use App\Http\Requests\Profile;
use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Orders\Functions\Orders;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Route;
use Newsletter;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Functions\General;

class ProfileController extends Controller
{

    use Orders;
    use General;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $user = Auth::user();

        if (!$user->password) {
            dd('a?');
            return view('frontend.pages.setpassword', [
                'user' => Auth::user(),
                'pageTitle' => _t('translations.profile')
            ]);
        }

        return view('frontend.pages.profile', ['user' => Auth::user(), 'pageTitle' => _t('translations.profile')]);
    }

    public function store(Profile $request)
    {
        $request->validated();

        $emailUser = User::where(['email' => request()->get('email')])->first();

        // If Email Changes
        if (Auth::user()->email != request()->get('email')) {
            $changes = request()->all();
            $changes['is_legal'] = $changes['is_legal']??0;
            $changes['newsletter'] = $changes['newsletter']??0;
            // If there is unregistered user with changed email
            if ($emailUser && $emailUser->id != Auth::user()->id) {
                $joinUser = $emailUser->id;
            }

            EmailChange::create([
                'invokedBy'    => Auth::user()->id,
                'changes'      => $changes,
                'joinUser'     => $joinUser ?? null,
                'verifyString' => str_random(60),
            ]);
        } else {
            $changes = request()->all();
            $rn = explode(".", Route::currentRouteName());
            if($rn[0] == "profile" && !Auth::user()->registered)
                $changes['registered'] = 1;

            $changes['is_legal'] = $changes['is_legal']??0;
            $changes['newsletter'] = $changes['newsletter']??0;
            Auth::user()->update($changes);

            if($request->input('rules')){
                Newsletter::subscribe(Auth::user()->email);
            }
        }

        \Session::flash('message', ["msg"=>_t('translations.data_saved'),"isError"=>false]);
        return redirect()->route('profile.default');
    }

    public function savePassword(Request $request)
    {
        $user = Auth::user();

        if ($user->password) {
            abort(404);
        }

        $data = array_merge([
            "id" => $user->id,
        ], $request->all());

        $user->updateUser($data);

        return redirect()->route('profile.default');
    }

    public function verify($action, $string)
    {
        if ($action == 'emailchange') {
            return $this->verifyChange($string);
        }

        return $this->verifyRegister();
    }

    public function verifyChange($verifyString)
    {

        /** @var EmailChange $changes */
        $changes = EmailChange::where(['verifyString' => $verifyString])->first();
        if ($changes && $changes->state=='sent') {
            /** @var User $usr */
            $usr = User::find($changes->invokedBy);
            if ($usr) {
                if($changes->joinUser) {
                    $this->rewriteOrders($changes->invokedBy, $changes->joinUser);
                }
                $result = $usr->update($changes->changes);
                if ($result) {
                    $changes->update(['state' => 'verified']);
                    if (!Auth::user()) {
                        Auth::login($usr->id);
                    }
                    return redirect()->route('profile.default');
                }
            }
        } elseif($changes && $changes->state=='verified') {
            return "Changes have been already verified";
        } else {
            return "Changes have not been found!";
        }
        return false;
    }

    private function rewriteOrders($currentUser, $userToRemove) {
        /** @var User $cu */
        $cu = User::find($currentUser);
        /** @var User $ru */
        $ru = User::find($userToRemove);

        foreach($ru->cart()->where('state', '!=', 'draft')->get()??[] as $order) {
            $order->update(['user_id', $cu->id]);
        }

        $ru_openCart = $ru->cart()->where('state', 'draft')->first();
        $cu_openCart = $cu->cart()->where('state', 'draft')->first();

        $cu->increment('order_count', $ru->order_count);

        if($ru_openCart->items()->count()>0) {
            $ru_openCart->items()->update('order_header_id', $cu_openCart->id);
        }
        $ru_openCart->delete();
        $ru->delete();
    }

    public function verifyRegister()
    {
        // dd(request()->all());
    }

    /**
     * @param bool $id
     * @param bool $page
     *
     * @throws NotFoundHttpException
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history($id = false, $page = false) {

        if($id && $id!=__('translations.urlPage')) {
            return $this->showHistoryItem($id);
        }

        if(!$page || (int)$page==$page) {
            /** @var Builder $o */


            $o = Auth::user()->orders();

            /** @var LengthAwarePaginator $orders */
            if($page) {
                $orders = $o->paginate(10, ['*'], 'lapa', $page);
            } else {
                $orders = $o->paginate(10);
            }
            return view('frontend.pages.history', ['orders' => $orders, 'pageTitle' => _t('translations.orderHistory')]);
        }
        abort(404);
        return null;
    }

    private function showHistoryItem($orderId) {

        /** @var Builder $o */
        $o = Auth::user()->orders($orderId);

        /** @var OrderHeader $order */
        $order = $o->first();

        if(!$order) {
            abort(404);
        }

        $orderItems = $order->items()->get();

        return view('frontend.pages.historyItem', [
            'order' => $order,
            'items' => $orderItems,
            'pageTitle' => view('frontend.partials.orderHistoryTitle', ['data' => $order->toArray()])->render(),
            'displayValues' => $this->getDisplayValuesOrderInfo($order, $orderItems),
            'totalPrices' => getCartTotals($order, [], $original??false),
        ]);
    }

    public function favouriteProucts()
    {
        $user = Auth::user();

        return view('frontend.pages.favouriteProducts', [
            'favouriteProducts' => $user->favouriteProducts,
            'pageTitle' => _t('translations.favouriteProducts')
        ]);
    }

    public function supplierProfile()
    {
        $user = Auth::user()->load('supplier.metaData');

        if (!$user->supplier) {
            return abort(404);
        }

        // dd($user->supplier->toArray());

        return view('frontend.pages.supplierprofile', [
            'supplier' => $user->supplier,
            'pageTitle' => "Informācija par Saimniecību"
        ]);
    }

    public function supplierProfileStore(Request $request)
    {
        $fileresult = null;
        $user = Auth::user()->load('supplier.metaData');

        if (!$user->supplier) {
            return abort(404);
        }

        if ($request->hasFile('supplier_image')) {
            $request->request->add([
                'path' => 'supplier_image',
                'owner' => 'supplier_image'
            ]);

            $fileresult = (new \App\Plugins\Admin\AdminController)->uploadFile($request, true);
        }

        try {
            DB::beginTransaction();

            $request = request();
            $supplier = $user->supplier;

            $supplier->email = $request->input('email');
            $supplier->coords = $request->input('coords');

            foreach ($request->input('meta') as $meta => $value) {
                $md = $supplier->metaData()->updateOrCreate([
                    'meta_name' => $meta,
                    'language' => language(),
                ],['meta_value' => $value]);
            }

            if ($fileresult) {
                $files = [];
                foreach($fileresult['data'] as $res) {
                    $files[0][] = $res->filePath;
                    $files[1][] = $res->main;
                    $files[2][] = $res->id;
                }
                $this->handleImages($supplier, false, $files);
            }

            $supplier->save();

            DB::commit();

        } catch (\PDOException $e) {
            DB::rollBack();

            session()->flash("message", ['msg' => $e->getMessage(), 'isError' => true]);

            return redirect()->back();
        }

        \Session::flash('message', ["msg"=>_t('translations.data_saved'),"isError"=>false]);
        return redirect()->route('supplierProfile.default');
    }
}
