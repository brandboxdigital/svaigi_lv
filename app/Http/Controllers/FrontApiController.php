<?php

namespace App\Http\Controllers;

use App\Plugins\Banners\Model\Banner;
use App\Plugins\Categories\Model\Category;
use App\Plugins\Categories\Model\CategoryMeta;
use App\Plugins\Products\Model\Product;
use App\Plugins\Suppliers\Model\Supplier;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use App\Plugins\Sales\Model\Sale;
use Illuminate\Pagination\Paginator;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class FrontApiController extends Controller
{
    public function index()
    {
        $marketDays = [];
        $menu = [];

        $idx = 0;
        foreach ((new CacheController)->getClosestMarketDayList() as $availableTo => $marketDay) {
            $marketDays[$idx]['isActive'] = (new CacheController)->isSelectedMarketDay($marketDay);
            $marketDays[$idx]['day'] = $marketDay->date->format('d');
            $marketDays[$idx]['month'] = ucfirst(__('translations.'.$marketDay->date->format('F')));
            $marketDays[$idx]['name'] = $marketDay->name;
            $marketDays[$idx]['availableTo'] = $availableTo;

            $idx++;
        }

        $menuCache = (new CacheController)->getMenuCache("main");
        foreach ($menuCache->getItems() as $idx => $item) {
            $menu[$idx]['href'] = $menuCache->getUrl($item);
            $menu[$idx]['name'] = $menuCache->getName($item);
            $menu[$idx]['slug'] = $menuCache->getSlug($item);
            $menu[$idx]['id'] = Category::where('unique_name', $menuCache->getSlug($item))->first()->id ?? null;
        }

        return [
            'menu' => $menu,
            'marketDays' => $marketDays,
            'selectedMarketDay' => (new \App\Http\Controllers\CacheController)->getSelectedMarketDay()->id
        ];
    }

    public function getRootCategory()
    {
        $categorySlug = \Route::current()->parameter('slug');

        $category = Category::where('unique_name', $categorySlug)
            ->get()
            ->first();

        $categoryTree = Category::query()
            ->defaultOrder()
            // ->whereHas('products')
            // ->whereDescendantsAndSelf($category->id)
            ->descendantsAndSelf($category->id)
            ->toTree();

        // $category->children = $category->children->sortBy(fn($v) => $v->getAttributes()['unique_name'] == $categorySlug);

        // if (isset($category['children'])) {
        //     foreach ($category['children'] as $idx => $child) {
        //         $category['children'][$idx]['children'] = Category::where('parent_id', $child['id'])->get();

        //         if (isset($category['children'][$idx]['children'])) {
        //             foreach ($category['children'][$idx]['children'] as $c) {
        //                 $c->children = $c->children->sortBy(fn($v) => $v->getAttributes()['unique_name'] == $c->unique_name);
        //             }
        //         }

        //     }
        // }

        if ($category = $categoryTree->first()) {
            return response()->json(['rootCategory' => $category->toArray()]);
        }

        return null;
    }

    /**
     * Show Category products
     *
     * @param $categorySlug
     * @param $categoryId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCategory()
    {
        $queryParams = request()->query();
        session()->put('lastCategory', request()->route()->parameters);
        $filters = false;
        $slug_category = null;
        $landingCategory = null;

        $categorySlug = \Route::current()->parameter('slug');

        $slug_category = Category::where('unique_name', $categorySlug)->get()->first();

        /**
         * There are some rare cases where $slug_category does not exist...
         * ..so try different way to get it
         *
         * @todo find out why these cases happen
         */
        if (!$slug_category) {
            $meta = CategoryMeta::where([
                    "meta_value" => $categorySlug,
                    'meta_name' => 'slug'
                ])
                ->firstOrFail();

            $slug_category = optional($meta)->category;
        }

        /**
         * if it has parents then show the category that link points to
         */
        if ($slug_category->parent) {
            $category = $slug_category;

        }
        /**
         * If no parent then show first child category. Also in this case show popular products
         */
        else if ($slug_category->descendants->first()) {

            /**
             * Updated to get as first gatavi-produktu-grozi
             */
            if ($slug_category->unique_name === 'partika') {
                $category = $slug_category;
                // $landingCategory = Category::where('unique_name', 'gatavi-produktu-grozi')->get()->first();
            } else {
                $category = count($slug_category->products()->get()) ? $slug_category : $slug_category->descendants->first();
            }
        }

        /**
         * If that does not exist, then again show currently selectect category by url
         */
        else {
            $category = $slug_category;
        }


        /**
         * Put produktu-grozi as first item in collection
         */
        $slug_category->children = $slug_category->children->sortBy(fn($v) => $v->getAttributes()['unique_name'] !== 'produktu-grozi');

        $banners = ($slug_category)
            ? $slug_category->banners()->whereRaw("'".Carbon::now()."' between dateFrom and dateTo")->get()
            : collect();

        $banners = $banners->merge(Banner::whereDoesntHave('categories')->whereRaw("'".Carbon::now()."' between dateFrom and dateTo")->get());


        $meta = $this->getMeta($category);
        $breadcrumbs = $slug_category->getBreadcrumbs();

        $slugCategoryArray = $slug_category->toArray();

        $landingCategoryBreadcrumbs = null;
        if ($landingCategory) {
            $landingCategoryBreadcrumbs = $landingCategory->getBreadcrumbs();
        }

        $categoryArray = $category->toArray();
        $categoryArray['parent'] = $category->parent ? $category->parent->toArray() : null;

        if (isset($categoryArray['children'])) {
            foreach ($categoryArray['children'] as $idx => $child) {
                $categoryArray['children'][$idx]['children'] = Category::where('parent_id', $child['id'])->get()->toArray();
            }
        }

        $emptyProductsData = [
            'products' => null,
            'pagination' => null
        ];

        return response()->json([
            'productsAndPagination' => $landingCategory ? $emptyProductsData : $this->getProductsCache($category, $categorySlug, $queryParams),
            'category' => $category,
            'breadcrumbs' => $breadcrumbs,
            'landingCategoryBreadcrumbs' => $landingCategoryBreadcrumbs,
            'categoryArray' => $categoryArray,
            'slugCategoryArray' => $slugCategoryArray,
            'landingCategory' => $landingCategory,
            'landingCategoryProducts' => $landingCategory ? $this->getProductsCache($landingCategory, $landingCategory->unique_name, $queryParams) : $emptyProductsData,
        ]);
    }

    /**
     * @return array
     * @param Category $category
     */
    public function getMeta(Category $category): array
    {
        $current = CategoryMeta::where('owner_id', $category->id);
        $meta['title'] = $current->where('meta_name', 'name')->get()->first()->toArray();

        $keywords = $current->where('meta_name', 'google_keywords')->get()->first();
        $description = $current->where('meta_name', 'google_description')->get()->first();

        if ($keywords) {
            $meta['keywords'] = $keywords->toArray();
        }

        if ($description) {
            $meta['description'] = $description->toArray();
        }

        return $meta;
    }

    public function getCacheKey($queryParams)
    {
        $md = (new CacheController)->getSelectedMarketDay();

        $queryString = '';

        foreach($queryParams as $key => $param) {
            $queryString .= str_slug($key);
            $queryString .= '-';
            $queryString .= str_slug($param);
        }

        return str_replace('/', '-', request()->path()) . $queryString . "market-day-{$md->id}";
    }

    public function getProductsCache($category, $categorySlug, $queryParams)
    {
        $searchString = isset($queryParams['search']) ? $queryParams['search'] : false;
        $sortBy = isset($queryParams['sort']) ? $queryParams['sort'] : false;

        $md = (new CacheController)->getSelectedMarketDay();

        return Cache::remember($this->getCacheKey($queryParams), 600, function () use ($category, $categorySlug, $searchString, $sortBy, $md) {
            /** @var \App\Plugins\Products\Model\Product $products */
            if (!$searchString) {

                if ($category) {
                    $cat_ids = array_merge([$category->id], $category->descendants->pluck('id')->toArray());
                    $products = Product::whereHas('extra_categories', function($q) use ($cat_ids) {
                            $q->whereIn('id', $cat_ids);
                        })
                        ->whereHas('market_days', function (Builder $q) {
                            $md = (new CacheController)->getSelectedMarketDay();

                            return $q->where('market_day_id', $md->id);
                        });
                } else {
                    $products = Product::whereHas('market_days', function (Builder $q) {
                        $md = (new CacheController)->getSelectedMarketDay();

                        return $q->where('market_day_id', $md->id);
                    });
                }

                if (session()->get('filters', null)) {
                    $filters = session()->get('filters');
                }

                if (request()->input('supplier')) {
                    $filters['suppliers'] = collect(request()->input('supplier'))->toArray();
                    session()->put('filters', $filters);
                }



                if (($filters['filters'] ?? false) ) {
                    $products = $products->whereHas('attributeValues', function (Builder $q) use ($filters) {
                        $q->whereIn('attribute_value_id', $filters['filters']);
                    });
                    $filters = true;
                }

                if (($filters['suppliers'] ?? false) ) {
                    $products = $products->whereIn('supplier_id', $filters['suppliers']);
                }
            } else {
                $products = new Product();
                $products = $products
                    ->isPublished()
                    ->whereIn('supplier_id', array_keys(preg_grep('/' . $searchString . '/i', __('supplier.name'))))
                    ->orWhereHas('metaData', function (Builder $q) use ($searchString) {
                        $q->where('meta_value', 'like', "%$searchString%")->where('language', language())->whereIn('meta_name', ['name'])->where('state', 1);
                    })
                    ->orWhereHas('extra_categories', function(Builder $q) use($searchString) {
                        $q->whereIn('category_id', array_keys(preg_grep("/$searchString/i", __('category.name'))))->where('state', 1);
                    });
            }

            $products = $products
                ->whereHas('market_days', function ($query) use ($md) {
                    $query->where('market_day_id', $md->id);
                })
                ->select('products.*')->where('state', 1)->limit(20);

            switch ($sortBy) {
                case "svaigi_order":
                default:
                    $products = $products->addSelect(DB::raw('product_metas.meta_value as metaName'))->join('product_metas', function ($join) {
                        $join->on('products.id', '=', 'product_metas.owner_id')
                            ->where('meta_name', '=', 'name');
                    })->orderBy('metaName', 'asc');

                    break;

                case "name_asc":
                    $products = $products->addSelect(DB::raw('product_metas.meta_value as metaName'))->join('product_metas', function ($join) {
                        $join->on('products.id', '=', 'product_metas.owner_id')
                            ->where('meta_name', '=', 'name');
                    })->orderBy('metaName', 'asc');
                    break;

                case "name_desc":
                    $products = $products->addSelect(DB::raw('product_metas.meta_value as metaName'))->join('product_metas', function ($join) {
                        $join->on('products.id', '=', 'product_metas.owner_id')
                            ->where('meta_name', '=', 'name');
                    })->orderBy('metaName', 'desc');
                    break;

                case "price_asc":
                    $products = $products->addSelect(DB::raw('(cost*(1+mark_up/100))*(1+vats.amount/100) as price'))->join('vats', function ($join) {
                        $join->on('products.vat_id', '=', 'vats.id');
                    })->orderBy('price', 'asc');
                    break;

                case "price_desc":
                    $products = $products->addSelect(DB::raw('(cost*(1+mark_up/100))*(1+vats.amount/100) as price'))->join('vats', function ($join) {
                        $join->on('products.vat_id', '=', 'vats.id');
                    })->orderBy('price', 'desc');
                    break;

                case "popularity":
                    $products = $products->orderBy('times_bought', 'desc');
                    break;

            }

            /**
             * Get pagination object and get product list that is used in frionted
             */
            $prodQuery = $products
                ->where(function($q) {
                    $q->where('storage_amount', '>', 0)->orWhereNull('storage_amount');
                });
            $allProducts = clone $prodQuery;

            $pagination = $prodQuery->paginate(20);
            if (isset($searchString)) {
                $pagination->appends(['search' => $searchString]);
            }

            // $allProducts = $allProducts->get()->pluck('storage_amount', 'id')->toArray();

            // $products   = collect($pagination->items())->pluck('storage_amount', 'id')->toArray();
            $allProducts = $allProducts->get()->toArray();

            $products   = collect($pagination->items())->toArray();

            if ($categorySlug === 'piedavajumi') {

                $salesProducts = [];
                $date = Carbon::now();
                $categorySales = Sale::where('discount_to', 'category')->where('valid_from', '<=', $date)->where('valid_to', '>=', $date)->get()->pluck('discount_target_id')->all();

                foreach ($categorySales as $sale) {
                    foreach ($sale as $id) {
                        $salesCategory = Category::find($id);
                        foreach ($salesCategory->products()->get() as $p) {
                            $salesProducts[] = $p;
                        }
                    }
                }

                $productSales = Sale::where('discount_to', 'product')->where('valid_from', '<=', $date)->where('valid_to', '>=', $date)->get()->pluck('discount_target_id')->all();
                foreach ($productSales as $sale) {
                    foreach ($sale as $id) {
                        $salesProducts[] = Product::find($id);
                    }
                }

                $pagination = new Paginator($salesProducts, 20);
                $pagination->setPath(url('/piedavajumi'));

                $products = collect($pagination->items())->pluck('storage_amount', 'id')->toArray();
            }

            $products = array_map(function ($product) {
                $item = (new CacheController)->getProduct($product['id']);
                $product['has_many_prices'] = $item->hasManyPrices();
                $product['lowest_amount'] = $item->lowestAmount();
                $product['is_sale'] = $item->isSale();
                $product['is_new'] = $item->isNew();
                $product['url'] = $item->getUrl();
                $product['supplier'] = [
                    'name' => __("supplier.name.{$item->supplier_id}"),
                    'url' => r('supplierOpen', [getSupplierSlugs(true),__("supplier.slug.{$item->supplier_id}")])
                ];

                $product['prices'] = (array)$item->prices();

                foreach ($item->prices() ?? [] as $id => $price) {
                    if (is_int($id)) {
                        $product['prices'][$id] = (array)$price;
                    }
                }

                $product['market_days'] = $item->marketDays;
                $product['is_in_market_days_array'] = in_array((new \App\Http\Controllers\CacheController)->getSelectedMarketDay()->id,$item->marketDays);

                //ProductCache->isAvailable() always return true
                $product['is_available'] = true;

                $product['background_image_url'] = $item->image(config('app.imageSize.product_image.list'));
                return $product;
            }, $products);

            // dd($products);
            return [
                'products' => $products,
                'pagination' => $pagination,
            ];
        });
    }

    public function loadSuppliers()
    {
        $queryParams = request()->query();
        $routeParams = request()->route()->parameters;
        $category = Category::where('unique_name', $routeParams['slug'])->first();

        if (!$category) {
            return [];
        }

        $cat_ids = array_merge([$category->id], $category->descendants->pluck('id')->toArray());
        $md = (new CacheController)->getSelectedMarketDay();

        return Cache::remember($this->getCacheKey($queryParams) . 'suppliers', 600, function () use ($queryParams, $cat_ids, $routeParams, $md) {
            return isset($queryParams['searchSuppliers'])
                ? $this->searchSuppliers($queryParams['searchSuppliers'])
                : Supplier::orderBy('custom_id')
                    ->whereHas('products', function($query) use($cat_ids, $md) {
                        $query
                            ->isPublished()
                            ->whereHas('market_days', function ($query) use ($md) {
                                $query->where('market_day_id', $md->id);
                            })
                            ->whereHas('extra_categories', function($query) use($cat_ids) {
                                $query->whereIn('id', $cat_ids);
                            });
                    })
                    ->with([
                        'products' => function($query) use( $cat_ids, $md) {
                            $query
                                ->isPublished()
                                ->whereHas('market_days', function ($query) use ($md) {
                                    $query->where('market_day_id', $md->id);
                                })
                                ->whereHas('extra_categories', function($query) use($cat_ids) {
                                    $query->whereIn('id', $cat_ids);
                                });
                        },
                    ])
                    ->paginate(10)
                    ->toArray();
        });
    }

    public function searchSuppliers($searchString)
    {
        return Supplier::query()
            ->where(function($query) use($searchString){
                $query
                    ->where('custom_id', 'like', "%$searchString%")
                    ->orWhere(function($query) use($searchString) {
                        $query->whereHas('products', function($query) use($searchString) {
                            $query->isPublished();

                            $query->whereHas('metaName', function($query) use($searchString) {
                                $query->where('meta_value', 'like', "%$searchString%");
                            });

                            $query->whereHas('market_days', function($query) use($searchString) {
                                $md = (new CacheController)->getSelectedMarketDay();
                                $query->where('market_day_id', $md->id);
                            });
                        });
                    })
                    ->whereHas('products.market_days', function($query) {
                        $md = (new CacheController)->getSelectedMarketDay();
                        $query->where('market_day_id', $md->id);
                    });
            })
            ->with(['products' => function($query) use($searchString) {
                $query->isPublished();

                $query->where(function($query) use($searchString) {
                    $query->whereHas('metaName', function($query) use($searchString) {
                        $query->where('meta_value', 'like', "%$searchString%");
                    })->whereHas('market_days', function ($query) {
                        $md = (new CacheController)->getSelectedMarketDay();
                        $query->where('market_day_id', $md->id);
                    });
                });

                if (count($query->get()->toArray()) < 1) {
                    $query->orWhere(function($query) {
                        $query->whereHas('market_days', function ($query) {
                            $md = (new CacheController)->getSelectedMarketDay();
                            $query->where('market_day_id', $md->id);
                        });
                    });
                }
            }])
            ->whereHas('products', function($query) use($searchString) {
                $query->isPublished();
                $query->where(function($query) use($searchString) {
                    $query->whereHas('market_days', function ($query) {
                        $md = (new CacheController)->getSelectedMarketDay();
                        $query->where('market_day_id', $md->id);
                    });
                });
            })
            ->paginate(10)
            ->toArray();
    }
}
