<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\BerniSaimniekoAnalytics;
use Illuminate\Http\JsonResponse;
use App\Mail\NewsletterConfirmation;
use Illuminate\Support\Facades\Mail;

class NewsletterApiController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function subscribe(Request $request): JsonResponse
    {
        $validated = $request->validate([
            'email' => 'required|email',
            'personasDatuApstrade' => 'accepted',
        ]);

        if ($validated) {
            $user = $this->createOrUpdateUser($request);
            Mail::to($user->email)->send(new NewsletterConfirmation($user));
        }

        return response()->json([
            'status' => 'ok',
        ]);

    }

    /**
     * @param Request $request
     * @return User
     */
    public function createOrUpdateUser(Request $request): User
    {
        $user = User::firstOrNew([
            'email' => $request->input('email'),
        ]);

        if (!$user->name) {
            $user->name = '';
        }

        $user->save();

        return $user;
    }

    public function subscribeBySubscriptionComponent(Request $request)
    {
        $email = $request->input('email');
        $name  = $request->input('name');

        BerniSaimniekoAnalytics::countUserNewsletterSubscription($email, $name, '');

        return redirect()->back()->with('swal_message', ['msg' => 'Paldies!', 'body' => "Pieteikums nosūtīts veiksmīgi!"]);
    }

    public function subscribeByBlogSubscriptionComponent(Request $request)
    {
        $email    = $request->input('email');
        $phone    = $request->input('phone');
        $name     = $request->input('name');
        $surname  = $request->input('surname', '');
        $type     = $request->input('type');

        if ($type == 'masterclass') {
            BerniSaimniekoAnalytics::countUserMasterclassSubscription($email, $name, $surname, $phone);

            return redirect()->back()->with('swal_message', ['msg' => 'Paldies!', 'body' => "Pieteikums meistarklasei nosūtīts veiksmīgi!"]);
        } else {
            BerniSaimniekoAnalytics::countUserNewsletterSubscription($email, $name, $surname);

            return redirect()->back()->with('swal_message', ['msg' => 'Paldies!', 'body' => "Pieteikums nosūtīts veiksmīgi!"]);
        }


    }
}
