<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        /**
         * @fix On initial artisan migration run, this code checks for DB which does not exist then
         */
        if (! app()->runningInConsole() ) {
            if (!Auth::user() && !session()->get('user')) {
                try {
                    $user = User::find(99);
                    session()->put('user', $user);
                } catch (\Throwable $th) {}
            }
        }
    }

    public function cache() {
        return new CacheController;
    }

    public function adminLogin() {
        return redirect()->route('login');
    }
}
