<?php

namespace App\Http\Requests;

use App\Rules\MyEmailOrUnregistered;
use App\Rules\UniqueNotRegistered;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Plugins\Orders\Functions\CartFunctions;

class Profile extends FormRequest
{
    use CartFunctions;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'name'             => 'required|min:3|regex:/^[^0-9]+/i',
            'last_name'        => 'required|min:3|regex:/^[^0-9]+/i',
            'phone'            => 'required|numeric',
            'legal_name'       => 'required_if:is_legal,1',
            'legal_address'    => 'required_if:is_legal,1',
            'legal_reg_nr'     => 'required_if:is_legal,1',
            'legal_vat_reg_nr' => 'required_if:is_legal,1',
        ];

        if ($this->needAddress()) {

            $rules = array_merge($rules, [
                'address'          => 'required',
                'city'             => 'required|min:3|regex:/^[^0-9]+/i',
                'postal_code'      => 'required',
            ]);
        }

        $rn = explode(".", Route::currentRouteName());

        switch ($rn[0]) {
            case "register":
                $rules['password'] = 'required|confirmed';
                $rules['rules'] = 'accepted';
                $rules['email'] = ['required','email', new UniqueNotRegistered()];
                break;

            case "checkout":
                //if(!Auth::user() || !Auth::user()->registered)
                    //$rules['password'] = 'required|confirmed';
                $rules['rules'] = 'accepted';
                $rules['email'] = ['required','email']; // , new MyEmailOrUnregistered()

                $action = $this->input('action');
                if($action == "password"){
                    $rules = [];
                    $rules['check_password'] = 'required';
                    $rules['email'] = 'required';

                }

                break;

            case "profile":
//                if(Auth::user() && !Auth::user()->registered)
//                    $rules['password'] = 'required|confirmed';
//                else
//                    $rules['password'] = 'confirmed';

                if(Auth::user() && !Auth::user()->isSocialAuthed())
                    $rules['password'] = 'confirmed';

                $rules['email'] = ['required', 'email', new MyEmailOrUnregistered()];




                break;
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required'             => 'E-pasts ir obligāts',
            "name.required"              => 'Vārds ir obligāts',
            "last_name.required"         => 'Uzvārds ir obligāts',
            "phone.required"             => 'Telefons ir obligāts',
            "legal_name.required"        => 'Uzņēmuma nosaukums ir obligāts',
            "legal_address.required"     => 'Uzņēmuma adrese ir obligāta',
            "legal_reg_nr.required"      => 'Uzņēmuma reģistrācijas nummurs ir obligāts',
            "legal_vat_reg_nr.required"  => 'PVN nummurs ir obligāts',
            "address.required"           => 'Adrese ir obligāta',
            "city.required"              => 'Pilsēta ir obligāta',
            "postal_code.required"       => 'Pasta indeks ir obligāts',
            "rules.accepted"             => "Lūdzu, piekrīti mājas lapas lietošanas noteikumiem.",
        ];
    }

    public function needAddress()
    {
        try {
            $cart = $this->getCart();

            if (isset($cart) && $cart->delivery) {
                return $cart->delivery->needAddess();
            }

        } catch (\Throwable $th) {}

        return true;
    }
}
