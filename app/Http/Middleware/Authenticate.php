<?php

namespace App\Http\Middleware;

use App\EmailChange;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        // action
        // verifyString

        if ($request->action == 'emailchange') {
            $changes = EmailChange::where(['verifyString' => $request->verifyString])->first();
            // Auth::login($user);e
            Auth::loginUsingId($changes->invokedBy);

            return redirect()->back();
        }

        if (! $request->expectsJson()) {

            if($request->route()->getPrefix()=='/admin') {
                return route('login');
            }

            session()->flash('openLogin', true);

            return r('page');
        }
    }
}
