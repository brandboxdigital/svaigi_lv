<?php namespace App\Console\Commands\Fix;

use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Suppliers\Model\SupplierMeta;

use Goutte\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Plugins\Admin\Model\File as SvFile;

use App\Functions\General;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FixUsers extends \App\Console\Commands\InitialData\ImporterBase
{
    use General;

    /**
     * Used source
     * @todo Use this class to import from other sources
     */
    public $source        = self::SOURCE_DB;
    public $db_connection = 'mysql_old';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->log('Fixing - ' . get_class($this));
        $this->log('');

        $this->createProgressBar();

        $this->eachRowTable('client', function($row){
            $this->fixUser($row);
        });
    }

    public function prune()
    {
        // SupplierMeta::truncate();
        // exec('rm -f ' . storage_path('app/public/suppliers/*'));
    }

    public function fixUser($oldpost)
    {
        if (empty($oldpost->email) ) {
            return null;
        }

        $model = User::where('email', $oldpost->email)->first();

        // Try to skip import where new has no email
        if (!$model) {
            return null;
        }

        $isNotChanged = $model->updated_at->between(Carbon::parse('2021-11-03 19:15:00'), Carbon::parse('2021-11-03 19:38:00'));

        if ($isNotChanged && $model->id >= 100 ) {
            DB::table('users')->where('id',$model->id)->update([
                'password'   => null,
                'registered' => $oldpost->is_registered == 'y',
            ]);
        }
    }

}
