<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'svaigi:clear-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all data. USE WITH CAUTION!!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->alert('This WILL delete all data!');

        if (!$this->confirm('Are you sure?')) {
            $this->info('Canceled!');
            return;
        }

        if (!$this->confirm('Are you REALLY sure?')) {
            $this->info('Canceled!');
            return;
        }

        $this->warn('Deleting order_headers');
        DB::table('order_headers')->truncate();

        $this->warn('Deleting order_lines');
        DB::table('order_lines')->truncate();

        $this->warn('Deleting original_orders');
        DB::table('original_orders')->truncate();

        $this->warn('Deleting paysera');
        DB::table('paysera')->truncate();

        $this->warn('Deleting schedules');
        DB::table('schedules')->truncate();

        $this->warn('Deleting sessions');
        DB::table('sessions')->truncate();
    }
}
