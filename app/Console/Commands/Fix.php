<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Seeder;

class Fix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'svaigi:fix {fix_class_name? : What Fix to run?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run some specific fix for swaigi site. Most probably DB bulk update of some kind';


    private $class = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $className = $this->argument('fix_class_name', false);

        $this->class = "\\App\\Console\\Commands\\Fix\\Fix" . $className;

        if (!$this->class || !\class_exists($this->class)) {
            $class = $this->class;
            $this->error("Fix Class $class does not exist!");
            return;
        }

        $this->runStuff();
    }


    private function runStuff()
    {
        // $only = $this->option('only');
        $class = $this->class;

        $class::make($this)->run();
    }
}
