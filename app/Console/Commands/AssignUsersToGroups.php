<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Plugins\UserGroups\Functions\AssignUserGroups;

class AssignUsersToGroups extends Command
{
    use AssignUserGroups;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'svaigi:asign-users-to-groups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mainy used in CRON to asign users to groups..';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->runUserAsigment();
    }
}
