<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Seeder;

class InitSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'svaigi:initsite {--full} {--prune} {--only=} {--start-from=} {--show} {--shedule} {--onlyimages}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init site. Seed initial data and configuration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        // /**
        //  * Import initial data Seed Classes.
        //  * These classes are used only here and no where else in app!
        //  */
        // foreach (glob(__DIR__. "/initialdata/*.php") as $filename) {
        //     try {
        //         include_once $filename;
        //     } catch (\Throwable $th) {
        //         $this->command->warn("Tried and failded to include $filename");
        //     }
        // }

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $is_full = $this->option('full', false);
        $is_prune = $this->option('prune', false);

        if ($is_full) {

            if (!$this->confirm('Full import. Do you wish to continue?')) {
                return false;
            }

        }

        if ($is_prune) {

            if (!$this->confirm('This will also Prune (delete) old data. Do you wish to continue?')) {
                return false;
            }

        }

        $this->runStuff();
    }


    private function runStuff()
    {
        $only = $this->option('only');

        // \App\Console\Commands\initialData\ReCache::make($this)->run();
        // \App\Console\Commands\initialData\TranslationsMigrate::make($this)->run();
        // \App\Console\Commands\initialData\MarketDays::make($this)->run();
        // \App\Console\Commands\initialData\Pages::make($this)->run();

        if ($only == null) {
            \App\Console\Commands\initialData\SuppliersMigrate::make($this)->run();
            \App\Console\Commands\initialData\CategoriesMigrate::make($this)->run();

            \App\Console\Commands\initialData\PrepareLatestProducts::make($this)->run();
            \App\Console\Commands\initialData\ProductsMigrate::make($this)->run();
            \App\Console\Commands\initialData\ProductImagesMigrate::make($this)->run();

            \App\Console\Commands\initialData\Blogs::make($this)->run();
        } else {

            foreach (\explode(',',$only) as $onlyClass) {
                $class1 = "\\App\\Console\\Commands\\initialData\\" . $onlyClass;
                $class2 = "\\App\\Console\\Commands\\initialData\\" . $onlyClass . "Migrate";

                if (\class_exists($class1)) {
                    $class1::make($this)->run();
                } else if (\class_exists($class2)) {
                    $class2::make($this)->run();
                } else {
                    $this->error("Class $class1 nor $class2 does not exist!");
                }
            }

        }
    }
}
