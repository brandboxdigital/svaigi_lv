<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ResetAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'svaigi:resetAdmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resset admin user password.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin = \App\User::find(1);

        $pass = $this->secret('Input new password (min length 4 symbols)', null);

        if ($pass && strlen($pass) > 4) {
            $admin->password = 'admin';
            $admin->save();

            $this->info('Admin password changed!');
            $this->info('');
        } else {
            $this->alert('No/Short password detected');
        }
    }
}
