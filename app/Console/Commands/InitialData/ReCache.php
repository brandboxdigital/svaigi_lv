<?php namespace App\Console\Commands\InitialData;

use Goutte\Client;
use App\Plugins\Products\Model\Product;
use App\Plugins\Categories\Model\Category;
use App\Plugins\Menu\Model\FrontendMenu;
use App\Plugins\MarketDays\Model\MarketDay;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Attributes\Model\Attribute;

use App\Plugins\Pages\Model\Page;
use App\Plugins\Pages\Model\PageMeta;


use App\Http\Controllers\CacheController;


class ReCache extends ImporterBase
{
    /**
     * Used source
     * @todo Use this class to import from other sources
     */
    public $source = self::SOURCE_NONE;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([
            Product::class       => 'createProductCache',
            Category::class      => 'createCategoryCache',
            FrontendMenu::class  => 'createMenuCache',
            // Slug::class          => 'createSlugCache',
            // MarketDay::class     => null,
            Supplier::class      => 'createSupplierCache',
            // Page::class          => 'createPageCache',
            Attribute::class     => 'createAttributeCache',

        ] as $class => $function) {
            $this->recache($class, $function);
        }

        $this->recache__pages();
    }


    private function recache($class, $cacheFunctionName, $attrName = 'id')
    {
        $name = str_plural(class_basename($class));

        $this->newLine();
        $this->log("Re-Caching $name:");

        $models = $class::get();

        $this->createProgressBar();
        $this->progressBar->start($models->count());


        $counter = 0;

        if ( method_exists(new CacheController, $cacheFunctionName) ) {

            $models->each(function($item) use ($counter, $name, $cacheFunctionName, $attrName) {
                $this->progressBar->setMessage($name . ": " . $counter++);

                (new CacheController)->$cacheFunctionName($item->$attrName, true);

                $this->progressBar->advance();
            });

        } else {
            $this->log("  - `$cacheFunctionName` does not exist!");
        }

        $this->progressBar->finish();

        $this->newLine();
        $this->log("Done!");
        $this->newLine(2);
    }


    private function recache__pages()
    {
        $this->newLine();
        $this->log("Re-Caching Pages:");

        $models = PageMeta::where('meta_name', 'slug')->get();

        $this->createProgressBar();
        $this->progressBar->start($models->count());

        $counter = 0;


        $models->each(function($item) use ($counter) {
            $this->progressBar->setMessage("Page: " . $counter++);

            (new CacheController)->createPageCache($item->meta_value, true);

            $this->progressBar->advance();
        });

        $this->progressBar->finish();

        $this->newLine();
        $this->log("Done!");
        $this->newLine(2);
    }
}
