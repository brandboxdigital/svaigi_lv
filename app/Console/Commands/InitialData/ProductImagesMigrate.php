<?php namespace App\Console\Commands\InitialData;

use App\Plugins\Products\Model\Product;
use App\Plugins\Products\Model\ProductMeta;
use App\Plugins\Products\Model\ProductVariation;

use App\Plugins\Categories\Model\Category;

use App\Schedules;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use Exception;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Plugins\Admin\Model\File as SvFile;

use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;


use App\Functions\General;

use Illuminate\Support\Facades\DB;

use Dcblogdev\Dropbox\Facades\Dropbox;


class ProductImagesMigrate extends ImporterBase
{
    use General;

    const MAX = 10;
    const CHUNK_SIZE = 500;
    const TARGET_PATH = 'imports/product_images';


    public $source        = self::SOURCE_DB;
    public $db_connection = 'mysql_old';


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->log('Importing - ' . get_class($this));
        $this->log('');
        $this->createProgressBar();

        if ($this->console->option('prune', false)) {
            $this->prune();
        }

        $this->callOldPage();

        /**
         * Run dropbox only if full import.
         */
        if ($this->console->option('full', false)) {
            $this->callDropbox();
        }

        $this->log('Done!');
    }

    public function prune()
    {
        $this->log('Deleting old product files');
        exec('rm -fr ' . storage_path('app/public/products/*'));
    }

    public function callDropbox()
    {
        $this->log('Starting Dropbox import:');
        $zip = new \ZipArchive();
        $rootFolderList = Dropbox::files()->listContents('');

        foreach ($rootFolderList["entries"] as $key => $folder) {

            $this->log('Downloading image zip from Dropbox dir ' . $folder["path_lower"]);

            $filePath = $this->downloadZIP($folder["path_lower"], self::TARGET_PATH);
            $zresult = $zip->open($filePath);

            if ($zresult === true) {
                $this->log('Extracting ' . $folder["path_lower"]);
                $this->extractAndFlattenDownloadDir($zip);


                $this->log('Done');
                $this->log('');

                $this->runScheduler();

                $this->cleanOutDownloadDir();
            } else {
                dd('eeeeerrrooor');
            }
        }

        $this->log(' -- Dropbox Import Done!');
    }

    public function callOldPage()
    {
        $this->log('Starting Old Page Import:');

        $this->cleanOutDownloadDir();

        $counter = 1;
        $this->eachRowTable('product', function($row, $import) use (&$counter) {

            /**
             * If 'start-from' is defindes then start inport only after that counter
             */
            if ($this->console->option('start-from') == null || $counter >= $this->console->option('start-from')) {
                $this->getProductFile($row);

                if (!$this->console->option('full', false) && $counter > self::MAX) {
                    $import->stopImport();
                    $this->runScheduler();
                }

                if ($counter % self::CHUNK_SIZE == 0) {
                    $this->log(" - Pause @ $counter");
                    $this->runScheduler();
                }
            }


            $counter++;
        });

        $this->log(' -- Old Page Import Done!');
    }

    public function cleanOutDownloadDir()
    {
        foreach (Storage::allDirectories(self::TARGET_PATH) as $path) {
            Storage::deleteDirectory($path);
        }

        Storage::delete(Storage::allFiles(self::TARGET_PATH));
    }

    public function getAllDropboxEntries($folder)
    {
        $this->log('Getting files from Dropbox dir ' . $folder["path_lower"]);
        $this->log('');

        $this->progressBar->start();

        $files = Dropbox::files()->listContents($folder["path_lower"]);
        $entires = collect();

        $iteration = 0;

        do {
            $hasMore = $files["has_more"];

            $iteration++;
            $entires = $entires->merge($files["entries"]);

            $this->progressBar->advance();

            if ($hasMore) {
                $files = Dropbox::files()->listContentsContinue($files["cursor"]);
            }
        } while ($hasMore && $iteration < 10);

        $this->progressBar->finish();
        $this->log('');

        return $entires;
    }

    private function runScheduler()
    {
        $this->log("");
        $this->log("Scheduling and Calling 'svaigi:importImages'");

        Schedules::create([
            'filename'    => 'images',
            'type'        => 'productImageImport',
            'total_lines' => count(Storage::files(self::TARGET_PATH)),
        ]);

        \Artisan::call('svaigi:importImages', [], $this->console->getOutput());

        $this->cleanOutDownloadDir();

        $this->log("Done");
        $this->log("");
    }

    private function getProductFile($row)
    {
        $result  = null;
        $imgName = $row->img ?? null;
        $sku     = $row->code ?? null;

        if ($imgName == null) {
            return $result;
        }

        $pathInfo = pathinfo($imgName);
        $newFilePath = self::TARGET_PATH . '/' . $sku . '.' . $pathInfo["extension"];

        if (Storage::exists(self::TARGET_PATH)) {
            try {
                $result = Storage::put($newFilePath, file_get_contents("https://svaigi.lv/file/product/original/$imgName"));
            } catch (\Throwable $th) {
                $this->console->error($th->getMessage());
            }
        }

        return $result;
    }

    private function downloadZIP($path, $target)
    {
        $folderPath = Storage::path($target);
        $filePath   = $folderPath . '/zip.zip';

        $path = $this->forceStartingSlash($path);

        try {
            $client = new Client;

            if (! is_dir($folderPath)) {
                mkdir($folderPath);
            }

            $response = $client->post("https://content.dropboxapi.com/2/files/download_zip", [
                'headers' => [
                    'Authorization' => 'Bearer ' . config('dropbox.accessToken'),
                    'Dropbox-API-Arg' => json_encode([
                        'path' => $path
                    ])
                ],

                'sink' => $filePath,
            ]);

            return $filePath;

        } catch (ClientException $e) {
            throw new Exception($e->getResponse()->getBody()->getContents());
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    protected function forceStartingSlash($string)
    {
        if (substr($string, 0, 1) !== "/") {
            $string = "/$string";
        }

        return $string;
    }

    protected function extractAndFlattenDownloadDir($zip)
    {
        $target = Storage::path(self::TARGET_PATH);

        $zip->extractTo($target);
        unlink($zip->filename);
        $zip->close();

        foreach (Storage::allFiles(self::TARGET_PATH) as $file) {
            $name = basename($file);

            try {
                Storage::move($file, self::TARGET_PATH . "/" . $name);
            } catch (\Throwable $th) {}
        }
    }
}
