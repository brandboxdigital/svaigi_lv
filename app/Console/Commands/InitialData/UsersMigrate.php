<?php namespace App\Console\Commands\InitialData;

use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Suppliers\Model\SupplierMeta;

use Goutte\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Plugins\Admin\Model\File as SvFile;

use App\Functions\General;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersMigrate extends ImporterBase
{
    use General;

    /**
     * Used source
     * @todo Use this class to import from other sources
     */
    public $source        = self::SOURCE_DB;
    public $db_connection = 'mysql_old';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->log('Importing - ' . get_class($this));
        $this->log('');
        $this->createProgressBar();

        if ($this->console->option('prune', false)) {
            // $this->prune();
        }

        $onlyimages = $this->console->option('onlyimages', false);

        $this->eachRowTable('client', function($row) use ($onlyimages){
            $this->saveUser($row);
        });
    }

    public function prune()
    {
        SvFile::where('owner', 'supplier_image')
            ->orWhere('owner', 'supplier_image')
            ->get()
            ->each(function($file) {
                $file->delete();
            });

        Supplier::truncate();
        SupplierMeta::truncate();

        exec('rm -f ' . storage_path('app/public/suppliers/*'));
    }

    public function saveUser($oldpost)
    {
        if ($oldpost->is_public == 'n' || $oldpost->is_deleted == 'y' || empty($oldpost->email) ) {
            return null;
        }

        // `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        // `order_count` int(11) DEFAULT NULL,
        // `user_group_id` int(11) DEFAULT '1',
        // `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
        // `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
        // `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
        // `email_verified_at` timestamp NULL DEFAULT NULL,
        // `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `registered` tinyint(1) NOT NULL DEFAULT '0',
        // `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `is_legal` tinyint(1) NOT NULL DEFAULT '0',
        // `legal_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `legal_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `legal_reg_nr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `legal_vat_reg_nr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `newsletter` tinyint(1) NOT NULL DEFAULT '0',
        // `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
        // `address_comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        // `created_at` timestamp NULL DEFAULT NULL,
        // `updated_at` timestamp NULL DEFAULT NULL,

        $isLegal = ($oldpost->customer_type == 'company')
            ? true
            : false;

        $data = [
            "username" => null,
            "name" => $oldpost->name,
            "last_name" => $oldpost->surname,
            "email" => $oldpost->email,
            "password" => NULL,
            "registered" => true,
            "phone" => $oldpost->phone,

            "is_legal" => $isLegal,
            "legal_name" => $oldpost->company_name,
            "legal_address" => $oldpost->legal_address,
            "legal_reg_nr" => $oldpost->reg_num,
            "legal_vat_reg_nr" => $oldpost->vat_num,

            "address" => $oldpost->address,
            "city" => $oldpost->city,
            "postal_code" => $oldpost->postal_code,
            "newsletter" => false,
            "address_comments" => null,

            "created_at" => $oldpost->created_at,
        ];

        $model = User::where('email', $oldpost->email)->first();

        // Try to skip import where new has no email
        if ($model) {
            return null;
        }

        try {
            $model = User::updateOrCreate(['email' => $oldpost->email], $data);
        } catch (\Throwable $th) {

        }
    }

}
