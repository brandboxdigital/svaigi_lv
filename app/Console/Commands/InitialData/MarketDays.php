<?php namespace App\Console\Commands\InitialData;

use App\Console\Commands\initialData\ImporterBase;
use Illuminate\Http\Request;

use App\Plugins\MarketDays\Model\MarketDay;

class MarketDays extends ImporterBase
{
    public $googleFileId  = '1_S6bxusNKTW-vCyf1k7vXdqbRkTelI1x9roDE2W0wZw';
    public $googleSheetId = '2059590153';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // MarketDays
        MarketDay::withTrashed()->get()->each(function($marketDay) {
            try {
                $marketDay->restore();
            } catch (\Throwable $th) {}
        });
    }

}
