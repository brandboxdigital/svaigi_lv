<?php namespace App\Console\Commands\InitialData;

use App\Plugins\Categories\Model\Category;
use App\Plugins\Menu\Model\FrontendMenu;
use App\Plugins\Menu\Model\FrontendMenuItem;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class CategoriesMigrate extends ImporterBase
{
    public $googleFileId = '1_S6bxusNKTW-vCyf1k7vXdqbRkTelI1x9roDE2W0wZw';
    public $googleSheetId = '763731733';

    private $lastCategoryLevel1 = null;
    private $lastCategoryLevel2 = null;
    private $lastCategoryLevel3 = null;
    private $lastCategoryLevel4 = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->console->option('prune', false)) {
            $this->prune();
        }

        $this->eachRow(function($row) {
            if ($row[1]) {
                $this->lastCategoryLevel1 = $this->makeCategory($row[1]);
            }

            if ($row[2]) {
                $this->lastCategoryLevel2 = $this->makeCategory($row[2], $this->lastCategoryLevel1);
                $this->lastCategoryLevel3 = $this->lastCategoryLevel2;
            }

            if ($row[3]) {
                $this->lastCategoryLevel3 = $this->makeCategory($row[3], $this->lastCategoryLevel2);
            }

            if ($row[4]) {
                $this->lastCategoryLevel4 = $this->makeCategory($row[4], $this->lastCategoryLevel3, $row[0]);
            }
        });

        $this->reCreateMenus();

        \Artisan::call('cache:clear');
    }

    public function prune()
    {
        foreach (Category::all() as $key => $cat) {
            $cat->metaData()->delete();
            $cat->delete();
        }

        Category::truncate();
    }

    private function makeCategory($name, $parent = null, $importId = null) {
        $cat = Category::create();

        $cat->id_from_import_file = $importId;

        $cat->metaData()->updateOrCreate([
            'meta_name' => 'name',
            'language'  => config('app.locale'),
        ], [
            'meta_value' => $name,
        ]);

        $slug = $this->findSlug($name, Category::class);

        $cat->metaData()->updateOrCreate([
            'meta_name' => 'slug',
            'language'  => config('app.locale'),
        ], [
            'meta_value' => $slug,
        ]);

        if ($parent && $parent->id) {
            $cat->appendToNode($parent);
        }

        $cat->save();

        return $cat;
    }

    private function reCreateMenus()
    {
        $mainCategories = Category::whereIsRoot()->with('metaData')->get();

        $ShopMenu = FrontendMenu::updateOrCreate([
            'slug' => "shop",
        ], [
            'name' => "Shop Menu",
        ]);

        $MainMenu = FrontendMenu::updateOrCreate([
            'slug' => "main",
        ], [
            'name' => "Main Menu",
        ]);

        foreach ($mainCategories as $key => $category) {
            $ShopMenu->menuItems()->updateOrCreate([
                'menu_owner' => 'category',
                'sequence' => $key,
            ],[
                'owner_id' => $category->id,
            ]);

            $MainMenu->menuItems()->updateOrCreate([
                'menu_owner' => 'category',
                'sequence' => $key,
            ],[
                'owner_id' => $category->id,
            ]);
        }
    }

}
