<?php namespace App\Console\Commands\InitialData;

use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Suppliers\Model\SupplierMeta;

use Goutte\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Plugins\Admin\Model\File as SvFile;

use App\Functions\General;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UsersUpdate extends ImporterBase
{
    use General;

    /**
     * Used source
     * @todo Use this class to import from other sources
     */
    public $source        = self::SOURCE_DB;
    public $db_connection = 'mysql_old';


    public $orderCount = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->log('Importing - ' . get_class($this));
        $this->log('');
        $this->createProgressBar();

        if ($this->console->option('prune', false)) {
            // $this->prune();
        }

        $onlyimages = $this->console->option('onlyimages', false);

        $this->orderCount = collect();

        $this->eachRowTable('order_count', function($row) use ($onlyimages){
            $this->collectUser($row);
        });

        $this->orderCount = $this->orderCount->pluck('order_amount', 'user_id')->toArray();

        $this->eachRowTable('client', function($row) use ($onlyimages){
            $this->updateUser($row);
        });
    }

    public function prune()
    {
        // SvFile::where('owner', 'supplier_image')
        //     ->orWhere('owner', 'supplier_image')
        //     ->get()
        //     ->each(function($file) {
        //         $file->delete();
        //     });

        // Supplier::truncate();
        // SupplierMeta::truncate();

        // exec('rm -f ' . storage_path('app/public/suppliers/*'));
    }

    public function updateUser($oldpost)
    {
        $model = User::where('email', $oldpost->email)->first();

        if ($model) {
            $lastBuy = DB::connection($this->db_connection)
                ->table('order')
                ->where('user_id', $oldpost->id)
                ->orderBy('created_at', 'desc')
                ->first();

            if ($lastBuy) {
                $date = Carbon::createFromTimestamp( $lastBuy->accepted_at );

                $model->last_buy_at = $date;
            }
        }

        if ($model && isset($this->orderCount[$oldpost->id])) {
            $model->order_count = $this->orderCount[$oldpost->id];
        }

        if ($model) {
            $model->save();
        }
    }

    public function collectUser($row)
    {
        $this->orderCount->push($row);
    }

}
