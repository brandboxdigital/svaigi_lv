<?php
namespace App\Console\Commands\InitialData;

use App\Plugins\Blog\Model\Blog;
use App\Plugins\Blog\Model\BlogMeta;
use App\Plugins\Blog\Model\BlogCategories;
use App\Plugins\Blog\Model\BlogCategoriesMeta;
use Goutte\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Plugins\Admin\Model\File as SvFile;

use App\Functions\General;
use Illuminate\Support\Facades\DB;

class Blogs extends ImporterBase
{
    use General;

    /**
     * Used source
     * @todo Use this class to import from other sources
     */
    public $source        = self::SOURCE_DB;
    public $db_connection = 'mysql_old';

    protected $delimiter = '/';
    protected $uri = 'https://svaigi.lv';
    protected $lang = 'lv';
    protected $api = 'blogs';
    protected $data = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->log('Importing - ' . get_class($this));
        $this->log('');
        $this->createProgressBar();

        if ($this->console->option('prune', false)) {
            $this->prune();
        }

        $onlyimages = $this->console->option('onlyimages', false);

        $this->eachRowTable('news', function($row) use ($onlyimages) {

            if ($onlyimages) {
                $model = Blog::where('id', $row->id)->first();

                if ($model) {
                    $this->handleImages($model, false, $this->addFile($row->img));
                }
            } else {
                $this->savePost($row);
            }
        });

        Blog::orderBy('id', 'desc')->take(8)->get()->each(function($blog) {
            $blog->is_highlighted = true;
            $blog->save();
        });
    }

    public function prune()
    {
        SvFile::where('owner', 'blogs')
            ->orWhere('owner', 'blog_picture')
            ->get()
            ->each(function($file) {
                $file->delete();
            });

        Blog::truncate();
        BlogMeta::truncate();
        BlogCategories::truncate();
        BlogCategoriesMeta::truncate();

        exec('rm -f ' . storage_path('app/public/blogs/*'));
    }

    public function savePost($oldpost)
    {
        if ($oldpost->is_public == 'n' || $oldpost->is_deleted == 'y') {
            return null;
        }

        $metas = collect([
            'name' => ['lv' => $oldpost->title],
            'slug' => $oldpost->slug,
            'excerpt' => ['lv' => $oldpost->intro],
            'content' => ['lv' => $this->filterOutImages($oldpost->descr)],
            'google_keywords' => ['lv' => $oldpost->seo_keywords],
            'google_description' => ['lv' => $oldpost->seo_description],
        ]);

        $category = $this->getCategory($oldpost->main_cat_id);

        $posts = Blog::updateOrCreate(['id' => $oldpost->id],
        [
            'is_highlighted' => false,
            'main_category'  => $category->id,
        ]);

        $this->handleMetas($posts, $metas->keys(), 'name', $metas->toArray());
        $this->handleImages($posts, false, $this->addFile($oldpost->img));

        $categories = $this->getRelatedCategories($posts->id);
        $categories = array_merge([$category->id], $categories->pluck('id')->toArray());
        $posts->extra_categories()->sync($categories);

        $posts->linked_products()->sync(null??[]);
        $posts->linked_supplier()->sync(null??[]);

        $posts->forgetMeta();
    }

    public function getCategory($id)
    {
        $oldPage = DB::connection($this->db_connection)->table('page')->where('id', $id)->first();

        if (!$oldPage) {
            return null;
        }

        $meta = BlogCategoriesMeta::where('meta_name', 'name')->where('meta_value', $oldPage->title)->first();

        if ($meta) {
            $cat = BlogCategories::find($meta->owner_id);
        } else {
            $cat = BlogCategories::create([]);
        }

        $metas = collect([
            'name' => ['lv' => $oldPage->title],
            'slug' => ['lv' => str_slug($oldPage->title)],
        ]);

        $this->handleMetas($cat, $metas->keys(), 'name', $metas->toArray());

        $cat->forgetMeta();


        return $cat;
    }

    public function getRelatedCategories($news_id)
    {
        $oldPageIds = DB::connection($this->db_connection)->table('news_cat_rel')->where('news_id', $news_id)
            ->get()
            ->pluck('cat_id');

        $categoryList = collect();

        foreach ($oldPageIds as $id) {
            $categoryList->push($this->getCategory($id));
        }

        return $categoryList->filter();
    }

    protected function addFile($imgName)
    {
        if (!$imgName) {
            return null;
        }

        if (!Storage::exists("temp/$imgName")) {
            try {
                Storage::put("temp/$imgName", file_get_contents("https://svaigi.lv/file/news/original/$imgName"));
            } catch (\Throwable $th) {
                return null;
            }
        }

        $uploadedfile = new \Illuminate\Http\UploadedFile(Storage::path("temp/$imgName"), $imgName);

        $request = new \Illuminate\Http\Request;
        $request->setMethod('POST');
        $request->request->replace([
            'owner' => 'blog_picture',
            'path' => 'blog_picture',
        ]);
        $request->files->replace(['blog_picture' => [$uploadedfile] ]);

        $response = (new \App\Plugins\Admin\AdminController)->uploadFile($request);

        $returnObject = [];

        $doc = new \DOMDocument();
        foreach ($response['data'] as $strng) {
            $doc->loadHTML($strng);
            $image = [];

            foreach ($doc->getElementsByTagName('input') as $node) {

                if ($node->getAttribute('name') == 'image_url[]') {
                    $returnObject[0][] = $node->getAttribute('value');
                } else if ($node->getAttribute('name') == 'image_main[]') {
                    // $returnObject[1][] = $node->getAttribute('value');
                    $returnObject[1][] = 0;
                } else if ($node->getAttribute('name') == 'image_id[]') {
                    $returnObject[2][] = $node->getAttribute('value');
                }

                // $returnObject[str_replace('[]', '', $node->getAttribute('name'))][] = $node->getAttribute('value');
            }
        }

        return $returnObject;
    }

    protected function filterOutImages($html)
    {
        $re = '/<img.*\/>/m';
        return preg_replace($re, '', $html);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    private function doScraping()
    {
        $client = new Client();

        $this->log('Crawling old Svaigi.lv page');


        $crawler = $client->request('GET',
            $this->uri
            .$this->delimiter
            .$this->lang
            .$this->delimiter
            .$this->api
        );

        $uri = $this->uri;

        $filtered = $crawler->filter('#content .article-list .article-item');
        // Get the latest post in this category and display the titles

        $this->progressBar->start($filtered->count());

        $counter = 0;

        $filtered->each(function ($node) use ($uri, $client, $counter) {

            $this->progressBar->setMessage('Crawling: '. $counter++);

            $data = [
                'title' => $node->filter('.article-title a')->text(),
                'lead' => $node->filter('.article-lead p')->text(),
                'datetime' => $node->filter('.article-meta time')->text(),
            ];

            $link = $node->selectLink('Lasīt tālāk')->link();
            $tmp_crawler = $client->click($link);

            if ($tmp_crawler->filter('#content p')->count() > 0) {
                $data['content'] = $tmp_crawler->filter('#content .content-block section')->html();
            } else {
                $data['content'] = null;
            }

            if ($tmp_crawler->filter('#content img')->count() > 0) {
                $data['img'] = $uri.$tmp_crawler->filter('#content img')->attr('src');
            } else {
                $data['img'] = null;
            }

            array_push($this->data, $data);

            $this->progressBar->advance();
        });
    }

}
