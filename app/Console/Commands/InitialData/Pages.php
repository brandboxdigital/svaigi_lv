<?php namespace App\Console\Commands\InitialData;

use App\Console\Commands\initialData\ImporterBase;
use Illuminate\Http\Request;
use App\Plugins\Pages\PageController;

class Pages extends ImporterBase
{
    public $googleFileId  = '1_S6bxusNKTW-vCyf1k7vXdqbRkTelI1x9roDE2W0wZw';
    public $googleSheetId = '2059590153';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pageController = new PageController;

        $request = $this->fakeRequest([
            'title' => 'Sākumlapa',
            'template' => 2,
            'homepage' => 1,

            'name' => [
                'lv' => "Sākumlapa"
            ],
            'slug' => [
                'lv' => "/"
            ],
        ]);

        $pageController->store($request);
    }


    protected function fakeRequest(array $data = [], $method = 'POST')
    {
        $request = app('request');

        $request->setMethod($method);

        $request->request->add($data);


        return $request;
        // $request->replace(['foo' => 'bar']);
    }

}
