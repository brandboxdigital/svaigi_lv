<?php namespace App\Console\Commands\InitialData;

use App\Plugins\Products\Model\Product;
use App\Plugins\Products\Model\ProductMeta;
use App\Plugins\Products\Model\ProductVariation;

use App\Plugins\Categories\Model\Category;

use App\Schedules;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;

use App\Plugins\Products\Functions\ProductImport;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Suppliers\Model\SupplierMeta;
use Illuminate\Support\Facades\DB;

class ProductsMigrate extends ImporterBase
{
    use \App\Console\Commands\InitialData\Helpers\ProductImportHelpers;

    public $googleFileId = '1_S6bxusNKTW-vCyf1k7vXdqbRkTelI1x9roDE2W0wZw';
    public $googleSheetId = '1966855212';

    private $suppliers;

    const MAX = 10;


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->console->option('prune', false)) {
            $this->prune();
        }

        $fileName = 'product_export_' . '1' . '.csv';
        $file = $this->makeNewFile($fileName);

        if ($this->console->option('shedule', false)) {
            Schedules::create([
                'filename' => basename($fileName),
                'type'     => 'productImport',
            ]);

            $this->makeCopy($fileName);
        }

        \Artisan::call('cache:clear');

        $this->log('Im Done!');
    }

    public function prune()
    {
        foreach (ProductVariation::all() as $key => $pv) {
            $pv->delete();
        }

        Product::truncate();
        ProductMeta::truncate();

        ProductVariation::truncate();
        DB::table('category_product')->truncate();
    }

    public function makeCopy($fileName)
    {
        $filePath = 'imports/products/' . $fileName;
        $copyPath = str_replace('.csv', '_copy.csv', $filePath);

        \Storage::copy($filePath, $copyPath);
    }

    public function makeNewFile($fileName)
    {

        $productImportHelper = new ProductImport;
        $filePath = 'imports/products/' . $fileName;

        \Storage::put($filePath, null);
        $filePath = \Storage::path($filePath);

        /**
         * Create and insert Header
         */
        $csv = Writer::createFromPath($filePath);
        $fieldOrder = $productImportHelper->importFieldData();

        $csv->insertOne($fieldOrder);

        $counter = 0;

        $this->eachRow(function ($row, $import) use ($csv, &$counter) {
            $counter++;

            $importRow = [
                'SKU' => $row[0],
                'Farmer' => $this->findFarmer($row[4]),
                'Active' => $row[3],
                // 'Active' => 1,
                'Market Days' => $this->findDays($row[17]),
                'Product Name' => trim($row[2]),
                'Categories' => $this->findCategories($row[9]),
                'BIO' => $row[10] ? 1 : 0,
                'LV' => 1,
                'Suggested' => 0,
                'Highlighted' => 0,
                'Description' => $this->fixText($row[11]),
                'Ingridients' => $this->fixText($row[12]),
                'Expiration Date' => $this->findExpDate($row[13]),
                'Measurement Unit' => empty(trim($row[28])) ? 'kg' : trim($row[28]),
                'Variations' => $this->guessVariations($row[29]),
                'Price' => empty($row[31]) ? 0 : floatval($row[31]),
                'Mark Up' => round((floatval($row[20]) * 100)),
                'Vat' => $this->guessVat($row[7], $row[8]),
                'Language' => $row[1] ?? 'lv',
                'Sequence' => $counter,
                'Attributes' => $this->guessAttributes($row[27]),
                'SEO Keywords' => $row[15],
                'SEO Description' => $row[14],
            ];

            $importRow = $this->checkForUpdates($importRow['SKU'], $importRow);

            // Import only active
            if ($importRow['Active'] != 0) {
                $csv->insertOne($importRow);
            }


            if (!$this->console->option('full', false) && $counter >= self::MAX) {
                $import->stopImport();
            }
        });

        return (object) [
            'name' => $fileName,
            'path' => $filePath,
        ];
    }

    public function checkForUpdates($sku, $row)
    {
        $original = $row;
        if (!file_exists(database_path('database.sqlite'))) {
            return $row;
        }

        $sqlite = DB::connection('sqlite')->table('temp');

        $model = $sqlite->where('sku', $sku)->first();

        if ($model) {
            $diff = [];

            /**
             * Map object key with array key and check for changes
             */
            foreach ([
                'is_enabled'       => "Active",
                'name'             => "Product Name",
                'description'      => "Description",
                'ingridients'      => "Ingridients",
                'expiration-date'  => "Expiration Date",
                'market-days'      => "Market Days",
                'price'            => "Price",
                'mark-up'          => "Mark Up",
            ] as $key => $value) {
                $changed = $this->hasChanged($row[$value], $model->$key);

                if ($changed != null) {
                    $row[$value] = $changed;

                    $diff[] = $value;
                }
            }

            $sqlite->update([
                'hasCheckedForChange' => true,
                'differences' => \implode(',', $diff),
            ]);

        }

        return $row;
    }

    public function findFarmer($string)
    {
        Supplier::unguard();

        $s = Supplier::whereHas('metaData', function ($q) use ($string) {
                $q->where('meta_name', 'name')->where('meta_value', $string);
            })
            ->orWhere('custom_id', $string)
            ->first();

        if ($s) {

            try {
                $slug = $s->meta['slug']["lv"];
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }

        } else {
            $s = Supplier::create([
                'custom_id' => Str::slug($string),
                'email' => '',
                'coords' => '',
                'farmer' => 0,
                'craftsman' => 0,
                'featured' => 0,
            ]);

            $slug = $this->findSlug($string, Supplier::class);

            $s->metaData()->updateOrCreate([
                'meta_name' => 'name',
                'language'  => config('app.locale'),
            ], [
                'meta_value' => $string,
            ]);

            $s->metaData()->updateOrCreate([
                'meta_name' => 'slug',
                'language'  => config('app.locale'),
            ], [
                'meta_value' => $slug,
            ]);

            $s->save();
        }

        return $slug;
    }
}
