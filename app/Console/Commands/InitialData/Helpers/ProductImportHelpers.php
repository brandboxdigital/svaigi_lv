<?php namespace App\Console\Commands\InitialData\Helpers;

use App\Plugins\Products\Model\Product;
use App\Plugins\Products\Model\ProductMeta;
use App\Plugins\Products\Model\ProductVariation;

use App\Plugins\Categories\Model\Category;

use App\Schedules;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;

use App\Plugins\Products\Functions\ProductImport;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Suppliers\Model\SupplierMeta;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 *
 */
trait ProductImportHelpers
{

    public function hasChanged($old, $new) {
        if (is_numeric($old) && is_numeric($new)) {
            $parsed_old = ($old == (int) $old) ? (int) $old : (float) $old;
            $parsed_new = ($new == (int) $new) ? (int) $new : (float) $new;

            if ($parsed_new == 0 && $parsed_old != 0) {
                return null;
            }
        }

        if ($old != $new) {
            return $new;
        }

        return null;
    }

    public function fixText($text)
    {
        return \str_replace('"', "'", trim($text) . " ");
    }

    public function findDays($string)
    {
        $arr = collect(\explode(',', $string))
            ->map(function($item) {

                if (is_numeric($item)) {
                    return intval($item);
                }

                return null;
            })
            ->filter();

        if ($arr->isEmpty()) {
            return '0';
        }

        return $arr->implode('|');
    }

    public function findCategories($cats)
    {
        $subCategories = collect();

        foreach (\explode('|', $cats) as $key => $value) {
            $cat = Category::where('id_from_import_file', $value)->first();

            if ($cat) {
                $subCategories->push($cat->ancestors->pluck('slug'));
                $subCategories->push($cat->slug);
            }
        }

        $subCategories = $subCategories->flatten()->unique()->implode('|');

        return $subCategories;
    }

    public function findExpDate($text)
    {
        $string = $this->fixText($text);
        $string =  \str_replace('Relizācijas termiņš:', "", $string . " ");

        return trim($string);
    }

    public function findMesureUnit($string)
    {
        $arr = collect(explode(' ', trim($string)));

        if ($arr->count() < 2) {
            return 'g';
        }

        $unit = $arr->reverse()->first();

        if ($unit == 'gb') {
            $unit = 'gab';
        }

        return $unit;
    }

    public function guessVat($cat,$cat2)
    {
        $vatList = [
            'Sezonali1' => 5,
            'Darzeni' => 5,
            'Biezeni' => 12,
        ];

        $cats = collect(\explode(',', $cat))->merge(\explode(',', $cat2));

        foreach ($vatList as $catName => $vat) {
            if ($cats->contains($catName)) {
                return $vat;
            }
        }

        return 21;
    }

    public function guessAttributes($string)
    {
        // $mesureUnit = $this->findMesureUnit($string);

        return 'svars:100g,500g';
    }

    public function guessVariations($string)
    {
        if (empty($string)) {
            return "1:none";
        }

        return $string;

        // foreach (\explode('|', $cats) as $key => $value) {
        // }
        // return $subCategories->flatten()->unique()->implode('|');
    }
}
