<?php namespace App\Console\Commands\InitialData;

use Illuminate\Database\Seeder;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Faker\Factory;

use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;

class ImporterBase
{
    /**
     * @todo Use this class to import from other sources
     */
    const SOURCE_NONE    = 'none';
    const SOURCE_G_SHEET = 'google_sheet';
    const SOURCE_DB      = 'db';

    /**
     * Used source
     * @todo Use this class to import from other sources
     */
    public $source = self::SOURCE_G_SHEET;


    public $googleFileId  = null;  // Set this in child class
    public $googleSheetId = null;  // Set this in child class

    public $db_connection = null;  // Set this in child class

    public $console;
    public $output;
    public $faker;

    public $progressBar = null;
    public $fileHandle = null;
    public $fileLength = 0;
    public $currentRow = null;

    private $importFinished = false;


    public function __construct($console)
    {
        $this->faker          = Factory::create('lv_LV');
        $this->console        = $console;
        $this->output         = $console->getOutput();


        /**
         * Source setup checks
         */
        switch ($this->source) {

            /**
             * If google sheets, then IDs must exist
             */
            case self::SOURCE_G_SHEET:
                if (!$this->googleFileId || !$this->googleSheetId) {
                    throw new \Exception('$googleFileId and/or $googleSheetId not defined!');
                }
                break;

            case self::SOURCE_DB:
                if (!$this->db_connection) {
                    throw new \Exception('$db_connection and/or $db_table not defined!');
                }
                break;

            /**
             * if self::SOURCE_NONE
             */
            default:
                break;
        }
    }

    /**
     * Static creator
     */
    public static function make($console)
    {
        return new static($console);
    }

    /**
     * Use inside ->run() to do something inside each function
     *
     *
     *
     * e.g. if using G_SHEETS :
     *
     * $this->run(function($row, $importer) {
     *    Model::create([
     *       'field' => $row[0],
     *    ]);
     * })
     */
    public function eachRow($callable)
    {
        $functionToCall = 'eachRow' . '__' . $this->source;

        if (\method_exists($this, $functionToCall)) {
            $this->$functionToCall($callable);
        }
    }


    /**
     * Called if $this->source == self::SOURCE_G_SHEET
     */
    public function eachRow__google_sheet($callable)
    {
        DB::beginTransaction();

        $this->log('Importing - ' . get_class($this));
        $this->log('');
        $this->openFile();

        $this->createProgressBar();


        $this->progressBar->start($this->fileLength);

        $is_first = true;
        $row = 0;

        $records = $this->fileHandle->getRecords();

        foreach ($records as $offset => $line_of_text) {
            $this->currentRow = $offset;
            if ($this->importFinished) {
                break;
            }

            $row++;

            $this->progressBar->setMessage('Importing Row: '. $row);

            // skip first lines
            if ($is_first) {
                $is_first = false;
                $this->progressBar->setMessage('Skipped');
                // $this->console->info('Skipped');
                continue;
            }

            // skip empty lines and disabled
            if (!isset($line_of_text[0]) || $line_of_text[0] == '' || $line_of_text[0] == '0' || $line_of_text[0] == '-') {
                $this->progressBar->setMessage('Row #' . $row . ' Skipped');
                continue;
            }

            $callable($line_of_text, $this);

            $this->progressBar->advance();
        }

        $this->progressBar->finish();

        $this->newLine(2);

        // fclose($this->fileHandle);

        DB::commit();
    }

    /**
     * Called if $this->source == self::SOURCE_DB
     */
    public function eachRowTable($table, $callable)
    {
        // DB::beginTransaction();

        $this->log('Importing - ' . get_class($this));
        $this->log('');

        $this->createProgressBar();

        $is_first = true;
        $row = 0;

        $items = DB::connection($this->db_connection)
            ->table($table)
            ->get();

        $this->progressBar->start($items->count());

        foreach ($items as $item) {
            if ($this->importFinished) {
                break;
            }

            $this->progressBar->setMessage('Importing Row: '. $row);

            $callable($item, $this);

            $this->progressBar->advance();
            $row++;
        }

        $this->progressBar->finish();

        $this->newLine(2);

        // fclose($this->fileHandle);

        // DB::commit();
    }


    public function stopImport()
    {
        $at = $this->currentRow;

        $this->newLine();
        $this->log("Stopping Import at $at - " . get_class($this), true);
        $this->newLine();

        $this->importFinished = true;
    }

    // ---------------------------------------------------------------

    /**
     * Helper function inside eachRow() callable
     * @todo Not ready!!
     */
    public function file($path)
    {
        $fullPath = $this->seedFilesSource . '/' . $path;

        $fileExists = is_writable($fullPath);

        if ($fileExists) {
            return $fullPath;
        }

        return null;
    }

    /**
     * Helper function inside eachRow() callable
     * @todo Not ready!!
     */
    public function open($path)
    {
        $filePath = $this->file($path);

        if ($filePath) {
            return File::get($filePath);
        }

        return null;
    }

    /**
     * Helper function inside eachRow() callable
     */
    public function valueOrNull($value)
    {
        if (isset($value) && !empty($value) ) {

            if ( $value == '' || $value == '0' ) {
                return null;
            }

            return trim($value);
        }

        return null;
    }

    /**
     * Used to echo text in console
     */
    public function log($message, $isWarning = false)
    {
        if ($isWarning) {
            $this->console->warn($message);
        } else {
            $this->console->info($message);
        }
    }

    /**
     * Print 1 newline or multiple
     */
    public function newLine($lines = 1)
    {
        for ($i=0; $i < $lines; $i++) {
            $this->console->line('');
        }
    }

    /**
     * Generate random image file..
     * @todo Not ready!!
     */
    public static function randomImage($iid = null)
    {
        if (!$iid) {
            $iid = rand(0,1084);
        }

        $iter = 0;
        $info = null;
        $err  = '';

        do {
            $iid = $iid + $iter;

            try {
                $info = json_decode(file_get_contents("https://picsum.photos/id/$iid/info"));
            } catch (\Throwable $th) {
                $err = $th->getMessage();
            }

            $iter++;
        } while ( $info==null && $iter < 20);


        if ($info == null) {
            throw new Throwable("Error getting random image | " . $err);
        }

        $w = ceil($info->width / 4);
        $h = ceil($info->height / 4);

        $file = new \System\Models\File;
        $file->fromUrl("https://picsum.photos/id/$iid/$w/$h", 'image.jpg');

        return $file;
    }

    /**
     * Inner used function to generate prograssbar
     */
    public function createProgressBar()
    {
        $this->progressBar = $this->output->createProgressBar();
    }

    /**
     * Make temp file path. Also make the parent dir if does not exist
     */
    public function getCacheFilePath()
    {
        $storage = Storage::disk('local');

        $name = Str::slug(__CLASS__ . $this->googleSheetId . $this->googleFileId);
        $name = hash('md5', $name) . '.csv';

        $storage->makeDirectory('temp');

        return $storage->path('temp') . '/' . $name;
    }

    /**
     * Downloads CSV from google sheets.
     *
     * If file exists and is not older than 10 min, then uses that!
     * Sets file handle and file length
     */
    public function openFile()
    {
        $tmpCsvFile  = $this->getCacheFilePath();
        $fileExists  = is_writable($tmpCsvFile);
        $file_length = 0;

        /**
         * If file exists, but is old -> delete it
         */
        if( $fileExists ) {
            $minute = 60;

            if (time()-filemtime($tmpCsvFile) > 10 * $minute) {
                $this->log('File is older than 10 minutes. Clearing saved file!');
                $deleted = unlink($tmpCsvFile);

                $fileExists = false;
            }
        }

        /**
         * If file does not exist -> download
         */
        if (!$fileExists) {

            if ( is_writable($tmpCsvFile) ) {
                $deleted = unlink($tmpCsvFile);
            }

            $this->log('Downloading and Opening Google File');

            // $gSheetsFile = "https://docs.google.com/spreadsheets/d/" . $this->googleFileId . "/gviz/tq?tqx=out:csv&sheet=" . $this->googleSheetId;
            $gSheetsFile = "https://docs.google.com/spreadsheets/d/" . $this->googleFileId . "/export?format=csv&id=" . $this->googleFileId . "&gid=" . $this->googleSheetId;

            try {

                $arrContextOptions=array(
                    "ssl"=>array(
                        "verify_peer"=>false,
                        "verify_peer_name"=>false,
                    ),
                );

                $cont = file_get_contents($gSheetsFile, false, stream_context_create($arrContextOptions));

                file_put_contents($tmpCsvFile, $cont);

            } catch (\Throwable $th) {
                throw new \Exception("Error Downloading Google file! URL: $gSheetsFile | Error:" . $th->getMessage());
            }
        } else {
            $this->log('Using cached file');
        }

        /**
         * Try to get file length
         */
        try {
            $file_handle = Reader::createFromPath($tmpCsvFile);
            $file_length = count($file_handle);

        } catch (\Throwable $th) {
            throw new \Exception("Error Opening downloaded file!");
        }

        $this->fileLength = $file_length;
        $this->fileHandle = $file_handle;
    }

    public function findSlug($name, $class)
    {
        $metaClass = (new $class)->metaClass;
        $originalSlug = Str::slug($name);
        $slug = $originalSlug;
        $count = 1;

        while ($metaClass::where('meta_name', 'slug')->where('meta_value', $slug)->first() != null && $count < 10) {
            $slug = $originalSlug . '-' . $count;
            $count++;
        }

        return $slug;
    }
}
