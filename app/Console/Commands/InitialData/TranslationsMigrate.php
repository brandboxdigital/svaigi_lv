<?php namespace App\Console\Commands\InitialData;

use Illuminate\Http\Request;

class TranslationsMigrate extends ImporterBase
{
    public $googleFileId = '1_S6bxusNKTW-vCyf1k7vXdqbRkTelI1x9roDE2W0wZw';
    public $googleSheetId = '2059590153';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->eachRow(function($row) {
            dump($row);
        });

        dd('end');
    }

}
