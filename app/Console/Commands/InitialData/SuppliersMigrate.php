<?php namespace App\Console\Commands\InitialData;

use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Suppliers\Model\SupplierMeta;

use Goutte\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Plugins\Admin\Model\File as SvFile;

use App\Functions\General;
use Illuminate\Support\Facades\DB;

class SuppliersMigrate extends ImporterBase
{
    use General;

    /**
     * Used source
     * @todo Use this class to import from other sources
     */
    public $source        = self::SOURCE_DB;
    public $db_connection = 'mysql_old';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->log('Importing - ' . get_class($this));
        $this->log('');
        $this->createProgressBar();

        if ($this->console->option('prune', false)) {
            $this->prune();
        }

        $onlyimages = $this->console->option('onlyimages', false);

        $this->eachRowTable('farmer', function($row) use ($onlyimages){
            if ($onlyimages) {
                $model = Supplier::where('custom_id', $row->code)->first();

                if ($model) {
                    $this->handleImages($model, false, $this->addFile($row->img));
                }
            } else {
                $this->saveFarmer($row);
            }
        });
    }

    public function prune()
    {
        SvFile::where('owner', 'supplier_image')
            ->orWhere('owner', 'supplier_image')
            ->get()
            ->each(function($file) {
                $file->delete();
            });

        Supplier::truncate();
        SupplierMeta::truncate();

        exec('rm -f ' . storage_path('app/public/suppliers/*'));
    }

    public function saveFarmer($oldpost)
    {
        if ($oldpost->is_public == 'n' || $oldpost->is_deleted == 'y') {
            return null;
        }

        $metas = collect([
            'name'                => ['lv' => $oldpost->title],
            'slug'                => $oldpost->slug,
            // 'jur_name'         => ['lv' => $oldpost->title],
            'description'         => ['lv' => $this->filterOutImages($oldpost->descr)],
            'excerpt'             => ['lv' => $oldpost->intro],
            'google_keywords'     => ['lv' => $oldpost->seo_keywords],
            'google_description'  => ['lv' => $oldpost->seo_description],
            // 'location'         => ['lv' => ''],
        ]);

        $newHasEmail = \strlen($oldpost->email) > 1;

        $model = Supplier::where('custom_id', $oldpost->code)->first();

        // Try to skip import where new has no email
        if ($model) {
            $existingHasEmail = \strlen($model->email) > 1;

            if ($existingHasEmail && !$newHasEmail) {
                return null;
            }
        }

        $model = Supplier::updateOrCreate(['custom_id' => $oldpost->code], [
            'custom_id' => $oldpost->code,
            'email'     => $oldpost->email,
            'coords'    => '',
            'farmer'    => 1,
            'craftsman' => 0,
            'featured'  => 0,
            'user_id'   => null,
        ]);

        $this->handleMetas($model, $metas->keys(), 'name', $metas->toArray());
        $this->handleImages($model, false, $this->addFile($oldpost->img));

        $model->forgetMeta();
    }

    protected function addFile($imgName)
    {
        if (!$imgName) {
            return null;
        }

        if (!Storage::exists("temp/$imgName")) {
            try {
                Storage::put("temp/$imgName", file_get_contents("https://svaigi.lv/file/farmer/original/$imgName"));
            } catch (\Throwable $th) {
                return null;
            }
        }

        $uploadedfile = new \Illuminate\Http\UploadedFile(Storage::path("temp/$imgName"), $imgName);

        $request = new \Illuminate\Http\Request;
        $request->setMethod('POST');
        $request->request->replace([
            'owner' => 'supplier_image',
            'path' => 'supplier_image',
        ]);
        $request->files->replace(['supplier_image' => [$uploadedfile] ]);

        $response = (new \App\Plugins\Admin\AdminController)->uploadFile($request);

        $returnObject = [];

        $doc = new \DOMDocument();
        foreach ($response['data'] as $strng) {
            $doc->loadHTML($strng);
            $image = [];

            foreach ($doc->getElementsByTagName('input') as $node) {

                if ($node->getAttribute('name') == 'image_url[]') {
                    $returnObject[0][] = $node->getAttribute('value');
                } else if ($node->getAttribute('name') == 'image_main[]') {
                    // $returnObject[1][] = $node->getAttribute('value');
                    $returnObject[1][] = 0;
                } else if ($node->getAttribute('name') == 'image_id[]') {
                    $returnObject[2][] = $node->getAttribute('value');
                }

                // $returnObject[str_replace('[]', '', $node->getAttribute('name'))][] = $node->getAttribute('value');
            }
        }

        return $returnObject;
    }

    protected function filterOutImages($html)
    {
        $re = '/<img.*\/>/m';
        return preg_replace($re, '', $html);
    }

}
