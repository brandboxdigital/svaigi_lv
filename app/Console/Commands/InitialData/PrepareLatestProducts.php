<?php namespace App\Console\Commands\InitialData;

use App\Plugins\Products\Model\Product;
use App\Plugins\Products\Model\ProductMeta;
use App\Plugins\Products\Model\ProductVariation;

use App\Plugins\Categories\Model\Category;

use App\Schedules;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;

use App\Plugins\Products\Functions\ProductImport;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\Suppliers\Model\SupplierMeta;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PrepareLatestProducts extends \App\Console\Commands\InitialData\ImporterBase
{
    use \App\Console\Commands\InitialData\Helpers\ProductImportHelpers;

    public $googleFileId = '1_S6bxusNKTW-vCyf1k7vXdqbRkTelI1x9roDE2W0wZw';
    public $googleSheetId = '171753474';

    const MAX = 10;

    public $attrs = [
        'sku',
        'is_enabled',
        'name',

        'description',
        'ingridients',
        'expiration-date',

        'market-days',

        'price',
        'mark-up',

        'hasCheckedForChange',
        'differences',
    ];
    private $conn = null;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->console->option('show', false)) {
            $this->show();

            return null;
        }

        $this->prepareFreshSqlite();
        $this->conn = DB::connection('sqlite')->table('temp');

        $counter = 0;
        $this->eachRow(function ($row, $import) use (&$counter) {
            $counter++;

            $insert = array_combine($this->attrs, [
                $row[0],
                $row[3],
                $row[2],

                $this->fixText($row[10]),
                $this->fixText($row[11]),
                $this->findExpDate($row[12]),

                $this->findDays($row[16]),

                empty($row[18]) ? 0 : floatval($row[18]),
                round((floatval($row[19]) * 100)),

                false,
                '',
            ]);

            $this->conn->insert([
                $insert,
            ]);
        });

        $this->log('Latest product tmp db filled!');
    }

    public function show()
    {
        $sqlite = DB::connection('sqlite')->table('temp');

        $values = $sqlite->get()->map(function($item) {
            return [
                $item->sku,
                (boolean) $item->hasCheckedForChange == 1,
            ];
            return collect($item)->values();
         })->toArray();

        // $this->console->table($this->attrs, $values);
        $this->console->table(['SKU', 'Has Checked', 'What Changed'], $values);
    }

    public function prepareFreshSqlite()
    {
        if (file_exists(database_path('database.sqlite'))) {
            unlink(database_path('database.sqlite'));
        }

        touch(database_path('database.sqlite'));

        $attrs = $this->attrs;
        Schema::connection('sqlite')->create('temp', function (Blueprint $table) use($attrs) {
            $table->increments('id');

            foreach ($attrs as $attr) {
                $table->string($attr)->nullable();
            }
        });
    }

    public function moveLatestDataFileToSqlite()
    {


        $sqlite = DB::connection('sqlite')->table('temp');
        dd($sqlite->select()->get());

        $vals = array_combine($attrs, ['test','test','test','test','test','test','test','test']);



        dd('stop');
    }

}
