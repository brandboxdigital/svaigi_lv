<?php

namespace App\Console\Commands\Reports;

use App\Schedules;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use App\Plugins\Reports\Functions\UserReport as Report;

class UserReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reports:user-report {schedule_id? : Id of schedule to run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make user report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /** @var Schedules $schedule */
        if(!$this->getScheduleId()) {

            $this->info('Searching Scheduled jobs');
            $schedule = Schedules::whereStillIncompleate($this->getName())->first();
        } else {
            $this->info('Searching job with id: '.$this->getScheduleId());
            $schedule = Schedules::find($this->getScheduleId());

            if($schedule->type!=$this->getName()) {
                $this->error('Schedule is not User Report');
            }
        }

        if(!$schedule) {
            $this->error("Schedule not found");
            return;
        } else {
            $schedule->update(['running' => 1]);
        }

        try {

            $report = (new Report($schedule))->run();

        } catch (\Throwable $th) {
            $schedule->update([
                'status' => false,
                'message' => "Error " . $th->getMessage()
            ]);
        }
    }

    public function getScheduleId() {
        return $this->argument('schedule_id')??false;
    }


}
