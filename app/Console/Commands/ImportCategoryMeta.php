<?php

namespace App\Console\Commands;

use App\Schedules;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use League\Csv\Reader;
use League\Csv\Statement;
use App\Plugins\Categories\Model\Category;
use App\Plugins\Categories\Model\CategoryMeta;

class ImportCategoryMeta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'svaigi:importcategorymeta {schedule_id? : Id of schedule to run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Category meta, if there is uploaded import file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->runImport();

        return;
    }

    public function getScheduleId() {
        return $this->argument('schedule_id')??false;
    }

    private function runImport()
    {
        /** @var Schedules $schedule */
        if(!$this->getScheduleId()) {
            $this->info('Searching Scheduled jobs');
            $schedule = Schedules::where(['running' => 0, 'type' => 'categoryMetaImport', 'finished' => 0])->orWhere(function (Builder $q) {
                $q->where('running', 1)
                    ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                    ->where('type', 'categoryMetaImport')
                    ->where('finished', 0);
            })->first();
        } else {
            $this->info('Searching job with id: '.$this->getScheduleId());
            $schedule = Schedules::find($this->getScheduleId());
        }

        if(!$schedule) {
            $this->error("Schedule not found");
            return;
        } else {
            $schedule->update(['running' => 1]);
        }

        $result = $this->import($schedule->toArray());

        $msgType = "error";

        if($result['status']==true) {
            \Storage::delete('imports/category-meta/'.$schedule['filename']);
            $msgType="info";
        }

        $schedule->update(['running' => 0, 'finished' => 1, 'result_state' => $result['status'], 'result_message' => $result['message']]);

        $this->$msgType($result['message']);
    }

    public function import($schedule)
    {
        try {

            if (!ini_get("auto_detect_line_endings")) {
                ini_set("auto_detect_line_endings", '1');
            }

            $filename = storage_path("app/imports/category-meta/" . $schedule['filename']);
            if(!\Storage::exists("imports/category-meta/" . $schedule['filename'])) {
                return ['status' => false, 'message' => 'Import File not present'];
            }
            $csv = Reader::createFromPath($filename, 'r');
            // $csv->setHeaderOffset(0);

            // if(!empty(array_diff($csv->getHeader(), $this->importFieldData())) || !empty(array_diff($this->importFieldData(),$csv->getHeader()))) {
            //     return ['status' => false, 'message' => 'Missing fields in import (or import has renamed headers)'];
            // }

            Schedules::find($schedule['id'])->update(['total_lines' => $csv->count()]);

            $linereader = (new Statement())->offset($schedule['stopped_at']);
            $headers = [];

            // $previousProduct = false;
            foreach ($linereader->process($csv) as $l => $importLine) {

                if($l == 0) {
                    foreach($importLine as $line) {
                        $headers[] = $line;
                    }
                } else {
                    if (strlen($importLine[0]) > 0) {
                        $slug = explode('/', $importLine[0]);
                        $slug = end($slug);
                    }
                    if (strlen($importLine[1]) > 0) {
                        $slug = explode('/', $importLine[1]);
                        $slug = end($slug);
                    }
                    if (strlen($importLine[2]) > 0) {
                        $slug = explode('/', $importLine[2]);
                        $slug = end($slug);
                    }

                    $keyWords = $importLine[3];
                    $description = $importLine[6];

                    $category = Category::where('unique_name', $slug)->first();

                    if ($category) {
                        $metaKeywords = CategoryMeta::firstOrNew([
                            'owner_id' => $category->id,
                            'meta_name' => 'google_keywords'
                        ]);
                        $metaKeywords->meta_value = $keyWords;
                        $metaKeywords->meta_name = 'google_keywords';
                        $metaKeywords->language = 'lv';
                        $metaKeywords->save();

                        $metaGoogleDescription = CategoryMeta::firstOrNew([
                            'owner_id' => $category->id,
                            'meta_name' => 'google_description'
                        ]);
                        $metaGoogleDescription->meta_value = $description;
                        $metaGoogleDescription->meta_name = 'google_description';
                        $metaGoogleDescription->language = 'lv';
                        $metaGoogleDescription->save();

                    }
                }

            }

            $csv = null;

            \Artisan::call('cache:clear');
            return ['status' => true, 'message' => 'Import Successful'];
        } catch (\Throwable $th) {
            $sh = Schedules::find($schedule['id']);

            return ['status' => false, 'message' => 'Error @ line ' . $sh->stopped_at . " | " . $th->getMessage()];
        }
    }

}
