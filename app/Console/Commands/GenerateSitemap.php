<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use App\Plugins\Products\Model\Product;
use App\Plugins\Products\Model\ProductMeta;
use App\Plugins\Categories\Model\Category;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string|int $id
     * @param string $link
     *
     * @return string
     */
    public function getCategoryLink($id, $link = '')
    {
        $category = Category::find($id);

        if (!$category) {
            return $link;
        }

        $link .= "/{$category->unique_name}";

        if ($category->parent_id) {
            $this->getCategoryLink($category->parent_id, $link);
        }

        return $link;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::all();
        $svaigi = 'http://svaigi.lv';
        $sitemap = Sitemap::create('http://svaigi.lv');

        foreach ($products as $product) {
            $meta = ProductMeta::where('owner_id', $product->id)->where('meta_name', 'slug')->first()->meta_value;
            if ($product->main_category == 0) {
                $link = $svaigi . '/' . $meta;
            } else {
                $link = $svaigi . $this->getCategoryLink($product->main_category) . '/'  . $meta;
            }

            $sitemap->add("{$link}");
        }

        $sitemap->writeToFile(public_path('sitemap.xml'));

        echo('Sitemap generated');
    }
}
