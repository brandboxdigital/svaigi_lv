<?php

namespace App\Console;

use App\Schedules;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Console\Commands\OrderImport;
use App\Plugins\Reports\Model\Report;
use App\Console\Commands\ExportOrders;
use App\Console\Commands\PdfGeneration;
use App\Console\Commands\ImportProducts;
use App\Console\Commands\SendOrderEmails;
use Illuminate\Database\Eloquent\Builder;
use App\Console\Commands\ExportOrdersToPdf;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\ImportProductImages;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportProducts::class,
        ImportProductImages::class,
        ExportOrders::class,
        ExportOrdersToPdf::class,
        OrderImport::class,
        PdfGeneration::class,
        SendOrderEmails::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Import Products
        $this->importProductsSchedule($schedule);

        // Export Orders
        $this->exportOrders($schedule);

        // Import category meta
        $this->importCategoryMeta($schedule);


        // Import Orders
        $this->ImportOrders($schedule);


        $this->runReports($schedule);

        $this->cacheClear($schedule);
    }

    public function cacheClear($schedule)
    {
        try {
            $schedule->command('cache:clear')->dailyAt('03:00')
            ->appendOutputTo(storage_path('logs/schedule--cacheclear.log'))
            // ->withoutOverlapping()
            ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}
    }

    public function importCategoryMeta($schedule)
    {
        try {
            // Products
            $schedule->command("svaigi:importcategorymeta")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'categoryMetaImport', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'categoryMetaImport')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--categorymetaimport.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Product and product Image import scheduler
     *
     * @param Schedule $schedule
     */
    public function importProductsSchedule(Schedule $schedule)
    {
        try {
            // Products
            $schedule->command("svaigi:importproducts")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'productImport', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'productImport')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--importproducts.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}

        try {
            // Exporting Products
            $schedule->command("svaigi:importproducts --export")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'productExport', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'productExport')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--exportproducts.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}

        try {
            // Images
            $schedule->command("svaigi:importImages")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'productImageImport', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'productImageImport')
                        ->where('finished', 0);
                })->first();

                return $scheduled;

            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--importImages.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}
    }

    /**
     * Order Export Scheduler
     *
     * @param Schedule $schedule
     *
     */
    public function exportOrders(Schedule $schedule)
    {
        try {
            // Images
            $schedule->command("svaigi:exportOrders")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'orderExport', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'orderExport')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--exportOrders.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}

        try {
            // PDF
            $schedule->command("svaigi:exportOrdersToPdf")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'orderExportToPdf', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'orderExportToPdf')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--exportOrdersToPdf.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}
    }




    /**
     * Order Export Scheduler
     *
     * @param Schedule $schedule
     *
     */
    public function importOrders(Schedule $schedule)
    {
        try {
            // Order Update
            $schedule->command("svaigi:importOrders")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'orderImport', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'orderImport')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--importOrders.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}

        try {
            // PDF generation
            $schedule->command("svaigi:createPDF")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'createPDF', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'createPDF')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--createPDF.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}

        try {
            // Send PDF's
            $schedule->command("svaigi:sendOrderEmails")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'sendOrderEmails', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'sendOrderEmails')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
        } catch (\Throwable $th) {}

        try {
            // create order Summary
            $ev = $schedule->command("svaigi:createSummary")->everyMinute()->when(function () {
                $scheduled = Schedules::where(['running' => 0, 'type' => 'orderSummary', 'finished' => 0])->orWhere(function (Builder $q) {
                    $q->where('running', 1)
                        ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                        ->where('type', 'orderSummary')
                        ->where('finished', 0);
                })->first();

                return $scheduled;
            })
                ->runInBackground()
                ->appendOutputTo(storage_path('logs/schedule--createSummary.log'))
                // ->withoutOverlapping(10)
                ->evenInMaintenanceMode();
            // unset($ev->mutex);
            // dd($ev);
        } catch (\Throwable $th) {}
    }


    public function runReports(Schedule $schedule)
    {
        try {
            // Reports
            foreach (Report::get() as $report) {
                $log = Str::slug($report->slug);
                $schedule->command($report->slug)->everyMinute()->when(function () use ($report) {
                    return Schedules::whereStillIncompleate($report->slug)->first();
                })
                    ->runInBackground()
                    ->appendOutputTo(storage_path("logs/schedule--$log.log"))
                    ->withoutOverlapping(10);
            }
        } catch (\Throwable $th) {}
    }
}
