<?php

namespace App;

use App\SocialFacebookAccount;
use App\Cache\PromoCache;
use App\Http\Controllers\CacheController;
use App\Notifications\ResetPassword;
use App\Plugins\Orders\Model\OrderHeader;
use App\Plugins\Products\Model\Product;
use App\Plugins\Suppliers\Model\Supplier;
use App\Plugins\UserGroups\Model\UserGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

/**
 * Class User
 *
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'username',
        'name',
        'last_name',
        'email',
        'email_verified_at',
        'password',
        'registered',
        'phone',
        'is_legal',
        'legal_name',
        'legal_address',
        'legal_reg_nr',
        'legal_vat_reg_nr',
        'address',
        'city',
        'postal_code',
        'isAdmin',
        'comment',
        'address_comments'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Get all discounts for products, using current user
     *
     * @return int
     */
    public function discount($product = false, $category = false, $getName = false)
    {
        if(!$product || !$category) { return 0; }

        $pc = (new CacheController)->getPromotions();

        $discSeperated = [
            $pc->getKeyValuePair('product', $product, $this->user_group_id),
            $pc->getKeyValuePair('category', $category, $this->user_group_id)
        ];


        $promMaxKey = null;
        $promMaxValue = 0;

        for ($i=0; $i < 2; $i++) {
            foreach ($discSeperated[$i] as $key => $val) {
                if ($val > $promMaxValue) {
                    $promMaxValue = $val;
                    $promMaxKey = $key;
                }
            }
        }

        if($getName && $promMaxKey) {
            return $pc->promotionNames[$promMaxKey] ?? "Unknown Name";
        }

        // throw new Exception("Error Processing Request", 1);
        // dd(compact('discSeperated', 'pc', 'category', 'product', 'promMaxValue'));

        return $promMaxValue;
    }

	public function hasPassword()
	{
		return $this->password != NULL;
    }

    /**
     * Get user cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cart()
    {
        return $this->hasMany(OrderHeader::class);
    }

    public function orders($id = null) {
        $relation = $this->hasMany(OrderHeader::class)->where('state', '!=', 'draft');

        if($id) {
            return $relation->where('id', $id);
        }
        return $relation;
    }

    /**
     * return if
     *
     * @return bool
     */
    public function isAnonimous()
    {
        return ($this->id ?? false) == 99;
    }

    /**
     * @return mixed
     */
    public function isAdmin() {
        return $this->isAdmin ?? false;
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getFullNameAttribute() {
        return $this->name." ".$this->last_name;
    }

    /**
     * @param array $requestData
     *
     * @return User
     */
    public function updateUser(array $requestData) {

        if(($requestData['id']??false)) {
            $userFind['id'] = $requestData['id'];
        } else {
            $isInUsers = User::where(['email' => request('email'), 'registered' => 0])->first();
            $userFind['id'] = $isInUsers?$isInUsers->id:null;
        }
        $requestData['registered'] = 1;

        return User::updateOrCreate($userFind, $requestData);

    }

    public function group() {
        return $this->belongsTo(UserGroup::class, 'user_group_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function isSocialAuthed() {
        return $this->hasOne(SocialFacebookAccount::class);
    }

    public function supplier() {
        return $this->hasOne(Supplier::class);
    }

    public function isSocialAuthedCount() {
        return $this->hasOne(SocialFacebookAccount::class)->get()->count();
    }

    public function favouriteProducts() {
        return $this->belongsToMany(Product::class, 'user_favourite_products');
    }

    public function isProductFavourited($product_id) {
        return $this->favouriteProducts->find($product_id) !== null;
    }
}
