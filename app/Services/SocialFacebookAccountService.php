<?php

namespace App\Services;
use App\User;
use App\SocialFacebookAccount;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->id)
            ->first();
		
        if ($account) {
            return $account->user;
        } else {
            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->id,
                'provider' => 'facebook'
            ]);

            $user = User::where([['users.registered', 1], ['social_facebook_accounts.provider_user_id', $providerUser->id]])
                ->whereNotNull('social_facebook_accounts.user_id')
                ->leftJoin('social_facebook_accounts', 'social_facebook_accounts.user_id', '=', 'users.id')
                ->first();

            if (!$user) {
                $userToCreate = [];
                
                if (!empty($providerUser->email)) {
                    $userToCreate['email'] = $providerUser->email;
                }
                else {
                    $userToCreate['email'] = 'noemail@facebook.com';
                }

                if (!empty($providerUser->name)) {
                    $fullName = explode(' ', $providerUser->name);

                    if (!empty($fullName[0])) {
                        $userToCreate['name'] = $fullName[0];
                    }

                    if (!empty($fullName[1])) {
                        $userToCreate['last_name'] = $fullName[1];
                    }
                }

                $user = User::create($userToCreate);
            }

            $account->user()->associate($user);
            $account->save();
			
			$user = User::find($user->id); 

            return $user;
        }
    }
}