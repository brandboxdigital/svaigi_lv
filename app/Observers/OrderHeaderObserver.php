<?php

namespace App\Observers;

use Mail;
use App\Mail\OrderStatusUpdated;
use App\Plugins\Orders\Model\OrderHeader;


class OrderHeaderObserver
{
    /**
     * Handle the order header "created" event.
     *
     * @param  \App\Plugins\Orders\Model\OrderHeader  $orderHeader
     * @return void
     */
    public function created(OrderHeader $orderHeader)
    {
        //
    }

    /**
     * Handle the order header "updated" event.
     *
     * @param  \App\Plugins\Orders\Model\OrderHeader  $orderHeader
     * @return void
     */
    public function updated(OrderHeader $orderHeader)
    {
        $changes = $orderHeader->getChanges();

        if (\in_array('state', array_keys($changes))) {
            $email = new OrderStatusUpdated($orderHeader);

            if ($email->needToSend()) {
                Mail::send($email);
            }
        }
    }

    /**
     * Handle the order header "deleted" event.
     *
     * @param  \App\Plugins\Orders\Model\OrderHeader  $orderHeader
     * @return void
     */
    public function deleted(OrderHeader $orderHeader)
    {
        //
    }

    /**
     * Handle the order header "restored" event.
     *
     * @param  \App\Plugins\Orders\Model\OrderHeader  $orderHeader
     * @return void
     */
    public function restored(OrderHeader $orderHeader)
    {
        //
    }

    /**
     * Handle the order header "force deleted" event.
     *
     * @param  \App\Plugins\Orders\Model\OrderHeader  $orderHeader
     * @return void
     */
    public function forceDeleted(OrderHeader $orderHeader)
    {
        //
    }
}
