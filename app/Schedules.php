<?php

namespace App;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Plugins\Reports\Model\Report;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Schedules extends Model
{
    public $fillable = [
        'type',
        'filename',
        'total_lines',
        'stopped_at',
        'running',
        'finished',
        'result_state',
        'result_message',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $user = Auth::user();
            if ($user) {
                $model->user_id = $user->id;
            }
        });

        // self::created(function($model){
        //     // ... code here
        // });

        // self::updating(function($model){
        //     // ... code here
        // });

        // self::updated(function($model){
        //     // ... code here
        // });

        // self::deleting(function($model){
        //     // ... code here
        // });

        // self::deleted(function($model){
        //     // ... code here
        // });
    }

    public function getTypeNameAttribute()
    {
        $scheduleNames = array_merge(
            config('app.schedulenames', []),
            Report::get()->pluck('description','slug')->toArray()
        );

        return $scheduleNames[$this->type] ?? "Unknown Schedule";
    }

    public function getResultStateSwitchAttribute()
    {
        if ($this->finished) {
            return $this->result_state ? '<i class="fas fa-check text-success"></i>' : '<i class="fas fa-times text-danger"></i>';
        }

        $spinnerColor = $this->running ? "fa-spin" : "";

        return '<i class="fas fa-cog '.$spinnerColor.'"></i>';
    }

    public function getIsRunningPartialAttribute()
    {
        if (!$this->running) {
            return '';
        }

        $spinnerColor = $this->running ? "fa-spin" : "";

        return '<i class="fas fa-cog '.$spinnerColor.'"></i>';
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getUserNameAttribute()
    {
        if ($this->user) {
            return $this->user->name;
        }

        return 'unknown';
    }

    public function scopeWhereStillIncompleate($query, $type)
    {
        return $query
            ->where(['running' => 0, 'type' => $type, 'finished' => 0])
            ->orWhere(function (Builder $q) use ($type) {
                $q->where('running', 1)
                    ->where('updated_at', '>=', Carbon::now()->addMinutes(-5))
                    ->where('type', $type)
                    ->where('finished', 0);
            });
    }

}
