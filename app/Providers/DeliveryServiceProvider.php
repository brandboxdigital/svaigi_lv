<?php

namespace App\Providers;

use App\Plugins\Deliveries\Classes\DeliveryServiceContainer;
use Illuminate\Support\ServiceProvider;

class DeliveryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('delivery-providers', function ($app) {
            return new DeliveryServiceContainer();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
