<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderStatusUpdated extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
        $this->user = $order->buyer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->needToSend()) {
            return $this
                ->to($this->order->user_email)
                ->subject($this->getSubject())
                ->markdown($this->getView());
        }

        return null;
    }

    public function needToSend()
    {
        $posibilities = [
            'draft' => false,
            'ordered' => false,
            'accepted' => true,
            'finished' => false,
        ];

        try {
            return $posibilities[$this->order->state];
        } catch (\Throwable $th) {
            return false;
        }
    }


    public function getSubject()
    {
        // 'draft','ordered','accepted','finished'
        $subject = 'Pasūtījums no Svaigi.lv tiek gatavots!';

        $posibilities = [
            'draft' => false,
            'ordered' => false,
            'accepted' => 'Tavs Pasūtījums no Svaigi.lv tiek gatavots!',
            'finished' => false,
        ];

        try {
            $subject = $posibilities[$this->order->state];
        } catch (\Throwable $th) {}

        return $subject;
    }

    /**
     * Get correct mail view.
     * Currently available types are: 'money','invoice','card','paysera'
     */
    private function getView()
    {
        $posibilities = [
            'draft' => false,
            'ordered' => false,
            'accepted' => 'email.order.accepted',
            'finished' => false,
        ];

        try {
            return $posibilities[$this->order->state];
        } catch (\Throwable $th) {
            return false;
        }


        /**
         * If something vent wrong still send something..
         */
        return 'email.order_created';
    }
}
