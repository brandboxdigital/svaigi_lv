<?php

namespace App\Mail;

use App\Plugins\Suppliers\Model\Supplier;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\Finder\SplFileInfo;

class OrderSender extends Mailable
{
    use Queueable, SerializesModels;

    private $files;

    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * Create a new message instance.
     *
     * @param array $files
     * @param Supplier $supplier
     */
    public function __construct(array $files, Supplier $supplier, string $comment = '')
    {
        $this->files = $files;
        $this->supplier = $supplier;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $specialText = ___('translations._supplierTitleRow') ?? "Pielikumā pasūtījums pirmdienai, 1. novembrim.";

        $obj = $this
            ->from('saimniecibas@svaigi.lv')
            ->subject($specialText)
            ->markdown('email.supplier', [
                'title' => $this->supplier->getMeta('name'),
                'comment' => $this->comment,
                'specialText' => $specialText,
            ]);

        foreach($this->files as $file){
            $obj->attach($file);
        }

        return $obj;
    }
}
