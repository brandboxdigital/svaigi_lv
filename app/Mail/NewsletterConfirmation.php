<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class NewsletterConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;

        $this->subject = 'Jaunumu apstiprināšana no Svaigi.lv';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $signedUrl = URL::signedRoute('confirmNewsletter', ['user' => $this->user->id]);
        return $this
            ->subject($this->subject)
            ->markdown('email.newsletter_confirmation')->with('signedUrl', $signedUrl);
    }
}
