<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $totals;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $order)
    {
        $this->user = $user;
        $this->order = $order;
        $this->totals = getCartTotals($order);

        $this->subject = 'Svaigi.lv pasūtījuma apstiprinājums';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->subject)
            ->markdown($this->getView());
    }


    /**
     * Get correct mail view.
     * Currently available types are: 'money','invoice','card','paysera'
     */
    private function getView()
    {
        try {
            switch ($this->order->payment_type) {
                case 'money':
                case 'invoice':
                    return 'email.order_created__cash';
                    break;

                default:
                    return 'email.order_created';
                    break;
            }
        } catch (\Throwable $th) {}


        /**
         * If something vent wrong still send something..
         */
        return 'email.order_created';
    }
}
