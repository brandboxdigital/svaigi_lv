<?php

use App\Plugins\Categories\Model\Category;

/**
 * returns current language code from url, or default if no language specified
 *
 * @return \Illuminate\Config\Repository|\Illuminate\Routing\Route|mixed|object|string
 */
function language() {
    try {
        return request()->route('lang') ?? config('app.locale');
    } catch (\Exception $e) {
        return config('app.locale');
    }
}

/**
 * get available language list
 *
 * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
 */
function languages() {
    $languageCache = \Illuminate\Support\Facades\Cache::get('languagelist');
    if (!$languageCache) {
        $languageCache = \App\Languages::all();
    }
    \Illuminate\Support\Facades\Cache::put("languagelist", $languageCache, 1440);

    return $languageCache;

}

/**
 * @param $path
 * @param $collections
 *
 * @return mixed
 */
function getTranslations($path, $collections) {
    return $collections::selectRaw('id, concat("category.name.", id) as name')->get();
}

/**
 * @param $value
 * @param $results
 *
 * @return mixed
 */
function nullOrDate($value, $results) {
    return ($value ?? false) ? $results[1] : $results[0];
}

/**
 * Calculate price with all possible variations for product
 *
 * @param     $price
 * @param     $vat
 * @param     $markup
 * @param     $discount
 * @param int $amount
 *
 * @return object
 */
function calcPrice($price, $vat, $markup, $discount, $amount = 1) {
    $vat = 1 + ($vat / 100);
    $markup = 1 + ($markup / 100);
    $discount = 1 - ($discount / 100);


    $wovat = $price * $markup * $amount;

    //$wvat = $wovat * $vat;
    $wvat = round(ceil(($wovat * $vat) * 1000) / 1000, 2);
    $prices = [
        'cost' => $price,
        'markup' => $markup,
        'vat' => $vat,
        'discount' => $discount,
    ];

    $prices['full'] = (object)[
        'wovat' => number_format(round($wovat, 2), 2),
        'wvat' => number_format(round($wvat, 2), 2),
        'vat' => number_format(round($wvat - $wovat, 2), 2),
        'markup' => number_format(round($wovat - $price, 2), 2),
    ];

    $discountwvat = $wvat - ($wvat * $discount);
    $pricewdiscount = round(ceil(($wvat * $discount) * 1000) / 1000, 3);
    $pricewdiscountvat = $pricewdiscount - ($pricewdiscount / $vat);

    $prices['wdiscount'] = (object)[
        'discount' => number_format(round($discountwvat, 2), 2),
        'discountvat' => number_format(round($discountwvat - ($discountwvat / $vat), 2), 2),
        'price' => number_format(round($pricewdiscount, 2), 2, ".", ""),
        'pricevat' => number_format(round($pricewdiscountvat, 2), 2),
        'pricewovat' => number_format(round($pricewdiscount - $pricewdiscountvat, 2), 2),
    ];

    $prices['sum'] = (object)[
        'wdiscount' => (object)[
            'wovat' => number_format(round(($pricewdiscount * $amount) - ($pricewdiscountvat * $amount), 2), 2),
            'wvat' => number_format(round($pricewdiscount * $amount, 2), 2),
            'vat' => number_format(round($pricewdiscountvat * $amount, 2), 2),
        ],
        'wodiscount' => (object)[
            'wovat' => number_format(round($wovat * $amount, 2), 2),
            'wvat' => number_format(round($wvat * $amount, 2), 2),
            'vat' => number_format(round(($wvat * $amount) - ($wovat * $amount), 2), 2),
        ],
    ];

    return (object)$prices;
}

/**
 * Frontend Route url maker - with language addition
 *
 * @param       $name
 * @param array $params
 * @param bool $absolute
 *
 * @return string
 */
function r($name, $params = [], $absolute = true) {

    $name = $name . isDefaultLanguage();

    if (!isDefaultLang()) {
        $params['lang'] = $params['lang'] ?? request()->route('lang') ?? "";
    }

    return route($name, $params, $absolute);
}

/**
 * returns route name addition if selected language is default
 *
 * @return string
 */
function isDefaultLanguage() {
    return isDefaultLang() ? ".default" : "";
}

/**
 * detects if current language is defined as default
 *
 * @return bool
 */
function isDefaultLang() {
    return language() == config('app.locale');
}

function getDiscountItemsId(\App\Plugins\Orders\Model\OrderHeader $cart) {
    $discount_items = (is_array($cart->discount_items) && count($cart->discount_items) > 0) ? $cart->discount_items : [];
    $discountItemsId = [];
    if ($cart->discount_target == "product")
        $discountItemsId = $cart->discount_items;
    if ($cart->discount_target == "category") {
        foreach ($discount_items as $categoryId) {
            $category = Category::findOrFail($categoryId);
            if (is_null($category->parent_id)) {
                $categoryProducts = $category->products->pluck('id')->toArray();
                foreach ($categoryProducts as $productId) {
                    $discountItemsId[] = $productId;
                }
                foreach ($category->children->pluck('id') as $childrenCategoryId) {
                    $category = Category::findOrFail($childrenCategoryId);
                    $categoryProducts = $category->products->pluck('id')->toArray();
                    foreach ($categoryProducts as $productId) {
                        $discountItemsId[] = $productId;
                    }
                }

            }
            else {
                $categoryProducts = $category->products->pluck('id')->toArray();
                foreach ($categoryProducts as $productId) {
                    $discountItemsId[] = $productId;
                }
            }

        }

    }
    return array_unique($discountItemsId);
}

function getDiscountAndPrice(\App\Plugins\Orders\Model\OrderHeader $cart, $item, $discountItemsId) {
    $discountProduct = 0;
    $itemResultPrice = 0;

    if ($cart->discount_target == "product" || $cart->discount_target == "category") {
        if ((empty($discountItemsId) || in_array($item->product_id, $discountItemsId))) {

            /*if ($cart->discount_type == 'percent') {
                $discountItemPrice = $item->price - $item->price * $cart->discount_amount / 100;
                if ($discountItemPrice < $item->price)
                    $itemResultPrice = $discountItemPrice;

                $discountProduct = $item->price * $cart->discount_amount / 100;
            }*/

            if ($cart->discount_type == 'percent') {
                $discountItemPrice = $item->price_raw - $item->price_raw * $cart->discount_amount / 100;

                if ($discountItemPrice < $item->price_raw)
                    $itemResultPrice = $discountItemPrice;

                // $discountProduct = $item->price_raw * $cart->discount_amount / 100;
                $discountProduct = $item->price * $cart->discount_amount / 100;
            }

        }
    }

    $codeBetterThanPromo = 0;
    $vatResult = $item->vat;

    /**
     * so that "6.77" is equal "6.76"
     */
    $comparePricesToPrecision = abs(($item->price_raw-$item->price)/$item->price) < 0.01;

    if ( ($itemResultPrice >= $item->price && $itemResultPrice > 0)) {
        $discountProduct = 0;
        $itemResultPrice = $item->price;
    } else if ($itemResultPrice > 0) {
        // If the coupon discount turned out to be stronger than a discount on the product
        // remember the product
        $codeBetterThanPromo = $item->product_id;
        $vatResult = $item->vat_raw;

    } else if ($item->price_raw >= $item->price || $comparePricesToPrecision) {
        // Если скидки по купону не было, но скидка по ПРОМО есть, то берем ее
        $discountProduct = 0;
        $itemResultPrice = $item->price;
        $itemResultPrice = $item->price_raw;
    }


    return [
        'discountProduct' => $discountProduct,
        'itemResultPrice' => $itemResultPrice,
        'codeBetterThanPromo' => $codeBetterThanPromo,
        'vatResult' => $vatResult
    ];
}

/**
 * Calculates cart/order total amounts
 *
 * @param \App\Plugins\Orders\Model\OrderHeader $cart
 * @param array $items
 * @param bool $original
 *
 * @return object
 */
function getCartTotals(\App\Plugins\Orders\Model\OrderHeader $cart, array $items = [], $original = false, $debug = false)
{

    $woDiscount = (object)['sum' => 0, 'vatsum' => 0];

    if (!$cart) {
        return (object)[
            'productSum' => 0,
            'vatSum' => 0,
            'discount' => 0,
            'toPay' => 0,
            'depositSum' => 0,
        ];
    }


    $discountItemsId       = getDiscountItemsId($cart);

    $productSum            = 0;
    $productSumWoDiscount  = 0;
    $discountSum           = 0;
    $vatSum                = 0;
    $depositSum            = 0;


    foreach ($cart->items as $item) {
        if (!empty($items) && !in_array($item->product_id, $items))
            continue;

        $discountAndPrice = getDiscountAndPrice($cart, $item, $discountItemsId);

        $productSum += round($discountAndPrice['itemResultPrice'] * $item->amount, 2);
        $vatSum += $item->vat * $item->amount;
        //$vatSum += $discountAndPrice['vatResult'] * $item->amount;

        $discountSum += round($discountAndPrice['discountProduct'] * $item->amount, 2);
        if ($item->discount_name != "0" && $item->product_id != $discountAndPrice['codeBetterThanPromo']) {
            $price = round($item->price * $item->amount, 2);
            $productSumWoDiscount += $price;
        } else {
            // Here are the goods where the coupon gives more discount
            // $price = round($item->price_raw * $item->amount, 2);
            $price = round($item->price * $item->amount, 2);
            $productSumWoDiscount += $price;
        }
        // dd($item);
        // dd([
        //     'd' => $price,
        //     'd2' => $item->price * $item->amount,
        //     'raw' => $item->price_raw,
        //     'd3' => $item->price_raw * $item->amount,
        //     's' => $discountAndPrice,
        //     'i' => $item->id,
        //     'a' => $item->amount,
        // ]);

        if ($item->deposit_amount > 0) {
            $depositSum += round($item->deposit_amount * $item->amount, 2);
        }
    }

    // dd($productSumWoDiscount);
    // dd($cart->discount_type);
    if ($cart->discount_type == 'amount') {
        $discountSum += $cart->discount_amount;
    }

    if ($cart->discount_target == "delivery") {
        $deliveryVatAmount = $cart->delivery->vat->amount;
        $deliveryWithVat = round($cart->delivery_amount + $cart->delivery_amount * $deliveryVatAmount / 100, 2);

        $discountSum += ($cart->discount_type == 'percent' ? (($deliveryWithVat ?? 0) * ($cart->discount_amount / 100)) : $cart->discount_amount);
    }

    $discountSum = ceil($discountSum * 100) / 100;


    /** @var int $productSum */
    $productSum = number_format(round($productSum ?? 0, 2), 2, ".", "");

    // Похоже не используется
    switch ($cart->discount_target) {
        case "product":
        case "cateogry";
            $discount = ($cart->discount_type == 'percent' ? ($productSum * ($cart->discount_amount / 100)) : $cart->discount_amount);
            break;

        case "delivery":
            $discount = ($cart->discount_type == 'percent' ? (($cart->delivery_amount ?? 0) * ($cart->discount_amount / 100)) : $cart->discount_amount);
            break;

        /**
         * Unused (remved) - this is left in case it needs to return
         */
        case "all":
            $totalSum = $productSum + $cart->delivery_amount;
            $discount = ($cart->discount_type == 'percent' ? ($totalSum * ($cart->discount_amount / 100)) : $cart->discount_amount);
            break;

        default:
            $discount = 0;
            break;
    }

    $discount = $discountSum;
    $discount = number_format(($discount != abs($discount) ? ($totalSum ?? $productSum) : round(($discount ?? 0), 2)), 2, ".", "");

    $delivery = (!is_null($cart->delivery_amount)) ? $cart->delivery_amount : 0;

    if (!empty($cart->delivery_id)) {
        if ( \App\Plugins\Deliveries\Model\Delivery::where('id', $cart->delivery_id)->exists() ) {
            $vatId = \App\Plugins\Deliveries\Model\Delivery::find($cart->delivery_id)->vat_id;
            $vat   = \App\Plugins\Vat\Model\Vat::find($vatId)->amount ?? 0;
        } else {
            $cart->delivery_id = null;
            $vat = 0;
        }
    } else {
        $vat = 0;
    }

    $delVat = number_format($delivery * ($vat / 100), 2, ".", "");

    $toPayWoDiscount = $productSumWoDiscount + $delivery + $delVat;
    $toPay           = $toPayWoDiscount - ($discountSum ?? 0) + $depositSum;

    //sooo if discount_applicable_from is present..
    if ( $cart->discount_applicable_from && is_numeric(floatval($cart->discount_applicable_from)) ) {

        //..and product summ is smaller than threshold, then DON'T apply discount!
        if ( $productSumWoDiscount < floatval($cart->discount_applicable_from) ) {
            $toPay = $toPayWoDiscount;

            $discount = 0;
        }
    }

    if ($toPay < 0) {
        $toPay = 0;
    }

    return (object)[
        'productSum'            => number_format($productSumWoDiscount, 2, ".", ""),
        'vatSum'                => $vatSum,
        'discount'              => $discount,
        'delivery'              => number_format($delivery, 2, '.', ''),
        'delivery_vat'          => $delVat,
        'delievery_vat_amount'  => $vat,
        'discount_description'  => $cart->discount_code_description,
        'depositSum'            => number_format($depositSum, 2, ".", ""),
        'toPay'                 => number_format($toPay, 2, ".", ""),
    ];
}


/**
 * @param bool $change
 *
 * @return \App\User|mixed|null
 */
function currentUser($change = false) {
    if (!$change) {
        $cu = session()->get('cu');
    }

    $user = $cu ?? Auth::user() ?? \App\User::find(99);

    session()->put('cu', $user);

    return $user;
}

/**
 * Translate the given message.
 *
 * @param string $key
 * @param array $replace
 * @param string $locale
 *
 * @return string|array|null
 */
function _t($key, $replace = [], $locale = null) {

    $translation = app('translator')->getFromJson($key, $replace, $locale);

    $user = \Illuminate\Support\Facades\Auth::user();

    //    if ($user && isset($user->isAdmin)) {
    //        $translation = $key == $translation ? "UndefinedProperty" : $translation;
    //
    //        return view("admin.partials.translation", compact(['key', 'translation']))->render();
    //    }

    return $translation == $key ? "Undefined Property" : $translation;
}

/**
 * Translate the given message or return empty.
 *
 * @param string $key
 * @param array $replace
 * @param string $locale
 *
 * @return string|array|null
 */
function transOrEmpty($key, $replace = [], $locale = null) {

    $translation = app('translator')->getFromJson($key, $replace, $locale);

    return $translation == $key ? "" : $translation;
}

/**
 * Supplier Slug list for routing purposes (with built in backup if there is no suppliers)
 *
 * @param bool $language
 *
 * @return mixed|string
 */
function getSupplierSlugs($language = false) {


    if (pageTable()) {
        $slug = \App\Plugins\Pages\Model\Template::where('template', 'suppliers')->first();
        $slugs = [];
        foreach (($slug ? $slug->page : []) as $page) {
            $slugs[] = __("pages.slug.{$page->id}");
        }
        if (count($slugs) == 0) {
            if ($language) {
                return "#";
            }
            $slugs = ['401'];
        }

        if ($language) {
            return current($slugs);
        }

        return implode("|", $slugs);
    }

    return str_random(20);
}

/**
 * Checks if there's templates DB table for pages, to allow "pages"
 *
 * @return bool
 */
function pageTable() {
    return \Illuminate\Support\Facades\Schema::hasTable("templates");
}


/**
 * get current category filters (reset on category change)
 *
 * @param $catId
 *
 * @return array
 */
function getCurrentAttributes($catId) {
    if ($filters = session()->get('filters')) {
        if ($filters['category'] == $catId) {
            return $filters['filters'] ?? [];
        }
        session()->forget('filters');

        return [];
    }

    return [];
}

/**
 * @param $catId
 *
 * @return array
 */
function getCurrentSuppliers($catId) {
    if ($filters = session()->get('filters')) {
        if ($filters['category'] == $catId) {
            return $filters['suppliers'] ?? [];
        }
        session()->forget('filters');

        return [];
    }

    return [];
}

/**
 * get text for discount codes
 *
 * @param $item
 *
 * @return mixed
 */
function discountTo($item) {
    $texts = [
        'category' => 'Only Category(-ies)',
        'product' => 'Only Product(-s)',
        'delivery' => 'Only Delivery',
    ];

    return $texts[$item];
}

/**
 * Discount code unlimited symbol
 *
 * @param $item
 *
 * @return string
 */
function usesLeft($item) {
    if (is_null($item)) {
        return "∞";
    }

    return $item;
}

function deleteDir($path) {
    $it = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
    $files = new RecursiveIteratorIterator($it,
        RecursiveIteratorIterator::CHILD_FIRST);
    foreach ($files as $file) {
        if ($file->isDir()) {
            rmdir($file->getRealPath());
        }
        else {
            unlink($file->getRealPath());
        }
    }
    rmdir($path);
}

/**
 * Get meta from cache or NULL
 */
function ___($string)
{
    $cashedString = __($string);

    if (str_contains($cashedString, $string)) {
        return null;
    }

    return $cashedString;
}
