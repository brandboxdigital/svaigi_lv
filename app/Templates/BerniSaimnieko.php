<?php

namespace App\Templates;

use App\Plugins\Pages\Functions\Components;

class BerniSaimnieko
{
    use Components;

    public static function getTemplateName()
    {
        return "Bērni Saimnieko";
    }

    public function components()
    {
        return [
            // 'carouselBanner',
            // 'fullSizeBanner',
            // 'doubleBanner',
            // 'spacerSmall',
            // 'centeredTitle',
            // 'spacerMedium',
            // 'popular',
            // 'spacerMedium',
            // 'featuredSupplier',
            // 'spacerMedium',
            // 'centeredTitle',
            // 'spacerMedium',
            // 'blogposts',
            // 'spacerMedium',
            // 'howithappens',
        ];
    }
}
