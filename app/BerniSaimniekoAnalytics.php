<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BerniSaimniekoAnalytics extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'email',
        'name',
        'last_name',
        'phone',
        'page',
        'type',
        'data',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'berni_saimnieko_analytics';

    public static function countUserPageview($user, $page = null)
    {
        if ($user instanceof User) {
            self::create([
                'user_id'    => $user->id,
                'email'      => $user->email,
                'name'       => $user->name,
                'last_name'  => $user->last_name,
                'phone'      => $user->phone,
                'page'       => $page,

                'type'       => 'pageview',
            ]);
        }
    }

    public static function countUserNewsletterSubscription($email, $name, $surname = '', $page = null)
    {
        self::create([
            'user_id'    => null,
            'email'      => $email,
            'name'       => $name,
            'last_name'  => $surname,
            'phone'      => null,
            'page'       => $page,

            'type'       => 'newsletter',
        ]);
    }

    public static function countUserMasterclassSubscription($email, $name, $surname, $phone, $page = null)
    {
        self::create([
            'user_id'    => null,
            'email'      => $email,
            'name'       => $name,
            'last_name'  => $surname,
            'phone'      => $phone,
            'page'       => $page,

            'type'       => 'masterclass',
        ]);
    }
}
